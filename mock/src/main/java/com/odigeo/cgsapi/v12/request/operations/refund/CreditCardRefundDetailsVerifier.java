package com.odigeo.cgsapi.v12.request.operations.refund;


import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.creditcard.CreditCardRefundDetails;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by marcal.perapoch on 30/07/2015.
 */
public class CreditCardRefundDetailsVerifier extends RefundDetailsVerifier {

    private final CreditCard creditCard;

    public CreditCardRefundDetailsVerifier(CreditCardRefundDetailsVerifierBuilder builder) {
        super(builder);
        this.creditCard = builder.getCreditCard();
    }

    public void verify(RefundDetails refundDetails) {
        super.verify(refundDetails);
        assert refundDetails instanceof CreditCardRefundDetails : refundDetails.getClass();
        verify(CreditCardRefundDetails.class.cast(refundDetails).getCreditCard());
    }

    private void verify(CreditCard creditCard) {
        if (this.creditCard != null) {
            assertThat(creditCard, equalTo(this.creditCard));
        }
    }
}
