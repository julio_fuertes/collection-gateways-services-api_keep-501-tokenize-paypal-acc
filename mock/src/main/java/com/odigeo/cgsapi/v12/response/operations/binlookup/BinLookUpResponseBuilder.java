package com.odigeo.cgsapi.v12.response.operations.binlookup;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodBuilder;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import com.odigeo.cgsapi.v12.util.CustomPicker;

import java.util.Locale;
import java.util.Random;

public class BinLookUpResponseBuilder {

    private CreditCardType creditCardType;
    private String countryCode;
    private Integer numCountryCode;
    private GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder;

    public BinLookUpResponseBuilder creditCardType(CreditCardType creditCardType) {
        this.creditCardType = creditCardType;
        return this;
    }

    public BinLookUpResponseBuilder countryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public BinLookUpResponseBuilder numCountryCode(Integer numCountryCode) {
        this.numCountryCode = numCountryCode;
        return this;
    }

    public BinLookUpResponseBuilder gatewayCollectionMethodBuilder(GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder) {
        this.gatewayCollectionMethodBuilder = gatewayCollectionMethodBuilder;
        return this;
    }

    public BinLookUpResponse build(Random dice) {
        fillEmptyData(dice);
        BinLookUp binLookUp = new BinLookUp(null, null, countryCode, numCountryCode, creditCardType);
        BinLookUpResponse binLookUpResponse = new BinLookUpResponse(binLookUp);
        binLookUpResponse.setGatewayCollectionMethod(gatewayCollectionMethodBuilder.build(dice));
        return binLookUpResponse;
    }

    private void fillEmptyData(Random dice) {
        CustomPicker picker = new CustomPicker(dice);
        if (countryCode == null) {
            countryCode = picker.pickFrom(Locale.getISOCountries());
        }
        if (numCountryCode == null) {
            numCountryCode = picker.pickInt(1, 999);
        }
        if (creditCardType == null) {
            int temp = picker.pickInt(0, (CreditCardType.values().length - 1));
            creditCardType = CreditCardType.values()[temp];
        }
        if (gatewayCollectionMethodBuilder == null) {
            gatewayCollectionMethodBuilder = new GatewayCollectionMethodBuilder();
        }
    }
}
