package com.odigeo.cgsapi.v12.response.operations.confirm;


import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodBuilder;
import com.odigeo.cgsapi.v12.response.MovementBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ConfirmationResponseBuilder {

    private List<MovementBuilder> movementBuilders;
    private GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder;

    public ConfirmationResponseBuilder movementBuilders(List<MovementBuilder> movementBuilders) {
        this.movementBuilders = movementBuilders;
        return this;
    }

    public ConfirmationResponseBuilder gatewayCollectionMethodBuilder(GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder) {
        this.gatewayCollectionMethodBuilder = gatewayCollectionMethodBuilder;
        return this;
    }

    public ConfirmationResponse build(Random dice) {
        fillEmptyData(dice);
        ConfirmationResponse.Builder builder = new ConfirmationResponse.Builder();
        for (MovementBuilder movementBuilder : movementBuilders) {
            builder.addMovement(movementBuilder.build(dice));
        }
        ConfirmationResponse confirmationResponse = builder.build();
        confirmationResponse.setGatewayCollectionMethod(gatewayCollectionMethodBuilder.build(dice));
        return confirmationResponse;
    }

    private void fillEmptyData(Random dice) {
        if (movementBuilders == null) {
            movementBuilders = Arrays.asList(buildMovementBuilderWithValidStatus(dice));
        }
        if (gatewayCollectionMethodBuilder == null) {
            gatewayCollectionMethodBuilder = new GatewayCollectionMethodBuilder();
        }
    }

    private MovementBuilder buildMovementBuilderWithValidStatus(Random dice) {
        MovementBuilder movementBuilder = null;
        boolean correctMovement = false;
        while (!correctMovement) {
            try {
                correctMovement = true;
                movementBuilder = new MovementBuilder();
                new ConfirmationResponse.Builder().addMovement(movementBuilder.build(dice));
            } catch (IllegalArgumentException e) {
                correctMovement = false;
            }
        }
        return movementBuilder;
    }
}
