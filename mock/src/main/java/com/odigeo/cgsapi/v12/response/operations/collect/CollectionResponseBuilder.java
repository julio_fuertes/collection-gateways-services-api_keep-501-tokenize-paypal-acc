package com.odigeo.cgsapi.v12.response.operations.collect;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodBuilder;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementBuilder;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.RedirectionParametersBuilder;
import com.odigeo.cgsapi.v12.util.CustomPicker;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Random;

public class CollectionResponseBuilder {

    @Deprecated
    private MovementAction movementAction;
    @Deprecated
    private MovementStatus movementStatus;
    @Deprecated
    private BigDecimal amount;
    @Deprecated
    private Currency currency;
    @Deprecated
    private String merchantOrderId;
    @Deprecated
    private GatewayCollectionMethod gatewayCollectionMethod;

    private Integer numCountryCode;
    private List<MovementBuilder> movementBuilders;
    private RedirectionParametersBuilder redirectionParametersBuilder;
    private GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder;

    @Deprecated
    public CollectionResponseBuilder collectionStatus(CollectionStatus collectionStatus) {
        if (CollectionStatus.COLLECTED.equals(collectionStatus)) {
            this.movementAction = MovementAction.COLLECT;
            this.movementStatus = MovementStatus.PAID;
        } else if (CollectionStatus.COLLECTION_DECLINED.equals(collectionStatus)) {
            this.movementAction = MovementAction.COLLECT;
            this.movementStatus = MovementStatus.DECLINED;
        } else if (CollectionStatus.COLLECTION_ERROR.equals(collectionStatus)) {
            this.movementAction = MovementAction.COLLECT;
            this.movementStatus = MovementStatus.PAYERROR;
        }
        return this;
    }

    @Deprecated
    public CollectionResponseBuilder amount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    @Deprecated
    public CollectionResponseBuilder amount(int amount) {
        this.amount = new BigDecimal(amount);
        return this;
    }

    @Deprecated
    public CollectionResponseBuilder currency(Currency currency) {
        this.currency = currency;
        return this;
    }

    @Deprecated
    public CollectionResponseBuilder currency(String currencyCode) {
        this.currency = Currency.getInstance(currencyCode);
        return this;
    }

    @Deprecated
    public CollectionResponseBuilder merchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
        return this;
    }

    @Deprecated
    public CollectionResponseBuilder gatewayCollectionMethod(GatewayCollectionMethod gatewayCollectionMethod) {
        this.gatewayCollectionMethod = gatewayCollectionMethod;
        return this;
    }

    public CollectionResponseBuilder numCountryCode(Integer numCountryCode) {
        this.numCountryCode = numCountryCode;
        return this;
    }

    public CollectionResponseBuilder movementBuilders(List<MovementBuilder> movementBuilders) {
        this.movementBuilders = movementBuilders;
        return this;
    }

    public CollectionResponseBuilder redirectionParametersBuilder(RedirectionParametersBuilder redirectionParametersBuilder) {
        this.redirectionParametersBuilder = redirectionParametersBuilder;
        return this;
    }

    public CollectionResponseBuilder gatewayCollectionMethodBuilder(GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder) {
        this.gatewayCollectionMethodBuilder = gatewayCollectionMethodBuilder;
        return this;
    }

    @Deprecated
    public MovementAction getMovementAction() {
        return movementAction;
    }

    @Deprecated
    public BigDecimal getAmount() {
        return amount;
    }

    @Deprecated
    public Currency getCurrency() {
        return currency;
    }

    @Deprecated
    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    @Deprecated
    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

    @Deprecated
    public MovementStatus getMovementStatus() {
        return movementStatus;
    }

    @Deprecated
    public CollectionResponse build() {
        Movement movement = new Movement(merchantOrderId, "0", movementAction);
        movement.setAmount(amount);
        movement.setStatus(movementStatus);
        movement.setCurrency(currency);
        CollectionResponse.Builder builder = new CollectionResponse.Builder();
        CollectionResponse collectionResponse = builder.addMovement(movement).build();
        collectionResponse.setGatewayCollectionMethod(gatewayCollectionMethod);
        return collectionResponse;
    }

    public CollectionResponse build(Random dice) {
        fillEmptyData(dice);
        CollectionResponse.Builder builder = new CollectionResponse.Builder();
        builder.redirectionParameters(redirectionParametersBuilder.build(dice));
        builder.numCountryCode(numCountryCode);
        for (MovementBuilder movementBuilder : movementBuilders) {
            builder.addMovement(movementBuilder.build(dice));
        }
        CollectionResponse collectionResponse = builder.build();
        collectionResponse.setGatewayCollectionMethod(gatewayCollectionMethodBuilder.build(dice));
        return collectionResponse;
    }

    private void fillEmptyData(Random dice) {
        CustomPicker picker = new CustomPicker(dice);
        if (numCountryCode == null) {
            numCountryCode = picker.pickInt(0, 100);
        }
        if (movementBuilders == null) {
            movementBuilders = Arrays.asList(buildMovementBuilderWithValidStatus(dice));
        }
        if (gatewayCollectionMethodBuilder == null) {
            gatewayCollectionMethodBuilder = new GatewayCollectionMethodBuilder();
        }
        if (redirectionParametersBuilder == null) {
            redirectionParametersBuilder = new RedirectionParametersBuilder();
        }
    }

    private MovementBuilder buildMovementBuilderWithValidStatus(Random dice) {
        MovementBuilder movementBuilder = null;
        boolean correctMovement = false;
        while (!correctMovement) {
            try {
                correctMovement = true;
                movementBuilder = new MovementBuilder();
                new CollectionResponse.Builder().addMovement(movementBuilder.build(dice));
            } catch (IllegalArgumentException e) {
                correctMovement = false;
            }
        }
        return movementBuilder;
    }
}
