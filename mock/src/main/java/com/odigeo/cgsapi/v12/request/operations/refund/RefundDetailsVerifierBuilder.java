package com.odigeo.cgsapi.v12.request.operations.refund;


import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;

/**
 * Created by marcal.perapoch on 30/07/2015.
 */
public abstract class RefundDetailsVerifierBuilder {

    private String transactionId;
    private String merchantOrderId;
    private Money money;
    private GatewayCollectionMethod gatewayCollectionMethod;

    public RefundDetailsVerifierBuilder transactionId(String transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    public RefundDetailsVerifierBuilder merchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
        return this;
    }

    public RefundDetailsVerifierBuilder money(Money money) {
        this.money = money;
        return this;
    }

    public RefundDetailsVerifierBuilder gatewayCollectionMethod(GatewayCollectionMethod gatewayCollectionMethod) {
        this.gatewayCollectionMethod = gatewayCollectionMethod;
        return this;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    public Money getMoney() {
        return money;
    }

    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }
}
