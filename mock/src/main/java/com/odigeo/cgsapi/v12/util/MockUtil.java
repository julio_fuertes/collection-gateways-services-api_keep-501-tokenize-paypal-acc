package com.odigeo.cgsapi.v12.util;

public class MockUtil {

    public static String pickStringWhenNullParameter(String parameter, CustomPicker picker, int number) {
        String result = parameter;
        if (parameter == null) {
            result = picker.pickStringWithLettersAndNumbers(number);
        }
        return result;
    }
}
