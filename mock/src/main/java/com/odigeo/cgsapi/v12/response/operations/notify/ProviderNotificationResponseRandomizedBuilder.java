package com.odigeo.cgsapi.v12.response.operations.notify;

import com.odigeo.cgsapi.v12.util.CustomPicker;

import java.util.Random;

public class ProviderNotificationResponseRandomizedBuilder {

    public ProviderNotificationResponseBuilder provideRandomizedBuilder(Random dice) {
        ProviderNotificationResponseBuilder builder = new ProviderNotificationResponseBuilder();
        return provideRandomizedBuilder(builder, dice);
    }

    public ProviderNotificationResponseBuilder provideRandomizedBuilder(ProviderNotificationResponseBuilder builder, Random dice) {
        CustomPicker picker = new CustomPicker(dice);
        if (builder.getNotificationId() == null) {
            builder.notificationId(String.valueOf(picker.pickLong(Long.MIN_VALUE, Long.MAX_VALUE)));
        }
        if (builder.getResult() == null) {
            builder.result(picker.pickFrom(Result.values()));
        }
        if (builder.getMessage() == null) {
            builder.message(picker.pickStringWithOnlyLetters(20));
        }
        return builder;
    }
}
