package com.odigeo.cgsapi.v12.response.operations.authorize;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodBuilder;
import com.odigeo.cgsapi.v12.response.MovementBuilder;
import com.odigeo.cgsapi.v12.response.RedirectionParametersBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Random;


public class AuthorizationResponseBuilder {

    private List<MovementBuilder> movementBuilders;
    private GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder;
    private RedirectionParametersBuilder redirectionParametersBuilder;

    public AuthorizationResponseBuilder movementBuilders(List<MovementBuilder> movementBuilders) {
        this.movementBuilders = movementBuilders;
        return this;
    }

    public AuthorizationResponseBuilder redirectionParametersBuilder(RedirectionParametersBuilder redirectionParametersBuilder) {
        this.redirectionParametersBuilder = redirectionParametersBuilder;
        return this;
    }

    public AuthorizationResponseBuilder gatewayCollectionMethodBuilder(GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder) {
        this.gatewayCollectionMethodBuilder = gatewayCollectionMethodBuilder;
        return this;
    }

    public AuthorizationResponse build(Random dice) {
        fillEmptyData(dice);
        AuthorizationResponse.Builder builder = new AuthorizationResponse.Builder();
        builder.redirectionParameters(redirectionParametersBuilder.build(dice));
        for (MovementBuilder movementBuilder : movementBuilders) {
            builder.addMovement(movementBuilder.build(dice));
        }
        AuthorizationResponse authorizationResponse = builder.build();
        authorizationResponse.setGatewayCollectionMethod(gatewayCollectionMethodBuilder.build(dice));
        return authorizationResponse;
    }

    private void fillEmptyData(Random dice) {
        if (movementBuilders == null) {
            movementBuilders = Arrays.asList(buildMovementBuilderWithValidStatus(dice));
        }
        if (gatewayCollectionMethodBuilder == null) {
            gatewayCollectionMethodBuilder = new GatewayCollectionMethodBuilder();
        }
        if (redirectionParametersBuilder == null) {
            redirectionParametersBuilder = new RedirectionParametersBuilder();
        }
    }

    private MovementBuilder buildMovementBuilderWithValidStatus(Random dice) {
        MovementBuilder movementBuilder = null;
        boolean correctMovement = false;
        while (!correctMovement) {
            try {
                correctMovement = true;
                movementBuilder = new MovementBuilder();
                new AuthorizationResponse.Builder().addMovement(movementBuilder.build(dice));
            } catch (IllegalArgumentException e) {
                correctMovement = false;
            }
        }
        return movementBuilder;
    }
}
