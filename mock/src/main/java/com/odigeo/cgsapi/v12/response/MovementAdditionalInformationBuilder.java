package com.odigeo.cgsapi.v12.response;

import java.util.Random;

public interface MovementAdditionalInformationBuilder {
    MovementAdditionalInformation build(Random dice);
}
