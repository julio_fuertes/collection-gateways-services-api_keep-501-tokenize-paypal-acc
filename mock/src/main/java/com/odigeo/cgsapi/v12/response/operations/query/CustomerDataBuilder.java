package com.odigeo.cgsapi.v12.response.operations.query;

public class CustomerDataBuilder {

    private String firstName;
    private String middleName;
    private String lastName;
    private String sufix;
    private String email;
    private String countryCode;
    private String business;

    public CustomerDataBuilder firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public CustomerDataBuilder middleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public CustomerDataBuilder lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public CustomerDataBuilder sufix(String sufix) {
        this.sufix = sufix;
        return this;
    }

    public CustomerDataBuilder email(String email) {
        this.email = email;
        return this;
    }

    public CustomerDataBuilder countryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public CustomerDataBuilder business(String business) {
        this.business = business;
        return this;
    }

    public CustomerData build() {
        CustomerData.Builder builder = new CustomerData.Builder();
        builder.firstName(firstName);
        builder.middleName(middleName);
        builder.lastName(lastName);
        builder.sufix(sufix);
        builder.email(email);
        builder.countryCode(countryCode);
        builder.business(business);
        return builder.build();
    }
}
