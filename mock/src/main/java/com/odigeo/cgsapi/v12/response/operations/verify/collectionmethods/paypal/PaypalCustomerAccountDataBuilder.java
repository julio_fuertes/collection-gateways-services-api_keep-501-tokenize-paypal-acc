package com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal;

import com.odigeo.cgsapi.v12.response.operations.query.CustomerDataBuilder;
import com.odigeo.cgsapi.v12.response.operations.query.PaypalCustomerAccountData;

public class PaypalCustomerAccountDataBuilder {

    private CustomerDataBuilder customerDataBuilder;
    private String payerId;
    private PaypalAccountStatus payerStatus;
    private String addressStatus;

    public PaypalCustomerAccountDataBuilder customerDataBuilder(CustomerDataBuilder customerDataBuilder) {
        this.customerDataBuilder = customerDataBuilder;
        return this;
    }

    public PaypalCustomerAccountDataBuilder payerId(String payerId) {
        this.payerId = payerId;
        return this;
    }

    public PaypalCustomerAccountDataBuilder payerStatus(PaypalAccountStatus payerStatus) {
        this.payerStatus = payerStatus;
        return this;
    }

    public PaypalCustomerAccountDataBuilder addressStatus(String addressStatus) {
        this.addressStatus = addressStatus;
        return this;
    }

    public PaypalCustomerAccountData build() {
        PaypalCustomerAccountData.Builder builder = new PaypalCustomerAccountData.Builder();
        builder.customerData(customerDataBuilder == null ? null : customerDataBuilder.build());
        builder.payerId(payerId);
        builder.payerStatus(payerStatus);
        builder.addressStatus(addressStatus);
        return builder.build();
    }
}
