package com.odigeo.cgsapi.v12.response;

import com.odigeo.cgsapi.v12.util.CustomPicker;

import java.util.Map;
import java.util.Random;

public class RedirectionParametersBuilder {

    private RedirectionType redirectionType;
    private Integer redirectionStep;
    private String redirectionUrl;
    private RedirectionHttpMethod redirectionMethod;
    private Map<String, String> parameters;
    private String html;
    private String javascriptSnippet;

    public RedirectionParametersBuilder redirectionType(RedirectionType redirectionType) {
        this.redirectionType = redirectionType;
        return this;
    }

    public RedirectionParametersBuilder redirectionUrl(String redirectionUrl) {
        this.redirectionUrl = redirectionUrl;
        return this;
    }

    public RedirectionParametersBuilder redirectionMethod(RedirectionHttpMethod redirectionMethod) {
        this.redirectionMethod = redirectionMethod;
        return this;
    }

    public RedirectionParametersBuilder parameters(Map<String, String> parameters) {
        this.parameters = parameters;
        return this;
    }

    public RedirectionParametersBuilder redirectionStep(Integer redirectionStep) {
        this.redirectionStep = redirectionStep;
        return this;
    }

    public RedirectionParametersBuilder html(String html) {
        this.html = html;
        return this;
    }

    public RedirectionParametersBuilder javascriptSnippet(String javascriptSnippet) {
        this.javascriptSnippet = javascriptSnippet;
        return this;
    }

    public RedirectionType getRedirectionType() {
        return redirectionType;
    }

    public Integer getRedirectionStep() {
        return redirectionStep;
    }

    public String getRedirectionUrl() {
        return redirectionUrl;
    }

    public RedirectionHttpMethod getRedirectionMethod() {
        return redirectionMethod;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public String getHtml() {
        return html;
    }

    public String getJavascriptSnippet() {
        return javascriptSnippet;
    }

    public RedirectionParameters build(Random dice) {
        fillEmptyData(dice);
        return new RedirectionParameters(this);
    }

    private void fillEmptyData(Random dice) {
        CustomPicker picker = new CustomPicker(dice);
        if (redirectionType == null) {
            redirectionType = picker.pickFrom(RedirectionType.values());
        }
        if (redirectionStep == null) {
            redirectionStep = picker.pickInt(1, 10);
        }
        if (RedirectionType.FORM_REDIRECTION.equals(redirectionType)) {
            fillFormRedirectionEmptyData(dice);
        } else if (RedirectionType.HTML.equals(redirectionType)) {
            fillHtmlEmptyData();
        } else if (RedirectionType.JAVASCRIPT.equals(redirectionType)) {
            fillJavascriptEmptyData();
        } else if (RedirectionType.GOOGLE_PAY.equals(redirectionType)) {
            fillGooglePayEmptyData(dice);
        } else if (RedirectionType.APPLE_PAY.equals(redirectionType)) {
            fillApplePayEmptyData(dice);
        }
    }

    private void fillFormRedirectionEmptyData(Random dice) {
        CustomPicker picker = new CustomPicker(dice);
        if (redirectionUrl == null) {
            redirectionUrl = picker.pickEmail();
        }
        if (redirectionMethod == null) {
            redirectionMethod = picker.pickFrom(RedirectionHttpMethod.values());
        }
        if (parameters == null
                && (RedirectionType.FORM_REDIRECTION.equals(redirectionType)
                || RedirectionType.GOOGLE_PAY.equals(redirectionType)
                || RedirectionType.APPLE_PAY.equals(redirectionType))) {
            parameters = picker.pickMap();
        }
    }

    private void fillHtmlEmptyData() {
        if (html == null && RedirectionType.HTML.equals(redirectionType)) {
            html = "<html><body></body></html>";
        }
    }

    private void fillJavascriptEmptyData() {
        if (javascriptSnippet == null && RedirectionType.JAVASCRIPT.equals(redirectionType)) {
            javascriptSnippet = "function sayHi() { alert( 'Hello, world!' ); }";
        }
    }

    private void fillApplePayEmptyData(Random dice) {
        CustomPicker picker = new CustomPicker(dice);
        if (parameters == null
                && (RedirectionType.FORM_REDIRECTION.equals(redirectionType)
                || RedirectionType.GOOGLE_PAY.equals(redirectionType)
                || RedirectionType.APPLE_PAY.equals(redirectionType))) {
            parameters = picker.pickMap();
        }
    }

    private void fillGooglePayEmptyData(Random dice) {
        CustomPicker picker = new CustomPicker(dice);
        if (parameters == null
                && (RedirectionType.FORM_REDIRECTION.equals(redirectionType)
                || RedirectionType.GOOGLE_PAY.equals(redirectionType)
                || RedirectionType.APPLE_PAY.equals(redirectionType))) {
            parameters = picker.pickMap();
        }
    }
}
