package com.odigeo.cgsapi.v12.request.operations.collect;


import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;

/**
 * Created by marcal.perapoch on 30/07/2015.
 */
public abstract class CollectionDetailsVerifierBuilder {

    private String orderId;
    private Money money;
    private GatewayCollectionMethod gatewayCollectionMethod;

    public CollectionDetailsVerifierBuilder orderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public CollectionDetailsVerifierBuilder simpleMoney(Money money) {
        this.money = money;
        return this;
    }

    public CollectionDetailsVerifierBuilder gatewayCollectionMethod(GatewayCollectionMethod gatewayCollectionMethod) {
        this.gatewayCollectionMethod = gatewayCollectionMethod;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public Money getMoney() {
        return money;
    }

    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

}
