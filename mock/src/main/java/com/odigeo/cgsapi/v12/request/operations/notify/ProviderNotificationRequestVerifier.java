package com.odigeo.cgsapi.v12.request.operations.notify;

import com.odigeo.cgsapi.v12.request.VirtualPointOfSaleType;
import com.odigeo.cgsapi.v12.util.VerifyUtils;

import java.util.Map;

public class ProviderNotificationRequestVerifier {

    private VirtualPointOfSaleType virtualPointOfSale;
    private Map<String, Object> parameters;

    public void setVirtualPointOfSale(VirtualPointOfSaleType virtualPointOfSale) {
        this.virtualPointOfSale = virtualPointOfSale;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    public void verify(ProviderNotificationRequest providerNotificationRequest) {
        VerifyUtils.verifyEquals(virtualPointOfSale, providerNotificationRequest.getVirtualPointOfSale());
        VerifyUtils.verifyEquals(parameters, providerNotificationRequest.getParameters());
    }
}
