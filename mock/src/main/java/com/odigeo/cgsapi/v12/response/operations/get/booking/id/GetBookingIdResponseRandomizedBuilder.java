package com.odigeo.cgsapi.v12.response.operations.get.booking.id;

import com.odigeo.cgsapi.v12.util.CustomPicker;

import java.util.Random;

public class GetBookingIdResponseRandomizedBuilder {

    public GetBookingIdResponseBuilder provideRandomizedBuilder(Random dice) {
        GetBookingIdResponseBuilder builder = new GetBookingIdResponseBuilder();
        return provideRandomizedBuilder(builder, dice);
    }

    public GetBookingIdResponseBuilder provideRandomizedBuilder(GetBookingIdResponseBuilder builder, Random dice) {
        CustomPicker picker = new CustomPicker(dice);
        if (builder.getBookingId() == null) {
            builder.bookingId(picker.pickStringWithOnlyNumbers(8));
        }
        return builder;
    }
}
