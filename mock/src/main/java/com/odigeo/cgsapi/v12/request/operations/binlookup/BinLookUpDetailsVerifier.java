package com.odigeo.cgsapi.v12.request.operations.binlookup;


import com.odigeo.cgsapi.v12.request.operations.binlookup.collectionmethods.creditcard.BinLookUpDetails;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class BinLookUpDetailsVerifier {

    private final String bin;

    public BinLookUpDetailsVerifier(BinLookUpDetailsVerifierBuilder builder) {
        this.bin = builder.getBin();
    }

    public void verify(BinLookUpDetails binLookUpDetails) {
        verifyBin(binLookUpDetails.getBin(), bin);
    }

    private void verifyBin(String actual, String expected) {
        if (expected != null) {
            assertThat(actual, equalTo(expected));
        }
    }
}
