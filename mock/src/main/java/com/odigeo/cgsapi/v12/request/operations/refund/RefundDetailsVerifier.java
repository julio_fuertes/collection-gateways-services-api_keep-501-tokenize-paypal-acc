package com.odigeo.cgsapi.v12.request.operations.refund;


import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Created by marcal.perapoch on 30/07/2015.
 */
public abstract class RefundDetailsVerifier {

    private final String transactionId;
    private final String merchantOrderId;
    private final Money money;
    private final GatewayCollectionMethod gatewayCollectionMethod;

    public RefundDetailsVerifier(RefundDetailsVerifierBuilder builder) {
        this.transactionId = builder.getTransactionId();
        this.merchantOrderId = builder.getMerchantOrderId();
        this.money = builder.getMoney();
        this.gatewayCollectionMethod = builder.getGatewayCollectionMethod();
    }

    public void verify(RefundDetails refundDetails) {
        verify(refundDetails.getGatewayCollectionMethod());
        verify(refundDetails.getMoney());
        verifyMerchantOrderId(refundDetails.getMerchantOrderId());
        verifyTransactionId(refundDetails.getTransactionId());
    }

    private void verifyTransactionId(String transactionId) {
        if (this.transactionId != null) {
            assertThat(transactionId, equalTo(this.transactionId));
        }
    }

    private void verifyMerchantOrderId(String merchantOrderId) {
        if (this.merchantOrderId != null) {
            assertThat(merchantOrderId, equalTo(this.merchantOrderId));
        }
    }

    private void verify(Money money) {
        if (this.money != null) {
            assertThat(money, equalTo(this.money));
        }
    }

    private void verify(GatewayCollectionMethod gatewayCollectionMethod) {
        if (this.gatewayCollectionMethod != null) {
            assertThat(gatewayCollectionMethod, equalTo(this.gatewayCollectionMethod));
        }
    }
}
