package com.odigeo.cgsapi.v12.response;

import com.odigeo.cgsapi.v12.util.CustomPicker;
import com.odigeo.cgsapi.v12.util.MockUtil;

import java.util.Random;

public class BankTransferMovementAdditionalInformationBuilder implements MovementAdditionalInformationBuilder {

    private String paymentReference;
    private String accountHolder;
    private String bankName;
    private String city;
    private String swiftCode;
    private String bankAccountNumber;
    private String iban;
    private String countryDescription;
    private String type;

    public BankTransferMovementAdditionalInformationBuilder paymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
        return this;
    }

    public BankTransferMovementAdditionalInformationBuilder accountHolder(String accountHolder) {
        this.accountHolder = accountHolder;
        return this;
    }

    public BankTransferMovementAdditionalInformationBuilder city(String city) {
        this.city = city;
        return this;
    }

    public BankTransferMovementAdditionalInformationBuilder bankName(String bankName) {
        this.bankName = bankName;
        return this;
    }

    public BankTransferMovementAdditionalInformationBuilder swiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
        return this;
    }

    public BankTransferMovementAdditionalInformationBuilder bankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
        return this;
    }

    public BankTransferMovementAdditionalInformationBuilder iban(String iban) {
        this.iban = iban;
        return this;
    }

    public BankTransferMovementAdditionalInformationBuilder countryDescription(String countryDescription) {
        this.countryDescription = countryDescription;
        return this;
    }

    public BankTransferMovementAdditionalInformationBuilder type(String type) {
        this.type = type;
        return this;
    }

    public MovementAdditionalInformation build(Random dice) {
        fillEmptyData(dice);
        BankTransferMovementAdditionalInformation bankTransferMovementAdditionalInformation = new BankTransferMovementAdditionalInformation();
        bankTransferMovementAdditionalInformation.setPaymentReference(paymentReference);
        bankTransferMovementAdditionalInformation.setAccountHolder(accountHolder);
        bankTransferMovementAdditionalInformation.setBankName(bankName);
        bankTransferMovementAdditionalInformation.setCity(city);
        bankTransferMovementAdditionalInformation.setSwiftCode(swiftCode);
        bankTransferMovementAdditionalInformation.setBankAccountNumber(bankAccountNumber);
        bankTransferMovementAdditionalInformation.setIban(iban);
        bankTransferMovementAdditionalInformation.setCountryDescription(countryDescription);
        bankTransferMovementAdditionalInformation.setType(type);
        return bankTransferMovementAdditionalInformation;
    }

    private void fillEmptyData(Random dice) {
        CustomPicker picker = new CustomPicker(dice);
        paymentReference = MockUtil.pickStringWhenNullParameter(paymentReference, picker, 10);
        accountHolder = MockUtil.pickStringWhenNullParameter(accountHolder, picker, 10);
        bankName = MockUtil.pickStringWhenNullParameter(bankName, picker, 10);
        city = MockUtil.pickStringWhenNullParameter(city, picker, 10);
        swiftCode = MockUtil.pickStringWhenNullParameter(swiftCode, picker, 10);
        bankAccountNumber = MockUtil.pickStringWhenNullParameter(bankAccountNumber, picker, 10);
        iban = MockUtil.pickStringWhenNullParameter(iban, picker, 10);
        countryDescription = MockUtil.pickStringWhenNullParameter(countryDescription, picker, 10);
        type = MockUtil.pickStringWhenNullParameter(type, picker, 10);
    }

}
