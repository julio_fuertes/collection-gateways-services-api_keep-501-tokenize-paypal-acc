package com.odigeo.cgsapi.v12.util;

import com.odigeo.commons.test.random.Picker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;


public class CustomPicker extends Picker {

    private static final List<String> NUMERIC = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    private static final List<String> LOWER_ALPHA = Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
    private static final List<String> CAPITAL_ALPHA = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");

    public CustomPicker(Random random) {
        super(random);
    }

    public String pickStringWithOnlyLetters(int length) {
        List<String> alphabet = new ArrayList<String>();
        alphabet.addAll(LOWER_ALPHA);
        alphabet.addAll(CAPITAL_ALPHA);
        return buildStringFrom(alphabet, length);
    }

    public String pickStringWithLettersAndNumbers(int length) {
        List<String> alphabet = new ArrayList<String>();
        alphabet.addAll(NUMERIC);
        alphabet.addAll(LOWER_ALPHA);
        alphabet.addAll(CAPITAL_ALPHA);
        return buildStringFrom(alphabet, length);
    }

    public String pickStringWithOnlyNumbers(int length) {
        List<String> alphabet = new ArrayList<String>();
        alphabet.addAll(NUMERIC);
        return buildStringFrom(alphabet, length);
    }

    public String pickEmail() {
        StringBuilder builder = new StringBuilder()
            .append(pickStringWithLettersAndNumbers(10))
            .append('@')
            .append(pickStringWithOnlyLetters(5))
            .append('.')
            .append(pickStringWithOnlyLetters(3));
        return builder.toString();
    }

    public Map<String, String> pickMap() {
        Map<String, String> map = new HashMap<String, String>();
        int maxValueLoop = pickInt(1, 9);
        for (int i = 0; i < maxValueLoop; i++) {
            map.put("key" + i, "value" + i);
        }
        return map;
    }


    private String buildStringFrom(List<String> alphabet, int length) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            builder.append(pickFrom(alphabet));
        }
        return builder.toString();
    }


}
