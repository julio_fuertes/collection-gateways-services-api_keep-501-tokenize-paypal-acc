package com.odigeo.cgsapi.v12.request.operations.binlookup;

public class BinLookUpDetailsVerifierBuilder {

    private String bin;

    public BinLookUpDetailsVerifierBuilder bin(String bin) {
        this.bin = bin;
        return this;
    }

    public String getBin() {
        return bin;
    }
}
