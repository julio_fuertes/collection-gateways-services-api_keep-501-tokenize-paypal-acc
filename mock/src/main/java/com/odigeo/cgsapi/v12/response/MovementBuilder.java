package com.odigeo.cgsapi.v12.response;

import com.odigeo.cgsapi.v12.util.CustomPicker;
import com.odigeo.cgsapi.v12.util.MockUtil;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Random;


public class MovementBuilder {

    private String merchantOrderId;
    private String code;
    private MovementErrorType errorType;
    private String errorDescription;
    private MovementAction action;
    private MovementStatus status;
    private BigDecimal amount;
    private Currency currency;
    private String terminalId;
    private String transactionId;
    private String merchantStatus;
    private String merchantStatusSubcode;
    private String secure;
    private String transactionIdSub;
    private MovementAdditionalInformationBuilder additionalInformationBuilder;

    public MovementBuilder merchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
        return this;
    }

    public MovementBuilder code(String code) {
        this.code = code;
        return this;
    }

    public MovementBuilder errorType(MovementErrorType errorType) {
        this.errorType = errorType;
        return this;
    }

    public MovementBuilder errorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
        return this;
    }

    public MovementBuilder action(MovementAction action) {
        this.action = action;
        return this;
    }

    public MovementBuilder status(MovementStatus status) {
        this.status = status;
        return this;
    }

    public MovementBuilder amount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public MovementBuilder currency(Currency currency) {
        this.currency = currency;
        return this;
    }

    public MovementBuilder terminalId(String terminalId) {
        this.terminalId = terminalId;
        return this;
    }

    public MovementBuilder transactionId(String transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    public MovementBuilder merchantStatus(String merchantStatus) {
        this.merchantStatus = merchantStatus;
        return this;
    }

    public MovementBuilder merchantStatusSubcode(String merchantStatusSubcode) {
        this.merchantStatusSubcode = merchantStatusSubcode;
        return this;
    }

    public MovementBuilder secure(String secure) {
        this.secure = secure;
        return this;
    }

    public MovementBuilder transactionIdSub(String transactionIdSub) {
        this.transactionIdSub = transactionIdSub;
        return this;
    }

    public MovementBuilder additionalInformationBuilder(MovementAdditionalInformationBuilder movementAdditionalInformationBuilder) {
        this.additionalInformationBuilder = movementAdditionalInformationBuilder;
        return this;
    }

    public Movement build(Random dice) {
        fillEmptyData(dice);
        Movement movement = new Movement(merchantOrderId, code, action, secure);
        movement.setErrorType(errorType);
        movement.setErrorDescription(errorDescription);
        movement.setStatus(status);
        movement.setAmount(amount);
        movement.setCurrency(currency);
        movement.setTerminalId(terminalId);
        movement.setTransactionId(transactionId);
        movement.setMerchantStatus(merchantStatus);
        movement.setMerchantStatusSubcode(merchantStatusSubcode);
        movement.setTransactionIdSub(transactionIdSub);
        movement.setAdditionalInformation(additionalInformationBuilder.build(dice));
        return movement;
    }

    private void fillEmptyData(Random dice) {
        CustomPicker picker = new CustomPicker(dice);
        merchantOrderId = MockUtil.pickStringWhenNullParameter(merchantOrderId, picker, 10);
        code = MockUtil.pickStringWhenNullParameter(code, picker, 10);
        if (errorType == null) {
            errorType = picker.pickFrom(MovementErrorType.values());
        }
        errorDescription = MockUtil.pickStringWhenNullParameter(errorDescription, picker, 10);
        if (action == null) {
            action = picker.pickFrom(MovementAction.values());
        }
        if (status == null) {
            status = picker.pickFrom(MovementStatus.values());
        }
        if (amount == null) {
            amount = picker.pickDecimal(BigDecimal.ZERO, BigDecimal.TEN);
        }
        if (currency == null) {
            currency = Currency.getInstance(picker.pickLocale());
        }
        terminalId = MockUtil.pickStringWhenNullParameter(terminalId, picker, 10);
        transactionId = MockUtil.pickStringWhenNullParameter(transactionId, picker, 10);
        merchantStatus = MockUtil.pickStringWhenNullParameter(merchantStatus, picker, 10);
        merchantStatusSubcode = MockUtil.pickStringWhenNullParameter(merchantStatusSubcode, picker, 10);
        secure = MockUtil.pickStringWhenNullParameter(secure, picker, 10);
        transactionIdSub = MockUtil.pickStringWhenNullParameter(transactionIdSub, picker, 10);
        if (additionalInformationBuilder == null) {
            additionalInformationBuilder = new BankTransferMovementAdditionalInformationBuilder();
        }
    }
}
