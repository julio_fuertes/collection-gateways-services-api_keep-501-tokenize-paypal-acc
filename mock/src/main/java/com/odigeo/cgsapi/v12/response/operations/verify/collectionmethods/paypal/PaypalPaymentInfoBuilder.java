package com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal;

public class PaypalPaymentInfoBuilder {

    private String paymentType;
    private PaypalProtectionEligibility protectionEligibility;

    public PaypalPaymentInfoBuilder paymentType(String paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    public PaypalPaymentInfoBuilder protectionEligibility(PaypalProtectionEligibility protectionEligibility) {
        this.protectionEligibility = protectionEligibility;
        return this;
    }

    public PaypalPaymentInfo build() {
        PaypalPaymentInfo.Builder builder = new PaypalPaymentInfo.Builder();
        builder.paymentType(paymentType);
        builder.protectionEligibility(protectionEligibility);
        return new PaypalPaymentInfo(builder);
    }

}
