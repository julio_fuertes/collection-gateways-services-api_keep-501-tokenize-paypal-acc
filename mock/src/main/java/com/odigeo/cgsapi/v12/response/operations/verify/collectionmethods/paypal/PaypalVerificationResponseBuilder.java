package com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal;

import com.odigeo.cgsapi.v12.response.operations.verify.VerificationResponseBuilder;

public class PaypalVerificationResponseBuilder extends VerificationResponseBuilder {

    private PaypalCustomerAccountDataBuilder paypalCustomerAccountDataBuilder;
    private PaypalPaymentInfoBuilder paypalPaymentInfoBuilder;

    public PaypalVerificationResponseBuilder paypalCustomerAccountDataBuilder(PaypalCustomerAccountDataBuilder paypalCustomerAccountDataBuilder) {
        this.paypalCustomerAccountDataBuilder = paypalCustomerAccountDataBuilder;
        return this;
    }

    public PaypalVerificationResponseBuilder paypalPaymentInfoBuilder(PaypalPaymentInfoBuilder paypalPaymentInfoBuilder) {
        this.paypalPaymentInfoBuilder = paypalPaymentInfoBuilder;
        return this;
    }

    public PaypalVerificationResponse build() {
        PaypalVerificationResponse.PaypalBuilder paypalBuilder = new PaypalVerificationResponse.PaypalBuilder();
        paypalBuilder.paypalPaymentInfo(paypalPaymentInfoBuilder == null ? null : paypalPaymentInfoBuilder.build());
        paypalBuilder.paypalCustomerAccountData(paypalCustomerAccountDataBuilder == null ? null : paypalCustomerAccountDataBuilder.build());
        return paypalBuilder.build();

    }

}
