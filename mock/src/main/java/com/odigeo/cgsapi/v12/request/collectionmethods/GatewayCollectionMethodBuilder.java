package com.odigeo.cgsapi.v12.request.collectionmethods;

import com.odigeo.cgsapi.v12.util.CustomPicker;

import java.util.Random;

public class GatewayCollectionMethodBuilder {

    //This attribute is never used
    //protected Integer id;
    protected GatewayCollectionMethodType gatewayCollectionMethodType;

    public GatewayCollectionMethodBuilder gatewayCollectionMethodType(GatewayCollectionMethodType gatewayCollectionMethodType) {
        this.gatewayCollectionMethodType = gatewayCollectionMethodType;
        return this;
    }

    public GatewayCollectionMethod build(Random dice) {
        fillEmptyData(dice);
        return new GatewayCollectionMethod(gatewayCollectionMethodType);
    }

    protected void fillEmptyData(Random dice) {
        CustomPicker picker = new CustomPicker(dice);
        if (gatewayCollectionMethodType == null) {
            gatewayCollectionMethodType = picker.pickFrom(GatewayCollectionMethodType.values());
        }
    }
}
