package com.odigeo.cgsapi.v12.util;

import org.hamcrest.MatcherAssert;

import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isIn;


public class VerifyUtils {

    public static void verifyEquals(String actual, String expected) {
        MatcherAssert.assertThat(actual, is(equalTo(expected)));
    }

    public static void verifyEquals(Enum actual, Enum expected) {
        MatcherAssert.assertThat(actual, is(equalTo(expected)));
    }

    public static void verifyEquals(Map actual, Map expected) {
        MatcherAssert.assertThat(actual.size(), is(equalTo(expected.size())));
        MatcherAssert.assertThat(actual.entrySet(), everyItem(isIn(expected.entrySet())));
    }

}
