package com.odigeo.cgsapi.v12.response.operations.query;


import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodBuilder;
import com.odigeo.cgsapi.v12.response.MovementBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class QueryResponseBuilder {

    private List<MovementBuilder> movementBuilders;
    private GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder;

    public QueryResponseBuilder movementBuilders(List<MovementBuilder> movementBuilders) {
        this.movementBuilders = movementBuilders;
        return this;
    }

    public QueryResponseBuilder gatewayCollectionMethodBuilder(GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder) {
        this.gatewayCollectionMethodBuilder = gatewayCollectionMethodBuilder;
        return this;
    }

    public QueryResponse build(Random dice) {
        fillEmptyData(dice);
        QueryResponse.Builder builder = new QueryResponse.Builder();
        for (MovementBuilder movementBuilder : movementBuilders) {
            builder.addMovement(movementBuilder.build(dice));
        }
        QueryResponse queryResponse = builder.build();
        queryResponse.setGatewayCollectionMethod(gatewayCollectionMethodBuilder.build(dice));
        return queryResponse;
    }

    private void fillEmptyData(Random dice) {
        if (movementBuilders == null) {
            movementBuilders = Arrays.asList(buildMovementBuilderWithValidStatus(dice));
        }
        if (gatewayCollectionMethodBuilder == null) {
            gatewayCollectionMethodBuilder = new GatewayCollectionMethodBuilder();
        }
    }

    private MovementBuilder buildMovementBuilderWithValidStatus(Random dice) {
        MovementBuilder movementBuilder = null;
        boolean correctMovement = false;
        while (!correctMovement) {
            try {
                correctMovement = true;
                movementBuilder = new MovementBuilder();
                new QueryResponse.Builder().addMovement(movementBuilder.build(dice));
            } catch (IllegalArgumentException e) {
                correctMovement = false;
            }
        }
        return movementBuilder;
    }
}
