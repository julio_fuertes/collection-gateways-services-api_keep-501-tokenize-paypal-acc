package com.odigeo.cgsapi.v12.request.collectionmethods;

import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import com.odigeo.commons.test.random.Picker;

import java.util.Random;


public class CreditCardGatewayCollectionMethodBuilder extends GatewayCollectionMethodBuilder {

    private CreditCardType type;

    public CreditCardGatewayCollectionMethodBuilder type(CreditCardType type) {
        this.type = type;
        return this;
    }

    public CreditCardGatewayCollectionMethod build(Random dice) {
        fillEmptyData(dice);
        return new CreditCardGatewayCollectionMethod(gatewayCollectionMethodType, type);
    }

    protected void fillEmptyData(Random dice) {
        Picker picker = new Picker(dice);
        super.fillEmptyData(dice);
        if (type == null) {
            type = picker.pickFrom(CreditCardType.values());
        }
    }
}
