package com.odigeo.cgsapi.v12.response.operations.verify;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodBuilder;
import com.odigeo.cgsapi.v12.response.MovementBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Random;


public class VerificationResponseBuilder {

    private List<MovementBuilder> movementBuilders;
    private GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder;

    public VerificationResponseBuilder movementBuilders(List<MovementBuilder> movementBuilders) {
        this.movementBuilders = movementBuilders;
        return this;
    }

    public VerificationResponseBuilder gatewayCollectionMethodBuilder(GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder) {
        this.gatewayCollectionMethodBuilder = gatewayCollectionMethodBuilder;
        return this;
    }

    public VerificationResponse build(Random dice) {
        fillEmptyData(dice);
        VerificationResponse.Builder builder = new VerificationResponse.Builder();
        for (MovementBuilder movementBuilder : movementBuilders) {
            builder.addMovement(movementBuilder.build(dice));
        }
        VerificationResponse verificationResponse = builder.build();
        verificationResponse.setGatewayCollectionMethod(gatewayCollectionMethodBuilder.build(dice));
        return verificationResponse;
    }

    private void fillEmptyData(Random dice) {
        if (movementBuilders == null) {
            movementBuilders = Arrays.asList(buildMovementBuilderWithValidStatus(dice));
        }
        if (gatewayCollectionMethodBuilder == null) {
            gatewayCollectionMethodBuilder = new GatewayCollectionMethodBuilder();
        }
    }

    private MovementBuilder buildMovementBuilderWithValidStatus(Random dice) {
        MovementBuilder movementBuilder = null;
        boolean correctMovement = false;
        while (!correctMovement) {
            try {
                correctMovement = true;
                movementBuilder = new MovementBuilder();
                new VerificationResponse.Builder().addMovement(movementBuilder.build(dice));
            } catch (IllegalArgumentException e) {
                correctMovement = false;
            }
        }
        return movementBuilder;
    }
}
