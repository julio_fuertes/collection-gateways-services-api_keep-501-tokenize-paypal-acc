package com.odigeo.cgsapi.v12.request.operations.collect;


import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Created by marcal.perapoch on 30/07/2015.
 */
public abstract class CollectionDetailsVerifier {

    private final String orderId;
    private final Money money;
    private final GatewayCollectionMethod gatewayCollectionMethod;

    public CollectionDetailsVerifier(CollectionDetailsVerifierBuilder builder) {
        this.orderId = builder.getOrderId();
        this.money = builder.getMoney();
        this.gatewayCollectionMethod = builder.getGatewayCollectionMethod();
    }

    public void verify(CollectionDetails collectionDetails) {
        verifyOrderId(collectionDetails.getOrderId(), orderId);
        verifyMoney(collectionDetails.getMoney(), money);
        verifyGatewayCollectionMethod(collectionDetails.getGatewayCollectionMethod(), gatewayCollectionMethod);
    }

    private void verifyOrderId(String actual, String expected) {
        if (expected != null) {
            assertThat(actual, equalTo(expected));
        }
    }

    private void verifyMoney(Money actual, Money expected) {
        if (expected != null) {
            assertThat(actual, equalTo(expected));
        }
    }

    private void verifyGatewayCollectionMethod(GatewayCollectionMethod actual, GatewayCollectionMethod expected) {
        if (expected != null) {
            assertThat(actual, equalTo(expected));
        }
    }
}
