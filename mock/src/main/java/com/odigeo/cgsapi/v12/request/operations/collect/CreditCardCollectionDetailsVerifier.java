package com.odigeo.cgsapi.v12.request.operations.collect;


import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.creditcard.CreditCardCollectionDetails;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by marcal.perapoch on 30/07/2015.
 */
public class CreditCardCollectionDetailsVerifier extends CollectionDetailsVerifier {

    private final CreditCard creditCard;

    public CreditCardCollectionDetailsVerifier(CreditCardCollectionDetailsVerifierBuilder builder) {
        super(builder);
        this.creditCard = builder.getCreditCard();
    }

    public void verify(CollectionDetails collectionDetails) {
        super.verify(collectionDetails);
        assert collectionDetails instanceof CreditCardCollectionDetails : collectionDetails.getClass();
        verify(CreditCardCollectionDetails.class.cast(collectionDetails).getCreditCard());
    }

    private void verify(CreditCard creditCard) {
        if (this.creditCard != null) {
            assertThat(creditCard, equalTo(this.creditCard));
        }
    }
}
