package com.odigeo.cgsapi.v12.response.operations.refund;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodBuilder;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementBuilder;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Random;


public class RefundResponseBuilder {

    @Deprecated
    private MovementAction movementAction;
    @Deprecated
    private MovementStatus movementStatus;
    @Deprecated
    private BigDecimal amount;
    @Deprecated
    private Currency currency;
    @Deprecated
    private String merchantOrderId;
    @Deprecated
    private GatewayCollectionMethod gatewayCollectionMethod;

    private List<MovementBuilder> movementBuilders;
    private GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder;

    @Deprecated
    public RefundResponseBuilder collectionStatus(CollectionStatus collectionStatus) {
        if (CollectionStatus.REFUNDED.equals(collectionStatus)) {
            this.movementAction = MovementAction.COLLECT;
            this.movementStatus = MovementStatus.REFUNDED;
        }
        return this;
    }

    @Deprecated
    public RefundResponseBuilder amount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    @Deprecated
    public RefundResponseBuilder amount(int amount) {
        this.amount = new BigDecimal(amount);
        return this;
    }

    @Deprecated
    public RefundResponseBuilder currency(Currency currency) {
        this.currency = currency;
        return this;
    }

    @Deprecated
    public RefundResponseBuilder currency(String currencyCode) {
        this.currency = Currency.getInstance(currencyCode);
        return this;
    }

    @Deprecated
    public RefundResponseBuilder merchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
        return this;
    }

    @Deprecated
    public RefundResponseBuilder gatewayCollectionMethod(GatewayCollectionMethod gatewayCollectionMethod) {
        this.gatewayCollectionMethod = gatewayCollectionMethod;
        return this;
    }

    public RefundResponseBuilder movementBuilders(List<MovementBuilder> movementBuilders) {
        this.movementBuilders = movementBuilders;
        return this;
    }

    public RefundResponseBuilder gatewayCollectionMethodBuilder(GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder) {
        this.gatewayCollectionMethodBuilder = gatewayCollectionMethodBuilder;
        return this;
    }

    @Deprecated
    public MovementAction getMovementAction() {
        return movementAction;
    }

    @Deprecated
    public BigDecimal getAmount() {
        return amount;
    }

    @Deprecated
    public Currency getCurrency() {
        return currency;
    }

    @Deprecated
    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    @Deprecated
    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

    @Deprecated
    public MovementStatus getMovementStatus() {
        return movementStatus;
    }

    @Deprecated
    public RefundResponse build() {
        Movement movement = new Movement(merchantOrderId, "0", movementAction);
        movement.setAmount(amount);
        movement.setStatus(movementStatus);
        movement.setCurrency(currency);
        RefundResponse.Builder builder = new RefundResponse.Builder();
        RefundResponse collectionResponse = builder.addMovement(movement).build();
        collectionResponse.setGatewayCollectionMethod(gatewayCollectionMethod);
        return collectionResponse;
    }

    public RefundResponse build(Random dice) {
        fillEmptyData(dice);
        RefundResponse.Builder builder = new RefundResponse.Builder();
        for (MovementBuilder movementBuilder : movementBuilders) {
            builder.addMovement(movementBuilder.build(dice));
        }
        RefundResponse refundResponse = builder.build();
        refundResponse.setGatewayCollectionMethod(gatewayCollectionMethodBuilder.build(dice));
        return refundResponse;
    }

    private void fillEmptyData(Random dice) {
        if (movementBuilders == null) {
            movementBuilders = Arrays.asList(buildMovementBuilderWithValidStatus(dice));
        }
        if (gatewayCollectionMethodBuilder == null) {
            gatewayCollectionMethodBuilder = new GatewayCollectionMethodBuilder();
        }
    }

    private MovementBuilder buildMovementBuilderWithValidStatus(Random dice) {
        MovementBuilder movementBuilder = null;
        boolean correctMovement = false;
        while (!correctMovement) {
            try {
                correctMovement = true;
                movementBuilder = new MovementBuilder();
                new RefundResponse.Builder().addMovement(movementBuilder.build(dice));
            } catch (IllegalArgumentException e) {
                correctMovement = false;
            }
        }
        return movementBuilder;
    }
}
