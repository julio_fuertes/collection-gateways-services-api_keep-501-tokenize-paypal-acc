package com.odigeo.cgsapi.v12.request.operations.refund;


import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;

/**
 * Created by marcal.perapoch on 30/07/2015.
 */
public class CreditCardRefundDetailsVerifierBuilder extends RefundDetailsVerifierBuilder {

    private CreditCard creditCard;

    public CreditCardRefundDetailsVerifierBuilder creditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
        return this;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public CreditCardRefundDetailsVerifier build() {
        return new CreditCardRefundDetailsVerifier(this);
    }
}
