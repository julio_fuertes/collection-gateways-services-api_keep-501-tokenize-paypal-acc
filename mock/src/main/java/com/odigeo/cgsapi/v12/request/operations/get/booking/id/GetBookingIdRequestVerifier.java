package com.odigeo.cgsapi.v12.request.operations.get.booking.id;

import com.odigeo.cgsapi.v12.util.VerifyUtils;

public class GetBookingIdRequestVerifier {

    private String merchantOrderId;

    public void setMerchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
    }

    public void verify(GetBookingIdRequest getBookingIdRequest) {
        VerifyUtils.verifyEquals(merchantOrderId, getBookingIdRequest.getMerchantOrderId());
    }
}
