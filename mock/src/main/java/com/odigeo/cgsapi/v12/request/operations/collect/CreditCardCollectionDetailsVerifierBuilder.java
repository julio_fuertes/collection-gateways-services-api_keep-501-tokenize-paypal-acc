package com.odigeo.cgsapi.v12.request.operations.collect;


import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;

/**
 * Created by marcal.perapoch on 30/07/2015.
 */
public class CreditCardCollectionDetailsVerifierBuilder extends CollectionDetailsVerifierBuilder {

    private CreditCard creditCard;

    public CreditCardCollectionDetailsVerifierBuilder creditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
        return this;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public CreditCardCollectionDetailsVerifier build() {
        return new CreditCardCollectionDetailsVerifier(this);
    }
}
