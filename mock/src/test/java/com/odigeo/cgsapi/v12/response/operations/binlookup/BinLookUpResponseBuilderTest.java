package com.odigeo.cgsapi.v12.response.operations.binlookup;

import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class BinLookUpResponseBuilderTest {

    @Test
    public void testBuildWithAllParameters() {
        //Given
        BinLookUpResponseBuilder builder = new BinLookUpResponseBuilder();
        builder.countryCode("ES");
        builder.numCountryCode(754);
        builder.creditCardType(CreditCardType.MASTER_CARD);
        //When
        BinLookUpResponse binLookUpResponse = builder.build(new Random());
        //Then
        assertEquals(binLookUpResponse.getCountryCode(), "ES");
        assertEquals(binLookUpResponse.getNumCountryCode(), (Integer) 754);
        assertEquals(binLookUpResponse.getCreditCardType(), CreditCardType.MASTER_CARD);
        BinLookUp binLookUp = (BinLookUp) binLookUpResponse.getMovements().iterator().next();
        assertEquals(binLookUp.getCountryCode(), "ES");
        assertEquals(binLookUp.getNumCountryCode(), (Integer) 754);
        assertEquals(binLookUp.getCreditCardType(), CreditCardType.MASTER_CARD);
    }

    @Test
    public void testBuildWhenMissingParameters() {
        //Given
        BinLookUpResponseBuilder builder = new BinLookUpResponseBuilder();
        builder.gatewayCollectionMethodBuilder(null);
        builder.countryCode(null);
        builder.creditCardType(null);
        builder.numCountryCode(null);
        //When
        BinLookUpResponse binLookUpResponse = builder.build(new Random());
        //Then
        assertNotNull(binLookUpResponse.getCountryCode());
        assertNotNull(binLookUpResponse.getNumCountryCode());
        assertNotNull(binLookUpResponse.getCreditCardType());
        assertNotNull(binLookUpResponse.getGatewayCollectionMethod());
        BinLookUp binLookUp = (BinLookUp) binLookUpResponse.getMovements().iterator().next();
        assertNotNull(binLookUp.getCountryCode());
        assertNotNull(binLookUp.getNumCountryCode());
        assertNotNull(binLookUp.getCreditCardType());
    }
}
