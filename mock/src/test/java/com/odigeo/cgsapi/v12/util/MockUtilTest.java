package com.odigeo.cgsapi.v12.util;

import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class MockUtilTest {

    @Test
    public void testPickStringWhenNullParameter() {
        //given
        String aString = null;
        CustomPicker customPicker = new CustomPicker(new Random());
        //when
        aString = MockUtil.pickStringWhenNullParameter(aString, customPicker, 10);
        //then
        assertNotNull(aString);
        assertEquals(aString.length(), 10);
    }
}
