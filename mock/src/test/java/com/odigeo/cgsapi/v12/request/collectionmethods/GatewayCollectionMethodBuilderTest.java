package com.odigeo.cgsapi.v12.request.collectionmethods;


import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class GatewayCollectionMethodBuilderTest {

    @Test
    public void testBuildWithValues() {
        //Given
        GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder = new GatewayCollectionMethodBuilder();
        gatewayCollectionMethodBuilder.gatewayCollectionMethodType(GatewayCollectionMethodType.SOFORT);
        //When
        GatewayCollectionMethod gatewayCollectionMethod = gatewayCollectionMethodBuilder.build(new Random());
        //Then
        assertEquals(gatewayCollectionMethod.getGatewayCollectionMethodType(), GatewayCollectionMethodType.SOFORT);
    }

    @Test
    public void testBuildWithoutValues() {
        ///Given
        GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder = new GatewayCollectionMethodBuilder();
        gatewayCollectionMethodBuilder.gatewayCollectionMethodType(null);
        //When
        GatewayCollectionMethod gatewayCollectionMethod = gatewayCollectionMethodBuilder.build(new Random());
        //Then
        assertNotNull(gatewayCollectionMethod.getGatewayCollectionMethodType());
    }
}
