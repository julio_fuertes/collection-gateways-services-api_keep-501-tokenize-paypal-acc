package com.odigeo.cgsapi.v12.request.operations.notify;

import com.odigeo.cgsapi.v12.request.VirtualPointOfSaleType;
import org.testng.annotations.Test;

import java.util.HashMap;


public class ProviderNotificationRequestVerifierTest {

    @Test
    public void testVerify() {
        //Given
        ProviderNotificationRequestBuilder builder = new ProviderNotificationRequestBuilder();
        builder.virtualPointOfSale(VirtualPointOfSaleType.ADYEN).parameters(new HashMap<String, Object>());
        ProviderNotificationRequest providerNotificationRequest = builder.build();

        ProviderNotificationRequestVerifier verifier = new ProviderNotificationRequestVerifier();
        verifier.setVirtualPointOfSale(VirtualPointOfSaleType.ADYEN);
        verifier.setParameters(new HashMap<String, Object>());
        //When-Then
        verifier.verify(providerNotificationRequest);
    }
}
