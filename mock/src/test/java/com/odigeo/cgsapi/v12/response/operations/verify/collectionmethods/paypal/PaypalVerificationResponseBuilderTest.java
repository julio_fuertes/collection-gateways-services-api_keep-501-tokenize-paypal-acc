package com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal;

import org.mockito.Mock;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PaypalVerificationResponseBuilderTest {

    @Mock
    private PaypalCustomerAccountDataBuilder paypalCustomerAccountDataBuilder;
    @Mock
    private PaypalPaymentInfoBuilder paypalPaymentInfoBuilder;

    @Test
    public void testBuild() {
        //When
        //Given
        PaypalVerificationResponseBuilder builder = new PaypalVerificationResponseBuilder();
        builder.paypalCustomerAccountDataBuilder(paypalCustomerAccountDataBuilder).paypalPaymentInfoBuilder(paypalPaymentInfoBuilder);
        PaypalVerificationResponse response = builder.build();
        //Then
        assertEquals(response.getPaypalCustomerAccountData(), paypalCustomerAccountDataBuilder);
        assertEquals(response.getPaypalPaymentInfo(), paypalPaymentInfoBuilder);
    }
}
