package com.odigeo.cgsapi.v12.response.operations.collect;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodBuilder;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementBuilder;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.RedirectionParameters;
import com.odigeo.cgsapi.v12.response.RedirectionParametersBuilder;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Random;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class CollectionResponseBuilderTest {

    private Random dice = new Random();
    private CollectionResponseBuilder collectionResponseBuilder;

    @BeforeMethod
    private void init() {
        MockitoAnnotations.initMocks(this);
        collectionResponseBuilder = new CollectionResponseBuilder();
    }

    @Test
    public void testBuildWithValues() {
        //Given
        GatewayCollectionMethod gatewayCollectionMethod = Mockito.mock(GatewayCollectionMethod.class);
        GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder = Mockito.mock(GatewayCollectionMethodBuilder.class);
        when(gatewayCollectionMethodBuilder.build(dice)).thenReturn(gatewayCollectionMethod);
        RedirectionParameters redirectionParameters = Mockito.mock(RedirectionParameters.class);
        RedirectionParametersBuilder redirectionParametersBuilder = Mockito.mock(RedirectionParametersBuilder.class);
        when(redirectionParametersBuilder.build(dice)).thenReturn(redirectionParameters);
        Movement movement = Mockito.mock(Movement.class);
        when(movement.getStatus()).thenReturn(MovementStatus.PAID);
        MovementBuilder movementBuilder = Mockito.mock(MovementBuilder.class);
        when(movementBuilder.build(dice)).thenReturn(movement);
        collectionResponseBuilder.redirectionParametersBuilder(redirectionParametersBuilder);
        collectionResponseBuilder.gatewayCollectionMethodBuilder(gatewayCollectionMethodBuilder);
        collectionResponseBuilder.movementBuilders(Arrays.asList(movementBuilder));
        collectionResponseBuilder.numCountryCode(1);
        //When
        CollectionResponse collectionResponse = collectionResponseBuilder.build(dice);
        //Then
        assertEquals(collectionResponse.getRedirectionParameters(), redirectionParameters);
        assertEquals(collectionResponse.getGatewayCollectionMethod(), gatewayCollectionMethod);
        assertEquals(collectionResponse.getMovements().size(), 1);
        assertEquals(collectionResponse.getMovements().iterator().next(), movement);
        assertEquals(collectionResponse.getNumCountryCode(), new Integer(1));

    }

    @Test
    public void testBuildWithoutValues() {
        //Given
        collectionResponseBuilder.redirectionParametersBuilder(null);
        collectionResponseBuilder.gatewayCollectionMethodBuilder(null);
        collectionResponseBuilder.movementBuilders(null);
        //When
        CollectionResponse collectionResponse = collectionResponseBuilder.build(dice);
        //Then
        assertNotNull(collectionResponse.getRedirectionParameters());
        assertNotNull(collectionResponse.getGatewayCollectionMethod());
        assertNotNull(collectionResponse.getMovements());
    }

    @Test
    public void testDeprecatedMethods() {
        assertNull(collectionResponseBuilder.getMovementAction());
        assertNull(collectionResponseBuilder.getAmount());
        assertNull(collectionResponseBuilder.getCurrency());
        assertNull(collectionResponseBuilder.getMerchantOrderId());
        assertNull(collectionResponseBuilder.getGatewayCollectionMethod());
        assertNull(collectionResponseBuilder.getMovementStatus());
    }

}
