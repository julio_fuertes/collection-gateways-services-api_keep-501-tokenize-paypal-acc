package com.odigeo.cgsapi.v12.response;

import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Random;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class MovementBuilderTest {

    private Random dice = new Random();

    private MovementBuilder movementBuilder;

    @BeforeMethod
    private void init() {
        MockitoAnnotations.initMocks(this);
        movementBuilder = new MovementBuilder();
    }

    @Test
    public void testBuildWithValues() {
        //Given
        BankTransferMovementAdditionalInformation aBankTransferMovementAdditionalInformation = Mockito.mock(BankTransferMovementAdditionalInformation.class);
        BankTransferMovementAdditionalInformationBuilder aBankTransferMovementAdditionalInformationBuilder = Mockito.mock(BankTransferMovementAdditionalInformationBuilder.class);
        when(aBankTransferMovementAdditionalInformationBuilder.build(dice)).thenReturn(aBankTransferMovementAdditionalInformation);
        movementBuilder.merchantOrderId("String1");
        movementBuilder.code("String2");
        movementBuilder.errorType(MovementErrorType.UNKNOWN);
        movementBuilder.errorDescription("String3");
        movementBuilder.action(MovementAction.AUTHORIZE);
        movementBuilder.status(MovementStatus.AUTHORIZED);
        movementBuilder.amount(new BigDecimal(10));
        movementBuilder.currency(Currency.getInstance("EUR"));
        movementBuilder.terminalId("String4");
        movementBuilder.transactionId("String5");
        movementBuilder.merchantStatus("String6");
        movementBuilder.merchantStatusSubcode("String7");
        movementBuilder.secure("String8");
        movementBuilder.transactionIdSub("String9");
        movementBuilder.additionalInformationBuilder(aBankTransferMovementAdditionalInformationBuilder);
        //When
        Movement movement = movementBuilder.build(dice);
        //Then
        assertEquals(movement.getMerchantOrderId(), "String1");
        assertEquals(movement.getCode(), "String2");
        assertEquals(movement.getErrorType(), MovementErrorType.UNKNOWN);
        assertEquals(movement.getErrorDescription(), "String3");
        assertEquals(movement.getAction(), MovementAction.AUTHORIZE);
        assertEquals(movement.getStatus(), MovementStatus.AUTHORIZED);
        assertEquals(movement.getAmount().intValue(), 10);
        assertEquals(movement.getCurrency().getCurrencyCode(), "EUR");
        assertEquals(movement.getTerminalId(), "String4");
        assertEquals(movement.getTransactionId(), "String5");
        assertEquals(movement.getMerchantStatus(), "String6");
        assertEquals(movement.getMerchantStatusSubcode(), "String7");
        assertEquals(movement.getSecure(), "String8");
        assertEquals(movement.getTransactionIdSub(), "String9");
        assertEquals(movement.getAdditionalInformation(), aBankTransferMovementAdditionalInformation);
    }

    @Test
    public void testBuildWithoutValues() {
        //Given
        movementBuilder.merchantOrderId(null);
        movementBuilder.code(null);
        movementBuilder.errorType(null);
        movementBuilder.errorDescription(null);
        movementBuilder.action(null);
        movementBuilder.status(null);
        movementBuilder.amount(null);
        movementBuilder.currency(null);
        movementBuilder.terminalId(null);
        movementBuilder.transactionId(null);
        movementBuilder.merchantStatus(null);
        movementBuilder.merchantStatusSubcode(null);
        movementBuilder.secure(null);
        movementBuilder.transactionIdSub(null);
        movementBuilder.additionalInformationBuilder(null);
        //When
        Movement movement = movementBuilder.build(dice);
        //Then
        assertNotNull(movement.getMerchantOrderId());
        assertNotNull(movement.getCode());
        assertNotNull(movement.getErrorType());
        assertNotNull(movement.getErrorDescription());
        assertNotNull(movement.getAction());
        assertNotNull(movement.getStatus());
        assertNotNull(movement.getAmount());
        assertNotNull(movement.getCurrency().getCurrencyCode());
        assertNotNull(movement.getTerminalId());
        assertNotNull(movement.getTransactionId());
        assertNotNull(movement.getMerchantStatus());
        assertNotNull(movement.getMerchantStatusSubcode());
        assertNotNull(movement.getSecure());
        assertNotNull(movement.getTransactionIdSub());
        assertNotNull(movement.getAdditionalInformation());
    }
}
