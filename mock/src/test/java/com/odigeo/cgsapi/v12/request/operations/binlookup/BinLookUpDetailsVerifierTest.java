package com.odigeo.cgsapi.v12.request.operations.binlookup;

import com.odigeo.cgsapi.v12.request.operations.binlookup.collectionmethods.creditcard.BinLookUpDetails;
import org.testng.annotations.Test;

public class BinLookUpDetailsVerifierTest {

    @Test
    public void testVerifyWithSameData() {
        //Given
        BinLookUpDetailsVerifierBuilder builder = new BinLookUpDetailsVerifierBuilder().bin("12345");
        BinLookUpDetails details = new BinLookUpDetails("12345");
        //When
        new BinLookUpDetailsVerifier(builder).verify(details);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testVerifyWithDistinctBin() {
        //Given
        BinLookUpDetailsVerifierBuilder builder = new BinLookUpDetailsVerifierBuilder().bin("12345");
        BinLookUpDetails details = new BinLookUpDetails("99999");
        //When
        new BinLookUpDetailsVerifier(builder).verify(details);
    }
}
