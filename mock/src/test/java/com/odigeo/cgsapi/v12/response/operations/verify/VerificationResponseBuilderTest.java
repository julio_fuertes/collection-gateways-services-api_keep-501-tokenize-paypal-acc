package com.odigeo.cgsapi.v12.response.operations.verify;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodBuilder;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementBuilder;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Random;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class VerificationResponseBuilderTest {

    private Random dice = new Random();
    private VerificationResponseBuilder verificationResponseBuilder;

    @BeforeMethod
    private void init() {
        MockitoAnnotations.initMocks(this);
        verificationResponseBuilder = new VerificationResponseBuilder();
    }

    @Test
    public void testBuildWithValues() {
        //Given
        GatewayCollectionMethod gatewayCollectionMethod = Mockito.mock(GatewayCollectionMethod.class);
        GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder = Mockito.mock(GatewayCollectionMethodBuilder.class);
        when(gatewayCollectionMethodBuilder.build(dice)).thenReturn(gatewayCollectionMethod);
        Movement movement = Mockito.mock(Movement.class);
        when(movement.getStatus()).thenReturn(MovementStatus.PAID);
        MovementBuilder movementBuilder = Mockito.mock(MovementBuilder.class);
        when(movementBuilder.build(dice)).thenReturn(movement);
        verificationResponseBuilder.gatewayCollectionMethodBuilder(gatewayCollectionMethodBuilder);
        verificationResponseBuilder.movementBuilders(Arrays.asList(movementBuilder));
        //When
        VerificationResponse verificationResponse = verificationResponseBuilder.build(dice);
        //Then
        assertEquals(verificationResponse.getGatewayCollectionMethod(), gatewayCollectionMethod);
        assertEquals(verificationResponse.getMovements().size(), 1);
        assertEquals(verificationResponse.getMovements().iterator().next(), movement);
    }

    @Test
    public void testBuildWithoutValues() {
        //Given
        verificationResponseBuilder.gatewayCollectionMethodBuilder(null);
        verificationResponseBuilder.movementBuilders(null);
        //When
        VerificationResponse verificationResponse = verificationResponseBuilder.build(dice);
        //Then
        assertNotNull(verificationResponse.getGatewayCollectionMethod());
        assertNotNull(verificationResponse.getMovements());
    }
}
