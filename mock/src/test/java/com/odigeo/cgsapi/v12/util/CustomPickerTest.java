package com.odigeo.cgsapi.v12.util;

import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class CustomPickerTest {

    @Test
    public void testPickStringWithOnlyLetters() {
        //Given
        CustomPicker customPicker = new CustomPicker(new Random());
        String aString = null;
        //When
        aString = customPicker.pickStringWithOnlyLetters(10);
        //Then
        assertNotNull(aString);
        assertEquals(aString.length(), 10);
    }

    @Test
    public void testPickStringWithLettersAndNumbers() {
        //Given
        CustomPicker customPicker = new CustomPicker(new Random());
        String aString = null;
        //When
        aString = customPicker.pickStringWithLettersAndNumbers(10);
        //Then
        assertNotNull(aString);
        assertEquals(aString.length(), 10);
    }

    @Test
    public void testPickStringWithOnlyNumbers() {
        //Given
        CustomPicker customPicker = new CustomPicker(new Random());
        String aString = null;
        //When
        aString = customPicker.pickStringWithOnlyNumbers(10);
        //Then
        assertNotNull(aString);
        assertEquals(aString.length(), 10);
    }

    @Test
    public void testPickEmail() {
        //Given
        CustomPicker customPicker = new CustomPicker(new Random());
        String aString = null;
        //When
        aString = customPicker.pickEmail();
        //Then
        assertNotNull(aString);
        assertEquals(aString.length(), 20);
        assertTrue(aString.contains("@"));
        assertTrue(aString.contains("."));
    }
}
