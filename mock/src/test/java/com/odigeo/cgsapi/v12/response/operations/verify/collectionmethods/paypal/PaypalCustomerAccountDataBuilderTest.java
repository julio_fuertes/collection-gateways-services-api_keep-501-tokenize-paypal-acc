package com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal;

import com.odigeo.cgsapi.v12.response.operations.query.CustomerDataBuilder;
import com.odigeo.cgsapi.v12.response.operations.query.PaypalCustomerAccountData;
import org.mockito.Mock;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PaypalCustomerAccountDataBuilderTest {


    @Mock
    private CustomerDataBuilder customerDataBuilder;
    @Mock
    private PaypalAccountStatus payerStatus;

    @Test
    public void testBuild() {
        //When
        //Given
        PaypalCustomerAccountDataBuilder builder = new PaypalCustomerAccountDataBuilder();
        builder.customerDataBuilder(customerDataBuilder).payerId("payerId").payerStatus(payerStatus).addressStatus("addressStatus");
        PaypalCustomerAccountData data = builder.build();
        //Then
        assertEquals(data.getCustomerData(), customerDataBuilder);
        assertEquals(data.getPayerId(), "payerId");
        assertEquals(data.getPayerStatus(), payerStatus);
        assertEquals(data.getAddressStatus(), "addressStatus");
    }
}
