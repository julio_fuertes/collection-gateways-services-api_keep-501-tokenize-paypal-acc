package com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal;

import org.mockito.Mock;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PaypalPaymentInfoBuilderTest {

    @Mock
    private PaypalProtectionEligibility protectionEligibility;

    @Test
    public void testBuild() {
        //When
        //Given
        PaypalPaymentInfoBuilder builder = new PaypalPaymentInfoBuilder();
        builder.paymentType("paymentType").protectionEligibility(protectionEligibility);
        PaypalPaymentInfo info = builder.build();
        //Then
        assertEquals(info.getPaymentType(), "paymentType");
        assertEquals(info.getProtectionEligibility(), protectionEligibility);
    }
}
