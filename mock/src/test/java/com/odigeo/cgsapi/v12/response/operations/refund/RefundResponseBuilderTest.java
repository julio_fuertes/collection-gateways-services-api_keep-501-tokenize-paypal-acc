package com.odigeo.cgsapi.v12.response.operations.refund;


import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodBuilder;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementBuilder;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Currency;
import java.util.Random;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.AssertJUnit.assertNotNull;

public class RefundResponseBuilderTest {

    private Random dice = new Random();
    private RefundResponseBuilder refundResponseBuilder;

    @BeforeMethod
    private void init() {
        MockitoAnnotations.initMocks(this);
        refundResponseBuilder = new RefundResponseBuilder();
    }

    @Test
    public void testBuildWithValues() {
        //Given
        GatewayCollectionMethod gatewayCollectionMethod = Mockito.mock(GatewayCollectionMethod.class);
        GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder = Mockito.mock(GatewayCollectionMethodBuilder.class);
        when(gatewayCollectionMethodBuilder.build(dice)).thenReturn(gatewayCollectionMethod);
        Movement movement = Mockito.mock(Movement.class);
        when(movement.getStatus()).thenReturn(MovementStatus.REFUNDED);
        MovementBuilder movementBuilder = Mockito.mock(MovementBuilder.class);
        when(movementBuilder.build(dice)).thenReturn(movement);
        refundResponseBuilder.gatewayCollectionMethodBuilder(gatewayCollectionMethodBuilder);
        refundResponseBuilder.movementBuilders(Arrays.asList(movementBuilder));
        //When
        RefundResponse refundResponse = refundResponseBuilder.build(dice);
        //Then
        assertEquals(refundResponse.getGatewayCollectionMethod(), gatewayCollectionMethod);
        assertEquals(refundResponse.getMovements().size(), 1);
        assertEquals(refundResponse.getMovements().iterator().next(), movement);
    }

    @Test
    public void testBuildWithoutValues() {
        //Given
        refundResponseBuilder.gatewayCollectionMethodBuilder(null);
        refundResponseBuilder.movementBuilders(null);
        //When
        RefundResponse refundResponse = refundResponseBuilder.build(dice);
        //Then
        assertNotNull(refundResponse.getGatewayCollectionMethod());
        assertNotNull(refundResponse.getMovements());
    }

    @Test
    public void testDeprecatedMethod() {
        refundResponseBuilder.collectionStatus(CollectionStatus.CANCELLED).collectionStatus(CollectionStatus.REFUNDED)
                .amount(1).amount(BigDecimal.ONE).currency("EUR").currency(Currency.getInstance("EUR"))
                .merchantOrderId("merchantOrderId").gatewayCollectionMethod(null).getMovementAction();

        assertEquals(refundResponseBuilder.getAmount().intValue(), 1);
        assertEquals(refundResponseBuilder.getCurrency().getCurrencyCode(), "EUR");
        assertEquals(refundResponseBuilder.getMovementAction(), MovementAction.COLLECT);
        assertEquals(refundResponseBuilder.getMovementStatus(), MovementStatus.REFUNDED);
        assertNull(refundResponseBuilder.getGatewayCollectionMethod());
        assertEquals(refundResponseBuilder.getMerchantOrderId(), "merchantOrderId");
        assertNotNull(refundResponseBuilder.build());
    }
}
