package com.odigeo.cgsapi.v12.response.operations.confirm;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodBuilder;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementBuilder;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Random;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class ConfirmationResponseBuilderTest {

    private Random dice = new Random();
    private ConfirmationResponseBuilder confirmationResponseBuilder;

    @BeforeMethod
    private void init() {
        MockitoAnnotations.initMocks(this);
        confirmationResponseBuilder = new ConfirmationResponseBuilder();
    }

    @Test
    public void testBuildWithValues() {
        //Given
        GatewayCollectionMethod gatewayCollectionMethod = Mockito.mock(GatewayCollectionMethod.class);
        GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder = Mockito.mock(GatewayCollectionMethodBuilder.class);
        when(gatewayCollectionMethodBuilder.build(dice)).thenReturn(gatewayCollectionMethod);
        Movement movement = Mockito.mock(Movement.class);
        when(movement.getStatus()).thenReturn(MovementStatus.PAID);
        MovementBuilder movementBuilder = Mockito.mock(MovementBuilder.class);
        when(movementBuilder.build(dice)).thenReturn(movement);
        confirmationResponseBuilder.gatewayCollectionMethodBuilder(gatewayCollectionMethodBuilder);
        confirmationResponseBuilder.movementBuilders(Arrays.asList(movementBuilder));
        //When
        ConfirmationResponse confirmationResponse = confirmationResponseBuilder.build(dice);
        //Then
        assertEquals(confirmationResponse.getGatewayCollectionMethod(), gatewayCollectionMethod);
        assertEquals(confirmationResponse.getMovements().size(), 1);
        assertEquals(confirmationResponse.getMovements().iterator().next(), movement);
    }

    @Test
    public void testBuildWithoutValues() {
        //Given
        confirmationResponseBuilder.gatewayCollectionMethodBuilder(null);
        confirmationResponseBuilder.movementBuilders(null);
        //When
        ConfirmationResponse confirmationResponse = confirmationResponseBuilder.build(dice);
        //Then
        assertNotNull(confirmationResponse.getGatewayCollectionMethod());
        assertNotNull(confirmationResponse.getMovements());
    }
}
