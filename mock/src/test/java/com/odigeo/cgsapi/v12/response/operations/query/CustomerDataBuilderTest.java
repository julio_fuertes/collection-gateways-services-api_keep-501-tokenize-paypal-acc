package com.odigeo.cgsapi.v12.response.operations.query;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CustomerDataBuilderTest {

    @Test
    public void testBuild() {
        //Given
        //When
        CustomerDataBuilder builder = new CustomerDataBuilder();
        builder.business("business").countryCode("countryCode").email("email").firstName("firstName").lastName("lastName").middleName("middleName").sufix("sufix");
        CustomerData data = builder.build();
        //Then
        assertEquals(data.getBusiness(), "business");
        assertEquals(data.getCountryCode(), "countryCode");
        assertEquals(data.getEmail(), "email");
        assertEquals(data.getFirstName(), "firstName");
        assertEquals(data.getLastName(), "lastName");
        assertEquals(data.getMiddleName(), "middleName");
        assertEquals(data.getSufix(), "sufix");

    }
}
