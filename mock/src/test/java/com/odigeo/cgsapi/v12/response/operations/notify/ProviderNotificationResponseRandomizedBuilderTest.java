package com.odigeo.cgsapi.v12.response.operations.notify;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Random;

public class ProviderNotificationResponseRandomizedBuilderTest {

    @Test
    public void testProvideRandomizedBuilder() {
        //Given
        Random aRandom = new Random();
        ProviderNotificationResponseRandomizedBuilder randomizedBuilder = new ProviderNotificationResponseRandomizedBuilder();
        //When
        ProviderNotificationResponseBuilder builder = randomizedBuilder.provideRandomizedBuilder(aRandom);
        //Then
        Assert.assertNotNull(builder.getNotificationId());
        Assert.assertNotNull(builder.getResult());
        Assert.assertNotNull(builder.getMessage());
    }

    @Test
    public void testProvideRandomizedBuilderWithData() {
        //Given
        Random aRandom = new Random();
        ProviderNotificationResponseRandomizedBuilder randomizedBuilder = new ProviderNotificationResponseRandomizedBuilder();
        ProviderNotificationResponseBuilder builder = new ProviderNotificationResponseBuilder();
        builder.notificationId("ID").result(Result.SUCCESS).message("MESSAGE");
        //When
        ProviderNotificationResponseBuilder actualBuilder = randomizedBuilder.provideRandomizedBuilder(builder, aRandom);
        //Then
        Assert.assertEquals(actualBuilder.getNotificationId(), "ID");
        Assert.assertEquals(actualBuilder.getResult(), Result.SUCCESS);
        Assert.assertEquals(actualBuilder.getMessage(), "MESSAGE");
    }
}
