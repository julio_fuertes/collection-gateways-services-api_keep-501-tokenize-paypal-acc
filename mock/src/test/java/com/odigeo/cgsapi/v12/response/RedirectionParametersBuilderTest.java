package com.odigeo.cgsapi.v12.response;


import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class RedirectionParametersBuilderTest {

    public static final String HTML = "html";
    public static final String JAVASCRIPT_SNIPPET = "javascriptSnippet";
    private final String url = "www.foo.com";

    @Test
    public void testBuildWithValues() {
        //Given
        RedirectionParametersBuilder redirectionParametersBuilder = new RedirectionParametersBuilder();
        redirectionParametersBuilder.redirectionType(RedirectionType.FORM_REDIRECTION);
        redirectionParametersBuilder.redirectionStep(1);
        redirectionParametersBuilder.redirectionUrl(url);
        redirectionParametersBuilder.redirectionMethod(RedirectionHttpMethod.POST);
        Map<String, String> map = new HashMap<String, String>();
        map.put("key1", "value1");
        redirectionParametersBuilder.parameters(map);
        //When
        RedirectionParameters redirectionParameters = redirectionParametersBuilder.build(new Random());
        //Then
        assertEquals(redirectionParameters.getRedirectionType(), RedirectionType.FORM_REDIRECTION);
        assertEquals(redirectionParameters.getRedirectionStep().intValue(), 1);
        assertEquals(redirectionParameters.getRedirectionUrl(), url);
        assertEquals(redirectionParameters.getRedirectionMethod(), RedirectionHttpMethod.POST);
        assertEquals(redirectionParameters.getParameters(), map);
        assertNull(redirectionParameters.getHtml());
        assertNull(redirectionParameters.getJavascriptSnippet());
    }

    @Test
    public void testBuildFormRedirectionFillRandom() {
        //Given
        RedirectionParametersBuilder redirectionParametersBuilder = new RedirectionParametersBuilder();
        redirectionParametersBuilder.redirectionType(RedirectionType.FORM_REDIRECTION);
        //When
        RedirectionParameters redirectionParameters = redirectionParametersBuilder.build(new Random());
        //Then
        assertNotNull(redirectionParameters.getRedirectionStep());
        assertNotNull(redirectionParameters.getRedirectionUrl());
        assertNotNull(redirectionParameters.getRedirectionMethod());
        assertNotNull(redirectionParameters.getParameters());
        assertNull(redirectionParameters.getHtml());
        assertNull(redirectionParameters.getJavascriptSnippet());
    }

    @Test
    public void testBuildHtmlFillRandom() {
        //Given
        RedirectionParametersBuilder redirectionParametersBuilder = new RedirectionParametersBuilder();
        redirectionParametersBuilder.redirectionType(RedirectionType.HTML);
        redirectionParametersBuilder.html(HTML);
        //When
        RedirectionParameters redirectionParameters = redirectionParametersBuilder.build(new Random());
        //Then
        assertNotNull(redirectionParameters.getRedirectionStep());
        assertNull(redirectionParameters.getRedirectionUrl());
        assertNull(redirectionParameters.getRedirectionMethod());
        assertNull(redirectionParameters.getParameters());
        assertEquals(redirectionParameters.getHtml(), HTML);
        assertNull(redirectionParameters.getJavascriptSnippet());
    }

    @Test
    public void testBuildJavascriptFillRandom() {
        //Given
        RedirectionParametersBuilder redirectionParametersBuilder = new RedirectionParametersBuilder();
        redirectionParametersBuilder.redirectionType(RedirectionType.JAVASCRIPT);
        redirectionParametersBuilder.javascriptSnippet(JAVASCRIPT_SNIPPET);
        //When
        RedirectionParameters redirectionParameters = redirectionParametersBuilder.build(new Random());
        //Then
        assertNotNull(redirectionParameters.getRedirectionStep());
        assertNull(redirectionParameters.getRedirectionUrl());
        assertNull(redirectionParameters.getRedirectionMethod());
        assertNull(redirectionParameters.getParameters());
        assertNull(redirectionParameters.getHtml());
        assertEquals(redirectionParameters.getJavascriptSnippet(), JAVASCRIPT_SNIPPET);
    }

    @Test
    public void testBuildJavascriptFillEmpty() {
        //Given
        RedirectionParametersBuilder redirectionParametersBuilder = new RedirectionParametersBuilder();
        redirectionParametersBuilder.redirectionType(RedirectionType.JAVASCRIPT);
        //When
        RedirectionParameters redirectionParameters = redirectionParametersBuilder.build(new Random());
        //Then
        assertNotNull(redirectionParameters.getRedirectionStep());
        assertNull(redirectionParameters.getRedirectionUrl());
        assertNull(redirectionParameters.getRedirectionMethod());
        assertNull(redirectionParameters.getParameters());
        assertNull(redirectionParameters.getHtml());
        assertEquals(redirectionParameters.getJavascriptSnippet(), "function sayHi() { alert( 'Hello, world!' ); }");
    }

    @Test
    public void testBuildGoogleFillRandom() {
        //Given
        RedirectionParametersBuilder redirectionParametersBuilder = new RedirectionParametersBuilder();
        redirectionParametersBuilder.redirectionType(RedirectionType.GOOGLE_PAY);
        //When
        RedirectionParameters redirectionParameters = redirectionParametersBuilder.build(new Random());
        //Then
        assertNotNull(redirectionParameters.getRedirectionStep());
        assertNull(redirectionParameters.getRedirectionUrl());
        assertNull(redirectionParameters.getRedirectionMethod());
        assertNotNull(redirectionParameters.getParameters());
        assertNull(redirectionParameters.getHtml());
        assertNull(redirectionParameters.getJavascriptSnippet());
    }

    @Test
    public void testBuildAppleFillRandom() {
        //Given
        RedirectionParametersBuilder redirectionParametersBuilder = new RedirectionParametersBuilder();
        redirectionParametersBuilder.redirectionType(RedirectionType.APPLE_PAY);
        //When
        RedirectionParameters redirectionParameters = redirectionParametersBuilder.build(new Random());
        //Then
        assertNotNull(redirectionParameters.getRedirectionStep());
        assertNull(redirectionParameters.getRedirectionUrl());
        assertNull(redirectionParameters.getRedirectionMethod());
        assertNotNull(redirectionParameters.getParameters());
        assertNull(redirectionParameters.getHtml());
        assertNull(redirectionParameters.getJavascriptSnippet());
    }

}
