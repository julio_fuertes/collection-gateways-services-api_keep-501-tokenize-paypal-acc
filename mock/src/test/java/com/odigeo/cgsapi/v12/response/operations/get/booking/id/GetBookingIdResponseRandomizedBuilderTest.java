package com.odigeo.cgsapi.v12.response.operations.get.booking.id;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Random;

public class GetBookingIdResponseRandomizedBuilderTest {

    @Test
    public void testProvideRandomizedBuilder() {
        //Given
        Random aRandom = new Random();
        GetBookingIdResponseRandomizedBuilder randomizedBuilder = new GetBookingIdResponseRandomizedBuilder();
        //When
        GetBookingIdResponseBuilder builder = randomizedBuilder.provideRandomizedBuilder(aRandom);
        //Then
        Assert.assertNotNull(builder.getBookingId());
    }

    @Test
    public void testProvideRandomizedBuilderWithData() {
        //Given
        Random aRandom = new Random();
        GetBookingIdResponseRandomizedBuilder randomizedBuilder = new GetBookingIdResponseRandomizedBuilder();
        GetBookingIdResponseBuilder builder = new GetBookingIdResponseBuilder();
        builder.bookingId("bookingId");
        //When
        GetBookingIdResponseBuilder actualBuilder = randomizedBuilder.provideRandomizedBuilder(builder, aRandom);
        //Then
        Assert.assertEquals(actualBuilder.getBookingId(), "bookingId");
    }
}
