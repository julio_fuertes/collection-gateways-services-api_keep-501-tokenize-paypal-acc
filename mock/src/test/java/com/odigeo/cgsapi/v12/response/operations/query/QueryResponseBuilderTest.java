package com.odigeo.cgsapi.v12.response.operations.query;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodBuilder;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementBuilder;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Random;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class QueryResponseBuilderTest {

    private Random dice = new Random();
    private QueryResponseBuilder queryResponseBuilder;

    @BeforeMethod
    private void init() {
        MockitoAnnotations.initMocks(this);
        queryResponseBuilder = new QueryResponseBuilder();
    }

    @Test
    public void testBuildWithValues() {
        //Given
        GatewayCollectionMethod gatewayCollectionMethod = Mockito.mock(GatewayCollectionMethod.class);
        GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder = Mockito.mock(GatewayCollectionMethodBuilder.class);
        when(gatewayCollectionMethodBuilder.build(dice)).thenReturn(gatewayCollectionMethod);
        Movement movement = Mockito.mock(Movement.class);
        when(movement.getStatus()).thenReturn(MovementStatus.AUTHORIZED);
        MovementBuilder movementBuilder = Mockito.mock(MovementBuilder.class);
        when(movementBuilder.build(dice)).thenReturn(movement);
        queryResponseBuilder.gatewayCollectionMethodBuilder(gatewayCollectionMethodBuilder);
        queryResponseBuilder.movementBuilders(Arrays.asList(movementBuilder));
        //When
        QueryResponse queryResponse = queryResponseBuilder.build(dice);
        //Then
        assertEquals(queryResponse.getGatewayCollectionMethod(), gatewayCollectionMethod);
        assertEquals(queryResponse.getMovements().size(), 1);
        assertEquals(queryResponse.getMovements().iterator().next(), movement);
    }

    @Test
    public void testBuildWithoutValues() {
        //Given
        queryResponseBuilder.gatewayCollectionMethodBuilder(null);
        queryResponseBuilder.movementBuilders(null);
        //When
        QueryResponse queryResponse = queryResponseBuilder.build(dice);
        //Then
        assertNotNull(queryResponse.getGatewayCollectionMethod());
        assertNotNull(queryResponse.getMovements());
    }
}
