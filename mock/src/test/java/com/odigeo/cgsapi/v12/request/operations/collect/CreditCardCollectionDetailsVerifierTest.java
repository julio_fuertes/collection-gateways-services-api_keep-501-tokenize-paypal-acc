package com.odigeo.cgsapi.v12.request.operations.collect;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.creditcard.CreditCardCollectionDetails;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;

/**
 * Created by marcal.perapoch on 30/07/2015.
 */
public class CreditCardCollectionDetailsVerifierTest {

    private static final String ORDER_ID = "12356789";
    private static final Money MONEY = Money.createMoney(new BigDecimal(5), Currency.getInstance("EUR"));

    @Mock
    GatewayCollectionMethod gatewayCollectionMethod;
    @Mock
    CreditCard creditCard;
    @Mock
    CreditCardCollectionDetails creditCardCollectionDetails;
    @Mock
    GatewayCollectionMethod otherGatewayCollectionMethod;
    @Mock
    CreditCard otherCreditCard;

    private CreditCardCollectionDetailsVerifierBuilder creditCardCollectionDetailsVerifierBuilder;


    private CreditCardCollectionDetailsVerifier creditCardCollectionDetailsVerifier;

    @BeforeTest
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(gatewayCollectionMethod.getGatewayCollectionMethodType()).thenReturn(GatewayCollectionMethodType.CREDITCARD);
        when(creditCard.getNumber()).thenReturn("411111111111");
        creditCardCollectionDetailsVerifierBuilder = new CreditCardCollectionDetailsVerifierBuilder();
        creditCardCollectionDetailsVerifierBuilder.creditCard(creditCard)
                .gatewayCollectionMethod(gatewayCollectionMethod)
                .orderId(ORDER_ID)
                .simpleMoney(MONEY);
    }

    private void populateCreditCardCollectionDetails() {
        when(creditCardCollectionDetails.getGatewayCollectionMethod()).thenReturn(gatewayCollectionMethod);
        when(creditCardCollectionDetails.getOrderId()).thenReturn(ORDER_ID);
        when(creditCardCollectionDetails.getMoney()).thenReturn(MONEY);
        when(creditCardCollectionDetails.getCreditCard()).thenReturn(creditCard);
    }

    @Test
    public void testVerifyWithSameData() {
        creditCardCollectionDetailsVerifier = new CreditCardCollectionDetailsVerifier(creditCardCollectionDetailsVerifierBuilder);
        populateCreditCardCollectionDetails();
        creditCardCollectionDetailsVerifier.verify(creditCardCollectionDetails);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testVerifyWithDistinctMoney() {
        creditCardCollectionDetailsVerifier = new CreditCardCollectionDetailsVerifier(creditCardCollectionDetailsVerifierBuilder);
        populateCreditCardCollectionDetails();
        when(creditCardCollectionDetails.getMoney()).thenReturn(Money.createMoney(new BigDecimal(6), Currency.getInstance("EUR")));
        creditCardCollectionDetailsVerifier.verify(creditCardCollectionDetails);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testVerifyWithDistinctOrderId() {
        creditCardCollectionDetailsVerifier = new CreditCardCollectionDetailsVerifier(creditCardCollectionDetailsVerifierBuilder);
        populateCreditCardCollectionDetails();
        when(creditCardCollectionDetails.getOrderId()).thenReturn("987654321");
        creditCardCollectionDetailsVerifier.verify(creditCardCollectionDetails);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testVerifyWithDistinctGatewayCollectionMethod() {
        when(otherGatewayCollectionMethod.getGatewayCollectionMethodType()).thenReturn(GatewayCollectionMethodType.SECURE3D);
        creditCardCollectionDetailsVerifier = new CreditCardCollectionDetailsVerifier(creditCardCollectionDetailsVerifierBuilder);
        populateCreditCardCollectionDetails();
        when(creditCardCollectionDetails.getGatewayCollectionMethod()).thenReturn(otherGatewayCollectionMethod);
        creditCardCollectionDetailsVerifier.verify(creditCardCollectionDetails);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testVerifyWithDistinctCreditCard() {
        creditCardCollectionDetailsVerifier = new CreditCardCollectionDetailsVerifier(creditCardCollectionDetailsVerifierBuilder);
        when(otherCreditCard.getNumber()).thenReturn("000000000000");
        populateCreditCardCollectionDetails();
        when(creditCardCollectionDetails.getCreditCard()).thenReturn(otherCreditCard);
        creditCardCollectionDetailsVerifier.verify(creditCardCollectionDetails);
    }

    @Test
    public void testVerifyWithNoBuilderParameters() {
        creditCardCollectionDetailsVerifierBuilder = new CreditCardCollectionDetailsVerifierBuilder();
        creditCardCollectionDetailsVerifierBuilder.creditCard(null)
                .gatewayCollectionMethod(null)
                .orderId(null)
                .simpleMoney(null);
        creditCardCollectionDetailsVerifier = creditCardCollectionDetailsVerifierBuilder.build();

        populateCreditCardCollectionDetails();

        creditCardCollectionDetailsVerifier.verify(creditCardCollectionDetails);
    }


    @Test
    public void testVerifyWithOnlyCreditCardSet() {
        creditCardCollectionDetailsVerifierBuilder = new CreditCardCollectionDetailsVerifierBuilder();
        creditCardCollectionDetailsVerifierBuilder.creditCard(creditCard)
                .gatewayCollectionMethod(null)
                .orderId(null)
                .simpleMoney(null);
        creditCardCollectionDetailsVerifier = creditCardCollectionDetailsVerifierBuilder.build();

        populateCreditCardCollectionDetails();

        creditCardCollectionDetailsVerifier.verify(creditCardCollectionDetails);
    }
}
