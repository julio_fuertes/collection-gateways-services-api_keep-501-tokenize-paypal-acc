package com.odigeo.cgsapi.v12.request.operations.refund;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.creditcard.CreditCardRefundDetails;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;

/**
 * Created by marcal.perapoch on 30/07/2015.
 */
public class CreditCardRefundDetailsVerifierTest {

    private static final String A_MERCHANT_ORDER_ID = "12356789";
    private static final String TRANSACTION_ID = "12356789.1";
    private static final Money MONEY = Money.createMoney(new BigDecimal(5), Currency.getInstance("EUR"));

    @Mock
    GatewayCollectionMethod gatewayCollectionMethod;
    @Mock
    CreditCard creditCard;
    @Mock
    CreditCardRefundDetails creditCardRefundDetails;
    @Mock
    GatewayCollectionMethod otherGatewayCollectionMethod;
    @Mock
    CreditCard otherCreditCard;

    private CreditCardRefundDetailsVerifierBuilder creditCardRefundDetailsVerifierBuilder;

    private CreditCardRefundDetailsVerifier creditCardRefundDetailsVerifier;

    @BeforeTest
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(gatewayCollectionMethod.getGatewayCollectionMethodType()).thenReturn(GatewayCollectionMethodType.CREDITCARD);
        when(creditCard.getNumber()).thenReturn("411111111111");
        creditCardRefundDetailsVerifierBuilder = new CreditCardRefundDetailsVerifierBuilder();
        creditCardRefundDetailsVerifierBuilder.creditCard(creditCard)
                .gatewayCollectionMethod(gatewayCollectionMethod)
                .merchantOrderId(A_MERCHANT_ORDER_ID)
                .money(MONEY)
                .transactionId(TRANSACTION_ID);
    }

    private void populateCreditCardRefundDetails() {
        when(creditCardRefundDetails.getGatewayCollectionMethod()).thenReturn(gatewayCollectionMethod);
        when(creditCardRefundDetails.getMerchantOrderId()).thenReturn(A_MERCHANT_ORDER_ID);
        when(creditCardRefundDetails.getMoney()).thenReturn(MONEY);
        when(creditCardRefundDetails.getCreditCard()).thenReturn(creditCard);
        when(creditCardRefundDetails.getTransactionId()).thenReturn(TRANSACTION_ID);
    }

    @Test
    public void testVerifyWithSameData() {
        creditCardRefundDetailsVerifier = new CreditCardRefundDetailsVerifier(creditCardRefundDetailsVerifierBuilder);
        populateCreditCardRefundDetails();
        creditCardRefundDetailsVerifier.verify(creditCardRefundDetails);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testVerifyWithDistinctMoney() {
        creditCardRefundDetailsVerifier = new CreditCardRefundDetailsVerifier(creditCardRefundDetailsVerifierBuilder);
        populateCreditCardRefundDetails();
        when(creditCardRefundDetails.getMoney()).thenReturn(Money.createMoney(new BigDecimal(6), Currency.getInstance("EUR")));
        creditCardRefundDetailsVerifier.verify(creditCardRefundDetails);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testVerifyWithDistinctOrderId() {
        creditCardRefundDetailsVerifier = new CreditCardRefundDetailsVerifier(creditCardRefundDetailsVerifierBuilder);
        populateCreditCardRefundDetails();
        when(creditCardRefundDetails.getMerchantOrderId()).thenReturn("987654321");
        creditCardRefundDetailsVerifier.verify(creditCardRefundDetails);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testVerifyWithDistinctGatewayCollectionMethod() {
        when(otherGatewayCollectionMethod.getGatewayCollectionMethodType()).thenReturn(GatewayCollectionMethodType.SECURE3D);
        creditCardRefundDetailsVerifier = new CreditCardRefundDetailsVerifier(creditCardRefundDetailsVerifierBuilder);
        populateCreditCardRefundDetails();
        when(creditCardRefundDetails.getGatewayCollectionMethod()).thenReturn(otherGatewayCollectionMethod);
        creditCardRefundDetailsVerifier.verify(creditCardRefundDetails);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testVerifyWithDistinctCreditCard() {
        creditCardRefundDetailsVerifier = new CreditCardRefundDetailsVerifier(creditCardRefundDetailsVerifierBuilder);
        when(otherCreditCard.getNumber()).thenReturn("000000000000");
        populateCreditCardRefundDetails();
        when(creditCardRefundDetails.getCreditCard()).thenReturn(otherCreditCard);
        creditCardRefundDetailsVerifier.verify(creditCardRefundDetails);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testVerifyWithDistinctMerchantId() {
        creditCardRefundDetailsVerifier = new CreditCardRefundDetailsVerifier(creditCardRefundDetailsVerifierBuilder);
        populateCreditCardRefundDetails();
        when(creditCardRefundDetails.getMerchantOrderId()).thenReturn("000");
        creditCardRefundDetailsVerifier.verify(creditCardRefundDetails);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testVerifyWithDistinctTransactionId() {
        creditCardRefundDetailsVerifier = new CreditCardRefundDetailsVerifier(creditCardRefundDetailsVerifierBuilder);
        populateCreditCardRefundDetails();
        when(creditCardRefundDetails.getTransactionId()).thenReturn("000");
        creditCardRefundDetailsVerifier.verify(creditCardRefundDetails);
    }

    @Test
    public void testVerifyWithNoBuilderParameters() {
        creditCardRefundDetailsVerifierBuilder = new CreditCardRefundDetailsVerifierBuilder();
        creditCardRefundDetailsVerifierBuilder.creditCard(null)
                .gatewayCollectionMethod(null)
                .merchantOrderId(null)
                .money(null)
                .transactionId(null);
        creditCardRefundDetailsVerifier = creditCardRefundDetailsVerifierBuilder.build();
        populateCreditCardRefundDetails();
        creditCardRefundDetailsVerifier.verify(creditCardRefundDetails);
    }

    @Test
    public void testVerifyWithOnlyCreditCardSet() {
        creditCardRefundDetailsVerifierBuilder = new CreditCardRefundDetailsVerifierBuilder();
        creditCardRefundDetailsVerifierBuilder.creditCard(creditCard)
                .gatewayCollectionMethod(null)
                .merchantOrderId(null)
                .money(null)
                .transactionId(null);
        creditCardRefundDetailsVerifier = creditCardRefundDetailsVerifierBuilder.build();
        populateCreditCardRefundDetails();
        creditCardRefundDetailsVerifier.verify(creditCardRefundDetails);
    }
}
