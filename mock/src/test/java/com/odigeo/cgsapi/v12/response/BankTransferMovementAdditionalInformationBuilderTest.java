package com.odigeo.cgsapi.v12.response;

import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class BankTransferMovementAdditionalInformationBuilderTest {

    @Test
    public void testBuildWithValues() {
        //Given
        BankTransferMovementAdditionalInformationBuilder bankTransferMovementAdditionalInformationBuilder = new BankTransferMovementAdditionalInformationBuilder();
        bankTransferMovementAdditionalInformationBuilder.paymentReference("String1");
        bankTransferMovementAdditionalInformationBuilder.accountHolder("String2");
        bankTransferMovementAdditionalInformationBuilder.bankName("String3");
        bankTransferMovementAdditionalInformationBuilder.city("String4");
        bankTransferMovementAdditionalInformationBuilder.swiftCode("String5");
        bankTransferMovementAdditionalInformationBuilder.bankAccountNumber("String6");
        bankTransferMovementAdditionalInformationBuilder.iban("String7");
        bankTransferMovementAdditionalInformationBuilder.countryDescription("String8");
        bankTransferMovementAdditionalInformationBuilder.type("String9");
        //When
        BankTransferMovementAdditionalInformation bankTransferMovementAdditionalInformation = (BankTransferMovementAdditionalInformation) bankTransferMovementAdditionalInformationBuilder.build(new Random());
        //Then
        assertEquals(bankTransferMovementAdditionalInformation.getPaymentReference(), "String1");
        assertEquals(bankTransferMovementAdditionalInformation.getAccountHolder(), "String2");
        assertEquals(bankTransferMovementAdditionalInformation.getBankName(), "String3");
        assertEquals(bankTransferMovementAdditionalInformation.getCity(), "String4");
        assertEquals(bankTransferMovementAdditionalInformation.getSwiftCode(), "String5");
        assertEquals(bankTransferMovementAdditionalInformation.getBankAccountNumber(), "String6");
        assertEquals(bankTransferMovementAdditionalInformation.getIban(), "String7");
        assertEquals(bankTransferMovementAdditionalInformation.getCountryDescription(), "String8");
        assertEquals(bankTransferMovementAdditionalInformation.getType(), "String9");
    }

    @Test
    public void testBuildWithoutValues() {
        //Given
        BankTransferMovementAdditionalInformationBuilder bankTransferMovementAdditionalInformationBuilder = new BankTransferMovementAdditionalInformationBuilder();
        bankTransferMovementAdditionalInformationBuilder.paymentReference(null);
        bankTransferMovementAdditionalInformationBuilder.accountHolder(null);
        bankTransferMovementAdditionalInformationBuilder.bankName(null);
        bankTransferMovementAdditionalInformationBuilder.city(null);
        bankTransferMovementAdditionalInformationBuilder.swiftCode(null);
        bankTransferMovementAdditionalInformationBuilder.bankAccountNumber(null);
        bankTransferMovementAdditionalInformationBuilder.iban(null);
        bankTransferMovementAdditionalInformationBuilder.countryDescription(null);
        bankTransferMovementAdditionalInformationBuilder.type(null);
        //When
        BankTransferMovementAdditionalInformation bankTransferMovementAdditionalInformation = (BankTransferMovementAdditionalInformation) bankTransferMovementAdditionalInformationBuilder.build(new Random());
        //Then
        assertNotNull(bankTransferMovementAdditionalInformation.getPaymentReference());
        assertNotNull(bankTransferMovementAdditionalInformation.getAccountHolder());
        assertNotNull(bankTransferMovementAdditionalInformation.getBankName());
        assertNotNull(bankTransferMovementAdditionalInformation.getCity());
        assertNotNull(bankTransferMovementAdditionalInformation.getSwiftCode());
        assertNotNull(bankTransferMovementAdditionalInformation.getBankAccountNumber());
        assertNotNull(bankTransferMovementAdditionalInformation.getIban());
        assertNotNull(bankTransferMovementAdditionalInformation.getCountryDescription());
        assertNotNull(bankTransferMovementAdditionalInformation.getType());
    }
}
