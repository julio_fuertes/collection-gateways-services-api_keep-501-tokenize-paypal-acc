package com.odigeo.cgsapi.v12.request.operations.get.booking.id;

import org.testng.annotations.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.testng.Assert.assertTrue;

public class GetBookingIdRequestVerifierTest {

    @Test
    public void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        Constructor<GetBookingIdRequest> constructor = GetBookingIdRequest.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    public void testVerify() {
        //Given
        GetBookingIdRequestBuilder builder = new GetBookingIdRequestBuilder();
        builder.merchantOrderId("merchantOrderId");
        GetBookingIdRequest getBookingIdRequest = builder.build();

        GetBookingIdRequestVerifier verifier = new GetBookingIdRequestVerifier();
        verifier.setMerchantOrderId("merchantOrderId");
        //When-Then
        verifier.verify(getBookingIdRequest);
    }
}
