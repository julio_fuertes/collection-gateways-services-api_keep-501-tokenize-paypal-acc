package com.odigeo.cgsapi.v12.response.operations.cancel;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodBuilder;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementBuilder;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Random;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class CancelResponseBuilderTest {

    private Random dice = new Random();
    private CancelResponseBuilder cancelResponseBuilder;

    @BeforeMethod
    private void init() {
        MockitoAnnotations.initMocks(this);
        cancelResponseBuilder = new CancelResponseBuilder();
    }

    @Test
    public void testBuildWithValues() {
        //Given
        GatewayCollectionMethod gatewayCollectionMethod = Mockito.mock(GatewayCollectionMethod.class);
        GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder = Mockito.mock(GatewayCollectionMethodBuilder.class);
        when(gatewayCollectionMethodBuilder.build(dice)).thenReturn(gatewayCollectionMethod);
        Movement movement = Mockito.mock(Movement.class);
        when(movement.getStatus()).thenReturn(MovementStatus.CANCEL);
        MovementBuilder movementBuilder = Mockito.mock(MovementBuilder.class);
        when(movementBuilder.build(dice)).thenReturn(movement);
        cancelResponseBuilder.gatewayCollectionMethodBuilder(gatewayCollectionMethodBuilder);
        cancelResponseBuilder.movementBuilders(Arrays.asList(movementBuilder));
        //When
        CancelResponse cancelResponse = cancelResponseBuilder.build(dice);
        //Then
        assertEquals(cancelResponse.getGatewayCollectionMethod(), gatewayCollectionMethod);
        assertEquals(cancelResponse.getMovements().size(), 1);
        assertEquals(cancelResponse.getMovements().iterator().next(), movement);
    }

    @Test
    public void testBuildWithoutValues() {
        //Given
        cancelResponseBuilder.gatewayCollectionMethodBuilder(null);
        cancelResponseBuilder.movementBuilders(null);
        //When
        CancelResponse cancelResponse = cancelResponseBuilder.build(dice);
        //Then
        assertNotNull(cancelResponse.getGatewayCollectionMethod());
        assertNotNull(cancelResponse.getMovements());
    }
}
