package com.odigeo.cgsapi.v12.response.operations.authorize;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodBuilder;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementBuilder;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.RedirectionParameters;
import com.odigeo.cgsapi.v12.response.RedirectionParametersBuilder;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Random;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class AuthorizationResponseBuilderTest {

    private Random dice = new Random();
    private AuthorizationResponseBuilder authorizationResponseBuilder;

    @BeforeMethod
    private void init() {
        MockitoAnnotations.initMocks(this);
        authorizationResponseBuilder = new AuthorizationResponseBuilder();
    }

    @Test
    public void testBuildWithValues() {
        //Given
        GatewayCollectionMethod gatewayCollectionMethod = Mockito.mock(GatewayCollectionMethod.class);
        GatewayCollectionMethodBuilder gatewayCollectionMethodBuilder = Mockito.mock(GatewayCollectionMethodBuilder.class);
        when(gatewayCollectionMethodBuilder.build(dice)).thenReturn(gatewayCollectionMethod);
        RedirectionParameters redirectionParameters = Mockito.mock(RedirectionParameters.class);
        RedirectionParametersBuilder redirectionParametersBuilder = Mockito.mock(RedirectionParametersBuilder.class);
        when(redirectionParametersBuilder.build(dice)).thenReturn(redirectionParameters);
        Movement movement = Mockito.mock(Movement.class);
        when(movement.getStatus()).thenReturn(MovementStatus.AUTHORIZED);
        MovementBuilder movementBuilder = Mockito.mock(MovementBuilder.class);
        when(movementBuilder.build(dice)).thenReturn(movement);
        authorizationResponseBuilder.redirectionParametersBuilder(redirectionParametersBuilder);
        authorizationResponseBuilder.gatewayCollectionMethodBuilder(gatewayCollectionMethodBuilder);
        authorizationResponseBuilder.movementBuilders(Arrays.asList(movementBuilder));
        //When
        AuthorizationResponse authorizationResponse = authorizationResponseBuilder.build(dice);
        //Then
        assertEquals(authorizationResponse.getRedirectionParameters(), redirectionParameters);
        assertEquals(authorizationResponse.getGatewayCollectionMethod(), gatewayCollectionMethod);
        assertEquals(authorizationResponse.getMovements().size(), 1);
        assertEquals(authorizationResponse.getMovements().iterator().next(), movement);
    }

    @Test
    public void testBuildWithoutValues() {
        //Given
        authorizationResponseBuilder.redirectionParametersBuilder(null);
        authorizationResponseBuilder.gatewayCollectionMethodBuilder(null);
        authorizationResponseBuilder.movementBuilders(null);
        //When
        AuthorizationResponse authorizationResponse = authorizationResponseBuilder.build(dice);
        //Then
        assertNotNull(authorizationResponse.getRedirectionParameters());
        assertNotNull(authorizationResponse.getGatewayCollectionMethod());
        assertNotNull(authorizationResponse.getMovements());
    }
}
