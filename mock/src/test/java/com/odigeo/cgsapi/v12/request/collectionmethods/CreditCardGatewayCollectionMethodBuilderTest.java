package com.odigeo.cgsapi.v12.request.collectionmethods;

import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class CreditCardGatewayCollectionMethodBuilderTest {

    @Test
    public void testBuildWithValues() {
        //Given
        CreditCardGatewayCollectionMethodBuilder creditCardGatewayCollectionMethodBuilder = new CreditCardGatewayCollectionMethodBuilder();
        creditCardGatewayCollectionMethodBuilder.type(CreditCardType.VISA_CREDIT);
        //When
        CreditCardGatewayCollectionMethod creditCardGatewayCollectionMethod = creditCardGatewayCollectionMethodBuilder.build(new Random());
        //Then
        assertEquals(creditCardGatewayCollectionMethod.getType(), CreditCardType.VISA_CREDIT);
    }

    @Test
    public void testBuildWithoutValues() {
        ///Given
        CreditCardGatewayCollectionMethodBuilder creditCardGatewayCollectionMethodBuilder = new CreditCardGatewayCollectionMethodBuilder();
        creditCardGatewayCollectionMethodBuilder.type(null);
        //When
        CreditCardGatewayCollectionMethod creditCardGatewayCollectionMethod = creditCardGatewayCollectionMethodBuilder.build(new Random());
        //Then
        assertNotNull(creditCardGatewayCollectionMethod.getType());
    }
}
