package com.odigeo.cgsapi.v12.request.operations.query;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import org.testng.annotations.Test;

import java.util.Currency;

import static org.testng.Assert.assertEquals;

/**
 * {@link QueryDetails} unit tests
 *
 * @author egonz
 */
public class QueryDetailsTest {

    private static final String AN_ORDER_ID = "123";
    private static final Currency A_CURRENCY = Currency.getInstance("EUR");
    private static final String A_MERCHANT_ORDER_ID = "merchantOrderId";

    @Test
    public void queryDetailsCreation() {
        QueryDetails cancelDetails = new QueryDetails(AN_ORDER_ID, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ALIPAY), A_CURRENCY, A_MERCHANT_ORDER_ID, CollectionActionType.COLLECT);
        assertEquals(cancelDetails.getOrderId(), AN_ORDER_ID);
        assertEquals(cancelDetails.getGatewayCollectionMethod().getGatewayCollectionMethodType(), GatewayCollectionMethodType.ALIPAY);
        assertEquals(cancelDetails.getCurrency(), A_CURRENCY);
        assertEquals(cancelDetails.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(cancelDetails.getCollectionActionType(), CollectionActionType.COLLECT);
    }
}
