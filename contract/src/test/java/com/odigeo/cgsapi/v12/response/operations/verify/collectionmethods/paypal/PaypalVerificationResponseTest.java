package com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;
import com.odigeo.cgsapi.v12.response.operations.query.CustomerData;
import com.odigeo.cgsapi.v12.response.operations.query.PaypalCustomerAccountData;
import com.odigeo.cgsapi.v12.response.operations.query.PaypalCustomerAccountDataBuilder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class PaypalVerificationResponseTest {

    private static final PaypalCustomerAccountData PAYPAL_CUSTOMER_ACCOUNT_DATA = PaypalCustomerAccountDataBuilder.aPaypalCustomerAccountData().build();

    @Mock
    private Movement movement;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void verificationResponseMovementTest() {
        when(movement.getAmount()).thenReturn(new BigDecimal("85.23"));
        when(movement.getCurrency()).thenReturn(Currency.getInstance("EUR"));
        when(movement.getStatus()).thenReturn(MovementStatus.DECLINED);
        List<Movement> movements = new ArrayList<Movement>();
        movements.add(movement);
        PaypalVerificationResponse.PaypalBuilder builder = new PaypalVerificationResponse.PaypalBuilder().addMovements(movements).paypalCustomerAccountData(PAYPAL_CUSTOMER_ACCOUNT_DATA);
        PaypalVerificationResponse paypalVerificationResponse = builder.build();
        paypalVerificationResponse.setGatewayCollectionMethod(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));

        assertEquals(paypalVerificationResponse.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
        assertEquals(paypalVerificationResponse.getAmount(), new BigDecimal("85.23"));
        assertEquals(paypalVerificationResponse.getCollectionStatus(), CollectionStatus.COLLECTION_DECLINED);
        assertTrue(paypalVerificationResponse.getMovements().contains(movement));
        assertEquals(paypalVerificationResponse.getPaypalCustomerAccountData(), PAYPAL_CUSTOMER_ACCOUNT_DATA);

        builder.collectionStatus(CollectionStatus.COLLECTION_ERROR);
        paypalVerificationResponse = builder.build();
        assertEquals(paypalVerificationResponse.getCollectionStatus(), CollectionStatus.COLLECTION_ERROR);
    }

    @Test
    public void verificationResponseTest() {
        //Given
        PaypalCustomerAccountData.Builder paypalCustomerAccountDataBuilder = new PaypalCustomerAccountData.Builder();
        paypalCustomerAccountDataBuilder.payerId("a payerId");
        paypalCustomerAccountDataBuilder.payerStatus(PaypalAccountStatus.VERIFIED);
        paypalCustomerAccountDataBuilder.addressStatus("an address status");
        CustomerData.Builder customerDataBuilder = new CustomerData.Builder();
        CustomerData customerData = customerDataBuilder.build();
        paypalCustomerAccountDataBuilder.customerData(customerData);

        PaypalPaymentInfo.Builder paypalPaymentInfoBuilder = new PaypalPaymentInfo.Builder();
        paypalPaymentInfoBuilder.paymentType("a payment type");
        paypalPaymentInfoBuilder.protectionEligibility(PaypalProtectionEligibility.ELIGIBLE);

        PaypalVerificationResponse.PaypalBuilder builder = new PaypalVerificationResponse.PaypalBuilder();
        builder.paypalCustomerAccountData(paypalCustomerAccountDataBuilder.build());
        builder.paypalPaymentInfo(paypalPaymentInfoBuilder.build());
        //When
        PaypalVerificationResponse paypalVerificationResponse = builder.build();
        //Then
        Assert.assertSame(paypalVerificationResponse.getPaypalCustomerAccountData().getCustomerData(), customerData);
        Assert.assertEquals(paypalVerificationResponse.getPaypalCustomerAccountData().getPayerId(), "a payerId");
        Assert.assertEquals(paypalVerificationResponse.getPaypalCustomerAccountData().getPayerStatus(), PaypalAccountStatus.VERIFIED);
        Assert.assertEquals(paypalVerificationResponse.getPaypalCustomerAccountData().getAddressStatus(), "an address status");
        Assert.assertEquals(paypalVerificationResponse.getPaypalPaymentInfo().getProtectionEligibility(), PaypalProtectionEligibility.ELIGIBLE);
        Assert.assertEquals(paypalVerificationResponse.getPaypalPaymentInfo().getPaymentType(), "a payment type");
    }
}
