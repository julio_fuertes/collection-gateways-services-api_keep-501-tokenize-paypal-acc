package com.odigeo.cgsapi.v12.request.operations.get.booking.id;

import org.testng.Assert;
import org.testng.annotations.Test;

public class GetBookingIdRequestBuilderTest {

    @Test
    public void testBuild() {
        //Given
        GetBookingIdRequestBuilder builder = new GetBookingIdRequestBuilder();
        builder.merchantOrderId("merchantOrderId");
        //When
        GetBookingIdRequest getBookingIdRequest = builder.build();
        //Then
        Assert.assertEquals(getBookingIdRequest.getMerchantOrderId(), "merchantOrderId");
    }
}
