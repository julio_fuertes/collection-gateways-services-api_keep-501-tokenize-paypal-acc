package com.odigeo.cgsapi.v12.response.operations.confirm;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * Created by vrieraba on 29/05/2015.
 */
public class ConfirmationTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    private static final String ORDER_ID = "123";
    private static final String ERROR_CODE = "1";
    private static final String ERROR_DESCRIPTION = "error";

    @Mock
    private ConfirmationDetails confirmationDetails;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(confirmationDetails.getMerchantOrderId()).thenReturn(ORDER_ID);
    }

    @Test
    public void createConfirmation() {
        Confirmation confirmation = Confirmation.createConfirmation(confirmationDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.PAID);

        assertNotNull(confirmation);
        assertEquals(confirmation.getMerchantOrderId(), ORDER_ID);
        assertEquals(confirmation.getCode(), ERROR_CODE);
        assertEquals(confirmation.getErrorDescription(), ERROR_DESCRIPTION);
        assertEquals(confirmation.getAction(), MovementAction.CONFIRM);
        assertEquals(confirmation.getStatus(), MovementStatus.PAID);
    }

    @Test
    public void createFailedConfirmation() {
        Confirmation confirmation = Confirmation.createFailedConfirmation(confirmationDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION);

        assertNotNull(confirmation);
        assertEquals(confirmation.getMerchantOrderId(), ORDER_ID);
        assertEquals(confirmation.getCode(), ERROR_CODE);
        assertEquals(confirmation.getErrorDescription(), ERROR_DESCRIPTION);
        assertEquals(confirmation.getAction(), MovementAction.CONFIRM);
        assertEquals(confirmation.getStatus(), MovementStatus.PAYERROR);
    }

    @Test
    public void createFakeConfirmation() {
        Confirmation confirmation = Confirmation.createFakeConfirmation(confirmationDetails.getMerchantOrderId(), A_MONEY, MovementStatus.PAYERROR);

        assertNotNull(confirmation);
        assertEquals(confirmation.getMerchantOrderId(), ORDER_ID);
        assertEquals(confirmation.getAction(), MovementAction.CONFIRM);
        assertEquals(confirmation.getStatus(), MovementStatus.PAYERROR);
    }

    @Test
    public void equalsTest() {
        Confirmation confirmation1 = Confirmation.createConfirmation(confirmationDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.PAID);
        Confirmation confirmation2 = Confirmation.createConfirmation(confirmationDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.PAID);

        assertTrue(confirmation1.equals(confirmation1));
        assertTrue(confirmation1.equals(confirmation2));

        confirmation2 = Confirmation.createConfirmation("321", A_MONEY, "654", ERROR_DESCRIPTION, MovementStatus.PAID);

        assertFalse(confirmation1.equals(confirmation2));
        assertFalse(confirmation1.equals(null));
        assertFalse(confirmation1.equals(Boolean.TRUE));
    }

    @Test
    public void hashCodeTest() {
        Confirmation confirmation1 = Confirmation.createConfirmation(confirmationDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.PAID);

        assertTrue(confirmation1.hashCode() == confirmation1.hashCode());

        Confirmation confirmation2 = Confirmation.createConfirmation("321", A_MONEY, "654", ERROR_DESCRIPTION, MovementStatus.PAID);

        assertFalse(confirmation1.hashCode() == confirmation2.hashCode());
    }
}
