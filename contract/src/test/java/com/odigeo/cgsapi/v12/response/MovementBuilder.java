package com.odigeo.cgsapi.v12.response;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class MovementBuilder {

    private String orderId = "123";
    private String code = "123";
    private MovementErrorType errorType = null;
    private String errorDescription = "error";
    private MovementAction action = MovementAction.COLLECT;
    private MovementStatus status = MovementStatus.PAID;
    private BigDecimal amount = new BigDecimal("12.35");
    private Currency currency = Currency.getInstance("EUR");
    private String terminalId = "1";
    private String transactionId = "123";
    private String merchantStatus = "123";
    private String merchantStatusSubcode = "123";
    private String secure = "2";
    private String transactionIdSub = "15123";
    private MovementAdditionalInformation additionalInformation;
    private String protocolVersion;

    public Movement build() {
        Movement movement = new Movement(orderId, code, action, secure);
        movement.setAmount(amount);
        movement.setCurrency(currency);
        movement.setMerchantStatus(merchantStatus);
        movement.setMerchantStatusSubcode(merchantStatusSubcode);
        movement.setTerminalId(terminalId);
        movement.setTransactionId(transactionId);
        movement.setErrorType(errorType);
        movement.setErrorDescription(errorDescription);
        movement.setStatus(status);
        movement.setCode(code);
        movement.setTransactionIdSub(transactionIdSub);
        movement.setAdditionalInformation(additionalInformation);
        movement.setErrorType(errorType);
        movement.setProtocolVersion(protocolVersion);
        return movement;
    }

    public MovementBuilder withErrorType(MovementErrorType errorType) {
        this.errorType = errorType;
        return this;
    }

    public MovementBuilder withSecure(String secure) {
        this.secure = secure;
        return this;
    }

    public MovementBuilder withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public MovementBuilder withProtocolVersion(String protocolVersion) {
        this.protocolVersion = protocolVersion;
        return this;
    }
}
