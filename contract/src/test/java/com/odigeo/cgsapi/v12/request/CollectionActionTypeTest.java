package com.odigeo.cgsapi.v12.request;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertEquals;

/**
 * @author javier.garcia
 * @since 26/07/13
 */
public class CollectionActionTypeTest {

    @Test
    public void test() {
        AssertJUnit.assertEquals(CollectionActionType.getActionType("AUTHORIZE"), CollectionActionType.AUTHORIZE);
        assertEquals(CollectionActionType.getActionType("DIRECTY_PAYMENT"), CollectionActionType.DIRECTY_PAYMENT);
        assertEquals(CollectionActionType.getActionType("COLLECT"), CollectionActionType.COLLECT);

        try {
            CollectionActionType.getActionType("X");
            assertTrue(false);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }
}
