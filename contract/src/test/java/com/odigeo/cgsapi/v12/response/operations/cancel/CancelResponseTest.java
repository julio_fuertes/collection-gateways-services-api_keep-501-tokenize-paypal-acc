package com.odigeo.cgsapi.v12.response.operations.cancel;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class CancelResponseTest {

    @Mock
    private Movement movement;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void cancelResponseTest() {
        when(movement.getAmount()).thenReturn(new BigDecimal("85.23"));
        when(movement.getCurrency()).thenReturn(Currency.getInstance("EUR"));
        when(movement.getStatus()).thenReturn(MovementStatus.CANCEL);
        CancelResponse.Builder builder = new CancelResponse.Builder().addMovement(movement);
        CancelResponse cancelResponse = builder.build();
        cancelResponse.setGatewayCollectionMethod(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));

        assertEquals(cancelResponse.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
        assertEquals(cancelResponse.getAmount(), new BigDecimal("85.23"));
        assertEquals(cancelResponse.getCollectionStatus(), CollectionStatus.CANCELLED);
        assertTrue(cancelResponse.getMovements().contains(movement));
        assertNotNull(cancelResponse.getCurrency());

    }

    @Test
    public void cancelResponseTestError() {
        when(movement.getAmount()).thenReturn(new BigDecimal("85.23"));
        when(movement.getCurrency()).thenReturn(Currency.getInstance("EUR"));
        when(movement.getStatus()).thenReturn(MovementStatus.CANCELERR);
        CancelResponse.Builder builder = new CancelResponse.Builder().addMovement(movement);
        CancelResponse cancelResponse = builder.build();
        cancelResponse.setGatewayCollectionMethod(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));

        assertEquals(cancelResponse.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
        assertEquals(cancelResponse.getAmount(), new BigDecimal("85.23"));
        assertEquals(cancelResponse.getCollectionStatus(), CollectionStatus.CANCEL_ERROR);
        assertTrue(cancelResponse.getMovements().contains(movement));

        when(movement.getStatus()).thenReturn(MovementStatus.CANCEL);
        builder = new CancelResponse.Builder().addMovement(movement);
        cancelResponse = builder.build();
        cancelResponse.setGatewayCollectionMethod(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
        assertEquals(cancelResponse.getCollectionStatus(), CollectionStatus.CANCELLED);

        try {
            when(movement.getStatus()).thenReturn(MovementStatus.PAYERROR);
            builder = new CancelResponse.Builder().addMovement(movement);
            cancelResponse = builder.build();
            cancelResponse.setGatewayCollectionMethod(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
            assertEquals(cancelResponse.getCollectionStatus(), CollectionStatus.CANCELLED);
            fail("eror");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
