package com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.paypal;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class PaypalRefundDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Test
    public void paypalRefundDetails() {
        PaypalRefundDetails paypalRefundDetails = new PaypalRefundDetails("123", A_MONEY, "456");

        assertEquals(paypalRefundDetails.getMerchantOrderId(), "123");
        assertEquals(paypalRefundDetails.getMoney(), A_MONEY);
        assertEquals(paypalRefundDetails.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
        assertEquals(paypalRefundDetails.getTransactionId(), "456");
    }
}
