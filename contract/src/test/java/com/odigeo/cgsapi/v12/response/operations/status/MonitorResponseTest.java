package com.odigeo.cgsapi.v12.response.operations.status;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;


/**
 * Date: 26/05/14
 */
public class MonitorResponseTest {

    @Test
    public void testDefaultConstructor() throws Exception {
        MonitorResponse monitorResponse = new MonitorResponse();
        assertNotNull(monitorResponse);
    }

    @Test
    public void testIsActive() throws Exception {
        MonitorResponse monitorResponse = new MonitorResponse(true, "code", "description");
        assertEquals(monitorResponse.isActive(), true);
        assertEquals(monitorResponse.getCode(), "code");
        assertEquals(monitorResponse.getDescription(), "description");
    }
}
