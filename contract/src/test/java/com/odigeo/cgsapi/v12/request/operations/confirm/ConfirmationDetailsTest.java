package com.odigeo.cgsapi.v12.request.operations.confirm;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ConfirmationDetailsTest {

    private static final GatewayCollectionMethod A_GATEWAY_COLLECTION_METHOD = GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ALIPAY);
    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.56"), Currency.getInstance("EUR"));
    private static final String A_DYNAMIC_DESCRIPTOR = "websiteCode";

    @Test
    public void testEquals() {
        ConfirmationDetails cd = new ConfirmationDetails("orderId", A_MONEY, A_GATEWAY_COLLECTION_METHOD);
        ConfirmationDetails cd2 = new ConfirmationDetails("orderId2", A_MONEY, A_GATEWAY_COLLECTION_METHOD);
        ConfirmationDetails cd3 = new ConfirmationDetails("orderId3", A_MONEY, A_GATEWAY_COLLECTION_METHOD, A_DYNAMIC_DESCRIPTOR);
        assertTrue(cd.equals(cd));
        assertFalse(cd.equals(cd2));
        assertFalse(cd3.equals(null));
    }
}
