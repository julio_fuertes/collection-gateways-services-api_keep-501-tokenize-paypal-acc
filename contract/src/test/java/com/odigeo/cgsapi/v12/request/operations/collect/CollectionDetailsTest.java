package com.odigeo.cgsapi.v12.request.operations.collect;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class CollectionDetailsTest {

    private static final GatewayCollectionMethod A_GATEWAY_COLLECTION_METHOD = GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ALIPAY);
    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.56"), Currency.getInstance("EUR"));

    @Test
    public void testConstruction() {
        CollectionDetails collectionDetails = new CollectionDetails("orderId", A_MONEY, A_GATEWAY_COLLECTION_METHOD, null, null);
        CollectionDetails collectionDetails2 = new CollectionDetails("orderId2", A_MONEY, A_GATEWAY_COLLECTION_METHOD, null, null);
        assertTrue(collectionDetails.equals(collectionDetails));
        assertFalse(collectionDetails.equals(collectionDetails2));
        assertFalse(collectionDetails.equals(null));
    }

    @Test
    public void testToRefundDetails() {
        //Given
        CollectionDetails collectionDetails = new CollectionDetails("orderId", A_MONEY, A_GATEWAY_COLLECTION_METHOD, null, null);
        //When
        RefundDetails refundDetails = collectionDetails.toRefundDetails("transactionId", "merchantOrderId");
        //Then
        assertEquals(refundDetails.getMerchantOrderId(), "merchantOrderId");
        assertEquals(refundDetails.getTransactionId(), "transactionId");
        assertEquals(refundDetails.getMoney(), A_MONEY);
        assertEquals(refundDetails.getGatewayCollectionMethod(), A_GATEWAY_COLLECTION_METHOD);
    }


    @Test
    public void testToConfirmationDetails() {
        //Given
        CollectionDetails collectionDetails = new CollectionDetails("orderId", A_MONEY, A_GATEWAY_COLLECTION_METHOD, null, null);
        //When
        ConfirmationDetails confirmationDetails = collectionDetails.toConfirmationDetails("merchantOrderId");
        //Then
        assertEquals(confirmationDetails.getMerchantOrderId(), "merchantOrderId");
        assertEquals(confirmationDetails.getMoney(), A_MONEY);
        assertEquals(confirmationDetails.getGatewayCollectionMethod(), A_GATEWAY_COLLECTION_METHOD);
    }
}
