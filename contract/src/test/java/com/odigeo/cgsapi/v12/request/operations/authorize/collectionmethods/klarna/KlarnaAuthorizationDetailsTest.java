package com.odigeo.cgsapi.v12.request.operations.authorize.collectionmethods.klarna;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.klarna.KlarnaAccountHolder;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

public class KlarnaAuthorizationDetailsTest {

    private Money money = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    CollectionContext collectionContext;
    @Mock
    CollectionShoppingDetails collectionShoppingDetails;
    @Mock
    KlarnaAccountHolder klarnaAccountHolder;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testConstruction() {
        //Given
        String orderId = "123";
        //When
        KlarnaAuthorizationDetails klarnaAuthorizationDetails = new KlarnaAuthorizationDetails(orderId, money, collectionContext, collectionShoppingDetails, klarnaAccountHolder);
        //Then
        Assert.assertEquals(klarnaAuthorizationDetails.getOrderId(), "123");
        Assert.assertEquals(klarnaAuthorizationDetails.getMoney(), money);
        Assert.assertEquals(klarnaAuthorizationDetails.getCollectionContext(), collectionContext);
        Assert.assertEquals(klarnaAuthorizationDetails.getCollectionShoppingDetails(), collectionShoppingDetails);
        Assert.assertEquals(klarnaAuthorizationDetails.getKlarnaAccountHolder(), klarnaAccountHolder);
    }
}
