package com.odigeo.cgsapi.v12.request.operations.status;

import org.testng.annotations.Test;

import java.util.Currency;
import java.util.HashSet;
import java.util.Set;

import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertEquals;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class StatusDetailsTest {

    @Test
    public void statusDetailsCreation() {
        Set<Currency> currencySet = new HashSet<Currency>();
        currencySet.add(Currency.getInstance("EUR"));
        StatusDetails statusDetails = new StatusDetails(currencySet, 1L);
        assertTrue(statusDetails.getSupportedCurrencies().contains(Currency.getInstance("EUR")));
        assertEquals(statusDetails.getCurrencyAttemptsTimeout(), 1L);
    }
}
