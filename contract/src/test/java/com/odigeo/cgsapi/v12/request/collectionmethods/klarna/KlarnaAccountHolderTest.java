package com.odigeo.cgsapi.v12.request.collectionmethods.klarna;

import com.odigeo.cgsapi.v12.request.Gender;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.Test;

public class KlarnaAccountHolderTest {

    @Test
    public void testConstruction() {
        //Given
        Gender gender = Gender.MALE;
        String firstName = "a firstName";
        String lastName = "a lastName";
        String birthDate = "birthDate";
        Address address = Mockito.mock(Address.class);
        String telephoneNumber = "telephoneNumber";
        String email = "an email";
        //When
        KlarnaAccountHolder klarnaAccountHolder = new KlarnaAccountHolder(gender, firstName, lastName, birthDate, address, telephoneNumber, email);
        //Then
        Assert.assertEquals(klarnaAccountHolder.getGender(), Gender.MALE);
        Assert.assertEquals(klarnaAccountHolder.getFirstName(), "a firstName");
        Assert.assertEquals(klarnaAccountHolder.getLastName(), "a lastName");
        Assert.assertEquals(klarnaAccountHolder.getBirthDate(), "birthDate");
        Assert.assertEquals(klarnaAccountHolder.getAddress(), address);
        Assert.assertEquals(klarnaAccountHolder.getTelephoneNumber(), "telephoneNumber");
        Assert.assertEquals(klarnaAccountHolder.getEmail(), "an email");
    }

    @Test
    public void testSetSocialSecurityNumber() {
        //Given
        Address address = Mockito.mock(Address.class);
        KlarnaAccountHolder klarnaAccountHolder = new KlarnaAccountHolder(Gender.MALE, "firstName", "lastName", "birthdate", address, "telephoneNumber", "an email");
        //When
        klarnaAccountHolder.setSocialSecurityNumber("111111");
        //Then
        Assert.assertEquals(klarnaAccountHolder.getSocialSecurityNumber(), "111111");
    }

    @Test
    public void testEquals() {
        //Given
        Address address = Mockito.mock(Address.class);
        KlarnaAccountHolder klarnaAccountHolder1 = new KlarnaAccountHolder(Gender.MALE, "firstName", "lastName", "birthdate", address, "telephoneNumber", "an email");
        KlarnaAccountHolder klarnaAccountHolder2 = new KlarnaAccountHolder(Gender.MALE, "firstName", "lastName", "birthdate", address, "telephoneNumber", "an email");
        //When
        boolean equals = klarnaAccountHolder1.equals(klarnaAccountHolder2);
        //Then
        Assert.assertTrue(equals);
    }

    @Test
    public void testNotEquals() {
        //Given
        Address address = Mockito.mock(Address.class);
        KlarnaAccountHolder klarnaAccountHolder1 = new KlarnaAccountHolder(Gender.MALE, "firstName", "lastName", "birthdate", address, "telephoneNumber", "an email");
        KlarnaAccountHolder klarnaAccountHolder2 = new KlarnaAccountHolder(Gender.MALE, "anotherFirstName", "lastName", "birthdate", address, "telephoneNumber", "an email");
        //When
        boolean equals = klarnaAccountHolder1.equals(klarnaAccountHolder2);
        //Then
        Assert.assertFalse(equals);
    }


    @Test
    public void testHashCode() {
        //Given
        Address address = Mockito.mock(Address.class);
        KlarnaAccountHolder klarnaAccountHolder1 = new KlarnaAccountHolder(Gender.MALE, "firstName", "lastName", "birthdate", address, "telephoneNumber", "an email");
        KlarnaAccountHolder klarnaAccountHolder2 = new KlarnaAccountHolder(Gender.MALE, "firstName", "lastName", "birthdate", address, "telephoneNumber", "an email");
        //When
        int hashCode1 = klarnaAccountHolder1.hashCode();
        int hashCode2 = klarnaAccountHolder2.hashCode();
        //Then
        Assert.assertEquals(hashCode1, hashCode2);
    }

    @Test
    public void testHashCodeWithoutGender() {
        //Given
        Address address = Mockito.mock(Address.class);
        KlarnaAccountHolder klarnaAccountHolder1 = new KlarnaAccountHolder(null, "firstName", "lastName", "birthdate", address, "telephoneNumber", "an email");
        KlarnaAccountHolder klarnaAccountHolder2 = new KlarnaAccountHolder(null, "firstName", "lastName", "birthdate", address, "telephoneNumber", "an email");
        //When
        int hashCode1 = klarnaAccountHolder1.hashCode();
        int hashCode2 = klarnaAccountHolder2.hashCode();
        //Then
        Assert.assertEquals(hashCode1, hashCode2);
    }
}
