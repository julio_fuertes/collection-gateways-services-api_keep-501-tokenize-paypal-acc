package com.odigeo.cgsapi.v12.request.operations.common;

import com.odigeo.cgsapi.v12.LocalizedDate;
import org.apache.commons.lang.time.DateUtils;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by joaquin.silva on 21/04/2016.
 */
public class SectionBuilder {

    public static Section getSection() {

        Calendar cal = Calendar.getInstance();

        Integer sectionNumber = 1;
        String productType = "FLIGHT";
        String carrierCode = "FR";
        String flightNumber = "450";
        String departureIataCode = "BCN";
        String arrivalIataCode = "MAD";

        cal.set(2015, 10, 20);
        LocalizedDate departureDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());

        cal.set(2015, 10, 21);
        LocalizedDate arrivalDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());
        String departureCityName = "Barcelona";
        String arrivalCityName = "Madrid";
        String serviceClass = "X";

        return new Section(sectionNumber, productType, carrierCode, flightNumber, departureIataCode, arrivalIataCode, departureDate, arrivalDate, departureCityName, arrivalCityName, serviceClass);
    }

    public static Section getSection2() {
        Calendar cal = Calendar.getInstance();

        Integer sectionNumber = 2;
        String productType = "FLIGHT";
        String carrierCode = "VY";
        String flightNumber = "6398";
        String departureIataCode = "SEV";
        String arrivalIataCode = "PMI";
        cal.set(2013, 10, 20);
        LocalizedDate departureDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());

        cal.set(2013, 10, 21);
        LocalizedDate arrivalDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());
        String departureCityName = "Sevilla";
        String arrivalCityName = "Palma de Mallorca";
        String serviceClass = "Y";

        return new Section(sectionNumber, productType, carrierCode, flightNumber, departureIataCode, arrivalIataCode, departureDate, arrivalDate, departureCityName, arrivalCityName, serviceClass);
    }

    public static Section getSection3() {
        Calendar cal = Calendar.getInstance();

        Integer sectionNumber = 3;
        String productType = "FLIGHT";
        String carrierCode = "TO";
        String flightNumber = "3876";
        String departureIataCode = "ORY";
        String arrivalIataCode = "MUC";
        cal.set(2014, 1, 1);
        LocalizedDate departureDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());

        cal.set(2014, 1, 6);
        LocalizedDate arrivalDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());
        String departureCityName = "París";
        String arrivalCityName = "Múnich";
        String serviceClass = "F";

        return new Section(sectionNumber, productType, carrierCode, flightNumber, departureIataCode, arrivalIataCode, departureDate, arrivalDate, departureCityName, arrivalCityName, serviceClass);
    }

    public static Section getSection4() {
        Calendar cal = Calendar.getInstance();

        Integer sectionNumber = 9;
        String productType = "FLIGHT";
        String carrierCode = "4U";
        String flightNumber = "2589";
        String departureIataCode = "PMI";
        String arrivalIataCode = "STR";
        cal.set(2015, 9, 16);
        LocalizedDate departureDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());

        cal.set(2015, 9, 23);
        LocalizedDate arrivalDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());
        String departureCityName = "Palma de Mallorca";
        String arrivalCityName = "Stuttgart";
        String serviceClass = "Y";

        return new Section(sectionNumber, productType, carrierCode, flightNumber, departureIataCode, arrivalIataCode, departureDate, arrivalDate, departureCityName, arrivalCityName, serviceClass);
    }
}
