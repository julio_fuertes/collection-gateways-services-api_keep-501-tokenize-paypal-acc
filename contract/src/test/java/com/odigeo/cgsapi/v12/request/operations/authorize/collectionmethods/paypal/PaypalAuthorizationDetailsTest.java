package com.odigeo.cgsapi.v12.request.operations.authorize.collectionmethods.paypal;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

/**
 * Created by vrieraba on 26/05/2015.
 */
public class PaypalAuthorizationDetailsTest {

    private static final String AN_ORDER_ID = "123";
    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));
    private static final GatewayCollectionMethod PAYPAL_GATEWAY_COLLECTION_METHOD = GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL);
    private static final String RESUME_BASE_URL = "www.foo.com";

    @Mock
    private CreditCard creditCard;
    @Mock
    private CollectionContext collectionContext;
    @Mock
    private CollectionShoppingDetails collectionShoppingDetails;


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void paypalAuthorizationDetails() {
        when(creditCard.getType()).thenReturn(CreditCardType.VISA_CREDIT);
        PaypalAuthorizationDetails paypalAuthorizationDetails = new PaypalAuthorizationDetails(AN_ORDER_ID, A_MONEY, PAYPAL_GATEWAY_COLLECTION_METHOD, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);

        assertEquals(paypalAuthorizationDetails.getOrderId(), AN_ORDER_ID);
        assertEquals(paypalAuthorizationDetails.getMoney(), A_MONEY);
        assertEquals(paypalAuthorizationDetails.getGatewayCollectionMethod(), PAYPAL_GATEWAY_COLLECTION_METHOD);
        assertEquals(paypalAuthorizationDetails.getCollectionContext(), collectionContext);
        assertEquals(paypalAuthorizationDetails.getCollectionShoppingDetails(), collectionShoppingDetails);
        assertEquals(paypalAuthorizationDetails.getResumeBaseUrl(), RESUME_BASE_URL);
    }

    @Test
    public void testThrowpaypalAuthorizationDetailsDeprecatedConstructor() {
        new PaypalAuthorizationDetails(AN_ORDER_ID, A_MONEY, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "Invalid GatewayCollectionMethod")
    public void testThrowExceptionWhenGatewayCollectionMethodIsNull() {
        new PaypalAuthorizationDetails(AN_ORDER_ID, A_MONEY, null, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "Invalid GatewayCollectionMethod")
    public void testThrowExceptionWhenGatewayCollectionMethodIsNotPaypal() {
        new PaypalAuthorizationDetails(AN_ORDER_ID, A_MONEY, new GatewayCollectionMethod(GatewayCollectionMethodType.SOFORT), collectionContext, collectionShoppingDetails, RESUME_BASE_URL);
    }
}
