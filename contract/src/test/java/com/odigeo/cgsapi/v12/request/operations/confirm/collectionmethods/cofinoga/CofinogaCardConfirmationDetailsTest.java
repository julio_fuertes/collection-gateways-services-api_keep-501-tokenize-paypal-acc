package com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.cofinoga;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.cofinoga.CofinogaCard;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class CofinogaCardConfirmationDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    private CofinogaCard cofinogaCard = new CofinogaCard("owner", "number", "01", "12", "1988");

    @Test
    public void cofinogaCardConfirmationDetailsTest() {
        CofinogaCardConfirmationDetails details = new CofinogaCardConfirmationDetails("123", A_MONEY, cofinogaCard);
        assertEquals(details.getCofinogaCard(), cofinogaCard);
        assertEquals(details.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.COFINOGA));
        assertEquals(details.getMoney(), A_MONEY);
        assertEquals(details.getMerchantOrderId(), "123");

        CofinogaCardConfirmationDetails details2 = new CofinogaCardConfirmationDetails("123", A_MONEY, cofinogaCard);
        assertTrue(details.equals(details2));
        assertEquals(details.hashCode(), details2.hashCode());
        assertNotNull(details2.toString());
        assertTrue(details2.equals(details2));
        assertFalse(details2.equals("test"));
        assertFalse(details2.equals(null));

        CofinogaCardConfirmationDetails details3 = new CofinogaCardConfirmationDetails("111", A_MONEY, cofinogaCard, "dynamicDescriptor");
        assertEquals(details3.getDynamicDescriptor(), "dynamicDescriptor");
    }
}
