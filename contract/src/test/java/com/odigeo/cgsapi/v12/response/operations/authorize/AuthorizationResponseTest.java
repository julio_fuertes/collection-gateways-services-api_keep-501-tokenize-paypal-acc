package com.odigeo.cgsapi.v12.response.operations.authorize;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.RedirectionParameters;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;


/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class AuthorizationResponseTest {

    @Mock
    private Movement movement;
    @Mock
    private RedirectionParameters redirectionParameters;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void authorizationResponseTest() {
        when(movement.getAmount()).thenReturn(new BigDecimal("85.23"));
        when(movement.getCurrency()).thenReturn(Currency.getInstance("EUR"));
        when(movement.getStatus()).thenReturn(MovementStatus.AUTHORIZED);

        AuthorizationResponse.Builder builder = new AuthorizationResponse.Builder().addMovement(movement);
        builder.recurringTransactionId("aRecurringTransactionId");
        AuthorizationResponse authorizationResponse = builder.build();
        authorizationResponse.setGatewayCollectionMethod(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));

        assertEquals(authorizationResponse.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
        assertEquals(authorizationResponse.getAmount(), new BigDecimal("85.23"));
        assertEquals(authorizationResponse.getCollectionStatus(), CollectionStatus.AUTHORIZED);
        assertEquals(authorizationResponse.getRecurringTransactionId(), "aRecurringTransactionId");
        assertTrue(authorizationResponse.getMovements().contains(movement));
        assertFalse(authorizationResponse.needsRedirect());
        assertNotNull(authorizationResponse.getCurrency());

    }

    @Test
    public void transform() {
        when(movement.getStatus()).thenReturn(MovementStatus.INIT_INTERACTION);
        AuthorizationResponse.Builder builder = new AuthorizationResponse.Builder().addMovement(movement);
        AuthorizationResponse authorizationResponse = builder.build();
        assertEquals(authorizationResponse.getCollectionStatus(), CollectionStatus.ON_HOLD);

        when(movement.getStatus()).thenReturn(MovementStatus.DECLINED);
        builder = new AuthorizationResponse.Builder().addMovement(movement);
        authorizationResponse = builder.build();
        assertEquals(authorizationResponse.getCollectionStatus(), CollectionStatus.COLLECTION_DECLINED);

        when(movement.getStatus()).thenReturn(MovementStatus.AUTHORIZED);
        builder = new AuthorizationResponse.Builder().addMovement(movement);
        authorizationResponse = builder.build();
        assertEquals(authorizationResponse.getCollectionStatus(), CollectionStatus.AUTHORIZED);

        when(movement.getStatus()).thenReturn(MovementStatus.PAYERROR);
        builder = new AuthorizationResponse.Builder().addMovement(movement);
        authorizationResponse = builder.build();
        assertEquals(authorizationResponse.getCollectionStatus(), CollectionStatus.COLLECTION_ERROR);

        when(movement.getStatus()).thenReturn(MovementStatus.REFUNDED);
        builder = new AuthorizationResponse.Builder().addMovement(movement);
        authorizationResponse = builder.build();
        assertEquals(authorizationResponse.getCollectionStatus(), CollectionStatus.REFUNDED);

        when(movement.getStatus()).thenReturn(MovementStatus.REFUNDERR);
        builder = new AuthorizationResponse.Builder().addMovement(movement);
        authorizationResponse = builder.build();
        assertEquals(authorizationResponse.getCollectionStatus(), CollectionStatus.REFUND_ERROR);

        when(movement.getStatus()).thenReturn(MovementStatus.REQUESTED);
        builder = new AuthorizationResponse.Builder().addMovement(movement);
        authorizationResponse = builder.build();
        assertEquals(authorizationResponse.getCollectionStatus(), CollectionStatus.REQUESTED);

        try {
            when(movement.getStatus()).thenReturn(MovementStatus.CANCELERR);
            builder.addMovement(movement);
            builder.build();
            assertTrue(false);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }

    @Test
    public void collectionResponseRedirectionNeededTest() {
        when(movement.getAmount()).thenReturn(new BigDecimal("85.23"));
        when(movement.getCurrency()).thenReturn(Currency.getInstance("EUR"));
        when(movement.getStatus()).thenReturn(MovementStatus.INIT_INTERACTION);

        AuthorizationResponse.Builder builder = new AuthorizationResponse.Builder().addMovement(movement).redirectionParameters(redirectionParameters);
        AuthorizationResponse authorizationResponse = builder.build();
        authorizationResponse.setGatewayCollectionMethod(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));

        assertEquals(authorizationResponse.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
        assertEquals(authorizationResponse.getAmount(), new BigDecimal("85.23"));
        assertEquals(authorizationResponse.getCollectionStatus(), CollectionStatus.ON_HOLD);
        assertEquals(authorizationResponse.getRedirectionParameters(), redirectionParameters);
        assertTrue(authorizationResponse.getMovements().contains(movement));
        assertTrue(authorizationResponse.needsRedirect());
        assertNotNull(authorizationResponse.getCurrency());

    }

    @Test
    public void testBuilder() {
        AuthorizationResponse.Builder builder = new AuthorizationResponse.Builder();
        Movement movement1 = new Movement("a", "v", MovementAction.AUTHORIZE);
        movement1.setStatus(MovementStatus.DECLINED);
        Movement movement2 = new Movement("a", "v", MovementAction.CONFIRM);
        movement2.setStatus(MovementStatus.PAYERROR);
        Movement movement3 = new Movement("a", "v", MovementAction.COLLECT);
        movement3.setStatus(MovementStatus.DECLINED);
        Movement movement4 = new Movement("a", "v", MovementAction.VERIFY);
        movement4.setStatus(MovementStatus.INIT_INTERACTION);
        Movement movement5 = new Movement("a", "v", MovementAction.VERIFY);
        movement5.setStatus(MovementStatus.REFUNDERR);
        Movement movement6 = new Movement("a", "v", MovementAction.VERIFY);
        movement6.setStatus(MovementStatus.REQUESTED);
        builder.addMovement(movement1);
        builder.addMovement(movement2);
        builder.addMovement(movement3);
        builder.addMovement(movement4);
        builder.addMovement(movement5);
        builder.addMovement(movement6);
        AuthorizationResponse result = builder.build();
        CollectionStatus status = result.getCollectionStatus();
        Assert.assertEquals(status, CollectionStatus.REQUESTED);

        try {
            Movement movement9 = new Movement("a", "v", MovementAction.VERIFY);
            movement9.setStatus(MovementStatus.QUERYERR);
            builder.addMovement(movement9);
            fail("error");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
