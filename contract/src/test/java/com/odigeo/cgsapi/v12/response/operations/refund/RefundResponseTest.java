package com.odigeo.cgsapi.v12.response.operations.refund;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class RefundResponseTest {

    @Mock
    private Movement movement;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void refundResponseTest() {
        when(movement.getAmount()).thenReturn(new BigDecimal("85.23"));
        when(movement.getCurrency()).thenReturn(Currency.getInstance("EUR"));
        when(movement.getStatus()).thenReturn(MovementStatus.DECLINED);
        RefundResponse.Builder builder = new RefundResponse.Builder()
                .addMovement(movement);
        RefundResponse refundResponse = builder.build();
        refundResponse.setGatewayCollectionMethod(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));

        assertEquals(refundResponse.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
        assertEquals(refundResponse.getAmount(), new BigDecimal("85.23"));
        assertEquals(refundResponse.getCollectionStatus(), CollectionStatus.REFUNDED_DECLINED);
        assertTrue(refundResponse.getMovements().contains(movement));
        assertNotNull(refundResponse.getCurrency());

    }

    @Test
    public void transform() {
        when(movement.getStatus()).thenReturn(MovementStatus.DECLINED);
        RefundResponse.Builder builder = new RefundResponse.Builder().addMovement(movement);
        RefundResponse refundResponse = builder.build();
        assertEquals(refundResponse.getCollectionStatus(), CollectionStatus.REFUNDED_DECLINED);

        when(movement.getStatus()).thenReturn(MovementStatus.REFUNDED);
        builder.addMovement(movement);
        refundResponse = builder.build();
        assertEquals(refundResponse.getCollectionStatus(), CollectionStatus.REFUNDED);

        when(movement.getStatus()).thenReturn(MovementStatus.REFUNDERR);
        builder.addMovement(movement);
        refundResponse = builder.build();
        assertEquals(refundResponse.getCollectionStatus(), CollectionStatus.REFUND_ERROR);

        when(movement.getStatus()).thenReturn(MovementStatus.STATUS);
        builder.addMovement(movement);
        refundResponse = builder.build();
        assertEquals(refundResponse.getCollectionStatus(), CollectionStatus.REFUND_ERROR);

        when(movement.getStatus()).thenReturn(MovementStatus.QUERYERR);
        builder.addMovement(movement);
        refundResponse = builder.build();
        assertEquals(refundResponse.getCollectionStatus(), CollectionStatus.QUERY_ERROR);

        when(movement.getStatus()).thenReturn(MovementStatus.CANCEL);
        builder.addMovement(movement);
        refundResponse = builder.build();
        assertEquals(refundResponse.getCollectionStatus(), CollectionStatus.COLLECTION_CANCELLED);

        try {
            when(movement.getStatus()).thenReturn(MovementStatus.PAID);
            builder.addMovement(movement);
            refundResponse = builder.build();
            assertTrue(false);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }
}
