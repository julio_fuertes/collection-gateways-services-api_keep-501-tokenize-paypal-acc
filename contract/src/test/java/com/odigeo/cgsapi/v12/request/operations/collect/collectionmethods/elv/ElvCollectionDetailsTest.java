package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.elv;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class ElvCollectionDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private CollectionContext collectionContext;
    @Mock
    private CollectionShoppingDetails collectionShoppingDetails;
    private BankDetails bankDetails = new BankDetails("bankAccountNumber", "bankLocationId");
    private ElvUserDetails userDetails = new ElvUserDetails("name", "lastName", "street", "streetNumber", "city", "zipCode", "countryCode");

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void elvCollectionDetailsWithUserDetails() {
        ElvCollectionDetails elvCollectionDetails = new ElvCollectionDetails("123", A_MONEY, bankDetails, userDetails, collectionContext, collectionShoppingDetails);

        assertEquals(elvCollectionDetails.getOrderId(), "123");
        assertEquals(elvCollectionDetails.getMoney(), A_MONEY);
        assertEquals(elvCollectionDetails.getGatewayCollectionMethod(), new GatewayCollectionMethod(GatewayCollectionMethodType.ELV));
        assertEquals(elvCollectionDetails.getBankDetails().getBankAccountNumber(), "bankAccountNumber");
        assertEquals(elvCollectionDetails.getBankDetails().getBankLocationId(), "bankLocationId");
        assertEquals(elvCollectionDetails.getUserDetails().getName(), "name");
        assertEquals(elvCollectionDetails.getUserDetails().getLastName(), "lastName");
        assertEquals(elvCollectionDetails.getUserDetails().getStreet(), "street");
        assertEquals(elvCollectionDetails.getUserDetails().getStreetNumber(), "streetNumber");
        assertEquals(elvCollectionDetails.getUserDetails().getCity(), "city");
        assertEquals(elvCollectionDetails.getUserDetails().getZipCode(), "zipCode");
        assertEquals(elvCollectionDetails.getUserDetails().getCountryCode(), "countryCode");
        assertEquals(elvCollectionDetails.getCollectionContext(), collectionContext);
        assertEquals(elvCollectionDetails.getCollectionShoppingDetails(), collectionShoppingDetails);
        ElvCollectionDetails elvCollectionDetails2 = new ElvCollectionDetails("123", A_MONEY, bankDetails, userDetails, collectionContext, collectionShoppingDetails);
        assertTrue(elvCollectionDetails.equals(elvCollectionDetails2));
        assertEquals(elvCollectionDetails.hashCode(), elvCollectionDetails2.hashCode());
        assertNotNull(elvCollectionDetails2.toString());
        assertTrue(elvCollectionDetails2.equals(elvCollectionDetails2));
        assertFalse(elvCollectionDetails2.equals("test"));
        assertFalse(elvCollectionDetails2.equals(null));
    }

    @Test
    public void toRefund() {
        ElvCollectionDetails elvCollectionDetails = new ElvCollectionDetails("123", A_MONEY, bankDetails, userDetails, collectionContext, collectionShoppingDetails);
        RefundDetails refundDetails = elvCollectionDetails.toRefundDetails("123", "123");

        assertEquals(refundDetails.getTransactionId(), "123");
    }

    @Test
    public void toConfirmationDetails() {
        ElvCollectionDetails elvCollectionDetails = new ElvCollectionDetails("123", A_MONEY, bankDetails, userDetails, collectionContext, collectionShoppingDetails);
        ConfirmationDetails confirmationDetails = elvCollectionDetails.toConfirmationDetails("123");

        assertEquals(confirmationDetails.getMerchantOrderId(), "123");
    }
}
