package com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class PaypalAccountStatusTest {

    @Test
    public void get() {
        Assert.assertEquals(PaypalAccountStatus.get("verified"), PaypalAccountStatus.VERIFIED);
        assertEquals(PaypalAccountStatus.get("unverified"), PaypalAccountStatus.UNVERIFIED);
        assertEquals(PaypalAccountStatus.get("unknown"), PaypalAccountStatus.UNKNOWN);
        assertEquals(PaypalAccountStatus.get("AAA"), PaypalAccountStatus.UNKNOWN);
        assertEquals(PaypalAccountStatus.UNVERIFIED.getId(), "unverified");
    }
}
