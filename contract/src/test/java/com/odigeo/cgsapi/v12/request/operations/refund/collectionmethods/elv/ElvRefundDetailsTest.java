package com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.elv;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class ElvRefundDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Test
    public void elvRefundDetails() {
        ElvRefundDetails elvRefundDetails = new ElvRefundDetails("123", A_MONEY, "456");

        assertEquals(elvRefundDetails.getMerchantOrderId(), "123");
        assertEquals(elvRefundDetails.getMoney(), A_MONEY);
        assertEquals(elvRefundDetails.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ELV));
        assertEquals(elvRefundDetails.getTransactionId(), "456");
    }
}
