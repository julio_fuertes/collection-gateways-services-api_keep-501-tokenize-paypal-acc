package com.odigeo.cgsapi.v12.request.operations.binlookup;

import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import com.odigeo.cgsapi.v12.request.operations.binlookup.collectionmethods.creditcard.BinLookUpDetails;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.operations.binlookup.BinLookUp;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

/**
 * @author alain.munoz
 * @since 17/01/14
 */
public class BinLookUpTest {

    @Mock
    private BinLookUpDetails binLookUpDetails;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createFailedBinLookUp() {
        BinLookUp binLookUp = BinLookUp.createFailedBinLookUp("1", "error");

        assertEquals(binLookUp.getAction(), MovementAction.BIN_LOOK_UP);
        assertEquals(binLookUp.getStatus(), MovementStatus.DECLINED);
        assertNull(binLookUp.getMerchantOrderId());
        assertNull(binLookUp.getCountryCode());
        assertNull(binLookUp.getNumCountryCode());
        assertNull(binLookUp.getCreditCardType());
        assertNotNull(BinLookUp.fakeBinLookUp());
    }

    @Test
    public void equalsTest() {
        BinLookUp binLookUp1 = new BinLookUp("456", "error", "ES", 724, CreditCardType.VISA_CREDIT);
        BinLookUp binLookUp2 = new BinLookUp("456", "error", "ES", 724, CreditCardType.VISA_CREDIT);

        assertTrue(binLookUp1.equals(binLookUp1));
        assertTrue(binLookUp1.equals(binLookUp2));

        binLookUp2 = new BinLookUp("456", "error", "ES", 724, CreditCardType.MASTER_CARD);

        assertFalse(binLookUp1.equals(binLookUp2));
        assertFalse(binLookUp1.equals(null));
        assertFalse(binLookUp1.equals(Boolean.TRUE));
    }

    @Test
    public void hashCodeTest() {
        BinLookUp binLookUp1 = new BinLookUp("456", "error", "ES", 724, CreditCardType.VISA_CREDIT);
        BinLookUp binLookUp2 = new BinLookUp("456", "error", "ES", 724, CreditCardType.VISA_CREDIT);

        assertTrue(binLookUp1.hashCode() == binLookUp2.hashCode());

        binLookUp2 = new BinLookUp("456", "error", "ES", 724, CreditCardType.MASTER_CARD);

        assertFalse(binLookUp1.hashCode() == binLookUp2.hashCode());
    }
}
