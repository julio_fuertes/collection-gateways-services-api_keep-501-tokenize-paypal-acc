package com.odigeo.cgsapi.v12.response.operations.confirm;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class ConfirmationResponseTest {

    @Mock
    private Movement movement;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void confirmationResponseTest() {
        when(movement.getAmount()).thenReturn(new BigDecimal("85.23"));
        when(movement.getCurrency()).thenReturn(Currency.getInstance("EUR"));
        when(movement.getStatus()).thenReturn(MovementStatus.PAID);
        ConfirmationResponse.Builder builder = new ConfirmationResponse.Builder()
                .addMovement(movement);
        ConfirmationResponse confirmationResponse = builder.build();
        confirmationResponse.setGatewayCollectionMethod(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));

        assertEquals(confirmationResponse.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
        assertEquals(confirmationResponse.getAmount(), new BigDecimal("85.23"));
        assertEquals(confirmationResponse.getCollectionStatus(), CollectionStatus.COLLECTED);
        assertTrue(confirmationResponse.getMovements().contains(movement));
        assertNotNull(confirmationResponse.getCurrency());

    }

    @Test
    public void transform() {
        when(movement.getStatus()).thenReturn(MovementStatus.PAID);
        ConfirmationResponse.Builder builder = new ConfirmationResponse.Builder().addMovement(movement);
        ConfirmationResponse confirmationResponse = builder.build();

        assertEquals(confirmationResponse.getCollectionStatus(), CollectionStatus.COLLECTED);

        when(movement.getStatus()).thenReturn(MovementStatus.PAYERROR);
        builder.addMovement(movement);
        confirmationResponse = builder.build();

        assertEquals(confirmationResponse.getCollectionStatus(), CollectionStatus.COLLECTION_ERROR);

        when(movement.getStatus()).thenReturn(MovementStatus.REQUESTED);
        builder.addMovement(movement);
        confirmationResponse = builder.build();
        assertEquals(confirmationResponse.getCollectionStatus(), CollectionStatus.REQUESTED);

        when(movement.getStatus()).thenReturn(MovementStatus.DECLINED);
        builder.addMovement(movement);
        confirmationResponse = builder.build();

        assertEquals(confirmationResponse.getCollectionStatus(), CollectionStatus.COLLECTION_DECLINED);

        when(movement.getStatus()).thenReturn(MovementStatus.STATUS);
        builder.addMovement(movement);
        confirmationResponse = builder.build();

        assertEquals(confirmationResponse.getCollectionStatus(), CollectionStatus.COLLECTION_DECLINED);

        try {
            when(movement.getStatus()).thenReturn(MovementStatus.REFUNDED);
            builder.addMovement(movement);
            confirmationResponse = builder.build();
            assertTrue(false);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }
}
