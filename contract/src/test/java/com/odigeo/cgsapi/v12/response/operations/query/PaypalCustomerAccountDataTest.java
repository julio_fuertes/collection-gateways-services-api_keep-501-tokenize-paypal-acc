package com.odigeo.cgsapi.v12.response.operations.query;

import com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal.PaypalAccountStatus;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by ADEFREIT on 22/07/2015.
 */
public class PaypalCustomerAccountDataTest {

    private static final String PAYERID = "payerId";
    private static final String ADDRESS_STATUS = "an address status";
    private static final PaypalAccountStatus PAYER_STATUS = PaypalAccountStatus.VERIFIED;
    private static final CustomerData CUSTOMER_DATA = CustomerDataBuilder.aCustomerData().build();

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPaypalCustomerAccountData() {
        PaypalCustomerAccountData.Builder builder = new PaypalCustomerAccountData.Builder();
        builder.payerId(PAYERID);
        builder.payerStatus(PAYER_STATUS);
        builder.customerData(CUSTOMER_DATA);
        builder.addressStatus(ADDRESS_STATUS);

        PaypalCustomerAccountData paypalCustomerAccountData = builder.build();
        assertEquals(paypalCustomerAccountData.getPayerId(), PAYERID);
        assertEquals(paypalCustomerAccountData.getPayerStatus(), PAYER_STATUS);
        assertEquals(paypalCustomerAccountData.getCustomerData(), CUSTOMER_DATA);
        assertEquals(paypalCustomerAccountData.getAddressStatus(), ADDRESS_STATUS);
    }
}
