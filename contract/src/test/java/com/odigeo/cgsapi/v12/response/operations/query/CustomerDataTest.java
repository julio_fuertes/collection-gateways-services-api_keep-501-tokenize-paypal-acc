package com.odigeo.cgsapi.v12.response.operations.query;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Created by ADEFREIT on 22/07/2015.
 */

public class CustomerDataTest {

    private static final String FIRSTNAME = "firstName";
    private static final String MIDDLENAME = "middleName";
    private static final String LASTNAME = "lastName";
    private static final String SUFIX = "sufix";
    private static final String EMAIL = "email";
    private static final String COUNTRY_CODE = "countryCode";
    private static final String BUSSINESS = "business";

    @Test
    public void testCustomerData() {
        CustomerData.Builder builder = new CustomerData.Builder();
        builder.firstName(FIRSTNAME);
        builder.middleName(MIDDLENAME);
        builder.lastName(LASTNAME);
        builder.sufix(SUFIX);
        builder.email(EMAIL);
        builder.countryCode(COUNTRY_CODE);
        builder.business(BUSSINESS);

        CustomerData customerData = builder.build();

        assertEquals(customerData.getFirstName(), FIRSTNAME);
        assertEquals(customerData.getMiddleName(), MIDDLENAME);
        assertEquals(customerData.getLastName(), LASTNAME);
        assertEquals(customerData.getSufix(), SUFIX);
        assertEquals(customerData.getEmail(), EMAIL);
        assertEquals(customerData.getCountryCode(), COUNTRY_CODE);
        assertEquals(customerData.getBusiness(), BUSSINESS);
    }
}
