package com.odigeo.cgsapi.v12.response.operations.verify;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementContainer;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class VerificationResponseTest {

    private static final BigDecimal AN_AMOUNT = new BigDecimal("85.23");
    private static final Currency A_CURRENCY = Currency.getInstance("EUR");
    private static final String A_RECURRING_TRANSACTION_ID = "aRecurringTransactionId";
    private static final String A_PAYMENT_INSTRUMENT_TOKEN = "TYPE-UUId";

    @Mock
    private Movement movement;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void verificationResponseTest() {
        when(movement.getAmount()).thenReturn(AN_AMOUNT);
        when(movement.getCurrency()).thenReturn(A_CURRENCY);
        when(movement.getStatus()).thenReturn(MovementStatus.DECLINED);
        List<Movement> movements = new ArrayList<Movement>();
        movements.add(movement);
        VerificationResponse.Builder builder = new VerificationResponse.Builder()
                .addMovements(movements);
        builder.recurringTransactionId(A_RECURRING_TRANSACTION_ID);
        VerificationResponse verificationResponse = new VerificationResponse(builder);
        assertNotNull(verificationResponse);
        builder.paymentInstrumentToken(A_PAYMENT_INSTRUMENT_TOKEN);

        MovementContainer mc = new VerificationResponse(builder);
        assertNotNull(mc);
        assertNotNull(mc.getCurrency());
        assertNotNull(mc.getMovements());

        verificationResponse = builder.build();
        verificationResponse.setGatewayCollectionMethod(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));

        assertEquals(verificationResponse.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
        assertEquals(verificationResponse.getAmount(), AN_AMOUNT);
        assertEquals(verificationResponse.getCollectionStatus(), CollectionStatus.COLLECTION_DECLINED);
        assertEquals(verificationResponse.getRecurringTransactionId(), A_RECURRING_TRANSACTION_ID);
        assertEquals(verificationResponse.getPaymentInstrumentToken(), A_PAYMENT_INSTRUMENT_TOKEN);
        assertTrue(verificationResponse.getMovements().contains(movement));
        assertNotNull(verificationResponse.getMovements());

        builder.collectionStatus(CollectionStatus.COLLECTION_ERROR);
        verificationResponse = builder.build();
        assertEquals(verificationResponse.getCollectionStatus(), CollectionStatus.COLLECTION_ERROR);
        assertNotNull(verificationResponse.getCurrency());
    }

    @Test
    public void verificationResponseTest2() {
        when(movement.getAmount()).thenReturn(AN_AMOUNT);
        when(movement.getCurrency()).thenReturn(A_CURRENCY);
        when(movement.getStatus()).thenReturn(MovementStatus.PAID);
        VerificationResponse.Builder builder = new VerificationResponse.Builder()
                .addMovement(movement).collectionStatus(CollectionStatus.COLLECTED);
        VerificationResponse verificationResponse = builder.build();
        verificationResponse.setGatewayCollectionMethod(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));

        assertEquals(verificationResponse.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
        assertEquals(verificationResponse.getAmount(), AN_AMOUNT);
        assertEquals(verificationResponse.getCollectionStatus(), CollectionStatus.COLLECTED);
        assertEquals(verificationResponse.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
        assertTrue(verificationResponse.getMovements().contains(movement));
        assertNotNull(verificationResponse.getCurrency());
    }

    @Test
    public void transform() {
        when(movement.getStatus()).thenReturn(MovementStatus.DECLINED);
        VerificationResponse.Builder builder = new VerificationResponse.Builder().addMovement(movement);
        VerificationResponse verificationResponse = builder.build();

        assertEquals(verificationResponse.getCollectionStatus(), CollectionStatus.COLLECTION_DECLINED);

        when(movement.getStatus()).thenReturn(MovementStatus.STATUS);
        builder.addMovement(movement);
        verificationResponse = builder.build();

        assertEquals(verificationResponse.getCollectionStatus(), CollectionStatus.COLLECTION_DECLINED);

        when(movement.getStatus()).thenReturn(MovementStatus.REQUESTED);
        builder.addMovement(movement);
        verificationResponse = builder.build();
        assertEquals(verificationResponse.getCollectionStatus(), CollectionStatus.REQUESTED);

        when(movement.getStatus()).thenReturn(MovementStatus.CANCEL);
        builder.addMovement(movement);
        verificationResponse = builder.build();

        assertEquals(verificationResponse.getCollectionStatus(), CollectionStatus.COLLECTION_CANCELLED);

        when(movement.getStatus()).thenReturn(MovementStatus.PAID);
        builder.addMovement(movement);
        verificationResponse = builder.build();

        assertEquals(verificationResponse.getCollectionStatus(), CollectionStatus.COLLECTED);

        when(movement.getStatus()).thenReturn(MovementStatus.AUTHORIZED);
        builder.addMovement(movement);
        verificationResponse = builder.build();

        assertEquals(verificationResponse.getCollectionStatus(), CollectionStatus.AUTHORIZED);

        when(movement.getStatus()).thenReturn(MovementStatus.CANCELERR);
        builder.addMovement(movement);
        verificationResponse = builder.build();

        assertEquals(verificationResponse.getCollectionStatus(), CollectionStatus.COLLECTION_ERROR);

        when(movement.getStatus()).thenReturn(MovementStatus.PAYERROR);
        builder.addMovement(movement);
        verificationResponse = builder.build();
        assertEquals(verificationResponse.getCollectionStatus(), CollectionStatus.COLLECTION_ERROR);

        when(movement.getStatus()).thenReturn(MovementStatus.NEEDS_INTERACTION);
        builder.addMovement(movement);
        verificationResponse = builder.build();
        assertEquals(verificationResponse.getCollectionStatus(), CollectionStatus.ON_HOLD);

        when(movement.getStatus()).thenReturn(MovementStatus.INIT_INTERACTION);
        builder.addMovement(movement);
        verificationResponse = builder.build();
        assertEquals(verificationResponse.getCollectionStatus(), CollectionStatus.ON_HOLD);

        when(movement.getStatus()).thenReturn(MovementStatus.PAYERROR);
        builder.addMovement(movement);
        verificationResponse = builder.build();
        assertEquals(verificationResponse.getCollectionStatus(), CollectionStatus.COLLECTION_ERROR);

        try {
            when(movement.getStatus()).thenReturn(MovementStatus.REFUNDERR);
            builder.addMovement(movement);
            verificationResponse = builder.build();
            assertTrue(false);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }
}
