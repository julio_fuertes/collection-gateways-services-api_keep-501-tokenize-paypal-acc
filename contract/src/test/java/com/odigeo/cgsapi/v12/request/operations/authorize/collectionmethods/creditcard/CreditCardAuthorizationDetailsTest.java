package com.odigeo.cgsapi.v12.request.operations.authorize.collectionmethods.creditcard;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.RecurringDetails;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.common.PurchaserBuilder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class CreditCardAuthorizationDetailsTest {

    private static final Map<String, Integer> SOME_TEST_DIMENSIONS;

    static {
        SOME_TEST_DIMENSIONS = new HashMap<String, Integer>();
        SOME_TEST_DIMENSIONS.put("test1", 1);
        SOME_TEST_DIMENSIONS.put("test2", 2);
    }

    @Mock
    private CreditCard creditCard;
    private Money money = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));
    private CreditCardGatewayCollectionMethod creditCardGatewayCollectionMethod = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, CreditCardType.VISA_CREDIT);
    private CollectionContext collectionContext = new CollectionContext(Locale.US, "127.0.0.1", new PurchaserBuilder().buildOrderPurchaser(), "client reference id", "paymentCountryCode", "websiteCode", "brandName", "8F34AB751554ACB39A81903DF3E01481", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1", SOME_TEST_DIMENSIONS);
    private CollectionShoppingDetails collectionShoppingDetails = new CollectionShoppingDetails(1234L, null);
    private RecurringDetails recurringDetails = new RecurringDetails(true, "0123456789");


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void creditCardAuthorizationDetails() {
        when(creditCard.getType()).thenReturn(CreditCardType.VISA_CREDIT);
        CreditCardAuthorizationDetails creditCardAuthorizationDetails = new CreditCardAuthorizationDetails("123", money, creditCard, collectionContext, collectionShoppingDetails);
        creditCardAuthorizationDetails.setRecurringDetails(recurringDetails);
        assertEquals(creditCardAuthorizationDetails.getOrderId(), "123");
        assertEquals(creditCardAuthorizationDetails.getMoney(), money);
        assertEquals(creditCardAuthorizationDetails.getCreditCard(), creditCard);
        assertEquals(creditCardAuthorizationDetails.getGatewayCollectionMethod(), creditCardGatewayCollectionMethod);
        assertEquals(creditCardAuthorizationDetails.getCollectionContext(), collectionContext);
        assertEquals(creditCardAuthorizationDetails.getCollectionShoppingDetails(), collectionShoppingDetails);
        assertEquals(creditCardAuthorizationDetails.getRecurringDetails(), recurringDetails);
    }
}
