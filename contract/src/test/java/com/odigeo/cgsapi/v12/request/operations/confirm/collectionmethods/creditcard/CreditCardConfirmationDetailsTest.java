package com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.creditcard;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class CreditCardConfirmationDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private CreditCard creditCard;
    private CreditCardGatewayCollectionMethod creditCardGatewayCollectionMethod = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, CreditCardType.VISA_CREDIT);

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void creditCardConfirmationDetails() {
        CreditCardConfirmationDetails creditCardConfirmationDetails = new CreditCardConfirmationDetails("123", A_MONEY,
                creditCardGatewayCollectionMethod, creditCard);
        CreditCardConfirmationDetails creditCardConfirmationDetails2 = new CreditCardConfirmationDetails("111", A_MONEY,
                creditCardGatewayCollectionMethod, "dynamicDescriptor", creditCard);

        assertEquals(creditCardConfirmationDetails.getMerchantOrderId(), "123");
        assertEquals(creditCardConfirmationDetails.getMoney(), A_MONEY);
        assertEquals(creditCardConfirmationDetails.getCreditCard(), creditCard);
        assertEquals(creditCardConfirmationDetails.getGatewayCollectionMethod(), creditCardGatewayCollectionMethod);

        assertEquals(creditCardConfirmationDetails2.getDynamicDescriptor(), "dynamicDescriptor");
    }

    @Test
    public void testEquals() {
        CreditCardConfirmationDetails confirmationDetails = new CreditCardConfirmationDetails("111", A_MONEY, creditCardGatewayCollectionMethod, creditCard);
        CreditCardConfirmationDetails confirmationDetails2 = new CreditCardConfirmationDetails("222", A_MONEY, creditCardGatewayCollectionMethod, creditCard);
        assertTrue(confirmationDetails.equals(confirmationDetails));
        assertFalse(confirmationDetails.equals(confirmationDetails2));
        assertFalse(confirmationDetails.equals(null));
    }

    @Test
    public void testHashCode() {
        CreditCardConfirmationDetails confirmationDetails = new CreditCardConfirmationDetails("111", A_MONEY, creditCardGatewayCollectionMethod, creditCard);
        CreditCardConfirmationDetails confirmationDetails2 = new CreditCardConfirmationDetails("111", A_MONEY, creditCardGatewayCollectionMethod, creditCard);
        CreditCardConfirmationDetails confirmationDetails3 = new CreditCardConfirmationDetails("333", A_MONEY, creditCardGatewayCollectionMethod, creditCard);
        assertEquals(confirmationDetails.hashCode(), confirmationDetails2.hashCode());
        assertNotEquals(confirmationDetails.hashCode(), confirmationDetails3.hashCode());
    }
}
