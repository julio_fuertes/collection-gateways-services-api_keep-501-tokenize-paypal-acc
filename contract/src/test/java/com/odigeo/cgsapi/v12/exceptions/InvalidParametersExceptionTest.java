package com.odigeo.cgsapi.v12.exceptions;

import org.testng.annotations.Test;

import javax.ws.rs.core.Response;

import static org.testng.Assert.assertEquals;

public class InvalidParametersExceptionTest {

    @Test
    public void testSimple() {
        InvalidParametersException invalidParametersException = new InvalidParametersException("Test InvalidParamaterException");

        assertEquals(invalidParametersException.getMessage(), "Test InvalidParamaterException");
        assertEquals(invalidParametersException.getCause(), null);
        assertEquals((invalidParametersException).getResponse().getStatus(), Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    public void testWithCause() {
        Exception exception = new Exception("Test Throwable ");
        InvalidParametersException invalidParametersException = new InvalidParametersException("Test InvalidParamaterException", exception);

        assertEquals(invalidParametersException.getMessage(), "Test InvalidParamaterException");
        assertEquals(invalidParametersException.getCause(), exception);
        assertEquals((invalidParametersException).getResponse().getStatus(), Response.Status.BAD_REQUEST.getStatusCode());
    }
}
