package com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PaypalProtectionEligibilityTest {

    @DataProvider(name = "protectionEligibilityDataProvider")
    public Object[][] protectionEligibilityDataProvider() {
        return new Object[][]{
                {PaypalProtectionEligibility.ELIGIBLE, "Eligible"},
                {PaypalProtectionEligibility.INELIGIBLE, "Ineligible"},
                {PaypalProtectionEligibility.PARTIALLY_ELIGIBLE, "PartiallyEligible"}
        };
    }

    @Test(dataProvider = "protectionEligibilityDataProvider")
    public void testFindByCode(PaypalProtectionEligibility paypalProtectionEligibility, String code) {
        //Then
        Assert.assertEquals(PaypalProtectionEligibility.findByCode(code), paypalProtectionEligibility);
    }
}
