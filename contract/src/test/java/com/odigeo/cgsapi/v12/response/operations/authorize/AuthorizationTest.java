package com.odigeo.cgsapi.v12.response.operations.authorize;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.operations.authorize.AuthorizationDetails;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.RedirectionParameters;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class AuthorizationTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private AuthorizationDetails authorizationDetails;
    @Mock
    private RedirectionParameters redirectionParameters;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createAuthorization() {
        when(authorizationDetails.getOrderId()).thenReturn("123");
        Authorization authorization = Authorization.createAuthorization(authorizationDetails.getOrderId(), A_MONEY, "1", "error", MovementStatus.AUTHORIZED);

        assertNotNull(authorization);
        assertEquals(authorization.getMerchantOrderId(), "123");
        assertEquals(authorization.getAction(), MovementAction.AUTHORIZE);
        assertEquals(authorization.getStatus(), MovementStatus.AUTHORIZED);
    }

    @Test
    public void createInteractionAuthorization() {
        when(authorizationDetails.getOrderId()).thenReturn("123");
        Authorization authorization = Authorization.createInteractionAuthorization(authorizationDetails.getOrderId(), A_MONEY, "1", "error", redirectionParameters, MovementStatus.INIT_INTERACTION);

        assertNotNull(authorization);
        assertEquals(authorization.getMerchantOrderId(), "123");
        assertEquals(authorization.getAction(), MovementAction.AUTHORIZE);
        assertEquals(authorization.getStatus(), MovementStatus.INIT_INTERACTION);
        assertEquals(authorization.getRedirectionParameters(), redirectionParameters);
    }

    @Test
    public void createFailedAuthorization() {
        when(authorizationDetails.getOrderId()).thenReturn("123");
        Authorization authorization = Authorization.createFailedAuthorization(authorizationDetails.getOrderId(), A_MONEY, "1", "error");

        assertNotNull(authorization);
        assertEquals(authorization.getMerchantOrderId(), "123");
        assertEquals(authorization.getAction(), MovementAction.AUTHORIZE);
        assertEquals(authorization.getStatus(), MovementStatus.PAYERROR);
    }

    @Test
    public void createFakeAuthorization() {
        when(authorizationDetails.getOrderId()).thenReturn("123");
        Authorization authorization = Authorization.createFakeAuthorization(authorizationDetails.getOrderId(), A_MONEY, MovementStatus.PAYERROR);

        assertNotNull(authorization);
        assertEquals(authorization.getMerchantOrderId(), "123");
        assertEquals(authorization.getAction(), MovementAction.AUTHORIZE);
        assertEquals(authorization.getStatus(), MovementStatus.PAYERROR);
    }

    @Test
    public void equalsTest() {
        Authorization authorization1 = Authorization.createAuthorization("123", A_MONEY, "456", "error", MovementStatus.AUTHORIZED);
        Authorization authorization2 = Authorization.createAuthorization("123", A_MONEY, "456", "error", MovementStatus.AUTHORIZED);

        assertTrue(authorization1.equals(authorization1));
        assertTrue(authorization1.equals(authorization2));

        authorization2 = Authorization.createAuthorization("123", A_MONEY, "654", "error", MovementStatus.AUTHORIZED);

        assertFalse(authorization1.equals(authorization2));
        assertFalse(authorization1.equals(null));
        assertFalse(authorization1.equals(Boolean.TRUE));
    }

    @Test
    public void hashCodeTest() {
        Authorization authorization1 = Authorization.createAuthorization("123", A_MONEY, "456", "error", MovementStatus.AUTHORIZED);

        assertTrue(authorization1.hashCode() == authorization1.hashCode());

        Authorization authorization2 = Authorization.createAuthorization("321", A_MONEY, "654", "error", MovementStatus.AUTHORIZED);

        assertFalse(authorization1.hashCode() == authorization2.hashCode());
    }
}
