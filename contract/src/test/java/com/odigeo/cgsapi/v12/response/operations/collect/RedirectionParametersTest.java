package com.odigeo.cgsapi.v12.response.operations.collect;

import com.odigeo.cgsapi.v12.response.RedirectionHttpMethod;
import com.odigeo.cgsapi.v12.response.RedirectionParameters;
import com.odigeo.cgsapi.v12.response.RedirectionParametersBuilder;
import com.odigeo.cgsapi.v12.response.RedirectionType;
import org.mockito.Mockito;
import org.testng.annotations.Test;

import java.util.HashMap;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class RedirectionParametersTest {

    @Test
    public void redirectionParameters() {
        //Given
        HashMap<String, String> parameters = new HashMap<String, String>();
        RedirectionParametersBuilder builder = Mockito.mock(RedirectionParametersBuilder.class);
        Mockito.when(builder.getRedirectionType()).thenReturn(RedirectionType.FORM_REDIRECTION);
        Mockito.when(builder.getRedirectionStep()).thenReturn(1);
        Mockito.when(builder.getHtml()).thenReturn("html");
        Mockito.when(builder.getJavascriptSnippet()).thenReturn("javascriptSnippet");
        Mockito.when(builder.getParameters()).thenReturn(parameters);
        Mockito.when(builder.getRedirectionUrl()).thenReturn("redirectionUrl");
        Mockito.when(builder.getRedirectionMethod()).thenReturn(RedirectionHttpMethod.GET);
        //When
        RedirectionParameters redirectionParameters = new RedirectionParameters(builder);
        //Then
        assertEquals(redirectionParameters.getRedirectionType(), RedirectionType.FORM_REDIRECTION);
        assertEquals(redirectionParameters.getRedirectionStep().intValue(), 1);
        assertEquals(redirectionParameters.getHtml(), "html");
        assertEquals(redirectionParameters.getJavascriptSnippet(), "javascriptSnippet");
        assertEquals(redirectionParameters.getParameters(), parameters);
        assertEquals(redirectionParameters.getRedirectionUrl(), "redirectionUrl");
        assertEquals(redirectionParameters.getRedirectionMethod(), RedirectionHttpMethod.GET);
    }

    @Test
    public void testEquals() {
        HashMap<String, String> parameters = new HashMap<String, String>();
        RedirectionParametersBuilder builder = Mockito.mock(RedirectionParametersBuilder.class);
        Mockito.when(builder.getRedirectionType()).thenReturn(RedirectionType.FORM_REDIRECTION);
        Mockito.when(builder.getRedirectionStep()).thenReturn(1);
        Mockito.when(builder.getHtml()).thenReturn("html");
        Mockito.when(builder.getJavascriptSnippet()).thenReturn("javascriptSnippet");
        Mockito.when(builder.getParameters()).thenReturn(parameters);
        Mockito.when(builder.getRedirectionUrl()).thenReturn("redirectionUrl");
        Mockito.when(builder.getRedirectionMethod()).thenReturn(RedirectionHttpMethod.GET);

        RedirectionParameters redirectionParameters1 = new RedirectionParameters(builder);
        RedirectionParameters redirectionParameters2 = new RedirectionParameters(builder);

        assertTrue(redirectionParameters1.equals(redirectionParameters1));
        assertTrue(redirectionParameters1.equals(redirectionParameters2));

        Mockito.when(builder.getRedirectionMethod()).thenReturn(RedirectionHttpMethod.POST);
        redirectionParameters2 = new RedirectionParameters(builder);

        assertFalse(redirectionParameters1.equals(redirectionParameters2));
        assertFalse(redirectionParameters1.equals(null));
        assertFalse(redirectionParameters1.equals(Boolean.TRUE));
    }

    @Test
    public void testHashCode() {
        HashMap<String, String> parameters = new HashMap<String, String>();
        RedirectionParametersBuilder builder = Mockito.mock(RedirectionParametersBuilder.class);
        Mockito.when(builder.getRedirectionType()).thenReturn(RedirectionType.FORM_REDIRECTION);
        Mockito.when(builder.getRedirectionStep()).thenReturn(1);
        Mockito.when(builder.getHtml()).thenReturn("html");
        Mockito.when(builder.getJavascriptSnippet()).thenReturn("javascriptSnippet");
        Mockito.when(builder.getParameters()).thenReturn(parameters);
        Mockito.when(builder.getRedirectionUrl()).thenReturn("redirectionUrl");
        Mockito.when(builder.getRedirectionMethod()).thenReturn(RedirectionHttpMethod.GET);

        RedirectionParameters redirectionParameters1 = new RedirectionParameters(builder);
        RedirectionParameters redirectionParameters2 = new RedirectionParameters(builder);

        assertTrue(redirectionParameters1.hashCode() == redirectionParameters2.hashCode());

        Mockito.when(builder.getRedirectionMethod()).thenReturn(RedirectionHttpMethod.POST);
        redirectionParameters2 = new RedirectionParameters(builder);

        assertFalse(redirectionParameters1.hashCode() == redirectionParameters2.hashCode());
    }
}
