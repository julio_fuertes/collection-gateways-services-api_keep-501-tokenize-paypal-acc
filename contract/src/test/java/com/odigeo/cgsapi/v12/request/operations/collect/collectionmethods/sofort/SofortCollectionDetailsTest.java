package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.sofort;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * @author egonz
 * @since 16-may-2012
 */
public class SofortCollectionDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private CollectionContext collectionContext;
    @Mock
    private CollectionShoppingDetails collectionShoppingDetails;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void elvCollectionDetails() {
        SofortCollectionDetails sofortCollectionDetails = new SofortCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, "callbackUrl", "brand");

        assertEquals(sofortCollectionDetails.getOrderId(), "123");
        assertEquals(sofortCollectionDetails.getMoney(), A_MONEY);
        assertEquals(sofortCollectionDetails.getGatewayCollectionMethod(), new GatewayCollectionMethod(GatewayCollectionMethodType.SOFORT));
        assertEquals(sofortCollectionDetails.getCallbackUrl(), "callbackUrl");
        assertEquals(sofortCollectionDetails.getBrand(), "brand");
        assertEquals(sofortCollectionDetails.getCollectionContext(), collectionContext);
        assertEquals(sofortCollectionDetails.getCollectionShoppingDetails(), collectionShoppingDetails);

        SofortCollectionDetails sofortCollectionDetails2 = new SofortCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, "callbackUrl", "brand");
        assertTrue(sofortCollectionDetails.equals(sofortCollectionDetails2));
        assertEquals(sofortCollectionDetails.hashCode(), sofortCollectionDetails2.hashCode());
        assertNotNull(sofortCollectionDetails2.toString());
        assertTrue(sofortCollectionDetails2.equals(sofortCollectionDetails2));
        assertFalse(sofortCollectionDetails2.equals("test"));
        assertFalse(sofortCollectionDetails2.equals(null));
    }

    @Test
    public void toRefund() {
        SofortCollectionDetails sofortCollectionDetails = new SofortCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, "callbackUrl", "brand");
        RefundDetails refundDetails = sofortCollectionDetails.toRefundDetails("123", "123");

        assertEquals(refundDetails.getTransactionId(), "123");
    }

    @Test
    public void toConfirmationDetails() {
        SofortCollectionDetails sofortCollectionDetails = new SofortCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, "callbackUrl", "brand");
        ConfirmationDetails confirmationDetails = sofortCollectionDetails.toConfirmationDetails("123");

        assertEquals(confirmationDetails.getMerchantOrderId(), "123");
    }
}
