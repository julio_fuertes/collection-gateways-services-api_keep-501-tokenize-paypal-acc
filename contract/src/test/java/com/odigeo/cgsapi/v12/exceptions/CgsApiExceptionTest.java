package com.odigeo.cgsapi.v12.exceptions;

import org.testng.annotations.Test;

import javax.ws.rs.core.Response;

import static org.testng.Assert.assertEquals;

public class CgsApiExceptionTest {

    @Test
    public void testSimple() {

        CgsApiException cgsApiException = new CgsApiException("Test exception");

        assertEquals(cgsApiException.getMessage(), "Test exception");
        assertEquals(cgsApiException.getCause(), null);
        assertEquals((cgsApiException).getResponse().getStatus(), Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
    }

    @Test
    public void testWithThrowable() {

        Exception exception = new Exception("Test Throwable ");
        CgsApiException cgsApiException = new CgsApiException("Test exception", exception);

        assertEquals(cgsApiException.getMessage(), "Test exception");
        assertEquals(cgsApiException.getCause(), exception);
        assertEquals((cgsApiException).getResponse().getStatus(), Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());

    }

    @Test
    public void testWithStatus() {

        Exception exception = new Exception("Test Throwable ");
        CgsApiException cgsApiException = new CgsApiException("Test exception", exception, Response.Status.INTERNAL_SERVER_ERROR);

        assertEquals(cgsApiException.getMessage(), "Test exception");
        assertEquals(cgsApiException.getCause(), exception);
        assertEquals((cgsApiException).getResponse().getStatus(), Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());

    }

}
