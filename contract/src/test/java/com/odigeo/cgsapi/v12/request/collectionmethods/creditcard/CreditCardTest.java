package com.odigeo.cgsapi.v12.request.collectionmethods.creditcard;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class CreditCardTest {

    @Test
    public void creditCard() {
        CreditCard creditCard = new CreditCardBuilder().build();

        assertEquals(creditCard.getOwner(), "Pepe López");
        Assert.assertEquals(creditCard.getType(), CreditCardType.VISA_CREDIT);
        assertEquals(creditCard.getNumber(), "4111111111111111");
        assertEquals(creditCard.getVerificationValue(), "123");
        assertEquals(creditCard.getExpirationDate(), "1215");
        assertEquals(creditCard.getExpirationMonth(), "12");
        assertEquals(creditCard.getExpirationYear(), "15");
        assertEquals(creditCard.getType().getCode(), CreditCardType.VISA_CREDIT.getCode());

        assertEquals(CreditCardType.findByCode(CreditCardType.VISA_CREDIT.getCode()), CreditCardType.VISA_CREDIT);

        try {
            CreditCardType.findByCode("test");
            fail("error");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void equalsTest() {
        CreditCard creditCard1 = new CreditCardBuilder().build();
        CreditCard creditCard2 = new CreditCardBuilder().build();

        assertTrue(creditCard1.equals(creditCard1));
        assertTrue(creditCard1.equals(creditCard2));

        creditCard2 = new CreditCardBuilder().withNumber("4222222222222222").build();

        assertFalse(creditCard1.equals(creditCard2));
        assertFalse(creditCard1.equals(null));
        assertFalse(creditCard1.equals(Boolean.TRUE));
    }

    @Test
    public void hashCodeTest() {
        CreditCard creditCard1 = new CreditCardBuilder().build();
        CreditCard creditCard2 = new CreditCardBuilder().build();

        assertTrue(creditCard1.hashCode() == creditCard2.hashCode());

        creditCard2 = new CreditCardBuilder().withNumber("4222222222222222").build();

        assertFalse(creditCard1.hashCode() == creditCard2.hashCode());
    }
}
