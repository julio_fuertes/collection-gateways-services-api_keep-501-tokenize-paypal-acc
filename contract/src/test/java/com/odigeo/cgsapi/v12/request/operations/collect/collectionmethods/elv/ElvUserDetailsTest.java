package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.elv;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class ElvUserDetailsTest {

    @Test
    public void elvUserDetails() {
        ElvUserDetails userDetails = new ElvUserDetails("name", "lastName", "street", "streetNumber", "city", "zipCode", "countryCode");
        assertEquals(userDetails.getName(), "name");
        assertEquals(userDetails.getLastName(), "lastName");
        assertEquals(userDetails.getStreet(), "street");
        assertEquals(userDetails.getStreetNumber(), "streetNumber");
        assertEquals(userDetails.getCity(), "city");
        assertEquals(userDetails.getZipCode(), "zipCode");
        assertEquals(userDetails.getCountryCode(), "countryCode");

        ElvUserDetails userDetails2 = new ElvUserDetails("name", "lastName", "street", "streetNumber", "city", "zipCode", "countryCode");
        assertTrue(userDetails.equals(userDetails2));
        assertEquals(userDetails.hashCode(), userDetails2.hashCode());
        assertNotNull(userDetails2.toString());
        assertTrue(userDetails2.equals(userDetails2));
        assertFalse(userDetails2.equals("test"));
        assertFalse(userDetails2.equals(null));
    }
}
