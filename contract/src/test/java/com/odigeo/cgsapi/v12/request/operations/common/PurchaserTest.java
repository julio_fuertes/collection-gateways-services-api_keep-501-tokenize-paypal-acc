package com.odigeo.cgsapi.v12.request.operations.common;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class PurchaserTest {

    @Test
    public void orderPurchaser() {
        Purchaser purchaser = new PurchaserBuilder().buildOrderPurchaser();

        assertEquals(purchaser.getName(), "Pepe");
        assertEquals(purchaser.getLastNames(), "López");
        assertEquals(purchaser.getAddress(), "C/ Pez 25");
        assertEquals(purchaser.getZipCode(), "08008");
        assertEquals(purchaser.getCity(), "Barcelona");
        assertEquals(purchaser.getCountryCode(), "ES");
        assertEquals(purchaser.getEmail(), "pepe.lopez@edreams.com");
        assertEquals(purchaser.getTelephoneNumber(), "987654321");
    }

    @Test
    public void invoicingPurchaser() {
        Purchaser purchaser = new PurchaserBuilder().buildInvoicingPurchaser();

        assertNull(purchaser.getName());
        assertEquals(purchaser.getLastNames(), "López");
        assertEquals(purchaser.getAddress(), "C/ Pez 25");
        assertEquals(purchaser.getZipCode(), "08008");
        assertEquals(purchaser.getCity(), "Barcelona");
        assertEquals(purchaser.getCountryCode(), "ES");
        assertNull(purchaser.getEmail());
    }

    @Test
    public void invoicingPurchaserWithNullZipCodeForAustralianWebsites() {
        //Given
        PurchaserBuilder builder = new PurchaserBuilder();
        builder.withZipCode(null);
        //when
        Purchaser purchaser = builder.buildInvoicingPurchaser();
        //Then
        assertNull(purchaser.getName());
        assertEquals(purchaser.getLastNames(), "López");
        assertEquals(purchaser.getAddress(), "C/ Pez 25");
        assertNull(purchaser.getZipCode());
        assertEquals(purchaser.getCity(), "Barcelona");
        assertEquals(purchaser.getCountryCode(), "ES");
        assertNull(purchaser.getEmail());
    }

    @Test
    public void equalsTest() {
        Purchaser purchaser1 = new PurchaserBuilder().buildInvoicingPurchaser();
        Purchaser purchaser2 = new PurchaserBuilder().buildInvoicingPurchaser();

        assertTrue(purchaser1.equals(purchaser1));
        assertTrue(purchaser1.equals(purchaser2));

        purchaser2 = new PurchaserBuilder().withCountryCode("US").buildInvoicingPurchaser();

        assertFalse(purchaser1.equals(purchaser2));
        assertFalse(purchaser1.equals(null));
        assertFalse(purchaser1.equals(Boolean.TRUE));
    }

    @Test
    public void hashCodeTest() {
        Purchaser purchaser1 = new PurchaserBuilder().buildInvoicingPurchaser();
        Purchaser purchaser2 = new PurchaserBuilder().buildInvoicingPurchaser();

        assertTrue(purchaser1.hashCode() == purchaser2.hashCode());

        purchaser2 = new PurchaserBuilder().withCountryCode("US").buildInvoicingPurchaser();

        assertFalse(purchaser1.hashCode() == purchaser2.hashCode());
    }
}
