package com.odigeo.cgsapi.v12.request.operations.refund;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.operations.refund.Refund;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class RefundTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));
    private static final String A_MERCHANT_ORDER_ID = "123";
    private static final String ERROR_CODE = "1";
    private static final String ERROR_DESCRIPTION = "error";

    @Mock
    private RefundDetails refundDetails;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(refundDetails.getMerchantOrderId()).thenReturn(A_MERCHANT_ORDER_ID);
    }

    @Test
    public void createRefund() {
        Refund refund = Refund.createRefund(refundDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.REFUNDED);

        assertNotNull(refund);
        assertEquals(refund.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(refund.getCode(), ERROR_CODE);
        assertEquals(refund.getErrorDescription(), ERROR_DESCRIPTION);
        assertEquals(refund.getAction(), MovementAction.COLLECT);
        assertEquals(refund.getStatus(), MovementStatus.REFUNDED);

        refund = Refund.createRefund(refundDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, true);

        assertNotNull(refund);
        assertEquals(refund.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(refund.getCode(), ERROR_CODE);
        assertEquals(refund.getErrorDescription(), ERROR_DESCRIPTION);
        assertEquals(refund.getAction(), MovementAction.COLLECT);
        assertEquals(refund.getStatus(), MovementStatus.REFUNDED);

        refund = Refund.createRefund(refundDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, false);

        assertNotNull(refund);
        assertEquals(refund.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(refund.getCode(), ERROR_CODE);
        assertEquals(refund.getErrorDescription(), ERROR_DESCRIPTION);
        assertEquals(refund.getAction(), MovementAction.COLLECT);
        assertEquals(refund.getStatus(), MovementStatus.DECLINED);
    }

    @Test
    public void createFailedRefund() {
        Refund refund = Refund.createFailedRefund(refundDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION);

        assertNotNull(refund);
        assertEquals(refund.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(refund.getCode(), ERROR_CODE);
        assertEquals(refund.getErrorDescription(), ERROR_DESCRIPTION);
        assertEquals(refund.getAction(), MovementAction.COLLECT);
        assertEquals(refund.getStatus(), MovementStatus.REFUNDERR);
    }

    @Test
    public void createFakeRefund() {
        Refund refund = Refund.createFakeRefund(refundDetails.getMerchantOrderId(), A_MONEY, MovementStatus.REFUNDERR);

        assertNotNull(refund);
        assertEquals(refund.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(refund.getAction(), MovementAction.COLLECT);
        assertEquals(refund.getStatus(), MovementStatus.REFUNDERR);
    }

    @Test
    public void equalsTest() {
        Refund refund1 = Refund.createRefund(refundDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.REFUNDED);
        Refund refund2 = Refund.createRefund(refundDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.REFUNDED);

        assertTrue(refund1.equals(refund1));
        assertTrue(refund1.equals(refund2));

        refund2 = Refund.createRefund("321", A_MONEY, "654", ERROR_DESCRIPTION, MovementStatus.REFUNDED);

        assertFalse(refund1.equals(refund2));
        assertFalse(refund1.equals(null));
        assertFalse(refund1.equals(Boolean.TRUE));
    }

    @Test
    public void hashCodeTest() {
        Refund refund1 = Refund.createRefund(refundDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.REFUNDED);

        assertTrue(refund1.hashCode() == refund1.hashCode());

        Refund refund2 = Refund.createRefund("321", A_MONEY, "654", ERROR_DESCRIPTION, MovementStatus.REFUNDED);

        assertFalse(refund1.hashCode() == refund2.hashCode());
    }
}
