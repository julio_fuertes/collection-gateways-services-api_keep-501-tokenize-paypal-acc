package com.odigeo.cgsapi.v12.request.operations.common;

import org.testng.annotations.Test;

import java.util.Date;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class TravellerTest {


    @Test
    public void traveller() {
        Traveller traveller = TravellerBuilder.getTraveller();

        assertEquals(traveller.getTravellerNumber(), new Integer(1));
        assertEquals(traveller.getName(), "Samuel");
        assertEquals(traveller.getMiddleName(), "Lewis");
        assertEquals(traveller.getFirstLastName(), "Eto\'o");
        assertEquals(traveller.getSecondLastName(), "Camuñas");
        Date dateOfBirth = TravellerBuilder.getDate(1986, 10, 2);
        assertEquals(traveller.getDateOfBirth(), dateOfBirth);
    }

    @Test
    public void equalsTest() {
        Traveller traveller1 = TravellerBuilder.getTraveller();
        Traveller traveller2 = TravellerBuilder.getTraveller();

        assertTrue(traveller1.equals(traveller1));
        assertTrue(traveller1.equals(traveller2));

        traveller2 = TravellerBuilder.getTraveller2();

        assertFalse(traveller1.equals(traveller2));
        assertFalse(traveller1.equals(null));
        assertFalse(traveller1.equals(Boolean.TRUE));
    }

    @Test
    public void hashCodeTest() {
        Traveller traveller1 = new TravellerBuilder().getTraveller();
        Traveller traveller2 = new TravellerBuilder().getTraveller();

        assertTrue(traveller1.hashCode() == traveller2.hashCode());

        traveller2 = TravellerBuilder.getTraveller2();

        assertFalse(traveller1.hashCode() == traveller2.hashCode());
    }
}
