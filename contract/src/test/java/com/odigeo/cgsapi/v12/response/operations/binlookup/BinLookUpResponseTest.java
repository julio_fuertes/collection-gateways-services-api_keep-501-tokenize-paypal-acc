package com.odigeo.cgsapi.v12.response.operations.binlookup;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;
import org.testng.annotations.Test;

import java.util.Currency;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

/**
 * @author alain.munoz
 * @since 17/01/14
 */
public class BinLookUpResponseTest {

    @Test
    public void binLookUpResponseTest() {
        BinLookUp binLookUp = new BinLookUp("code", "error", "ES", 724);
        binLookUp.setCurrency(Currency.getInstance("EUR"));
        BinLookUpResponse binLookUpResponse = new BinLookUpResponse(binLookUp);
        binLookUpResponse.setGatewayCollectionMethod(new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, CreditCardType.VISA_CREDIT));

        assertEquals(binLookUpResponse.getGatewayCollectionMethod().getGatewayCollectionMethodType(), GatewayCollectionMethodType.CREDITCARD);
        assertEquals(binLookUpResponse.getNumCountryCode(), new Integer(724));
        assertEquals(binLookUpResponse.getCountryCode(), "ES");
        assertEquals(binLookUpResponse.getCollectionStatus(), CollectionStatus.INIT);
        assertNull(binLookUpResponse.getAmount());
        assertTrue(binLookUpResponse.getMovements().size() == 1);
        assertNull(binLookUpResponse.getCreditCardType());
        assertNotNull(binLookUpResponse.getCurrency());


        binLookUpResponse.setCountryCode("US");
        binLookUpResponse.setNumCountryCode(840);

        assertEquals(binLookUpResponse.getNumCountryCode(), new Integer(840));
        assertEquals(binLookUpResponse.getCountryCode(), "US");
    }
}
