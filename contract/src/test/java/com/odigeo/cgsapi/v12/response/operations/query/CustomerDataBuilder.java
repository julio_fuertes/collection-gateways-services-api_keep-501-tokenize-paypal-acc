package com.odigeo.cgsapi.v12.response.operations.query;

/**
 * Created by ADEFREIT on 22/07/2015.
 */
public class CustomerDataBuilder {

    private String firstName = "firstName";
    private String middleName = "middleName";
    private String lastName = "lastName";
    private String sufix = "sufix";
    private String email = "email";
    private String countryCode = "countryCode";
    private String business = "business";

    public static CustomerDataBuilder aCustomerData() {
        return new CustomerDataBuilder();
    }

    public CustomerData build() {
        CustomerData.Builder builder = new CustomerData.Builder();
        builder.firstName(firstName);
        builder.middleName(middleName);
        builder.lastName(lastName);
        builder.sufix(sufix);
        builder.email(email);
        builder.countryCode(countryCode);
        builder.business(business);
        return builder.build();
    }
}
