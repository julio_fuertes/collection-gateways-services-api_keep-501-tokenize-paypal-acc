package com.odigeo.cgsapi.v12.response.operations.collect;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.operations.collect.CollectionDetails;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.RedirectionParameters;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * Created by vrieraba on 26/05/2015.
 */
public class CollectionTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private CollectionDetails collectionDetails;
    @Mock
    private RedirectionParameters redirectionParameters;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(collectionDetails.getOrderId()).thenReturn("123");
    }


    @Test
    public void createCollection() {
        Collection collection = Collection.createCollection(collectionDetails.getOrderId(), A_MONEY, "1", "error", MovementStatus.PAID);

        assertNotNull(collection);
        assertEquals(collection.getMerchantOrderId(), "123");
        assertEquals(collection.getAction(), MovementAction.COLLECT);
        assertEquals(collection.getStatus(), MovementStatus.PAID);
    }

    @Test
    public void createInteractionCollection() {
        Collection collection = Collection.createInteractionCollection(collectionDetails.getOrderId(), A_MONEY, "1", "error", redirectionParameters, MovementStatus.INIT_INTERACTION);

        assertNotNull(collection);
        assertEquals(collection.getMerchantOrderId(), "123");
        assertEquals(collection.getAction(), MovementAction.COLLECT);
        assertEquals(collection.getStatus(), MovementStatus.INIT_INTERACTION);
        assertEquals(collection.getRedirectionParameters(), redirectionParameters);
    }

    @Test
    public void createFailedCollection() {
        Collection collection = Collection.createFailedCollection(collectionDetails.getOrderId(), A_MONEY, "1", "error");

        assertNotNull(collection);
        assertEquals(collection.getMerchantOrderId(), "123");
        assertEquals(collection.getAction(), MovementAction.COLLECT);
        assertEquals(collection.getStatus(), MovementStatus.PAYERROR);
    }

    @Test
    public void createFakeCollection() {
        Collection collection = Collection.createFakeCollection(collectionDetails.getOrderId(), A_MONEY, MovementStatus.PAYERROR);

        assertNotNull(collection);
        assertEquals(collection.getMerchantOrderId(), "123");
        assertEquals(collection.getAction(), MovementAction.COLLECT);
        assertEquals(collection.getStatus(), MovementStatus.PAYERROR);
    }

    @Test
    public void equalsTest() {
        Collection collection1 = Collection.createInteractionCollection("123", A_MONEY, "456", "error", redirectionParameters, MovementStatus.INIT_INTERACTION);
        Collection collection2 = Collection.createInteractionCollection("123", A_MONEY, "456", "error", redirectionParameters, MovementStatus.INIT_INTERACTION);

        assertTrue(collection1.equals(collection1));
        assertTrue(collection1.equals(collection2));

        collection2 = Collection.createInteractionCollection("123", A_MONEY, "654", "error", redirectionParameters, MovementStatus.INIT_INTERACTION);

        assertFalse(collection1.equals(collection2));
        assertFalse(collection1.equals(null));
        assertFalse(collection1.equals(Boolean.TRUE));
    }

    @Test
    public void hashCodeTest() {
        Collection collection1 = Collection.createInteractionCollection("123", A_MONEY, "456", "error", redirectionParameters, MovementStatus.INIT_INTERACTION);

        assertTrue(collection1.hashCode() == collection1.hashCode());

        Collection collection2 = Collection.createInteractionCollection("321", A_MONEY, "654", "error", redirectionParameters, MovementStatus.INIT_INTERACTION);

        assertFalse(collection1.hashCode() == collection2.hashCode());
    }
}
