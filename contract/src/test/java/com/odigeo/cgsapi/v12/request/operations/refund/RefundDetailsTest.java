package com.odigeo.cgsapi.v12.request.operations.refund;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;

public class RefundDetailsTest {

    private static final GatewayCollectionMethod A_GATEWAY_COLLECTION_METHOD = GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ALIPAY);
    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.56"), Currency.getInstance("EUR"));

    @Test
    public void testConstruction() {
        //When
        RefundDetails refundDetails = new RefundDetails("merchantOrderId", A_MONEY, A_GATEWAY_COLLECTION_METHOD, "transactionId");
        //Then
        assertEquals(refundDetails.getMerchantOrderId(), "merchantOrderId");
        assertEquals(refundDetails.getMoney(), A_MONEY);
        assertEquals(refundDetails.getGatewayCollectionMethod(), A_GATEWAY_COLLECTION_METHOD);
        assertEquals(refundDetails.getTransactionId(), "transactionId");
    }

}
