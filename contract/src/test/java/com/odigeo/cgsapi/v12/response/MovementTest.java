package com.odigeo.cgsapi.v12.response;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class MovementTest {

    @Test
    public void movement() {
        Movement movement = new MovementBuilder().build();

        assertEquals(movement.getMerchantOrderId(), "123");
        assertEquals(movement.getCode(), "123");
        Assert.assertEquals(movement.getAction(), MovementAction.COLLECT);
        assertEquals(movement.getErrorDescription(), "error");
        Assert.assertEquals(movement.getStatus(), MovementStatus.PAID);
        assertEquals(movement.getAmount(), new BigDecimal("12.35"));
        assertEquals(movement.getCurrency(), Currency.getInstance("EUR"));
        assertEquals(movement.getSecure(), "2");
        assertEquals(movement.getTerminalId(), "1");
        assertEquals(movement.getTransactionId(), "123");
        assertEquals(movement.getMerchantStatus(), "123");
        assertEquals(movement.getMerchantStatusSubcode(), "123");
        assertEquals(movement.getTransactionIdSub(), "15123");
        assertNull(movement.getErrorType());
        assertNull(movement.getAdditionalInformation());
        assertNotNull(movement.toString());
        assertNotNull(movement.getCreationDate());
    }

    @Test
    public void invalidCvv2() {
        Movement movement = new MovementBuilder().withErrorType(MovementErrorType.WRONG_CVV2).build();

        assertTrue(movement.isInvalidCvv2());

        movement = new MovementBuilder().withErrorType(MovementErrorType.WRONG_CREDITCARD_EXPIRATION_DATE).build();

        assertFalse(movement.isInvalidCvv2());
    }

    @Test
    public void fraudSuspicious() {
        Movement movement = new MovementBuilder().withErrorType(MovementErrorType.FRAUD_SUSPECT).build();

        assertTrue(movement.isFraudSuspicious());

        movement = new MovementBuilder().withErrorType(MovementErrorType.WRONG_CREDITCARD_EXPIRATION_DATE).build();

        assertFalse(movement.isFraudSuspicious());
    }

    @Test
    public void testProtocolVersion() {
        //Given
        String protocolVersion = "1.0.2";
        //When
        Movement movement = new MovementBuilder().withProtocolVersion(protocolVersion).build();
        //Then
        Assert.assertEquals(movement.getProtocolVersion(), protocolVersion);
    }

    @Test
    public void equalsTest() {
        Movement movement1 = new MovementBuilder().build();
        Movement movement2 = new MovementBuilder().build();

        assertTrue(movement1.equals(movement1));
        assertFalse(movement1.equals("test"));
        assertTrue(movement1.equals(movement2));
        assertFalse(movement2.equals("test"));


        movement2 = new MovementBuilder().withOrderId("456").build();

        assertFalse(movement1.equals(movement2));
        assertFalse(movement1.equals(null));
        assertFalse(movement1.equals(Boolean.TRUE));
    }

    @Test
    public void hashCodeTest() {
        Movement movement1 = new MovementBuilder().build();
        Movement movement2 = new MovementBuilder().build();

        assertTrue(movement1.hashCode() == movement1.hashCode());

        // apart from the creation date, the rest should be the same
        assertEquals(movement1.hashCode() == movement2.hashCode(), movement1.getCreationDate().equals(movement2.getCreationDate()));

        movement2 = new MovementBuilder().withOrderId("456").build();

        assertFalse(movement1.hashCode() == movement2.hashCode());
    }
}
