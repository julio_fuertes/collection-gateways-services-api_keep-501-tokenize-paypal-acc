package com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.unionpay;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;

public class UnionpayRefundDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Test
    public void unionpayConfirmationDetailsTest() {
        UnionpayRefundDetails details = new UnionpayRefundDetails("123", A_MONEY, "transactionId");
        assertEquals(details.getMerchantOrderId(), "123");
        assertEquals(details.getGatewayCollectionMethod().getGatewayCollectionMethodType(), GatewayCollectionMethodType.UNIONPAY);
        assertEquals(details.getMoney(), A_MONEY);
        assertEquals(details.getTransactionId(), "transactionId");
    }
}
