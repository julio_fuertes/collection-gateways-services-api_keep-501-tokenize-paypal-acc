package com.odigeo.cgsapi.v12.request.operations.cancel;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class CancelDetailsTest {

    @Test
    public void cancelDetailsCreation() {
        CancelDetails cancelDetails = new CancelDetails("123", GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ELV));
        assertEquals(cancelDetails.getMerchantOrderId(), "123");
        assertEquals(cancelDetails.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ELV));
        assertNull(cancelDetails.getMoney());

        cancelDetails = new CancelDetails("123", GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ELV), Money.createMoney(BigDecimal.ONE, Currency.getInstance("EUR")));
        assertNotNull(cancelDetails.getMoney());
        assertEquals(cancelDetails.getMoney().getAmount(), BigDecimal.ONE);
    }
}
