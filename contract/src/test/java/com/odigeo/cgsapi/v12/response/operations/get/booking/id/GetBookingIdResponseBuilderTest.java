package com.odigeo.cgsapi.v12.response.operations.get.booking.id;

import org.testng.Assert;
import org.testng.annotations.Test;

public class GetBookingIdResponseBuilderTest {

    @Test
    public void testBuild() {
        //Given
        GetBookingIdResponseBuilder builder = new GetBookingIdResponseBuilder();
        builder.bookingId("bookingId");
        //When
        GetBookingIdResponse getBookingIdResponse = builder.build();
        //Then
        Assert.assertEquals(getBookingIdResponse.getBookingId(), "bookingId");
    }
}
