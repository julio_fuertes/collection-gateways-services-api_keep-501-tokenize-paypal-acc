package com.odigeo.cgsapi.v12.request.operations.verify.collectionmethods.secure3d;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.RecurringDetails;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.assertEquals;

/**
 * Created by vrieraba on 21/06/2016.
 */
public class Secure3dVerificationDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));
    private static final GatewayCollectionMethod A_GATEWAY_COLLECTION_METHOD = new GatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D);
    private static final Map<String, String> SOME_PARAMS;

    static {
        SOME_PARAMS = new HashMap<String, String>();
        SOME_PARAMS.put("param1", "value1");
        SOME_PARAMS.put("param2", "value2");
    }

    @Mock
    private CreditCard creditCard;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void creditCardVerificationDetails() {
        CollectionContext collectionContext = Mockito.mock(CollectionContext.class);
        Secure3dVerificationDetails secure3dVerificationDetails = new Secure3dVerificationDetails("12345", A_MONEY, A_GATEWAY_COLLECTION_METHOD, SOME_PARAMS, CollectionActionType.COLLECT, creditCard, collectionContext, 1, "finishUrl");
        RecurringDetails recurringDetails = Mockito.mock(RecurringDetails.class);
        secure3dVerificationDetails.setRecurringDetails(recurringDetails);

        assertEquals(secure3dVerificationDetails.getMerchantOrderId(), "12345");
        assertEquals(secure3dVerificationDetails.getMoney(), A_MONEY);
        assertEquals(secure3dVerificationDetails.getParameters(), SOME_PARAMS);
        assertEquals(secure3dVerificationDetails.getGatewayCollectionMethod(), A_GATEWAY_COLLECTION_METHOD);
        assertEquals(secure3dVerificationDetails.getCollectionActionType(), CollectionActionType.COLLECT);
        assertEquals(secure3dVerificationDetails.getCreditCard(), creditCard);
        assertEquals(secure3dVerificationDetails.getInteractionStep().intValue(), 1);
        assertEquals(secure3dVerificationDetails.getCollectionContext(), collectionContext);
        assertEquals(secure3dVerificationDetails.getFinishUrl(), "finishUrl");
        assertEquals(secure3dVerificationDetails.getRecurringDetails(), recurringDetails);
    }

}
