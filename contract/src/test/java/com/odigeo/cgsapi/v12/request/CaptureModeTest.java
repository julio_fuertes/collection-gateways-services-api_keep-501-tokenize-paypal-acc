package com.odigeo.cgsapi.v12.request;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by vrieraba on 12/06/2015.
 */
public class CaptureModeTest {

    @Test
    public void test() {
        AssertJUnit.assertEquals(CaptureMode.findByCode("ALWAYS_ONLINE"), CaptureMode.ALWAYS_ONLINE);
        assertEquals(CaptureMode.findByCode("DEFERRABLE"), CaptureMode.DEFERRABLE);
        assertEquals(CaptureMode.findByCode("NONE"), CaptureMode.NONE);

        try {
            CaptureMode.findByCode("X");
            assertTrue(false);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }
}
