package com.odigeo.cgsapi.v12.request.collectionmethods;

import com.edreams.util.logic.ThreeValuedLogic;
import com.odigeo.cgsapi.v12.request.CollectionActionType;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class GatewayCollectionMethodTest {

    private static final int ID = 122;
    private static final CollectionActionType COLLECTION_ACTION_TYPE = CollectionActionType.AUTHORIZE;

    @Test
    public void collectionMethodTest() {
        GatewayCollectionMethod gatewayCollectionMethod = new GatewayCollectionMethod(GatewayCollectionMethodType.ELV);
        gatewayCollectionMethod.setId(ID);
        assertEquals(gatewayCollectionMethod.getGatewayCollectionMethodType(), GatewayCollectionMethodType.ELV);
        assertEquals(gatewayCollectionMethod.isChargeBackable(), ThreeValuedLogic.TRUE);
        assertEquals(gatewayCollectionMethod.getId(), ID);
        assertNotNull(gatewayCollectionMethod.toString());
    }

    @Test
    public void valueOf() {
        assertEquals(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ELV), new GatewayCollectionMethod(GatewayCollectionMethodType.ELV));
        assertEquals(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL), new GatewayCollectionMethod(GatewayCollectionMethodType.PAYPAL));
        assertEquals(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.COFINOGA), new GatewayCollectionMethod(GatewayCollectionMethodType.COFINOGA));
        assertEquals(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.BANK_TRANSFER), new GatewayCollectionMethod(GatewayCollectionMethodType.BANK_TRANSFER));
        assertEquals(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.SOFORT), new GatewayCollectionMethod(GatewayCollectionMethodType.SOFORT));

        try {
            GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.CREDITCARD);
            assertTrue(false);
        } catch (UnsupportedOperationException e) {
            assertTrue(true);
        }
    }

    @Test
    public void equalsTest() {
        GatewayCollectionMethod gatewayCollectionMethod1 = new GatewayCollectionMethod(GatewayCollectionMethodType.PAYPAL);
        GatewayCollectionMethod gatewayCollectionMethod2 = new GatewayCollectionMethod(GatewayCollectionMethodType.PAYPAL);

        assertTrue(gatewayCollectionMethod1.equals(gatewayCollectionMethod1));
        assertTrue(gatewayCollectionMethod1.equals(gatewayCollectionMethod2));

        gatewayCollectionMethod2 = GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ELV);

        assertFalse(gatewayCollectionMethod1.equals(gatewayCollectionMethod2));
        assertFalse(gatewayCollectionMethod1.equals(null));
        assertFalse(gatewayCollectionMethod1.equals(Boolean.TRUE));
    }

    @Test
    public void hashCodeTest() {
        GatewayCollectionMethod gatewayCollectionMethod1 = new GatewayCollectionMethod(GatewayCollectionMethodType.PAYPAL);
        GatewayCollectionMethod gatewayCollectionMethod2 = new GatewayCollectionMethod(GatewayCollectionMethodType.PAYPAL);

        assertTrue(gatewayCollectionMethod1.hashCode() == gatewayCollectionMethod2.hashCode());

        gatewayCollectionMethod2 = GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ELV);

        assertFalse(gatewayCollectionMethod1.hashCode() == gatewayCollectionMethod2.hashCode());

        gatewayCollectionMethod2 = new GatewayCollectionMethod(null);

        assertFalse(gatewayCollectionMethod1.hashCode() == gatewayCollectionMethod2.hashCode());
    }
}
