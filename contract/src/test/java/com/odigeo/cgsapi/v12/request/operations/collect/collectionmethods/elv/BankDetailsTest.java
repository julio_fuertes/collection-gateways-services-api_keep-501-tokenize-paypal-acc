package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.elv;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class BankDetailsTest {

    @Test
    public void userDetails() {
        BankDetails bankDetails = new BankDetails("bankAccountNumber", "bankLocationId");
        assertEquals(bankDetails.getBankAccountNumber(), "bankAccountNumber");
        assertEquals(bankDetails.getBankLocationId(), "bankLocationId");

        BankDetails bankDetails2 = new BankDetails("bankAccountNumber", "bankLocationId");
        assertTrue(bankDetails.equals(bankDetails2));
        assertEquals(bankDetails.hashCode(), bankDetails2.hashCode());
        assertNotNull(bankDetails2.toString());
        assertTrue(bankDetails2.equals(bankDetails2));
        assertFalse(bankDetails2.equals("test"));
        assertFalse(bankDetails2.equals(null));
    }
}
