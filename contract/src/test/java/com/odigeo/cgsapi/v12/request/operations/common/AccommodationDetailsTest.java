package com.odigeo.cgsapi.v12.request.operations.common;

import com.odigeo.cgsapi.v12.LocalizedDate;
import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.Test;

import java.util.Calendar;
import java.util.TimeZone;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class AccommodationDetailsTest {


    @Test
    public void testMembers() throws Exception {
        AccommodationDetails accommodationDetails = AccommodationDetailsBuilder.getAccommodationDetails();
        Calendar cal = Calendar.getInstance();
        cal.set(2016, 4, 21);
        LocalizedDate checkInDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());
        cal.set(2016, 4, 24);
        LocalizedDate checkOutDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());

        assertEquals(accommodationDetails.getHotelName(), "Ace Hotel New York");
        assertEquals(accommodationDetails.getCheckInDate(), checkInDate);
        assertEquals(accommodationDetails.getCheckOutDate(), checkOutDate);
        assertEquals(accommodationDetails.getAddress(), "20 W 29th St");
        assertEquals(accommodationDetails.getCountry(), "USA");
    }


    @Test
    public void testEquals() throws Exception {
        AccommodationDetails accommodationDetails1 = AccommodationDetailsBuilder.getAccommodationDetails();
        AccommodationDetails accommodationDetails2 = AccommodationDetailsBuilder.getAccommodationDetails();

        assertTrue(accommodationDetails1.equals(accommodationDetails1));
        assertTrue(accommodationDetails1.equals(accommodationDetails2));

        accommodationDetails2 = AccommodationDetailsBuilder.getAccommodationDetails2();

        assertFalse(accommodationDetails1.equals(accommodationDetails2));
        assertFalse(accommodationDetails1.equals("test"));
        assertFalse(accommodationDetails1.equals(null));
    }

    @Test
    public void testHashCode() throws Exception {
        AccommodationDetails accommodationDetails1 = AccommodationDetailsBuilder.getAccommodationDetails();
        AccommodationDetails accommodationDetails2 = AccommodationDetailsBuilder.getAccommodationDetails();

        assertTrue(accommodationDetails1.hashCode() == accommodationDetails1.hashCode());
        assertTrue(accommodationDetails1.hashCode() == accommodationDetails2.hashCode());

        accommodationDetails2 = AccommodationDetailsBuilder.getAccommodationDetails2();

        assertFalse(accommodationDetails1.hashCode() == accommodationDetails2.hashCode());
    }
}
