package com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.cofinoga;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.cofinoga.CofinogaCard;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;

public class CofinogaCardRefundDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));
    private CofinogaCard cofinogaCard = new CofinogaCard("owner", "number", "01", "12", "1988");

    @Test
    public void cofinogaCardConfirmationDetailsTest() {
        CofinogaCardRefundDetails details = new CofinogaCardRefundDetails("123", A_MONEY, cofinogaCard, "transactionId");
        assertEquals(details.getCofinogaCard(), cofinogaCard);
        assertEquals(details.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.COFINOGA));
        assertEquals(details.getMoney(), A_MONEY);
        assertEquals(details.getMerchantOrderId(), "123");
        assertEquals(details.getTransactionId(), "transactionId");
    }
}
