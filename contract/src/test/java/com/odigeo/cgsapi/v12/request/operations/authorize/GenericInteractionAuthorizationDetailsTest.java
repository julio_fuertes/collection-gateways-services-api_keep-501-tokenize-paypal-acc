package com.odigeo.cgsapi.v12.request.operations.authorize;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

public class GenericInteractionAuthorizationDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    private static final String ORDER_ID = "123";
    private static final GatewayCollectionMethod A_GATEWAY_COLLECTION_METHOD = GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL);
    private static final String RESUME_BASE_URL = "https://www.foo.com/resume";

    @Mock
    private CollectionContext collectionContext;
    @Mock
    private CollectionShoppingDetails collectionShoppingDetails;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testConstructor() {
        GenericInteractionAuthorizationDetails details = new GenericInteractionAuthorizationDetails(ORDER_ID, A_MONEY, A_GATEWAY_COLLECTION_METHOD, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);

        assertEquals(details.getOrderId(), ORDER_ID);
        assertEquals(details.getMoney(), A_MONEY);
        assertEquals(details.getGatewayCollectionMethod(), A_GATEWAY_COLLECTION_METHOD);
        assertEquals(details.getCollectionContext(), collectionContext);
        assertEquals(details.getCollectionShoppingDetails(), collectionShoppingDetails);
        assertEquals(details.getResumeBaseUrl(), RESUME_BASE_URL);
    }

    @Test
    public void testConstructorInvalidCollectionMethodException() {
        //Given
        GatewayCollectionMethod aNullGatewayCollectionMethod = null;
        try {
            //When
            new GenericInteractionAuthorizationDetails(ORDER_ID, A_MONEY, aNullGatewayCollectionMethod, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);
            fail();
        } catch (IllegalArgumentException e) {
            //Then
            assertEquals(e.getMessage(), "Invalid GatewayCollectionMethod");
        }
    }

    @Test
    public void testEquals() {
        GenericInteractionAuthorizationDetails details1 = new GenericInteractionAuthorizationDetails(ORDER_ID, A_MONEY, A_GATEWAY_COLLECTION_METHOD, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);
        GenericInteractionAuthorizationDetails details2 = new GenericInteractionAuthorizationDetails("12345", A_MONEY, A_GATEWAY_COLLECTION_METHOD, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);

        assertTrue(details1.equals(details1));
        assertFalse(details1.equals(details2));
    }
}
