package com.odigeo.cgsapi.v12.request.operations.authorize;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.common.PurchaserBuilder;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.testng.Assert.assertEquals;

/**
 * Created by dvillena on 17/10/2016.
 */
public class AuthorizationDetailsTest {

    private static final Map<String, Integer> SOME_TEST_DIMENSIONS;

    static {
        SOME_TEST_DIMENSIONS = new HashMap<String, Integer>();
        SOME_TEST_DIMENSIONS.put("test1", 1);
        SOME_TEST_DIMENSIONS.put("test2", 2);
    }

    private Money money = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));
    private GatewayCollectionMethod gatewayCollectionMethod = GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.SOFORT);
    private CollectionContext collectionContext = new CollectionContext(Locale.US, "127.0.0.1", new PurchaserBuilder().buildOrderPurchaser(), "client reference id", "paymentCountryCode", "websiteCode", "brandName", "8F34AB751554ACB39A81903DF3E01481", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1", SOME_TEST_DIMENSIONS);
    private CollectionShoppingDetails collectionShoppingDetails = new CollectionShoppingDetails(1234L, null);

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void authorizationDetails() {
        AuthorizationDetails authorizationDetails = new AuthorizationDetails("123", money, gatewayCollectionMethod, collectionContext, collectionShoppingDetails);

        assertEquals(authorizationDetails.getOrderId(), "123");
        assertEquals(authorizationDetails.getMoney(), money);
        assertEquals(authorizationDetails.getGatewayCollectionMethod(), gatewayCollectionMethod);
        assertEquals(authorizationDetails.getCollectionContext(), collectionContext);
        assertEquals(authorizationDetails.getCollectionShoppingDetails(), collectionShoppingDetails);
    }

}
