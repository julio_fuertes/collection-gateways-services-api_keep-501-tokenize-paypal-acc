package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.giropay;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.giropay.GiropayRefundDetails;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * Date: 16/07/13
 */
public class GiropayCollectionDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private CollectionContext collectionContext;
    @Mock
    private CollectionShoppingDetails collectionShoppingDetails;

    private String finishUrl = "www.foo.com";
    private String accountOwnerSurname = "SMITH";
    private String ibanAccountNumber = "ES2200555045065";
    private String bicBankCode = "LAXXA22";

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void giropayCollectionDetailsWithUserDetails() {
        GiropayCollectionDetails giropayCollectionDetails = new GiropayCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, accountOwnerSurname, ibanAccountNumber, bicBankCode, finishUrl);

        assertEquals(giropayCollectionDetails.getOrderId(), "123");
        assertEquals(giropayCollectionDetails.getMoney(), A_MONEY);
        assertEquals(giropayCollectionDetails.getGatewayCollectionMethod(), new GatewayCollectionMethod(GatewayCollectionMethodType.GIROPAY));
        assertEquals(giropayCollectionDetails.getAccountOwnerSurname(), "SMITH");

        assertEquals(giropayCollectionDetails.getIbanAccountNumber(), "ES2200555045065");
        assertEquals(giropayCollectionDetails.getBicBankCode(), "LAXXA22");
        assertEquals(giropayCollectionDetails.getFinishUrl(), "www.foo.com");
        assertEquals(giropayCollectionDetails.getCollectionContext(), collectionContext);

        GiropayCollectionDetails giropayCollectionDetails2 = new GiropayCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, accountOwnerSurname, ibanAccountNumber, bicBankCode, finishUrl);
        assertTrue(giropayCollectionDetails.equals(giropayCollectionDetails2));
        assertEquals(giropayCollectionDetails.hashCode(), giropayCollectionDetails2.hashCode());
        assertNotNull(giropayCollectionDetails2.toString());
        assertTrue(giropayCollectionDetails2.equals(giropayCollectionDetails2));
        assertFalse(giropayCollectionDetails2.equals("test"));
        assertFalse(giropayCollectionDetails2.equals(null));
    }

    @Test
    public void toRefund() {
        GiropayCollectionDetails giropayCollectionDetails = new GiropayCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, accountOwnerSurname, ibanAccountNumber, bicBankCode, finishUrl);
        RefundDetails refundDetails = giropayCollectionDetails.toRefundDetails("123", "123");

        assertTrue(refundDetails instanceof GiropayRefundDetails);
        assertEquals(refundDetails.getTransactionId(), "123");
        assertEquals(((GiropayRefundDetails) refundDetails).getIbanAccountNumber(), "ES2200555045065");
        assertEquals(((GiropayRefundDetails) refundDetails).getAccountOwnerSurname(), "SMITH");
    }

    @Test
    public void toConfirmationDetails() {
        GiropayCollectionDetails giropayCollectionDetails = new GiropayCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, accountOwnerSurname, ibanAccountNumber, bicBankCode, finishUrl);
        ConfirmationDetails confirmationDetails = giropayCollectionDetails.toConfirmationDetails("123");

        assertEquals(confirmationDetails.getMerchantOrderId(), "123");
    }

}
