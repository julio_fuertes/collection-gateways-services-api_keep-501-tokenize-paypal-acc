package com.odigeo.cgsapi.v12.request.operations.collect;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

public class GenericInteractionCollectionDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    private static final String ORDER_ID = "123";
    private static final GatewayCollectionMethod A_GATEWAY_COLLECTION_METHOD = GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL);
    private static final String RESUME_BASE_URL = "https://www.foo.com/resume";

    @Mock
    private CollectionContext collectionContext;
    @Mock
    private CollectionShoppingDetails collectionShoppingDetails;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testConstructor() {
        GenericInteractionCollectionDetails genericInteractionCollectionDetails = new GenericInteractionCollectionDetails(ORDER_ID, A_MONEY, A_GATEWAY_COLLECTION_METHOD, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);

        assertEquals(genericInteractionCollectionDetails.getOrderId(), ORDER_ID);
        assertEquals(genericInteractionCollectionDetails.getMoney(), A_MONEY);
        assertEquals(genericInteractionCollectionDetails.getGatewayCollectionMethod(), A_GATEWAY_COLLECTION_METHOD);
        assertEquals(genericInteractionCollectionDetails.getCollectionContext(), collectionContext);
        assertEquals(genericInteractionCollectionDetails.getCollectionShoppingDetails(), collectionShoppingDetails);
        assertEquals(genericInteractionCollectionDetails.getResumeBaseUrl(), RESUME_BASE_URL);
    }

    @Test
    public void testConstructorInvalidCollectionMethodException() {
        //Given
        GatewayCollectionMethod aNullGatewayCollectionMethod = null;
        try {
            //When
            new GenericInteractionCollectionDetails(ORDER_ID, A_MONEY, aNullGatewayCollectionMethod, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);
            fail();
        } catch (IllegalArgumentException e) {
            //Then
            assertEquals(e.getMessage(), "Invalid GatewayCollectionMethod");
        }
    }

    @Test
    public void testEquals() {
        GenericInteractionCollectionDetails details1 = new GenericInteractionCollectionDetails(ORDER_ID, A_MONEY, A_GATEWAY_COLLECTION_METHOD, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);
        GenericInteractionCollectionDetails details2 = new GenericInteractionCollectionDetails("12345", A_MONEY, A_GATEWAY_COLLECTION_METHOD, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);

        assertTrue(details1.equals(details1));
        assertFalse(details1.equals(details2));
    }
}
