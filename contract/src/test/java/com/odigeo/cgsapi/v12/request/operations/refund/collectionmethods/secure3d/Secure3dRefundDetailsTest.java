package com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.secure3d;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class Secure3dRefundDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private CreditCard creditCard;
    private CreditCardGatewayCollectionMethod secure3dGatewayCollectionMethod = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D, CreditCardType.VISA_CREDIT);

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void secure3dRefundDetails() {
        when(creditCard.getType()).thenReturn(CreditCardType.VISA_CREDIT);
        Secure3dRefundDetails secure3dRefundDetails = new Secure3dRefundDetails("123", A_MONEY, creditCard, "456");

        assertEquals(secure3dRefundDetails.getMerchantOrderId(), "123");
        assertEquals(secure3dRefundDetails.getMoney(), A_MONEY);
        assertEquals(secure3dRefundDetails.getCreditCard(), creditCard);
        assertEquals(secure3dRefundDetails.getGatewayCollectionMethod(), secure3dGatewayCollectionMethod);
        assertEquals(secure3dRefundDetails.getTransactionId(), "456");
    }
}
