package com.odigeo.cgsapi.v12.request.collectionmethods.cofinoga;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;

/**
 * Tests for {@link CofinogaCard}
 *
 * @author egonz
 */
public class CofinogaCardTest {

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void cofinogaCardWithWrongDateTest() {
        String owner = "owner";
        String number = "number";
        String dayOfBirth = "day";
        String monthOfBirth = "month";
        String yearOfBirth = "year";
        CofinogaCard cofinogaCard = new CofinogaCard(owner, number, dayOfBirth, monthOfBirth, yearOfBirth);
        assertEquals(cofinogaCard.getOwner(), owner);
        assertEquals(cofinogaCard.getNumber(), number);
        assertEquals(cofinogaCard.getDayOfBirth(), dayOfBirth);
        assertEquals(cofinogaCard.getMonthOfBirth(), monthOfBirth);
        assertEquals(cofinogaCard.getYearOfBirth(), yearOfBirth);
    }

    @Test
    public void cofinogaCardTest() {
        String owner = "owner";
        String number = "number";
        String dayOfBirth = "12";
        String monthOfBirth = "12";
        String yearOfBirth = "1988";
        CofinogaCard cofinogaCard = new CofinogaCard(owner, number, dayOfBirth, monthOfBirth, yearOfBirth);
        assertEquals(cofinogaCard.getOwner(), owner);
        assertEquals(cofinogaCard.getNumber(), number);
        assertEquals(cofinogaCard.getDayOfBirth(), dayOfBirth);
        assertEquals(cofinogaCard.getMonthOfBirth(), monthOfBirth);
        assertEquals(cofinogaCard.getYearOfBirth(), yearOfBirth);
    }

    @Test
    public void hashTest() {
        String owner = "owner";
        String number = "number";
        String dayOfBirth = "12";
        String monthOfBirth = "12";
        String yearOfBirth = "1988";
        CofinogaCard cofinogaCard = new CofinogaCard(owner, number, dayOfBirth, monthOfBirth, yearOfBirth);
        CofinogaCard cofinogaCard2 = new CofinogaCard(owner, number, dayOfBirth, monthOfBirth, yearOfBirth);
        assertEquals(cofinogaCard.hashCode(), cofinogaCard2.hashCode());
    }

    @Test
    public void equalsTest() {
        String owner = "owner";
        String number = "number";
        String dayOfBirth = "12";
        String monthOfBirth = "12";
        String yearOfBirth = "1988";
        CofinogaCard cofinogaCard = new CofinogaCard(owner, number, dayOfBirth, monthOfBirth, yearOfBirth);
        CofinogaCard cofinogaCard2 = new CofinogaCard(owner, number, dayOfBirth, monthOfBirth, yearOfBirth);
        assertEquals(cofinogaCard, cofinogaCard2);
        assertNotEquals(cofinogaCard, null);
        assertNotEquals(cofinogaCard, "");
        Assert.assertNotNull(cofinogaCard.toString());
        assertNotNull(cofinogaCard.toString());
        assertFalse(cofinogaCard.equals(null));
        assertFalse(cofinogaCard.equals("test"));
    }
}
