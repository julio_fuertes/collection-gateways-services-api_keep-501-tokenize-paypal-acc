package com.odigeo.cgsapi.v12.request.collectionmethods.klarna;

import org.testng.Assert;
import org.testng.annotations.Test;

public class AddressTest {

    @Test
    public void testConstruction() {
        //Given
        String street = "an street";
        String city = "a city";
        String countryCode = "a countryCode";
        String houseNumberOrName = "a house number";
        //When
        Address address = new Address(street, houseNumberOrName, city, countryCode);
        //Then
        Assert.assertEquals(address.getStreet(), "an street");
        Assert.assertEquals(address.getHouseNumberOrName(), "a house number");
        Assert.assertEquals(address.getCity(), "a city");
        Assert.assertEquals(address.getCountryCode(), "a countryCode");
    }

    @Test
    public void testSetPostalCode() {
        //Given
        Address address = new Address("a street", "a house number", "city", "country");
        //When
        address.setPostalCode("a postal code");
        //Then
        Assert.assertEquals(address.getPostalCode(), "a postal code");
    }

    @Test
    public void testEquals() {
        //Given
        Address address1 = new Address("a street", "a house number", "city", "country");
        Address address2 = new Address("a street", "a house number", "city", "country");
        //When
        boolean equals = address1.equals(address2);
        //Then
        Assert.assertTrue(equals);
    }

    @Test
    public void testNotEquals() {
        //Given
        Address address1 = new Address("a street", "a house number", "city", "country");
        Address address2 = new Address("a street2", "a house number", "city", "country");
        //When
        boolean equals = address1.equals(address2);
        //Then
        Assert.assertFalse(equals);
    }


    @Test
    public void testHashCode() {
        //Given
        Address address1 = new Address("a street", "a house number", "city", "country");
        Address address2 = new Address("a street", "a house number", "city", "country");
        //When
        int hashCode1 = address1.hashCode();
        int hashCode2 = address2.hashCode();
        //Then
        Assert.assertEquals(hashCode1, hashCode2);
    }

}
