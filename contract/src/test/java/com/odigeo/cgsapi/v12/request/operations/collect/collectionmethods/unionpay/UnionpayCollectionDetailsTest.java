package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.unionpay;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.collect.CollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementContainer;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class UnionpayCollectionDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private CollectionContext collectionContext;
    @Mock
    private CollectionShoppingDetails collectionShoppingDetails;
    @Mock
    private MovementContainer movementContainer;
    @Mock
    private Movement movement;

    private String purchaseDescription = "desc";
    private String resumeBaseUrl = "url";


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testUnionpayCollectionDetails() {
        when(collectionContext.getPaymentCountryCode()).thenReturn("country");
        UnionpayCollectionDetails collectionDetails1 = new UnionpayCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, resumeBaseUrl, purchaseDescription);
        UnionpayCollectionDetails collectionDetails2 = new UnionpayCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, resumeBaseUrl, purchaseDescription);

        assertTrue(collectionDetails1.equals(collectionDetails2));
        assertEquals(collectionDetails1.hashCode(), collectionDetails2.hashCode());
        assertNotNull(collectionDetails2.toString());
        assertTrue(collectionDetails2.equals(collectionDetails2));
        assertFalse(collectionDetails2.equals("test"));
        assertFalse(collectionDetails2.equals(null));

        assertNotNull(collectionDetails1.getPaymentCountryCode());
        assertNotNull(collectionDetails1.getPurchaseDescription());
        assertNotNull(collectionDetails1.getResumeBaseUrl());

        when(movementContainer.getMovements()).thenReturn(Collections.<Movement>emptyList());
        collectionDetails1.updateWith(movementContainer);

        List<Movement> al = new ArrayList<Movement>();
        when(movementContainer.getMovements()).thenReturn(al);
        al.add(movement);
        when(movement.getMerchantOrderId()).thenReturn(null);
        collectionDetails1.updateWith(movementContainer);

        al = new ArrayList<Movement>();
        when(movementContainer.getMovements()).thenReturn(al);
        al.add(movement);
        when(movement.getMerchantOrderId()).thenReturn("order");
        collectionDetails1.updateWith(movementContainer);

        CollectionDetails cd = new UnionpayCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, resumeBaseUrl, purchaseDescription);
        assertTrue(cd.equals(collectionDetails2));
        assertFalse(cd.equals("test"));
        assertFalse(cd.equals(null));
    }

    @Test
    public void unionpayCollectionDetailsWithUserDetails() {
        UnionpayCollectionDetails unionpayCollectionDetails = new UnionpayCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, resumeBaseUrl, purchaseDescription);

        assertEquals(unionpayCollectionDetails.getOrderId(), "123");
        assertEquals(unionpayCollectionDetails.getMoney(), A_MONEY);
        assertEquals(unionpayCollectionDetails.getGatewayCollectionMethod().getGatewayCollectionMethodType(), GatewayCollectionMethodType.UNIONPAY);
        assertEquals(unionpayCollectionDetails.getPurchaseDescription(), purchaseDescription);
        assertEquals(unionpayCollectionDetails.getResumeBaseUrl(), resumeBaseUrl);
        assertEquals(unionpayCollectionDetails.getCollectionContext(), collectionContext);
    }

    @Test
    public void toRefund() {
        UnionpayCollectionDetails unionpayCollectionDetails = new UnionpayCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, resumeBaseUrl, purchaseDescription);
        RefundDetails refundDetails = unionpayCollectionDetails.toRefundDetails("123", "123");
        assertEquals(refundDetails.getTransactionId(), "123");
    }

    @Test
    public void toConfirmationDetails() {
        UnionpayCollectionDetails unionpayCollectionDetails = new UnionpayCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, resumeBaseUrl, purchaseDescription);
        ConfirmationDetails confirmationDetails = unionpayCollectionDetails.toConfirmationDetails("123");
        assertEquals(confirmationDetails.getMerchantOrderId(), "123");
    }

    @Test
    public void testEquals() {
        UnionpayCollectionDetails collectionDetails = new UnionpayCollectionDetails("111", A_MONEY, collectionContext, collectionShoppingDetails, resumeBaseUrl, purchaseDescription);
        UnionpayCollectionDetails collectionDetails2 = new UnionpayCollectionDetails("222", A_MONEY, collectionContext, collectionShoppingDetails, resumeBaseUrl, purchaseDescription);
        assertTrue(collectionDetails.equals(collectionDetails));
        assertFalse(collectionDetails.equals(collectionDetails2));
        assertFalse(collectionDetails.equals(null));
    }

    @Test
    public void testHashCode() {
        UnionpayCollectionDetails collectionDetails = new UnionpayCollectionDetails("111", A_MONEY, collectionContext, collectionShoppingDetails, resumeBaseUrl, purchaseDescription);
        UnionpayCollectionDetails collectionDetails2 = new UnionpayCollectionDetails("111", A_MONEY, collectionContext, collectionShoppingDetails, resumeBaseUrl, purchaseDescription);
        UnionpayCollectionDetails collectionDetails3 = new UnionpayCollectionDetails("333", A_MONEY, collectionContext, collectionShoppingDetails, resumeBaseUrl, purchaseDescription);
        assertEquals(collectionDetails.hashCode(), collectionDetails2.hashCode());
        assertNotEquals(collectionDetails.hashCode(), collectionDetails3.hashCode());
    }
}
