package com.odigeo.cgsapi.v12.request.operations.notify;

import com.odigeo.cgsapi.v12.request.VirtualPointOfSaleType;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class ProviderNotificationRequestBuilderTest {

    @Test
    public void testBuild() {
        //Given
        VirtualPointOfSaleType virtualPointOfSale = VirtualPointOfSaleType.ADYEN;
        Map<String, Object> parameters = new HashMap<String, Object>();
        ProviderNotificationRequestBuilder builder = new ProviderNotificationRequestBuilder();
        builder.virtualPointOfSale(virtualPointOfSale).parameters(parameters);
        //When
        ProviderNotificationRequest providerNotificationRequest = builder.build();
        //Then
        Assert.assertEquals(providerNotificationRequest.getVirtualPointOfSale(), VirtualPointOfSaleType.ADYEN);
        Assert.assertEquals(providerNotificationRequest.getParameters(), parameters);
    }
}
