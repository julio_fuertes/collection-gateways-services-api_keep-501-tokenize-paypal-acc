package com.odigeo.cgsapi.v12.request.operations.verify;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.assertEquals;


/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class VerificationDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));
    private static final CollectionActionType A_COLLECTION_ACTION_TYPE = CollectionActionType.COLLECT;

    private GatewayCollectionMethod gatewayCollectionMethod = GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ELV);

    @Mock
    private CollectionContext collectionContext;

    @Test
    public void verificationDetails() {
        CollectionContext collectionContext = Mockito.mock(CollectionContext.class);

        VerificationDetails verificationDetails = new VerificationDetails("123", A_MONEY, gatewayCollectionMethod, Collections.<String, String>emptyMap(), A_COLLECTION_ACTION_TYPE, collectionContext, 1, "finishUrl");

        assertEquals(verificationDetails.getMerchantOrderId(), "123");
        assertEquals(verificationDetails.getMoney(), A_MONEY);
        assertEquals(verificationDetails.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ELV));
        assertEquals(verificationDetails.getCollectionActionType(), A_COLLECTION_ACTION_TYPE);
        assertEquals(verificationDetails.getInteractionStep().intValue(), 1);
        assertEquals(verificationDetails.getCollectionContext(), collectionContext);
        assertEquals(verificationDetails.getFinishUrl(), "finishUrl");

    }

    @Test
    public void verificationDetailsWithStatus() {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("verificationDetails", "SUCCESS");
        CollectionContext collectionContext = Mockito.mock(CollectionContext.class);
        VerificationDetails verificationDetails = new VerificationDetails("123", A_MONEY, gatewayCollectionMethod, parameters, A_COLLECTION_ACTION_TYPE, collectionContext, 1, "finishUrl");

        assertEquals(verificationDetails.getParameterValue("verificationDetails"), "SUCCESS");
    }
}
