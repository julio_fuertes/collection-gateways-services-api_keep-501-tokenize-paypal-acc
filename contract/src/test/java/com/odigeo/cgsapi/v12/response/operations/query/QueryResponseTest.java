package com.odigeo.cgsapi.v12.response.operations.query;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * {@link QueryResponse} unit tests
 *
 * @author egonz
 */
public class QueryResponseTest {
    @Mock
    private Movement movement;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void queryResponseTest() {
        when(movement.getAmount()).thenReturn(new BigDecimal("85.23"));
        when(movement.getCurrency()).thenReturn(Currency.getInstance("EUR"));
        when(movement.getStatus()).thenReturn(MovementStatus.DECLINED);
        QueryResponse.Builder builder = new QueryResponse.Builder()
                .addMovement(movement);
        QueryResponse queryResponse = builder.build();
        queryResponse.setGatewayCollectionMethod(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));

        assertEquals(queryResponse.getCurrency().getCurrencyCode(), "EUR");
        assertEquals(queryResponse.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
        assertEquals(queryResponse.getAmount(), new BigDecimal("85.23"));
        assertEquals(queryResponse.getCollectionStatus(), CollectionStatus.COLLECTION_DECLINED);
        assertTrue(queryResponse.getMovements().contains(movement));
    }

    @Test
    public void transform() {
        when(movement.getStatus()).thenReturn(MovementStatus.DECLINED);
        QueryResponse.Builder builder = new QueryResponse.Builder().addMovement(movement);
        QueryResponse queryResponse = builder.build();

        assertEquals(queryResponse.getCollectionStatus(), CollectionStatus.COLLECTION_DECLINED);

        when(movement.getStatus()).thenReturn(MovementStatus.REFUNDED);
        builder.addMovement(movement);
        queryResponse = builder.build();

        assertEquals(queryResponse.getCollectionStatus(), CollectionStatus.REFUNDED);

        when(movement.getStatus()).thenReturn(MovementStatus.PAID);
        builder.addMovement(movement);
        queryResponse = builder.build();

        assertEquals(queryResponse.getCollectionStatus(), CollectionStatus.COLLECTED);

        when(movement.getStatus()).thenReturn(MovementStatus.AUTHORIZED);
        builder.addMovement(movement);
        queryResponse = builder.build();

        assertEquals(queryResponse.getCollectionStatus(), CollectionStatus.AUTHORIZED);

        when(movement.getStatus()).thenReturn(MovementStatus.DECLINED);
        builder.addMovement(movement);
        queryResponse = builder.build();

        assertEquals(queryResponse.getCollectionStatus(), CollectionStatus.COLLECTION_DECLINED);

        when(movement.getStatus()).thenReturn(MovementStatus.CANCEL);
        builder.addMovement(movement);
        queryResponse = builder.build();

        assertEquals(queryResponse.getCollectionStatus(), CollectionStatus.COLLECTION_CANCELLED);

        when(movement.getStatus()).thenReturn(MovementStatus.QUERYERR);
        builder.addMovement(movement);
        queryResponse = builder.build();

        assertEquals(queryResponse.getCollectionStatus(), CollectionStatus.QUERY_ERROR);

        when(movement.getStatus()).thenReturn(MovementStatus.STATUS);
        builder.addMovement(movement);
        queryResponse = builder.build();

        assertEquals(queryResponse.getCollectionStatus(), CollectionStatus.QUERY_ERROR);

        when(movement.getStatus()).thenReturn(MovementStatus.REQUESTED);
        builder.addMovement(movement);
        queryResponse = builder.build();

        assertEquals(queryResponse.getCollectionStatus(), CollectionStatus.REQUESTED);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void illegalTransform() {
        when(movement.getStatus()).thenReturn(MovementStatus.DECLINED);
        QueryResponse.Builder builder = new QueryResponse.Builder().addMovement(movement);
        QueryResponse queryResponse = builder.build();

        when(movement.getStatus()).thenReturn(MovementStatus.REFUNDERR);
        builder.addMovement(movement);
        queryResponse = builder.build();

        assertEquals(queryResponse.getCollectionStatus(), CollectionStatus.REQUESTED);
    }
}
