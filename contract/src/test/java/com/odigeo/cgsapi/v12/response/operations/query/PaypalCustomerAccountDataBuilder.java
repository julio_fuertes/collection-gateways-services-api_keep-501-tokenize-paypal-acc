package com.odigeo.cgsapi.v12.response.operations.query;


import com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal.PaypalAccountStatus;

/**
 * Created by ADEFREIT on 22/07/2015.
 */
public class PaypalCustomerAccountDataBuilder {

    private String payerId = "payerId";
    private final PaypalAccountStatus payerStatus = PaypalAccountStatus.VERIFIED;
    private CustomerData customerData = CustomerDataBuilder.aCustomerData().build();
    private String addressStatus = "an address status";

    public static PaypalCustomerAccountDataBuilder aPaypalCustomerAccountData() {
        return new PaypalCustomerAccountDataBuilder();
    }


    public PaypalCustomerAccountData build() {
        PaypalCustomerAccountData.Builder builder = new PaypalCustomerAccountData.Builder();
        builder.customerData(customerData);
        builder.payerId(this.payerId);
        builder.payerStatus(this.payerStatus);
        builder.addressStatus(this.addressStatus);
        return builder.build();
    }
}
