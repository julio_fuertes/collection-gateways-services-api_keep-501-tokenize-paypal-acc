package com.odigeo.cgsapi.v12.request.collectionmethods.creditcard;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class CreditCardGatewayCollectionMethodTest {

    @Test
    public void creditCardGatewayCollectionMethod() {
        CreditCardGatewayCollectionMethod creditCardGatewayCollectionMethod = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, CreditCardType.VISA_CREDIT);

        assertEquals(creditCardGatewayCollectionMethod.getType(), CreditCardType.VISA_CREDIT);
        assertEquals(creditCardGatewayCollectionMethod.getGatewayCollectionMethodType(), GatewayCollectionMethodType.CREDITCARD);
        assertNotNull(creditCardGatewayCollectionMethod.toString());

        creditCardGatewayCollectionMethod = new CreditCardGatewayCollectionMethod();
        assertNotNull(creditCardGatewayCollectionMethod);
    }

    @Test
    public void secure3dGatewayCollectionMethod() {
        CreditCardGatewayCollectionMethod creditCardGatewayCollectionMethod = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D, CreditCardType.VISA_CREDIT);
        creditCardGatewayCollectionMethod.setType(CreditCardType.MASTER_CARD);

        assertEquals(creditCardGatewayCollectionMethod.getType(), CreditCardType.MASTER_CARD);
        assertEquals(creditCardGatewayCollectionMethod.getGatewayCollectionMethodType(), GatewayCollectionMethodType.SECURE3D);
    }

    @Test
    public void hasSuperGatewayCollectionMethod() {
        GatewayCollectionMethod method1 = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, CreditCardType.VISA_DEBIT);
        GatewayCollectionMethod method2 = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, CreditCardType.VISA_CREDIT);

        assertTrue(method1.hasSuperGatewayCollectionMethod(method2));

        method2 = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D, CreditCardType.VISA_CREDIT);

        assertFalse(method1.hasSuperGatewayCollectionMethod(method2));

        method2 = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, CreditCardType.MASTER_CARD);

        assertFalse(method1.hasSuperGatewayCollectionMethod(method2));

        method1 = new GatewayCollectionMethod(GatewayCollectionMethodType.ELV);

        assertFalse(method1.hasSuperGatewayCollectionMethod(method2));
        assertFalse(method2.hasSuperGatewayCollectionMethod(method1));

        method1 = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, CreditCardType.VISA_ELECTRON);
        method2 = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, CreditCardType.VISA_CREDIT);

        assertTrue(method1.hasSuperGatewayCollectionMethod(method2));
    }

    @Test
    public void equalsTest() {
        CreditCardGatewayCollectionMethod creditCardGatewayCollectionMethod1 = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D, CreditCardType.VISA_CREDIT);
        CreditCardGatewayCollectionMethod creditCardGatewayCollectionMethod2 = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D, CreditCardType.VISA_CREDIT);

        assertTrue(creditCardGatewayCollectionMethod1.equals(creditCardGatewayCollectionMethod1));
        assertTrue(creditCardGatewayCollectionMethod1.equals(creditCardGatewayCollectionMethod2));

        creditCardGatewayCollectionMethod2 = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, CreditCardType.VISA_CREDIT);

        assertFalse(creditCardGatewayCollectionMethod1.equals(creditCardGatewayCollectionMethod2));
        assertFalse(creditCardGatewayCollectionMethod1.equals(null));
        assertFalse(creditCardGatewayCollectionMethod1.equals(Boolean.TRUE));

        creditCardGatewayCollectionMethod2 = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D, CreditCardType.MASTER_CARD);

        assertFalse(creditCardGatewayCollectionMethod1.equals(creditCardGatewayCollectionMethod2));
    }

    @Test
    public void hashCodeTest() {
        CreditCardGatewayCollectionMethod creditCardGatewayCollectionMethod1 = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D, CreditCardType.VISA_CREDIT);
        CreditCardGatewayCollectionMethod creditCardGatewayCollectionMethod2 = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D, CreditCardType.VISA_CREDIT);

        assertTrue(creditCardGatewayCollectionMethod1.hashCode() == creditCardGatewayCollectionMethod2.hashCode());

        creditCardGatewayCollectionMethod2 = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, CreditCardType.VISA_CREDIT);

        assertFalse(creditCardGatewayCollectionMethod1.hashCode() == creditCardGatewayCollectionMethod2.hashCode());
    }
}
