package com.odigeo.cgsapi.v12.response.operations.cancel;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.operations.cancel.CancelDetails;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * Created by vrieraba on 28/05/2015.
 */
public class CancellationTest {

    private static final String A_MERCHANT_ORDER_ID = "123";
    private static final String ERROR_CODE = "1";
    private static final String ERROR_DESCRIPTION = "error";
    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private CancelDetails cancelDetails;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(cancelDetails.getMerchantOrderId()).thenReturn(A_MERCHANT_ORDER_ID);
    }

    @Test
    public void createCancellation() {
        Cancellation cancellation = Cancellation.createCancellation(cancelDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.CANCEL);

        assertNotNull(cancellation);
        assertEquals(cancellation.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(cancellation.getAction(), MovementAction.CANCEL);
        assertEquals(cancellation.getStatus(), MovementStatus.CANCEL);
        assertEquals(cancellation.getCode(), ERROR_CODE);
        assertEquals(cancellation.getErrorDescription(), ERROR_DESCRIPTION);
    }

    @Test
    public void createFailedCancellation() {
        Cancellation cancellation = Cancellation.createFailedCancellation(cancelDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION);

        assertNotNull(cancellation);
        assertEquals(cancellation.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(cancellation.getAction(), MovementAction.CANCEL);
        assertEquals(cancellation.getStatus(), MovementStatus.CANCELERR);
        assertEquals(cancellation.getCode(), ERROR_CODE);
        assertEquals(cancellation.getErrorDescription(), ERROR_DESCRIPTION);
    }

    @Test
    public void createFakeCancellation() {
        Cancellation cancellation = Cancellation.createFakeCancellation(cancelDetails.getMerchantOrderId(), A_MONEY, MovementStatus.CANCELERR);

        assertNotNull(cancellation);
        assertEquals(cancellation.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(cancellation.getAction(), MovementAction.CANCEL);
        assertEquals(cancellation.getStatus(), MovementStatus.CANCELERR);
    }

    @Test
    public void equalsTest() {
        Cancellation cancellation1 = Cancellation.createCancellation(cancelDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.CANCEL);
        Cancellation cancellation2 = Cancellation.createCancellation(cancelDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.CANCEL);

        assertTrue(cancellation1.equals(cancellation1));
        assertTrue(cancellation1.equals(cancellation2));

        cancellation2 = Cancellation.createCancellation("321", A_MONEY, "654", ERROR_DESCRIPTION, MovementStatus.CANCEL);

        assertFalse(cancellation1.equals(cancellation2));
        assertFalse(cancellation1.equals(null));
        assertFalse(cancellation1.equals(Boolean.TRUE));
    }

    @Test
    public void hashCodeTest() {
        Cancellation cancellation1 = Cancellation.createCancellation(cancelDetails.getMerchantOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.CANCEL);

        assertTrue(cancellation1.hashCode() == cancellation1.hashCode());

        Cancellation cancellation2 = Cancellation.createCancellation("321", A_MONEY, "654", ERROR_DESCRIPTION, MovementStatus.CANCEL);

        assertFalse(cancellation1.hashCode() == cancellation2.hashCode());
    }
}
