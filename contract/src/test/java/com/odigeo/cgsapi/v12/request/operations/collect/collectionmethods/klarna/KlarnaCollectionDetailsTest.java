package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.klarna;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.klarna.KlarnaAccountHolder;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

public class KlarnaCollectionDetailsTest {

    private Money money = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    CollectionContext collectionContext;
    @Mock
    CollectionShoppingDetails collectionShoppingDetails;
    @Mock
    KlarnaAccountHolder klarnaAccountHolder;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testConstruction() {
        //Given
        String orderId = "123";
        //When
        KlarnaCollectionDetails klarnaCollectionDetails = new KlarnaCollectionDetails(orderId, money, collectionContext, collectionShoppingDetails, klarnaAccountHolder);
        //Then
        Assert.assertEquals(klarnaCollectionDetails.getOrderId(), "123");
        Assert.assertEquals(klarnaCollectionDetails.getMoney(), money);
        Assert.assertEquals(klarnaCollectionDetails.getCollectionContext(), collectionContext);
        Assert.assertEquals(klarnaCollectionDetails.getCollectionShoppingDetails(), collectionShoppingDetails);
        Assert.assertEquals(klarnaCollectionDetails.getKlarnaAccountHolder(), klarnaAccountHolder);
    }

    @Test
    public void testEquals() {
        //Given
        KlarnaCollectionDetails klarnaCollectionDetails1 = new KlarnaCollectionDetails("123", money, collectionContext, collectionShoppingDetails, klarnaAccountHolder);
        KlarnaCollectionDetails klarnaCollectionDetails2 = new KlarnaCollectionDetails("123", money, collectionContext, collectionShoppingDetails, klarnaAccountHolder);
        //When
        boolean equals = klarnaCollectionDetails1.equals(klarnaCollectionDetails2);
        //Then
        Assert.assertTrue(equals);
    }

    @Test
    public void testNotEquals() {
        //Given
        KlarnaCollectionDetails klarnaCollectionDetails1 = new KlarnaCollectionDetails("123", money, collectionContext, collectionShoppingDetails, klarnaAccountHolder);
        KlarnaCollectionDetails klarnaCollectionDetails2 = new KlarnaCollectionDetails("456", money, collectionContext, collectionShoppingDetails, klarnaAccountHolder);
        //When
        boolean equals = klarnaCollectionDetails1.equals(klarnaCollectionDetails2);
        //Then
        Assert.assertFalse(equals);
    }

    @Test
    public void testHashCode() {
        //Given
        KlarnaCollectionDetails klarnaCollectionDetails1 = new KlarnaCollectionDetails("123", money, collectionContext, collectionShoppingDetails, klarnaAccountHolder);
        KlarnaCollectionDetails klarnaCollectionDetails2 = new KlarnaCollectionDetails("123", money, collectionContext, collectionShoppingDetails, klarnaAccountHolder);
        //When
        int hashCode1 = klarnaCollectionDetails1.hashCode();
        int hashCode2 = klarnaCollectionDetails2.hashCode();
        //Then
        Assert.assertEquals(hashCode1, hashCode2);
    }
}
