package com.odigeo.cgsapi.v12.response.operations.verify;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * Created by vrieraba on 27/05/2015.
 */
public class VerificationTest {

    private static final String A_MERCHANT_ORDER_ID = "123";
    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));
    private static final String ERROR_CODE = "1";
    private static final String ERROR_DESCRIPTION = "error";

    @Test
    public void createVerification() {
        Verification collectionVerification = Verification.createVerification(A_MERCHANT_ORDER_ID, A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.PAID, CollectionActionType.COLLECT);
        assertNotNull(collectionVerification);
        assertEquals(collectionVerification.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(collectionVerification.getCode(), ERROR_CODE);
        assertEquals(collectionVerification.getErrorDescription(), ERROR_DESCRIPTION);
        assertEquals(collectionVerification.getAmount(), A_MONEY.getAmount());
        assertEquals(collectionVerification.getCurrency(), A_MONEY.getCurrency());
        assertEquals(collectionVerification.getAction(), MovementAction.COLLECT);
        assertEquals(collectionVerification.getStatus(), MovementStatus.PAID);

        Verification authorizationVerification = Verification.createVerification(A_MERCHANT_ORDER_ID, A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.AUTHORIZED, CollectionActionType.AUTHORIZE);
        assertNotNull(authorizationVerification);
        assertEquals(authorizationVerification.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(authorizationVerification.getCode(), ERROR_CODE);
        assertEquals(authorizationVerification.getErrorDescription(), ERROR_DESCRIPTION);
        assertEquals(authorizationVerification.getAmount(), A_MONEY.getAmount());
        assertEquals(authorizationVerification.getCurrency(), A_MONEY.getCurrency());
        assertEquals(authorizationVerification.getAction(), MovementAction.AUTHORIZE);
        assertEquals(authorizationVerification.getStatus(), MovementStatus.AUTHORIZED);
    }

    @Test
    public void createFailedVerification() {
        Verification collectionVerification = Verification.createFailedVerification(A_MERCHANT_ORDER_ID, A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, CollectionActionType.COLLECT);
        assertNotNull(collectionVerification);
        assertEquals(collectionVerification.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(collectionVerification.getCode(), ERROR_CODE);
        assertEquals(collectionVerification.getErrorDescription(), ERROR_DESCRIPTION);
        assertEquals(collectionVerification.getAmount(), A_MONEY.getAmount());
        assertEquals(collectionVerification.getCurrency(), A_MONEY.getCurrency());
        assertEquals(collectionVerification.getAction(), MovementAction.COLLECT);
        assertEquals(collectionVerification.getStatus(), MovementStatus.PAYERROR);

        Verification authorizationVerification = Verification.createFailedVerification(A_MERCHANT_ORDER_ID, A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, CollectionActionType.AUTHORIZE);
        assertNotNull(authorizationVerification);
        assertEquals(authorizationVerification.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(authorizationVerification.getCode(), ERROR_CODE);
        assertEquals(authorizationVerification.getErrorDescription(), ERROR_DESCRIPTION);
        assertEquals(authorizationVerification.getAmount(), A_MONEY.getAmount());
        assertEquals(authorizationVerification.getCurrency(), A_MONEY.getCurrency());
        assertEquals(authorizationVerification.getAction(), MovementAction.AUTHORIZE);
        assertEquals(authorizationVerification.getStatus(), MovementStatus.PAYERROR);
    }

    @Test
    public void createFakeVerification() {
        Verification collectionVerification = Verification.createFakeVerification(A_MERCHANT_ORDER_ID, A_MONEY, MovementStatus.PAYERROR, CollectionActionType.COLLECT);
        assertNotNull(collectionVerification);
        assertEquals(collectionVerification.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(collectionVerification.getAction(), MovementAction.COLLECT);
        assertEquals(collectionVerification.getStatus(), MovementStatus.PAYERROR);

        Verification authorizationVerification = Verification.createFakeVerification(A_MERCHANT_ORDER_ID, A_MONEY, MovementStatus.PAYERROR, CollectionActionType.AUTHORIZE);
        assertNotNull(authorizationVerification);
        assertEquals(authorizationVerification.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(authorizationVerification.getAction(), MovementAction.AUTHORIZE);
        assertEquals(authorizationVerification.getStatus(), MovementStatus.PAYERROR);
    }

    @Test
    public void equalsTest() {
        Verification verification1 = Verification.createVerification(A_MERCHANT_ORDER_ID, A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.PAID, CollectionActionType.COLLECT);
        Verification verification2 = Verification.createVerification(A_MERCHANT_ORDER_ID, A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.PAID, CollectionActionType.COLLECT);

        assertTrue(verification1.equals(verification1));
        assertTrue(verification1.equals(verification2));

        verification2 = Verification.createVerification("321", A_MONEY, "456", ERROR_DESCRIPTION, MovementStatus.PAID, CollectionActionType.COLLECT);

        assertFalse(verification1.equals(verification2));
        assertFalse(verification1.equals(null));
        assertFalse(verification1.equals(Boolean.TRUE));
    }

    @Test
    public void hashCodeTest() {
        Verification verification1 = Verification.createVerification(A_MERCHANT_ORDER_ID, A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, MovementStatus.PAID, CollectionActionType.COLLECT);

        assertTrue(verification1.hashCode() == verification1.hashCode());

        Verification verification2 = Verification.createVerification("321", A_MONEY, "456", ERROR_DESCRIPTION, MovementStatus.PAID, CollectionActionType.COLLECT);

        assertFalse(verification1.hashCode() == verification2.hashCode());
    }
}
