package com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.creditcard;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class CreditCardRefundDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private CreditCard creditCard;
    private CreditCardGatewayCollectionMethod creditCardGatewayCollectionMethod = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, CreditCardType.VISA_CREDIT);

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void creditCardRefundDetails() {
        when(creditCard.getType()).thenReturn(CreditCardType.VISA_CREDIT);
        CreditCardRefundDetails creditCardRefundDetails = new CreditCardRefundDetails("123", A_MONEY, creditCard, "456");

        assertEquals(creditCardRefundDetails.getMerchantOrderId(), "123");
        assertEquals(creditCardRefundDetails.getMoney(), A_MONEY);
        assertEquals(creditCardRefundDetails.getCreditCard(), creditCard);
        assertEquals(creditCardRefundDetails.getGatewayCollectionMethod(), creditCardGatewayCollectionMethod);
        assertEquals(creditCardRefundDetails.getTransactionId(), "456");
    }
}
