package com.odigeo.cgsapi.v12.response.operations.query;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.operations.query.QueryDetails;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * Created by ADEFREIT on 22/07/2015.
 */
public class PaypalQueryTest {

    private static final String ORDER_ID = "123";
    private static final String ERROR_CODE = "1";

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));
    private static final String ERROR_DESCRIPTION = "e rror";
    private static final CollectionActionType COLLECTION_ACTION_TYPE = CollectionActionType.COLLECT;
    private static final MovementStatus MOVEMENT_STATUS = MovementStatus.PAID;
    private PaypalCustomerAccountData paypalCustomerAccountData = PaypalCustomerAccountDataBuilder.aPaypalCustomerAccountData().build();

    @Mock
    private QueryDetails queryDetails;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(queryDetails.getOrderId()).thenReturn(ORDER_ID);
    }

    @Test
    public void createPaypalQuery() {
        PaypalQuery paypalQuery = PaypalQuery.createPaypalQuery(queryDetails.getOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, COLLECTION_ACTION_TYPE, MOVEMENT_STATUS, paypalCustomerAccountData);

        assertNotNull(paypalQuery);
        assertEquals(paypalQuery.getPaypalCustomerAccountData(), paypalCustomerAccountData);
    }

    @Test
    public void createFailedQuery() {
        PaypalQuery paypalQuery = PaypalQuery.createFailedPaypalQuery(queryDetails.getOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, COLLECTION_ACTION_TYPE);
        assertNotNull(paypalQuery);
    }

    @Test
    public void createFakeQuery() {
        PaypalQuery query = PaypalQuery.createFakePaypalQuery(queryDetails.getOrderId(), A_MONEY, COLLECTION_ACTION_TYPE, MOVEMENT_STATUS, paypalCustomerAccountData);

        assertNotNull(query);
        assertEquals(query.getPaypalCustomerAccountData(), paypalCustomerAccountData);
    }

    @Test
    public void equalsTest() {
        PaypalQuery paypalQuery1 = PaypalQuery.createPaypalQuery(queryDetails.getOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, COLLECTION_ACTION_TYPE, MOVEMENT_STATUS, paypalCustomerAccountData);
        PaypalQuery paypalQuery2 = PaypalQuery.createPaypalQuery(queryDetails.getOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, COLLECTION_ACTION_TYPE, MOVEMENT_STATUS, paypalCustomerAccountData);

        assertTrue(paypalQuery1.equals(paypalQuery1));
        assertTrue(paypalQuery1.equals(paypalQuery2));

        paypalQuery2 = PaypalQuery.createPaypalQuery("321", A_MONEY, "654", ERROR_DESCRIPTION, COLLECTION_ACTION_TYPE, MOVEMENT_STATUS, paypalCustomerAccountData);

        assertFalse(paypalQuery1.equals(paypalQuery2));
        assertFalse(paypalQuery1.equals(null));
        assertFalse(paypalQuery1.equals(Boolean.TRUE));
    }

    @Test
    public void hashCodeTest() {
        PaypalQuery paypalQuery1 = PaypalQuery.createPaypalQuery(queryDetails.getOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, COLLECTION_ACTION_TYPE, MOVEMENT_STATUS, paypalCustomerAccountData);
        assertTrue(paypalQuery1.hashCode() == paypalQuery1.hashCode());

        PaypalQuery paypalQuery2 = PaypalQuery.createPaypalQuery("321", A_MONEY, "654", ERROR_DESCRIPTION, COLLECTION_ACTION_TYPE, MOVEMENT_STATUS, paypalCustomerAccountData);
        assertFalse(paypalQuery1.hashCode() == paypalQuery2.hashCode());
    }
}
