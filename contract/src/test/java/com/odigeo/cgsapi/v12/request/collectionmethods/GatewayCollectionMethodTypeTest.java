package com.odigeo.cgsapi.v12.request.collectionmethods;

import com.edreams.util.logic.ThreeValuedLogic;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class GatewayCollectionMethodTypeTest {

    @Test
    public void collectionMethodType() {
        GatewayCollectionMethodType gatewayCollectionMethodType = GatewayCollectionMethodType.CREDITCARD;

        assertEquals(gatewayCollectionMethodType.code(), 'C');
        assertEquals(gatewayCollectionMethodType.isChargeBackable(), ThreeValuedLogic.TRUE);
    }

    @Test
    public void findByCode() {
        assertEquals(GatewayCollectionMethodType.findByCode('C'), GatewayCollectionMethodType.CREDITCARD);
        assertEquals(GatewayCollectionMethodType.findByCode('E'), GatewayCollectionMethodType.ELV);
        assertEquals(GatewayCollectionMethodType.findByCode('P'), GatewayCollectionMethodType.PAYPAL);
        assertEquals(GatewayCollectionMethodType.findByCode('S'), GatewayCollectionMethodType.SECURE3D);
        assertEquals(GatewayCollectionMethodType.findByCode('2'), GatewayCollectionMethodType.SECURE3D2);
        assertEquals(GatewayCollectionMethodType.findByCode('B'), GatewayCollectionMethodType.BANK_TRANSFER);
        assertEquals(GatewayCollectionMethodType.findByCode('F'), GatewayCollectionMethodType.COFINOGA);
        assertEquals(GatewayCollectionMethodType.findByCode('G'), GatewayCollectionMethodType.GIROPAY);
        assertEquals(GatewayCollectionMethodType.findByCode('K'), GatewayCollectionMethodType.SOFORT);
        assertEquals(GatewayCollectionMethodType.findByCode('L'), GatewayCollectionMethodType.ALIPAY);
        assertEquals(GatewayCollectionMethodType.findByCode('U'), GatewayCollectionMethodType.UNIONPAY);
        assertEquals(GatewayCollectionMethodType.findByCode('J'), GatewayCollectionMethodType.TRUSTLY);
        assertEquals(GatewayCollectionMethodType.findByCode('I'), GatewayCollectionMethodType.IDEAL);
        assertEquals(GatewayCollectionMethodType.findByCode('X'), GatewayCollectionMethodType.SEPADD);
        assertEquals(GatewayCollectionMethodType.findByCode('Z'), GatewayCollectionMethodType.DOTPAY);
        assertEquals(GatewayCollectionMethodType.findByCode('R'), GatewayCollectionMethodType.KLARNA);
        assertEquals(GatewayCollectionMethodType.findByCode('W'), GatewayCollectionMethodType.APPLEPAY);
        assertEquals(GatewayCollectionMethodType.findByCode('O'), GatewayCollectionMethodType.POLI);
        assertEquals(GatewayCollectionMethodType.findByCode('M'), GatewayCollectionMethodType.MULTIBANCO);
        assertEquals(GatewayCollectionMethodType.findByCode('N'), GatewayCollectionMethodType.SAMSUNGPAY);
        assertEquals(GatewayCollectionMethodType.findByCode('Q'), GatewayCollectionMethodType.GOOGLEPAY);


        try {
            GatewayCollectionMethodType.findByCode('Y');
            assertTrue(false);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }

    @Test
    public void isNeedsInteraction() {
        assertEquals(ThreeValuedLogic.FALSE, GatewayCollectionMethodType.CREDITCARD.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.FALSE, GatewayCollectionMethodType.ELV.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.PAYPAL.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.SECURE3D.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.SECURE3D2.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.FALSE, GatewayCollectionMethodType.BANK_TRANSFER.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.FALSE, GatewayCollectionMethodType.COFINOGA.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.GIROPAY.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.SOFORT.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.ALIPAY.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.UNIONPAY.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.TRUSTLY.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.IDEAL.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.SEPADD.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.DOTPAY.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.KLARNA.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.APPLEPAY.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.POLI.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.MULTIBANCO.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.SAMSUNGPAY.isNeedsUserInteraction());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.GOOGLEPAY.isNeedsUserInteraction());
    }

    @Test
    public void isChargeBackable() {
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.CREDITCARD.isChargeBackable());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.ELV.isChargeBackable());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.PAYPAL.isChargeBackable());
        assertEquals(ThreeValuedLogic.UNKNOWN, GatewayCollectionMethodType.SECURE3D.isChargeBackable());
        assertEquals(ThreeValuedLogic.UNKNOWN, GatewayCollectionMethodType.SECURE3D2.isChargeBackable());
        assertEquals(ThreeValuedLogic.FALSE, GatewayCollectionMethodType.BANK_TRANSFER.isChargeBackable());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.COFINOGA.isChargeBackable());
        assertEquals(ThreeValuedLogic.FALSE, GatewayCollectionMethodType.GIROPAY.isChargeBackable());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.SOFORT.isChargeBackable());
        assertEquals(ThreeValuedLogic.FALSE, GatewayCollectionMethodType.ALIPAY.isChargeBackable());
        assertEquals(ThreeValuedLogic.FALSE, GatewayCollectionMethodType.UNIONPAY.isChargeBackable());
        assertEquals(ThreeValuedLogic.FALSE, GatewayCollectionMethodType.TRUSTLY.isChargeBackable());
        assertEquals(ThreeValuedLogic.FALSE, GatewayCollectionMethodType.IDEAL.isChargeBackable());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.SEPADD.isChargeBackable());
        assertEquals(ThreeValuedLogic.FALSE, GatewayCollectionMethodType.DOTPAY.isChargeBackable());
        assertEquals(ThreeValuedLogic.FALSE, GatewayCollectionMethodType.KLARNA.isChargeBackable());
        assertEquals(ThreeValuedLogic.FALSE, GatewayCollectionMethodType.KLARNA_ACCOUNT.isChargeBackable());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.APPLEPAY.isChargeBackable());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.POLI.isChargeBackable());
        assertEquals(ThreeValuedLogic.FALSE, GatewayCollectionMethodType.MULTIBANCO.isChargeBackable());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.SAMSUNGPAY.isChargeBackable());
        assertEquals(ThreeValuedLogic.TRUE, GatewayCollectionMethodType.GOOGLEPAY.isChargeBackable());
    }
}
