package com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PaypalPaymentInfoTest {

    @Test
    public void testPaypalPaymentInfo() {
        //Given
        PaypalPaymentInfo.Builder builder = new PaypalPaymentInfo.Builder();
        builder.paymentType("a paymentType");
        builder.protectionEligibility(PaypalProtectionEligibility.ELIGIBLE);
        //When
        PaypalPaymentInfo paypalPaymentInfo = builder.build();
        //Then
        Assert.assertEquals(paypalPaymentInfo.getPaymentType(), "a paymentType");
        Assert.assertEquals(paypalPaymentInfo.getProtectionEligibility(), PaypalProtectionEligibility.ELIGIBLE);
    }
}
