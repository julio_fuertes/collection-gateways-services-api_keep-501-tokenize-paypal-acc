package com.odigeo.cgsapi.v12.request.operations.common;

import java.util.Arrays;
import java.util.List;

/**
 * Created on 21/04/2016.
 */
public class SegmentBuilder {

    public static Segment getSegment() {

        String mainCarrier = "Ryanair";
        List<Section> sections = Arrays.asList(new Section[]{SectionBuilder.getSection(), SectionBuilder.getSection2()});
        Integer segmentNumber = 1;

        return new Segment(mainCarrier, sections, segmentNumber);
    }

    public static Segment getSegment2() {

        String mainCarrier = "Vueling";
        List<Section> sections = Arrays.asList(new Section[]{SectionBuilder.getSection(), SectionBuilder.getSection2(), SectionBuilder.getSection3(), SectionBuilder.getSection4()});
        Integer segmentNumber = 2;

        return new Segment(mainCarrier, sections, segmentNumber);
    }

    public static Segment getSegment3() {

        String mainCarrier = "Iberia";
        List<Section> sections = Arrays.asList(new Section[]{SectionBuilder.getSection3(), SectionBuilder.getSection3()});
        Integer segmentNumber = 2;

        return new Segment(mainCarrier, sections, segmentNumber);
    }
}
