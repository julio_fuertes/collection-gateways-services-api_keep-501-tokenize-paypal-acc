package com.odigeo.cgsapi.v12.request.operations.binlookup.collectionmethods.creditcard;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * @author alain.munoz
 * @since 17/01/14
 */
public class BinLookUpDetailsTest {

    @Test
    public void creditCardBinLookUpDetails() {
        BinLookUpDetails binLookUpDetails = new BinLookUpDetails("1111111111111111");

        assertEquals(binLookUpDetails.getBin(), "1111111111111111");
    }
}
