package com.odigeo.cgsapi.v12.request.operations.common;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static voldemort.utils.Utils.assertNotNull;

public class CollectionShoppingDetailsTest {

    @Test
    public void test() {
        List<Traveller> travellers = Arrays.asList(new Traveller[]{TravellerBuilder.getTraveller(), TravellerBuilder.getTraveller2()});
        CollectionShoppingDetails collectionShoppingDetails = new CollectionShoppingDetails(1234L, travellers);
        assertNotNull(collectionShoppingDetails.getTravellers());
        assertNotNull(collectionShoppingDetails.getBookingId());
        assertEquals(collectionShoppingDetails.getBookingId(), 1234L);
        assertEquals(collectionShoppingDetails.getTravellers(), travellers);

        ItineraryDetails itineraryDetails = ItineraryDetailsBuilder.getItineraryDetails();
        collectionShoppingDetails.setItineraryDetails(itineraryDetails);
        assertEquals(collectionShoppingDetails.getItineraryDetails(), itineraryDetails);
        AccommodationDetails accommodationDetails = AccommodationDetailsBuilder.getAccommodationDetails();
        collectionShoppingDetails.setAccommodationDetails(accommodationDetails);
        assertEquals(collectionShoppingDetails.getAccommodationDetails(), accommodationDetails);

        CollectionShoppingDetails collectionShoppingDetails2 = new CollectionShoppingDetails(1234L, Arrays.asList(new Traveller[]{TravellerBuilder.getTraveller(), TravellerBuilder.getTraveller2()}));
        collectionShoppingDetails2.setItineraryDetails(ItineraryDetailsBuilder.getItineraryDetails());
        collectionShoppingDetails2.setAccommodationDetails(AccommodationDetailsBuilder.getAccommodationDetails());

        assertTrue(collectionShoppingDetails.equals(collectionShoppingDetails2));
        assertEquals(collectionShoppingDetails.hashCode(), collectionShoppingDetails2.hashCode());
        Assert.assertNotNull(collectionShoppingDetails2.toString());
        assertTrue(collectionShoppingDetails2.equals(collectionShoppingDetails2));

        assertFalse(collectionShoppingDetails.equals(new CollectionShoppingDetails(1235L, travellers)));
        assertFalse(collectionShoppingDetails2.equals("test"));
        assertFalse(collectionShoppingDetails2.equals(null));
    }
}
