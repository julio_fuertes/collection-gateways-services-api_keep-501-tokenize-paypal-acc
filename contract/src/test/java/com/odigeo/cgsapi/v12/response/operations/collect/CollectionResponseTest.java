package com.odigeo.cgsapi.v12.response.operations.collect;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.RedirectionParameters;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class CollectionResponseTest {

    @Mock
    private Movement movement;
    @Mock
    private RedirectionParameters redirectionParameters;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void collectionResponseTest() {
        when(movement.getAmount()).thenReturn(new BigDecimal("85.23"));
        when(movement.getCurrency()).thenReturn(Currency.getInstance("EUR"));
        when(movement.getStatus()).thenReturn(MovementStatus.PAID);
        CollectionResponse.Builder builder = new CollectionResponse.Builder()
                .addMovement(movement).numCountryCode(77);
        builder.recurringTransactionId("aRecurringTransactionId");
        CollectionResponse collectionResponse = builder.build();
        collectionResponse.setGatewayCollectionMethod(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));

        assertEquals(collectionResponse.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
        assertEquals(collectionResponse.getAmount(), new BigDecimal("85.23"));
        Assert.assertEquals(collectionResponse.getCollectionStatus(), CollectionStatus.COLLECTED);
        assertEquals(collectionResponse.getNumCountryCode(), new Integer(77));
        assertEquals(collectionResponse.getRecurringTransactionId(), "aRecurringTransactionId");
        assertTrue(collectionResponse.getMovements().contains(movement));
        assertFalse(collectionResponse.needsRedirect());
        assertNotNull(collectionResponse.getCurrency());

    }

    @Test
    public void transform() {
        when(movement.getStatus()).thenReturn(MovementStatus.NEEDS_INTERACTION);
        CollectionResponse.Builder builder = new CollectionResponse.Builder().addMovement(movement);
        CollectionResponse collectionResponse = builder.build();
        assertEquals(collectionResponse.getCollectionStatus(), CollectionStatus.ON_HOLD);

        when(movement.getStatus()).thenReturn(MovementStatus.INIT_INTERACTION);
        builder.addMovement(movement);
        collectionResponse = builder.build();
        assertEquals(collectionResponse.getCollectionStatus(), CollectionStatus.ON_HOLD);

        when(movement.getStatus()).thenReturn(MovementStatus.DECLINED);
        builder.addMovement(movement);
        collectionResponse = builder.build();
        assertEquals(collectionResponse.getCollectionStatus(), CollectionStatus.COLLECTION_DECLINED);

        when(movement.getStatus()).thenReturn(MovementStatus.PAID);
        builder.addMovement(movement);
        collectionResponse = builder.build();
        assertEquals(collectionResponse.getCollectionStatus(), CollectionStatus.COLLECTED);

        when(movement.getStatus()).thenReturn(MovementStatus.PAYERROR);
        builder.addMovement(movement);
        collectionResponse = builder.build();
        assertEquals(collectionResponse.getCollectionStatus(), CollectionStatus.COLLECTION_ERROR);

        when(movement.getStatus()).thenReturn(MovementStatus.USE_BATCH);
        builder.addMovement(movement);
        collectionResponse = builder.build();
        assertEquals(collectionResponse.getCollectionStatus(), CollectionStatus.USE_BATCH_COLLECTION);

        when(movement.getStatus()).thenReturn(MovementStatus.REFUNDED);
        builder.addMovement(movement);
        collectionResponse = builder.build();
        assertEquals(collectionResponse.getCollectionStatus(), CollectionStatus.REFUNDED);

        when(movement.getStatus()).thenReturn(MovementStatus.REFUNDERR);
        builder.addMovement(movement);
        collectionResponse = builder.build();
        assertEquals(collectionResponse.getCollectionStatus(), CollectionStatus.REFUND_ERROR);

        when(movement.getStatus()).thenReturn(MovementStatus.REQUESTED);
        builder.addMovement(movement);
        collectionResponse = builder.build();
        assertEquals(collectionResponse.getCollectionStatus(), CollectionStatus.REQUESTED);

        try {
            when(movement.getStatus()).thenReturn(MovementStatus.CANCELERR);
            builder.addMovement(movement);
            collectionResponse = builder.build();
            assertTrue(false);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }

    @Test
    public void collectionResponseRedirectionNeededTest() {
        when(movement.getAmount()).thenReturn(new BigDecimal("85.23"));
        when(movement.getCurrency()).thenReturn(Currency.getInstance("EUR"));
        when(movement.getStatus()).thenReturn(MovementStatus.PAID);
        CollectionResponse.Builder builder = new CollectionResponse.Builder()
                .addMovement(movement).numCountryCode(77).redirectionParameters(redirectionParameters);
        CollectionResponse collectionResponse = builder.build();
        collectionResponse.setGatewayCollectionMethod(GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));

        assertEquals(collectionResponse.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
        assertEquals(collectionResponse.getAmount(), new BigDecimal("85.23"));
        assertEquals(collectionResponse.getCollectionStatus(), CollectionStatus.COLLECTED);
        assertEquals(collectionResponse.getNumCountryCode(), new Integer(77));
        assertEquals(collectionResponse.getRedirectionParameters(), redirectionParameters);
        assertTrue(collectionResponse.getMovements().contains(movement));
        assertTrue(collectionResponse.needsRedirect());
        assertNotNull(collectionResponse.getCurrency());

    }

    @Test
    public void testBuilder() {
        CollectionResponse.Builder builder = new CollectionResponse.Builder();
        Movement movement1 = new Movement("a", "v", MovementAction.AUTHORIZE);
        movement1.setStatus(MovementStatus.REFUNDED);
        Movement movement2 = new Movement("a", "v", MovementAction.CANCEL);
        movement2.setStatus(MovementStatus.USE_BATCH);
        Movement movement3 = new Movement("a", "v", MovementAction.CONFIRM);
        movement3.setStatus(MovementStatus.PAYERROR);
        Movement movement4 = new Movement("a", "v", MovementAction.COLLECT);
        movement4.setStatus(MovementStatus.DECLINED);
        Movement movement5 = new Movement("a", "v", MovementAction.QUERY);
        movement5.setStatus(MovementStatus.NEEDS_INTERACTION);
        Movement movement6 = new Movement("a", "v", MovementAction.VERIFY);
        movement6.setStatus(MovementStatus.INIT_INTERACTION);
        Movement movement7 = new Movement("a", "v", MovementAction.VERIFY);
        movement7.setStatus(MovementStatus.REFUNDERR);
        Movement movement8 = new Movement("a", "v", MovementAction.VERIFY);
        movement8.setStatus(MovementStatus.REQUESTED);
        builder.addMovement(movement1);
        builder.addMovement(movement2);
        builder.addMovement(movement3);
        builder.addMovement(movement4);
        builder.addMovement(movement5);
        builder.addMovement(movement6);
        builder.addMovement(movement7);
        builder.addMovement(movement8);
        CollectionResponse result = builder.build();
        CollectionStatus status = result.getCollectionStatus();
        Assert.assertEquals(status, CollectionStatus.REQUESTED);

        try {
            Movement movement9 = new Movement("a", "v", MovementAction.VERIFY);
            movement9.setStatus(MovementStatus.QUERYERR);
            builder.addMovement(movement9);
            fail("error");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
