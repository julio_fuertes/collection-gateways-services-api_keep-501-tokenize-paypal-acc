package com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.paypal;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class PaypalConfirmationDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Test
    public void paypalConfirmationDetails() {
        PaypalConfirmationDetails paypalConfirmationDetails = new PaypalConfirmationDetails("123", A_MONEY);

        assertEquals(paypalConfirmationDetails.getMerchantOrderId(), "123");
        assertEquals(paypalConfirmationDetails.getMoney(), A_MONEY);
        assertEquals(paypalConfirmationDetails.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));

        PaypalConfirmationDetails paypalConfirmationDetails2 = new PaypalConfirmationDetails("111", A_MONEY, "dynamicDescriptor");
        assertEquals(paypalConfirmationDetails2.getDynamicDescriptor(), "dynamicDescriptor");
    }
}
