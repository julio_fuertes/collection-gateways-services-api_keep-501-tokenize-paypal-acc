package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.paypal;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.verify.VerificationDetails;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Currency;
import java.util.Map;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class PaypalCollectionDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    private static final String ORDER_ID = "123";
    private static final Map<String, String> ANY_MAP = Collections.emptyMap();
    private static final GatewayCollectionMethod PAYPAL_GATEWAY_COLLECTION_METHOD = GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL);
    private static final String RESUME_BASE_URL = "https://www.foo.com/resume";

    @Mock
    private CollectionContext collectionContext;
    @Mock
    private CollectionShoppingDetails collectionShoppingDetails;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void paypalCollectionDetails() {
        PaypalCollectionDetails paypalCollectionDetails = new PaypalCollectionDetails(ORDER_ID, A_MONEY, PAYPAL_GATEWAY_COLLECTION_METHOD, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);
        assertEquals(paypalCollectionDetails.getOrderId(), ORDER_ID);
        assertEquals(paypalCollectionDetails.getMoney(), A_MONEY);
        assertEquals(paypalCollectionDetails.getGatewayCollectionMethod(), PAYPAL_GATEWAY_COLLECTION_METHOD);
        assertEquals(paypalCollectionDetails.getCollectionContext(), collectionContext);
        assertEquals(paypalCollectionDetails.getCollectionShoppingDetails(), collectionShoppingDetails);
        assertEquals(paypalCollectionDetails.getResumeBaseUrl(), RESUME_BASE_URL);

        PaypalCollectionDetails paypalCollectionDetails2 = new PaypalCollectionDetails("123", A_MONEY, PAYPAL_GATEWAY_COLLECTION_METHOD, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);
        assertTrue(paypalCollectionDetails.equals(paypalCollectionDetails2));
        assertEquals(paypalCollectionDetails.hashCode(), paypalCollectionDetails2.hashCode());
        assertNotNull(paypalCollectionDetails2.toString());
        assertTrue(paypalCollectionDetails2.equals(paypalCollectionDetails2));
        assertFalse(paypalCollectionDetails2.equals("test"));
        assertFalse(paypalCollectionDetails2.equals(null));

    }

    @Test
    public void toRefund() {
        PaypalCollectionDetails paypalCollectionDetails = new PaypalCollectionDetails(ORDER_ID, A_MONEY, PAYPAL_GATEWAY_COLLECTION_METHOD, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);
        RefundDetails refundDetails = paypalCollectionDetails.toRefundDetails("123", "12345");

        assertEquals(refundDetails.getTransactionId(), "123");
        assertEquals(refundDetails.getMerchantOrderId(), "12345");
    }

    @Test
    public void toConfirmationDetails() {
        PaypalCollectionDetails paypalCollectionDetails = new PaypalCollectionDetails(ORDER_ID, A_MONEY, PAYPAL_GATEWAY_COLLECTION_METHOD, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);
        ConfirmationDetails confirmationDetails = paypalCollectionDetails.toConfirmationDetails("123");

        assertEquals(confirmationDetails.getMerchantOrderId(), "123");
    }

    @Test
    public void confirmWith() {
        PaypalCollectionDetails paypalCollectionDetails = new PaypalCollectionDetails(ORDER_ID, A_MONEY, PAYPAL_GATEWAY_COLLECTION_METHOD, collectionContext, collectionShoppingDetails, RESUME_BASE_URL);
        VerificationDetails verificationDetails = paypalCollectionDetails.confirmWith(ANY_MAP, "123", CollectionActionType.COLLECT, collectionContext, 1, "finishUrl");

        assertEquals(verificationDetails.getMerchantOrderId(), "123");
        assertEquals(verificationDetails.getCollectionActionType(), CollectionActionType.COLLECT);
        assertEquals(verificationDetails.getInteractionStep().intValue(), 1);
    }
}
