package com.odigeo.cgsapi.v12.request.operations.authorize.collectionmethods.secure3d;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Currency;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

/**
 * Created by vrieraba on 15/06/2016.
 */
public class Secure3dAuthorizationDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private CreditCard creditCard;
    @Mock
    private CollectionContext collectionContext;
    @Mock
    private CollectionShoppingDetails collectionShoppingDetails;

    private GatewayCollectionMethodType gatewayCollectionMethodType = GatewayCollectionMethodType.SECURE3D;
    private static final Map<String, String> ANY_MAP = Collections.emptyMap();

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void secure3dAuthorizationDetails() {
        when(creditCard.getType()).thenReturn(CreditCardType.VISA_CREDIT);
        Secure3dAuthorizationDetails secure3dAuthorizationDetails = new Secure3dAuthorizationDetails("123", A_MONEY, creditCard, gatewayCollectionMethodType, collectionContext, collectionShoppingDetails,
                "https://www.foo.com/callback", "https://www.foo.com/finish");

        assertEquals(secure3dAuthorizationDetails.getOrderId(), "123");
        assertEquals(secure3dAuthorizationDetails.getMoney(), A_MONEY);
        assertEquals(secure3dAuthorizationDetails.getCreditCard(), creditCard);
        assertEquals(secure3dAuthorizationDetails.getGatewayCollectionMethod(), new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D, creditCard.getType()));
        assertEquals(secure3dAuthorizationDetails.getCollectionContext(), collectionContext);
        assertEquals(secure3dAuthorizationDetails.getCollectionShoppingDetails(), collectionShoppingDetails);
        assertEquals(secure3dAuthorizationDetails.getCallbackUrl(), "https://www.foo.com/callback");
        assertEquals(secure3dAuthorizationDetails.getFinishUrl(), "https://www.foo.com/finish");
    }
}
