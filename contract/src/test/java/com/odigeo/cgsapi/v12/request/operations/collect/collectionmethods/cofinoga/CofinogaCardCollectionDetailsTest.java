package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.cofinoga;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.cofinoga.CofinogaCard;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * @author egonz
 * @since 16-may-2012
 */
public class CofinogaCardCollectionDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private CollectionContext collectionContext;
    @Mock
    private CollectionShoppingDetails collectionShoppingDetails;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void cofinogaCardCollectionDetails() {
        CofinogaCard cofinogaCard = Mockito.mock(CofinogaCard.class);
        CofinogaCardCollectionDetails cofinogaCollectionDetails = new CofinogaCardCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, cofinogaCard, "1");

        assertEquals(cofinogaCollectionDetails.getOrderId(), "123");
        assertEquals(cofinogaCollectionDetails.getMoney(), A_MONEY);
        assertEquals(cofinogaCollectionDetails.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.COFINOGA));
        assertEquals(cofinogaCollectionDetails.getCofinogaCard(), cofinogaCard);
        assertEquals(cofinogaCollectionDetails.getCommercialOperation(), "1");
        assertEquals(cofinogaCollectionDetails.getCollectionContext(), collectionContext);
        assertEquals(cofinogaCollectionDetails.getCollectionShoppingDetails(), collectionShoppingDetails);

        CofinogaCardCollectionDetails cofinogaCollectionDetails2 = new CofinogaCardCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, cofinogaCard, "1");
        assertTrue(cofinogaCollectionDetails.equals(cofinogaCollectionDetails2));
        assertEquals(cofinogaCollectionDetails.hashCode(), cofinogaCollectionDetails2.hashCode());
        assertNotNull(cofinogaCollectionDetails2.toString());
        assertTrue(cofinogaCollectionDetails2.equals(cofinogaCollectionDetails2));
        assertFalse(cofinogaCollectionDetails2.equals("test"));
        assertFalse(cofinogaCollectionDetails2.equals(null));
    }

    @Test
    public void toRefund() {
        CofinogaCard cofinogaCard = Mockito.mock(CofinogaCard.class);
        CofinogaCardCollectionDetails cofinogaCollectionDetails = new CofinogaCardCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, cofinogaCard, "1");
        RefundDetails refundDetails = cofinogaCollectionDetails.toRefundDetails("123", "123");

        assertEquals(refundDetails.getTransactionId(), "123");
    }

    @Test
    public void toConfirmationDetails() {
        CofinogaCard cofinogaCard = Mockito.mock(CofinogaCard.class);
        CofinogaCardCollectionDetails cofinogaCollectionDetails = new CofinogaCardCollectionDetails("123", A_MONEY, collectionContext, collectionShoppingDetails, cofinogaCard, "1");
        ConfirmationDetails confirmationDetails = cofinogaCollectionDetails.toConfirmationDetails("123");

        assertEquals(confirmationDetails.getMerchantOrderId(), "123");
    }
}
