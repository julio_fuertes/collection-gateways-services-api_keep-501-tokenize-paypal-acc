package com.odigeo.cgsapi.v12.request.collectionmethods.creditcard;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

/**
 * Created by vrieraba on 20/06/2016.
 */
public class CreditCardTypeTest {

    @Test
    public void testFindByCode() {
        assertEquals(CreditCardType.findByCode("VI"), CreditCardType.VISA_CREDIT);
        assertEquals(CreditCardType.findByCode("VD"), CreditCardType.VISA_DEBIT);
        assertEquals(CreditCardType.findByCode("CA"), CreditCardType.MASTER_CARD);
        assertEquals(CreditCardType.findByCode("MD"), CreditCardType.MASTER_CARD_DEBIT);
        assertEquals(CreditCardType.findByCode("MA"), CreditCardType.MAESTRO);
        assertEquals(CreditCardType.findByCode("DC"), CreditCardType.DINERS_CLUB);
        assertEquals(CreditCardType.findByCode("AX"), CreditCardType.AMERICAN_EXPRESS);
        assertEquals(CreditCardType.findByCode("DL"), CreditCardType.VISA_DELTA);
        assertEquals(CreditCardType.findByCode("JC"), CreditCardType.JCB);
        assertEquals(CreditCardType.findByCode("VE"), CreditCardType.VISA_ELECTRON);
        assertEquals(CreditCardType.findByCode("CB"), CreditCardType.CARTE_BANCAIRE);

        try {
            CreditCardType.findByCode("X");
            assertTrue(false);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }

    @Test
    public void testSuperType() {
        assertNull(CreditCardType.VISA_CREDIT.getSuperType());
        assertEquals(CreditCardType.VISA_DEBIT.getSuperType(), CreditCardType.VISA_CREDIT);
        assertNull(CreditCardType.MASTER_CARD.getSuperType());
        assertEquals(CreditCardType.MASTER_CARD_DEBIT.getSuperType(), CreditCardType.MASTER_CARD);
        assertNull(CreditCardType.MAESTRO.getSuperType());
        assertNull(CreditCardType.DINERS_CLUB.getSuperType());
        assertNull(CreditCardType.AMERICAN_EXPRESS.getSuperType());
        assertEquals(CreditCardType.VISA_DELTA.getSuperType(), CreditCardType.VISA_DEBIT);
        assertNull(CreditCardType.JCB.getSuperType());
        assertEquals(CreditCardType.VISA_ELECTRON.getSuperType(), CreditCardType.VISA_DEBIT);
        assertNull(CreditCardType.CARTE_BANCAIRE.getSuperType());
    }

}
