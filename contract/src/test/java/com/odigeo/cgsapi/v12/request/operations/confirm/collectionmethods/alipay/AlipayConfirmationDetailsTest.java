package com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.alipay;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;

public class AlipayConfirmationDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Test
    public void alipayConfirmationDetailsTest() {
        AlipayConfirmationDetails details = new AlipayConfirmationDetails("123", A_MONEY);
        assertEquals(details.getGatewayCollectionMethod().getGatewayCollectionMethodType(), GatewayCollectionMethodType.ALIPAY);
        assertEquals(details.getMoney(), A_MONEY);
        assertEquals(details.getMerchantOrderId(), "123");

        AlipayConfirmationDetails extendedDetails = new AlipayConfirmationDetails("111", A_MONEY, "dynamicDescriptor");
        assertEquals(extendedDetails.getDynamicDescriptor(), "dynamicDescriptor");
    }
}
