package com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.elv;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class ElvConfirmationDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Test
    public void elvConfirmationDetails() {
        ElvConfirmationDetails elvConfirmationDetails = new ElvConfirmationDetails("123", A_MONEY);

        assertEquals(elvConfirmationDetails.getMerchantOrderId(), "123");
        assertEquals(elvConfirmationDetails.getMoney(), A_MONEY);
        assertEquals(elvConfirmationDetails.getGatewayCollectionMethod(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ELV));

        ElvConfirmationDetails elvConfirmationDetails2 = new ElvConfirmationDetails("111", A_MONEY, "dynamicDescriptor");
        assertEquals(elvConfirmationDetails2.getDynamicDescriptor(), "dynamicDescriptor");
    }
}
