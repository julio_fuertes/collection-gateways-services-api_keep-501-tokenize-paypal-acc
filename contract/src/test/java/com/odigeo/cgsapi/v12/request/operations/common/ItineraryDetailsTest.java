package com.odigeo.cgsapi.v12.request.operations.common;

import org.testng.annotations.Test;

import java.util.Arrays;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ItineraryDetailsTest {

    @Test
    public void testElements() throws Exception {
        ItineraryDetails itineraryDetails = ItineraryDetailsBuilder.getItineraryDetails();
        assertEquals(itineraryDetails.getPnrs(), Arrays.asList(new String[]{"XF234", "PNR TEST", "GS8X3", "PY3X9"}));
        assertEquals(itineraryDetails.getSegments(), Arrays.asList(new Segment[]{SegmentBuilder.getSegment(), SegmentBuilder.getSegment2()}));
    }

    @Test
    public void testEquals() throws Exception {
        ItineraryDetails itineraryDetails1 = ItineraryDetailsBuilder.getItineraryDetails();
        ItineraryDetails itineraryDetails2 = ItineraryDetailsBuilder.getItineraryDetails();

        assertTrue(itineraryDetails1.equals(itineraryDetails1));
        assertTrue(itineraryDetails1.equals(itineraryDetails2));

        itineraryDetails2 = ItineraryDetailsBuilder.getItineraryDetails2();

        assertFalse(itineraryDetails1.equals(itineraryDetails2));
        assertFalse(itineraryDetails1.equals("Test"));
        assertFalse(itineraryDetails1.equals(Boolean.TRUE));
        assertFalse(itineraryDetails1.equals(null));
    }

    @Test
    public void testHashCode() throws Exception {
        ItineraryDetails itineraryDetails1 = ItineraryDetailsBuilder.getItineraryDetails();
        ItineraryDetails itineraryDetails2 = ItineraryDetailsBuilder.getItineraryDetails();

        assertTrue(itineraryDetails1.hashCode() == itineraryDetails1.hashCode());
        assertTrue(itineraryDetails1.hashCode() == itineraryDetails2.hashCode());

        itineraryDetails2 = ItineraryDetailsBuilder.getItineraryDetails2();

        assertFalse(itineraryDetails1.hashCode() == itineraryDetails2.hashCode());

    }
}
