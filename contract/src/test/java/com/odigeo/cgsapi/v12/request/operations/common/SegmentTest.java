package com.odigeo.cgsapi.v12.request.operations.common;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class SegmentTest {

    @Test
    public void testElements() throws Exception {
        List<Section> sections = Arrays.asList(new Section[]{SectionBuilder.getSection(), SectionBuilder.getSection2()});
        Integer segmentNumber = 1;

        Segment segment1 = SegmentBuilder.getSegment();

        assertEquals(segment1.getSegmentNumber(), segmentNumber);
        assertEquals(segment1.getSections(), sections);
        assertEquals(segment1.getMainCarrier(), "Ryanair");
    }

    @Test
    public void testEquals() throws Exception {
        Segment segment1 = SegmentBuilder.getSegment();
        Segment segment2 = SegmentBuilder.getSegment();

        assertTrue(segment1.equals(segment1));
        assertTrue(segment1.equals(segment2));

        segment2 = SegmentBuilder.getSegment2();

        assertFalse(segment1.equals(segment2));
        assertFalse(segment1.equals("Test"));
        assertFalse(segment1.equals(Boolean.TRUE));
        assertFalse(segment1.equals(null));
    }

    @Test
    public void testHashCode() throws Exception {
        Segment segment1 = SegmentBuilder.getSegment();
        Segment segment2 = SegmentBuilder.getSegment();

        assertTrue(segment1.hashCode() == segment2.hashCode());
        assertTrue(segment1.hashCode() == segment2.hashCode());

        segment2 = SegmentBuilder.getSegment2();

        assertFalse(segment1.hashCode() == segment2.hashCode());
    }
}
