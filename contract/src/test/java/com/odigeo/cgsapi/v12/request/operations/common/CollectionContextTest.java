package com.odigeo.cgsapi.v12.request.operations.common;

import org.mockito.Mockito;
import org.testng.annotations.Test;

import java.util.Locale;
import java.util.Map;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class CollectionContextTest {

    @Test
    public void testConstructionWithoutInvoicingPurchaser() {
        //Given
        Locale userLocale = Locale.ENGLISH;
        String userIpAddress = "anIp";
        Purchaser orderPurchaser = new PurchaserBuilder().buildOrderPurchaser();
        String clientReferenceId = "clientReferenceId";
        String paymentCountryCode = "paymentCountryCode";
        String websiteCode = "websiteCode";
        String brandName = "brandName";
        String sessionId = "sessionId";
        String userAgent = "userAgent";
        Map<String, Integer> testDimensions = Mockito.mock(Map.class);
        //When
        CollectionContext collectionContext = new CollectionContext(userLocale, userIpAddress, orderPurchaser, clientReferenceId, paymentCountryCode, websiteCode, brandName, sessionId, userAgent, testDimensions);
        //Then
        assertEquals(collectionContext.getUserLocale(), userLocale);
        assertEquals(collectionContext.getUserIpAddress(), userIpAddress);
        assertEquals(collectionContext.getOrderPurchaser(), orderPurchaser);
        assertEquals(collectionContext.getClientReferenceId(), clientReferenceId);
        assertEquals(collectionContext.getUserCountryCode(), userLocale.getCountry());
        assertEquals(collectionContext.getUserLanguageCode(), userLocale.getLanguage());
        assertEquals(collectionContext.getPaymentCountryCode(), paymentCountryCode);
        assertEquals(collectionContext.getWebsiteCode(), websiteCode);
        assertEquals(collectionContext.getBrandName(), brandName);
        assertEquals(collectionContext.getSessionId(), sessionId);
        assertEquals(collectionContext.getUserAgent(), userAgent);
        assertEquals(collectionContext.getTestDimensions(), testDimensions);
        assertNull(collectionContext.getInvoicingPurchaser());
    }

    @Test
    public void testConstructionWithInvoicingPurchaser() {
        //Given
        Locale userLocale = Locale.ENGLISH;
        String userIpAddress = "anIp";
        Purchaser orderPurchaser = new PurchaserBuilder().buildOrderPurchaser();
        Purchaser invoicingPurchaser = new PurchaserBuilder().buildInvoicingPurchaser();
        String clientReferenceId = "clientReferenceId";
        String paymentCountryCode = "paymentCountryCode";
        String websiteCode = "websiteCode";
        String brandName = "brandName";
        String sessionId = "sessionId";
        String userAgent = "userAgent";
        Map<String, Integer> testDimensions = Mockito.mock(Map.class);
        //When
        CollectionContext collectionContext = new CollectionContext(userLocale, userIpAddress, orderPurchaser, invoicingPurchaser, clientReferenceId, paymentCountryCode, websiteCode, brandName, sessionId, userAgent, testDimensions);
        //Then
        assertEquals(collectionContext.getUserLocale(), userLocale);
        assertEquals(collectionContext.getUserIpAddress(), userIpAddress);
        assertEquals(collectionContext.getOrderPurchaser(), orderPurchaser);
        assertEquals(collectionContext.getInvoicingPurchaser(), invoicingPurchaser);
        assertEquals(collectionContext.getClientReferenceId(), clientReferenceId);
        assertEquals(collectionContext.getUserCountryCode(), userLocale.getCountry());
        assertEquals(collectionContext.getUserLanguageCode(), userLocale.getLanguage());
        assertEquals(collectionContext.getPaymentCountryCode(), paymentCountryCode);
        assertEquals(collectionContext.getWebsiteCode(), websiteCode);
        assertEquals(collectionContext.getBrandName(), brandName);
        assertEquals(collectionContext.getSessionId(), sessionId);
        assertEquals(collectionContext.getUserAgent(), userAgent);
        assertEquals(collectionContext.getTestDimensions(), testDimensions);
    }

    @Test
    public void testConstructionWithoutUserLocale() {
        //Given
        Locale userLocale = null;
        String userIpAddress = "anIp";
        Purchaser orderPurchaser = new PurchaserBuilder().buildOrderPurchaser();
        String clientReferenceId = "clientReferenceId";
        String paymentCountryCode = "paymentCountryCode";
        String websiteCode = "websiteCode";
        String brandName = "brandName";
        String sessionId = "sessionId";
        String userAgent = "userAgent";
        Map<String, Integer> testDimensions = Mockito.mock(Map.class);
        //When
        CollectionContext collectionContext = new CollectionContext(userLocale, userIpAddress, orderPurchaser, clientReferenceId, paymentCountryCode, websiteCode, brandName, sessionId, userAgent, testDimensions);
        //Then
        assertNull(collectionContext.getUserLocale());
        assertNull(collectionContext.getUserCountryCode());
        assertNull(collectionContext.getUserLanguageCode());
    }

    @Test
    public void testSetDynamicDescriptor() {
        //Given
        CollectionContext collectionContext = new CollectionContext();
        String dynamicDescriptor = "aDynamicDescriptor";
        //When
        collectionContext.setDynamicDescriptor(dynamicDescriptor);
        //Then
        assertEquals(collectionContext.getDynamicDescriptor(), dynamicDescriptor);
    }

    @Test
    public void testEqualsWithNullObject() {
        //Given
        CollectionContext collectionContext = new CollectionContext();
        //when
        boolean equals = collectionContext.equals(null);
        //Then
        assertFalse(equals);
    }

    @Test
    public void testEqualsWithSameObject() {
        //Given
        CollectionContext collectionContext = new CollectionContext();
        //when
        boolean equals = collectionContext.equals(collectionContext);
        //Then
        assertTrue(equals);
    }

    @Test
    public void testEqualsWithDifferentClassObject() {
        //Given
        CollectionContext collectionContext = new CollectionContext();
        CollectionShoppingDetails collectionShoppingDetails = new CollectionShoppingDetails();
        //when
        boolean equals = collectionContext.equals(collectionShoppingDetails);
        //Then
        assertFalse(equals);
    }

    @Test
    public void testEqualsWithSameValues() {
        //Given
        Locale userLocale = Locale.ENGLISH;
        String userIpAddress = "anIp";
        Purchaser orderPurchaser = new PurchaserBuilder().buildOrderPurchaser();
        Purchaser invoicingPurchaser = new PurchaserBuilder().buildInvoicingPurchaser();
        String clientReferenceId = "clientReferenceId";
        String paymentCountryCode = "paymentCountryCode";
        String websiteCode = "websiteCode";
        String brandName = "brandName";
        String sessionId = "sessionId";
        String userAgent = "userAgent";
        Map<String, Integer> testDimensions = Mockito.mock(Map.class);
        CollectionContext collectionContext1 = new CollectionContext(userLocale, userIpAddress, orderPurchaser, invoicingPurchaser, clientReferenceId, paymentCountryCode, websiteCode, brandName, sessionId, userAgent, testDimensions);
        CollectionContext collectionContext2 = new CollectionContext(userLocale, userIpAddress, orderPurchaser, invoicingPurchaser, clientReferenceId, paymentCountryCode, websiteCode, brandName, sessionId, userAgent, testDimensions);
        //when
        boolean equals = collectionContext1.equals(collectionContext2);
        //Then
        assertTrue(equals);
    }

    @Test
    public void testEqualsWithDifferentValues() {
        //Given
        Locale userLocale = Locale.ENGLISH;
        String userIpAddress = "anIp";
        Purchaser orderPurchaser = new PurchaserBuilder().buildOrderPurchaser();
        Purchaser invoicingPurchaser = new PurchaserBuilder().buildInvoicingPurchaser();
        String clientReferenceId = "clientReferenceId";
        String paymentCountryCode = "paymentCountryCode";
        String websiteCode = "websiteCode";
        String brandName = "brandName";
        String sessionId = "sessionId";
        String userAgent = "userAgent";
        String dynamicDescriptor = "dynamicDescriptor";
        Map<String, Integer> testDimensions = Mockito.mock(Map.class);
        CollectionContext collectionContext1 = new CollectionContext(userLocale, userIpAddress, orderPurchaser, invoicingPurchaser, clientReferenceId, paymentCountryCode, websiteCode, brandName, sessionId, userAgent, testDimensions);
        CollectionContext collectionContext2 = new CollectionContext(userLocale, userIpAddress, orderPurchaser, invoicingPurchaser, clientReferenceId, paymentCountryCode, websiteCode, brandName, sessionId, userAgent, testDimensions);
        collectionContext2.setDynamicDescriptor(dynamicDescriptor);
        //when
        boolean equals = collectionContext1.equals(collectionContext2);
        //Then
        assertFalse(equals);
    }

    @Test
    public void testHashCode() {
        //Given
        Locale userLocale = Locale.ENGLISH;
        String userIpAddress = "anIp";
        Purchaser orderPurchaser = new PurchaserBuilder().buildOrderPurchaser();
        Purchaser invoicingPurchaser = new PurchaserBuilder().buildInvoicingPurchaser();
        String clientReferenceId = "clientReferenceId";
        String paymentCountryCode = "paymentCountryCode";
        String websiteCode = "websiteCode";
        String brandName = "brandName";
        String sessionId = "sessionId";
        String userAgent = "userAgent";
        String dynamicDescriptor = "dynamicDescriptor";
        Map<String, Integer> testDimensions = Mockito.mock(Map.class);
        CollectionContext collectionContext = new CollectionContext(userLocale, userIpAddress, orderPurchaser, invoicingPurchaser, clientReferenceId, paymentCountryCode, websiteCode, brandName, sessionId, userAgent, testDimensions);
        collectionContext.setDynamicDescriptor(dynamicDescriptor);
        //when
        int hashCode = collectionContext.hashCode();
        //Then
        assertEquals(hashCode, 1801785381);
    }
}
