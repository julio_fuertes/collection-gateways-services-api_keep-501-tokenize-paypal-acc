package com.odigeo.cgsapi.v12.request.operations.common;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class PurchaserBuilder {

    private String name = "Pepe";
    private String lastNames = "López";
    private String address = "C/ Pez 25";
    private String zipCode = "08008";
    private String city = "Barcelona";
    private String countryCode = "ES";
    private String email = "pepe.lopez@edreams.com";
    private String telephoneNumber = "987654321";

    public Purchaser buildOrderPurchaser() {
        return Purchaser.createOrderPurchaser(name, lastNames, address, zipCode, city, countryCode, email, telephoneNumber);
    }

    public Purchaser buildInvoicingPurchaser() {
        return Purchaser.createInvoicingPurchaser(lastNames, address, zipCode, city, countryCode);
    }

    public PurchaserBuilder withCountryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public PurchaserBuilder withZipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

}
