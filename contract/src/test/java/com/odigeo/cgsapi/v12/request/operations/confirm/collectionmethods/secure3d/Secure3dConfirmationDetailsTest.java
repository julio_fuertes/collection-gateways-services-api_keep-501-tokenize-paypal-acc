package com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.secure3d;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class Secure3dConfirmationDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private CreditCard creditCard;
    private CreditCardGatewayCollectionMethod secure3dGatewayCollectionMethod = new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D, CreditCardType.VISA_CREDIT);

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(creditCard.getType()).thenReturn(CreditCardType.MASTER_CARD_DEBIT);
    }

    @Test
    public void secure3dConfirmationDetails() {
        when(creditCard.getType()).thenReturn(CreditCardType.VISA_CREDIT);
        Secure3dConfirmationDetails creditCardConfirmationDetails = new Secure3dConfirmationDetails("123", A_MONEY, creditCard);
        Secure3dConfirmationDetails creditCardConfirmationDetails2 = new Secure3dConfirmationDetails("111", A_MONEY, "dynamicDescriptor", creditCard);

        assertEquals(creditCardConfirmationDetails.getMerchantOrderId(), "123");
        assertEquals(creditCardConfirmationDetails.getMoney(), A_MONEY);
        assertEquals(creditCardConfirmationDetails.getCreditCard(), creditCard);
        assertEquals(creditCardConfirmationDetails.getGatewayCollectionMethod(), secure3dGatewayCollectionMethod);

        assertEquals(creditCardConfirmationDetails2.getDynamicDescriptor(), "dynamicDescriptor");
    }

    @Test
    public void testEquals() {
        Secure3dConfirmationDetails confirmationDetails = new Secure3dConfirmationDetails("111", A_MONEY, creditCard);
        Secure3dConfirmationDetails confirmationDetails2 = new Secure3dConfirmationDetails("222", A_MONEY, creditCard);
        assertTrue(confirmationDetails.equals(confirmationDetails));
        assertFalse(confirmationDetails.equals(confirmationDetails2));
        assertFalse(confirmationDetails.equals(null));
    }

    @Test
    public void testHashCode() {
        Secure3dConfirmationDetails confirmationDetails = new Secure3dConfirmationDetails("111", A_MONEY, creditCard);
        Secure3dConfirmationDetails confirmationDetails2 = new Secure3dConfirmationDetails("111", A_MONEY, creditCard);
        Secure3dConfirmationDetails confirmationDetails3 = new Secure3dConfirmationDetails("333", A_MONEY, creditCard);
        assertEquals(confirmationDetails.hashCode(), confirmationDetails2.hashCode());
        assertNotEquals(confirmationDetails.hashCode(), confirmationDetails3.hashCode());
    }
}
