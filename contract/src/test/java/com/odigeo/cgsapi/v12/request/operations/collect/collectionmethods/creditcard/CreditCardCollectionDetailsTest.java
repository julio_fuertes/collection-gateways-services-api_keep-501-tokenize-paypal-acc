package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.creditcard;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.RecurringDetails;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.verify.VerificationDetails;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Currency;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;


/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class CreditCardCollectionDetailsTest {

    private static final Map<String, String> ANY_MAP = Collections.emptyMap();
    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private CreditCard creditCard;
    @Mock
    private CollectionContext collectionContext;
    @Mock
    private CollectionShoppingDetails collectionShoppingDetails;
    private RecurringDetails recurringDetails = new RecurringDetails(true, "0123456789");

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(creditCard.getType()).thenReturn(CreditCardType.VISA_CREDIT);
    }

    @Test
    public void testConstruction() {
        when(creditCard.getType()).thenReturn(CreditCardType.VISA_CREDIT);
        CreditCardCollectionDetails creditCardCollectionDetails = new CreditCardCollectionDetails("123", A_MONEY, creditCard, collectionContext, collectionShoppingDetails);
        creditCardCollectionDetails.setRecurringDetails(recurringDetails);
        assertEquals(creditCardCollectionDetails.getOrderId(), "123");
        assertEquals(creditCardCollectionDetails.getMoney(), A_MONEY);
        assertEquals(creditCardCollectionDetails.getCreditCard(), creditCard);
        assertEquals(creditCardCollectionDetails.getGatewayCollectionMethod(), new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, creditCard.getType()));
        assertEquals(creditCardCollectionDetails.getCollectionContext(), collectionContext);
        assertEquals(creditCardCollectionDetails.getRecurringDetails(), recurringDetails);
    }

    @Test
    public void toRefund() {
        when(creditCard.getType()).thenReturn(CreditCardType.VISA_CREDIT);
        CreditCardCollectionDetails creditCardCollectionDetails = new CreditCardCollectionDetails("123", A_MONEY, creditCard, collectionContext, collectionShoppingDetails);
        creditCardCollectionDetails.setRecurringDetails(recurringDetails);
        RefundDetails refundDetails = creditCardCollectionDetails.toRefundDetails("123", "123");

        assertEquals(refundDetails.getTransactionId(), "123");
    }

    @Test
    public void toConfirmationDetails() {
        when(creditCard.getType()).thenReturn(CreditCardType.VISA_CREDIT);
        CreditCardCollectionDetails creditCardCollectionDetails = new CreditCardCollectionDetails("123", A_MONEY, creditCard, collectionContext, collectionShoppingDetails);
        creditCardCollectionDetails.setRecurringDetails(recurringDetails);
        ConfirmationDetails confirmationDetails = creditCardCollectionDetails.toConfirmationDetails("123");

        assertEquals(confirmationDetails.getMerchantOrderId(), "123");
    }

    @Test
    public void confirmWith() {
        when(creditCard.getType()).thenReturn(CreditCardType.VISA_CREDIT);
        CreditCardCollectionDetails creditCardCollectionDetails = new CreditCardCollectionDetails("123", A_MONEY, creditCard, collectionContext, collectionShoppingDetails);
        creditCardCollectionDetails.setRecurringDetails(recurringDetails);
        VerificationDetails verificationDetails = creditCardCollectionDetails.confirmWith(ANY_MAP, "123456", CollectionActionType.COLLECT, collectionContext, 1, "finishUrl");

        assertEquals(verificationDetails.getMerchantOrderId(), "123456");
        assertEquals(verificationDetails.getCollectionActionType(), CollectionActionType.COLLECT);
        assertEquals(verificationDetails.getInteractionStep().intValue(), 1);
    }

    @Test
    public void testEquals() {
        CreditCardCollectionDetails collectionDetails = new CreditCardCollectionDetails("111", A_MONEY, creditCard, collectionContext, collectionShoppingDetails);
        collectionDetails.setRecurringDetails(recurringDetails);
        CreditCardCollectionDetails collectionDetails2 = new CreditCardCollectionDetails("222", A_MONEY, creditCard, collectionContext, collectionShoppingDetails);
        collectionDetails2.setRecurringDetails(recurringDetails);
        assertTrue(collectionDetails.equals(collectionDetails));
        assertFalse(collectionDetails.equals(collectionDetails2));
        assertFalse(collectionDetails.equals(null));
    }

    @Test
    public void testHashCode() {
        CreditCardCollectionDetails collectionDetails = new CreditCardCollectionDetails("111", A_MONEY, creditCard, collectionContext, collectionShoppingDetails);
        collectionDetails.setRecurringDetails(recurringDetails);
        CreditCardCollectionDetails collectionDetails2 = new CreditCardCollectionDetails("111", A_MONEY, creditCard, collectionContext, collectionShoppingDetails);
        collectionDetails2.setRecurringDetails(recurringDetails);
        CreditCardCollectionDetails collectionDetails3 = new CreditCardCollectionDetails("333", A_MONEY, creditCard, collectionContext, collectionShoppingDetails);
        collectionDetails3.setRecurringDetails(recurringDetails);
        assertEquals(collectionDetails.hashCode(), collectionDetails2.hashCode());
        assertNotEquals(collectionDetails.hashCode(), collectionDetails3.hashCode());
    }
}
