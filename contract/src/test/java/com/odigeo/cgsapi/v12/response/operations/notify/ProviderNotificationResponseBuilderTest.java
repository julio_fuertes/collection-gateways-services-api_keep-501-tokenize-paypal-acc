package com.odigeo.cgsapi.v12.response.operations.notify;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ProviderNotificationResponseBuilderTest {

    @Test
    public void testBuild() {
        //Given
        String notificationId = "1";
        Result result = Result.SUCCESS;
        String message = "message";
        ProviderNotificationResponseBuilder builder = new ProviderNotificationResponseBuilder();
        builder.notificationId(notificationId).result(result).message(message);
        //When
        ProviderNotificationResponse collectionGatewaysNotificationServiceResponse = builder.build();
        //Then
        Assert.assertEquals(collectionGatewaysNotificationServiceResponse.getNotificationId(), "1");
        Assert.assertEquals(collectionGatewaysNotificationServiceResponse.getResult(), Result.SUCCESS);
        Assert.assertEquals(collectionGatewaysNotificationServiceResponse.getMessage(), "message");

    }
}
