package com.odigeo.cgsapi.v12.request;

import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * Created by vrieraba on 15/06/2016.
 */
public class MoneyTest {

    @Test
    public void testCreateMoney() {
        Money money = Money.createMoney(new BigDecimal("12.36"), Currency.getInstance("EUR"));
        assertEquals(money.getAmount(), new BigDecimal("12.36"));
        assertEquals(money.getCurrency(), Currency.getInstance("EUR"));
    }

    @Test
    public void equalsTest() {
        Money money1 = Money.createMoney(new BigDecimal("12.36"), Currency.getInstance("EUR"));
        Money money2 = Money.createMoney(new BigDecimal("12.36"), Currency.getInstance("EUR"));

        assertTrue(money1.equals(money1));
        assertTrue(money1.equals(money2));

        money2 = Money.createMoney(new BigDecimal("33.33"), Currency.getInstance("EUR"));

        assertFalse(money1.equals(money2));
        assertFalse(money1.equals(null));
        assertFalse(money1.equals(Boolean.TRUE));
    }

    @Test
    public void hashCodeTest() {
        Money money1 = Money.createMoney(new BigDecimal("12.36"), Currency.getInstance("EUR"));
        Money money2 = Money.createMoney(new BigDecimal("12.36"), Currency.getInstance("EUR"));

        assertTrue(money1.hashCode() == money2.hashCode());

        money2 = Money.createMoney(new BigDecimal("33.33"), Currency.getInstance("EUR"));

        assertFalse(money1.hashCode() == money2.hashCode());
    }
}
