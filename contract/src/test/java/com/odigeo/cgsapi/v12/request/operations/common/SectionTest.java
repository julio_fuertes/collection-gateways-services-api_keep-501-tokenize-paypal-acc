package com.odigeo.cgsapi.v12.request.operations.common;

import com.odigeo.cgsapi.v12.LocalizedDate;
import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.Test;

import java.util.Calendar;
import java.util.TimeZone;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class SectionTest {

    @Test
    public void testElements() throws Exception {
        Section section = SectionBuilder.getSection();
        Calendar cal = Calendar.getInstance();

        assertEquals(section.getSectionNumber(), new Integer(1));
        assertEquals(section.getProductType(), "FLIGHT");
        assertEquals(section.getCarrierCode(), "FR");
        assertEquals(section.getFlightNumber(), "450");
        assertEquals(section.getDepartureIataCode(), "BCN");
        assertEquals(section.getArrivalIataCode(), "MAD");
        cal.set(2015, 10, 20);
        LocalizedDate departureDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());
        assertEquals(section.getDepartureDate(), departureDate);
        cal.set(2015, 10, 21);
        LocalizedDate arrivalDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());
        assertEquals(section.getArrivalDate(), arrivalDate);
        assertEquals(section.getDepartureCityName(), "Barcelona");
        assertEquals(section.getArrivalCityName(), "Madrid");
        assertEquals(section.getServiceClass(), "X");
    }

    @Test
    public void testEquals() throws Exception {
        Section section1 = SectionBuilder.getSection();
        Section section2 = SectionBuilder.getSection();

        assertTrue(section1.equals(section1));
        assertTrue(section1.equals(section2));

        section2 = SectionBuilder.getSection2();

        assertFalse(section1.equals(section2));
        assertFalse(section1.equals(null));
        assertFalse(section1.equals(Boolean.TRUE));
    }

    @Test
    public void testHashCode() throws Exception {
        Section section1 = SectionBuilder.getSection();
        Section section2 = SectionBuilder.getSection();

        assertTrue(section1.hashCode() == section1.hashCode());
        assertTrue(section1.hashCode() == section2.hashCode());

        section2 = SectionBuilder.getSection2();
        assertFalse(section1.hashCode() == section2.hashCode());
    }
}
