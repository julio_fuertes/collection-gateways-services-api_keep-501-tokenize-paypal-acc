package com.odigeo.cgsapi.v12.response;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;

/**
 * @author javier.garcia
 * @since 26/07/13
 */
public class BankTransferMovementAdditionalInformationTest {

    @Test
    public void testMovement() {
        BankTransferMovementAdditionalInformation additionalInformation = new BankTransferMovementAdditionalInformation();
        additionalInformation.setAccountHolder("AccountHolder");
        assertEquals(additionalInformation.getAccountHolder(), "AccountHolder");

        additionalInformation.setBankAccountNumber("BankAccountNumber");
        assertEquals(additionalInformation.getBankAccountNumber(), "BankAccountNumber");

        additionalInformation.setBankName("BankName");
        assertEquals(additionalInformation.getBankName(), "BankName");

        additionalInformation.setCity("City");
        assertEquals(additionalInformation.getCity(), "City");

        additionalInformation.setCountryDescription("CountryDescription");
        assertEquals(additionalInformation.getCountryDescription(), "CountryDescription");

        additionalInformation.setIban("Iban");
        assertEquals(additionalInformation.getIban(), "Iban");

        additionalInformation.setPaymentReference("PaymentReference");
        assertEquals(additionalInformation.getPaymentReference(), "PaymentReference");

        additionalInformation.setSwiftCode("SwiftCode");
        assertEquals(additionalInformation.getSwiftCode(), "SwiftCode");

        additionalInformation.setType("Type");
        assertEquals(additionalInformation.getType(), "Type");

        assertNotNull(additionalInformation.hashCode());

        assertFalse(additionalInformation.equals(new BankTransferMovementAdditionalInformation()));
        assertFalse(additionalInformation.equals(""));
        assertEquals(additionalInformation, additionalInformation);
    }
}
