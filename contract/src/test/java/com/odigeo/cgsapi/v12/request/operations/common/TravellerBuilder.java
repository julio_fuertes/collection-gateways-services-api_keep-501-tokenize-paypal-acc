package com.odigeo.cgsapi.v12.request.operations.common;

import java.util.Calendar;
import java.util.Date;

/**
 * Created on 21/04/2016.
 */
public class TravellerBuilder {

    public static Traveller getTraveller() {
        String name = "Samuel";
        String middleName = "Lewis";
        String firstLastName = "Eto\'o";
        String secondLastName = "Camuñas";
        Date dateOfBirth = getDate(1986, 10, 2);

        return new Traveller(1, name, middleName, firstLastName, secondLastName, dateOfBirth);
    }

    public static Traveller getTraveller2() {
        String name = "John";
        String middleName = "Björk";
        String firstLastName = "Aonuma";
        String secondLastName = "Fernández";
        Date dateOfBirth = getDate(1991, 1, 22);

        return new Traveller(1, name, middleName, firstLastName, secondLastName, dateOfBirth);
    }

    static Date getDate(int year, int month, int day) {
        Calendar dateOfBirth = Calendar.getInstance();
        dateOfBirth.set(year, month, day);
        dateOfBirth.set(Calendar.HOUR_OF_DAY, 0);
        dateOfBirth.set(Calendar.MINUTE, 0);
        dateOfBirth.set(Calendar.SECOND, 0);
        dateOfBirth.set(Calendar.MILLISECOND, 0);
        return dateOfBirth.getTime();
    }
}
