package com.odigeo.cgsapi.v12.response.operations.query;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.operations.query.QueryDetails;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * Created by ADEFREIT on 22/07/2015.
 */
public class AbstractQueryTest {

    private static final String A_MERCHANT_ORDER_ID = "123";
    private static final String ERROR_CODE = "1";
    private static final String ERROR_DESCRIPTION = "error";
    private static final CollectionActionType COLLECTION_ACTION_TYPE = CollectionActionType.COLLECT;
    private static final MovementStatus MOVEMENT_STATUS = MovementStatus.PAID;
    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private QueryDetails queryDetails;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(queryDetails.getOrderId()).thenReturn(A_MERCHANT_ORDER_ID);
    }

    @Test
    public void testAbstractQuery() {
        Query query = Query.createQuery(queryDetails.getOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, COLLECTION_ACTION_TYPE, MOVEMENT_STATUS);

        assertEquals(query.getCollectionActionType(), COLLECTION_ACTION_TYPE);
        assertEquals(query.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(query.getStatus(), MOVEMENT_STATUS);
        assertEquals(query.getAmount(), A_MONEY.getAmount());
        assertEquals(query.getCurrency(), A_MONEY.getCurrency());
        assertEquals(query.getCode(), ERROR_CODE);
        assertEquals(query.getErrorDescription(), ERROR_DESCRIPTION);
    }

    @Test
    public void equalsTest() {
        Query query1 = Query.createQuery(queryDetails.getOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, CollectionActionType.COLLECT, MovementStatus.PAID);
        Query query2 = Query.createQuery(queryDetails.getOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, CollectionActionType.COLLECT, MovementStatus.PAID);

        assertTrue(query1.equals(query1));
        assertTrue(query1.equals(query2));

        query2 = Query.createQuery("321", A_MONEY, "654", ERROR_DESCRIPTION, CollectionActionType.COLLECT, MovementStatus.PAID);

        assertFalse(query1.equals(query2));
        assertFalse(query1.equals(null));
        assertFalse(query1.equals(Boolean.TRUE));
    }

    @Test
    public void hashCodeTest() {
        Query query1 = Query.createQuery(queryDetails.getOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, CollectionActionType.COLLECT, MovementStatus.PAID);

        assertTrue(query1.hashCode() == query1.hashCode());

        Query query2 = Query.createQuery("321", A_MONEY, "654", ERROR_DESCRIPTION, CollectionActionType.COLLECT, MovementStatus.PAID);

        assertFalse(query1.hashCode() == query2.hashCode());
    }
}
