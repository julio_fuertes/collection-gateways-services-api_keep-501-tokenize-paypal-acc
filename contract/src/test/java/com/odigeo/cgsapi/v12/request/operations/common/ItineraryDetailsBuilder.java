package com.odigeo.cgsapi.v12.request.operations.common;

import java.util.Arrays;
import java.util.List;

/**
 * Created on 21/04/2016.
 */
public class ItineraryDetailsBuilder {

    public static ItineraryDetails getItineraryDetails() {
        List<String> pnrList = Arrays.asList(new String[]{"XF234", "PNR TEST", "GS8X3", "PY3X9"});
        List<Segment> segments = Arrays.asList(new Segment[]{SegmentBuilder.getSegment(), SegmentBuilder.getSegment2()});
        return new ItineraryDetails(pnrList, segments);
    }

    public static ItineraryDetails getItineraryDetails2() {
        List<String> pnrList = Arrays.asList(new String[]{"GS8X3", "PY3X9"});
        List<Segment> segments = Arrays.asList(new Segment[]{SegmentBuilder.getSegment2(), SegmentBuilder.getSegment3()});
        return new ItineraryDetails(pnrList, segments);
    }

}
