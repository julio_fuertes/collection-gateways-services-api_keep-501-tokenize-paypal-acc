package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.banktransfer;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import org.mockito.Mock;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * @author javier.garcia
 * @since 26/07/13
 */
public class BankTransferCollectionDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private CollectionShoppingDetails collectionShoppingDetails;
    @Mock
    private CollectionContext collectionContext;

    @Test
    public void testBankTransferCollectionDetails() {
        BankTransferUserDetails userDetails = new BankTransferUserDetails("PurchaserFirstname", "PurchaserLastname", "PurchaserCity", "ES");
        BankTransferCollectionDetails details = new BankTransferCollectionDetails("orderId", A_MONEY, collectionContext, userDetails, collectionShoppingDetails);
        assertEquals(details.getOrderId(), "orderId");
        assertEquals(details.getMoney(), A_MONEY);
        assertEquals(details.getCollectionContext(), collectionContext);

        assertEquals(details.getPurchaserCity(), "PurchaserCity");
        assertEquals(details.getPurchaserFirstname(), "PurchaserFirstname");
        assertEquals(details.getPurchaserLastname(), "PurchaserLastname");
        assertEquals(details.getWebsiteCountryCode(), "ES");

        userDetails.setPurchaserCpf("FiscalNumber");
        assertEquals(details.getPurchaserCpf(), "FiscalNumber");

        userDetails.setPurchaserStreet("Street");
        assertEquals(details.getPurchaserStreet(), "Street");

        userDetails.setPurchaserState("State");
        assertEquals(details.getPurchaserState(), "State");

        userDetails.setPurchaserEmail("Email");
        assertEquals(details.getPurchaserEmail(), "Email");

        userDetails.setPurchaserCountryCode("GB");
        assertEquals(details.getPurchaserCountryCode(), "GB");

        try {
            details.toConfirmationDetails(null);
            fail("fail");
        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
        }

        try {
            details.toRefundDetails(null, null);
            fail("fail");
        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
        }

        BankTransferCollectionDetails details2 = new BankTransferCollectionDetails("orderId", A_MONEY, collectionContext, userDetails, collectionShoppingDetails);
        assertTrue(details.equals(details2));
        assertEquals(details.hashCode(), details2.hashCode());
        assertNotNull(details2.toString());
        assertTrue(details2.equals(details2));
        assertFalse(details2.equals("test"));
        assertFalse(details2.equals(null));

        userDetails = new BankTransferUserDetails("PurchaserFirstname", "PurchaserLastname", "PurchaserCity", "ES");
        BankTransferUserDetails userDetails2 = new BankTransferUserDetails("PurchaserFirstname", "PurchaserLastname", "PurchaserCity", "ES");
        assertTrue(userDetails.equals(userDetails2));
        assertEquals(userDetails.hashCode(), userDetails2.hashCode());
        assertNotNull(userDetails2.toString());
        assertTrue(userDetails2.equals(userDetails2));
        assertFalse(userDetails2.equals("test"));
        assertFalse(userDetails2.equals(null));
    }
}
