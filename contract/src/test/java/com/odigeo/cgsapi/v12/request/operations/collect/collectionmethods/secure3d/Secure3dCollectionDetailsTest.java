package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.secure3d;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.verify.VerificationDetails;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Currency;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class Secure3dCollectionDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private CreditCard creditCard;
    @Mock
    private CollectionContext collectionContext;
    @Mock
    private CollectionShoppingDetails collectionShoppingDetails;

    private GatewayCollectionMethod gatewayCollectionMethod = new GatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D);

    private static final Map<String, String> ANY_MAP = Collections.emptyMap();

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(creditCard.getType()).thenReturn(CreditCardType.MASTER_CARD);
    }

    @Test
    public void secure3dCollectionDetails() {
        when(creditCard.getType()).thenReturn(CreditCardType.VISA_CREDIT);
        Secure3dCollectionDetails secure3dCollectionDetails = new Secure3dCollectionDetails("123", A_MONEY, creditCard, gatewayCollectionMethod.getGatewayCollectionMethodType(), collectionContext, collectionShoppingDetails,
                "https://www.foo.com/callback", "https://www.foo.com/finish");

        assertEquals(secure3dCollectionDetails.getOrderId(), "123");
        assertEquals(secure3dCollectionDetails.getMoney(), A_MONEY);
        assertEquals(secure3dCollectionDetails.getCreditCard(), creditCard);
        assertEquals(secure3dCollectionDetails.getGatewayCollectionMethod(), new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D, creditCard.getType()));
        assertEquals(secure3dCollectionDetails.getCollectionContext(), collectionContext);
        assertEquals(secure3dCollectionDetails.getCollectionShoppingDetails(), collectionShoppingDetails);
        assertEquals(secure3dCollectionDetails.getCallbackUrl(), "https://www.foo.com/callback");
        assertEquals(secure3dCollectionDetails.getFinishUrl(), "https://www.foo.com/finish");
    }

    @Test
    public void toRefund() {
        when(creditCard.getType()).thenReturn(CreditCardType.VISA_CREDIT);
        Secure3dCollectionDetails secure3dCollectionDetails = new Secure3dCollectionDetails("123", A_MONEY, creditCard, gatewayCollectionMethod.getGatewayCollectionMethodType(), collectionContext, collectionShoppingDetails,
                "https://www.foo.com/callback", "https://www.foo.com/finish");
        RefundDetails refundDetails = secure3dCollectionDetails.toRefundDetails("123", "123");

        assertEquals(refundDetails.getTransactionId(), "123");
    }

    @Test
    public void toConfirmationDetails() {
        when(creditCard.getType()).thenReturn(CreditCardType.VISA_CREDIT);
        Secure3dCollectionDetails secure3dCollectionDetails = new Secure3dCollectionDetails("123", A_MONEY, creditCard, gatewayCollectionMethod.getGatewayCollectionMethodType(), collectionContext, collectionShoppingDetails,
                "https://www.foo.com/callback", "https://www.foo.com/finish");
        ConfirmationDetails confirmationDetails = secure3dCollectionDetails.toConfirmationDetails("123");

        assertEquals(confirmationDetails.getMerchantOrderId(), "123");
    }

    @Test
    public void confirmWith() {
        when(creditCard.getType()).thenReturn(CreditCardType.VISA_CREDIT);
        Secure3dCollectionDetails secure3dCollectionDetails = new Secure3dCollectionDetails("123", A_MONEY, creditCard, gatewayCollectionMethod.getGatewayCollectionMethodType(), collectionContext, collectionShoppingDetails,
                "https://www.foo.com/callback", "https://www.foo.com/finish");
        VerificationDetails verificationDetails = secure3dCollectionDetails.confirmWith(ANY_MAP, "12345", CollectionActionType.COLLECT, collectionContext, 1, "finishUrl");

        assertEquals(verificationDetails.getMerchantOrderId(), "12345");
        assertEquals(verificationDetails.getCollectionActionType(), CollectionActionType.COLLECT);
        assertEquals(verificationDetails.getInteractionStep().intValue(), 1);
    }

    @Test
    public void testEquals() {
        Secure3dCollectionDetails collectionDetails = new Secure3dCollectionDetails("111", A_MONEY, creditCard, gatewayCollectionMethod.getGatewayCollectionMethodType(), collectionContext, collectionShoppingDetails, "https://www.foo.com/callback", "https://www.foo.com/finish");
        Secure3dCollectionDetails collectionDetails2 = new Secure3dCollectionDetails("222", A_MONEY, creditCard, gatewayCollectionMethod.getGatewayCollectionMethodType(), collectionContext, collectionShoppingDetails, "https://www.foo.com/callback", "https://www.foo.com/finish");
        assertTrue(collectionDetails.equals(collectionDetails));
        assertFalse(collectionDetails.equals(collectionDetails2));
        assertFalse(collectionDetails.equals(null));
    }

    @Test
    public void testHashCode() {
        Secure3dCollectionDetails collectionDetails = new Secure3dCollectionDetails("111", A_MONEY, creditCard, gatewayCollectionMethod.getGatewayCollectionMethodType(), collectionContext, collectionShoppingDetails, "https://www.foo.com/callback", "https://www.foo.com/finish");
        Secure3dCollectionDetails collectionDetails2 = new Secure3dCollectionDetails("111", A_MONEY, creditCard, gatewayCollectionMethod.getGatewayCollectionMethodType(), collectionContext, collectionShoppingDetails, "https://www.foo.com/callback", "https://www.foo.com/finish");
        Secure3dCollectionDetails collectionDetails3 = new Secure3dCollectionDetails("333", A_MONEY, creditCard, gatewayCollectionMethod.getGatewayCollectionMethodType(), collectionContext, collectionShoppingDetails, "https://www.foo.com/callback", "https://www.foo.com/finish");
        assertEquals(collectionDetails.hashCode(), collectionDetails2.hashCode());
        assertNotEquals(collectionDetails.hashCode(), collectionDetails3.hashCode());
    }
}
