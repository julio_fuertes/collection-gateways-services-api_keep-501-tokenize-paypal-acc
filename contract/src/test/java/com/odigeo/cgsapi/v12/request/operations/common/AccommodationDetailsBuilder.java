package com.odigeo.cgsapi.v12.request.operations.common;

import com.odigeo.cgsapi.v12.LocalizedDate;
import org.apache.commons.lang.time.DateUtils;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created on 21/04/2016.
 */
public class AccommodationDetailsBuilder {

    public static AccommodationDetails getAccommodationDetails() {

        String hotelName = "Ace Hotel New York";
        Calendar cal = Calendar.getInstance();
        cal.set(2016, 4, 21);
        LocalizedDate checkInDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());
        cal.set(2016, 4, 24);
        LocalizedDate checkOutDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());
        String address = "20 W 29th St";
        String city = "New York";
        String country = "USA";

        return new AccommodationDetails(hotelName, checkInDate, checkOutDate, address, city, country);
    }


    public static AccommodationDetails getAccommodationDetails2() {

        String hotelName = "Palace Hotel Tokyo";
        Calendar cal = Calendar.getInstance();
        cal.set(2012, 1, 6);
        LocalizedDate checkInDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());
        cal.set(2012, 1, 9);
        LocalizedDate checkOutDate = new LocalizedDate(TimeZone.getDefault().toString(), DateUtils.truncate(cal, Calendar.HOUR_OF_DAY).getTime());
        String address = "Chiyoda, Marunouchi, 1 Chome−1−1";
        String city = "Tokyo";
        String country = "Japan";

        return new AccommodationDetails(hotelName, checkInDate, checkOutDate, address, city, country);
    }

}
