package com.odigeo.cgsapi.v12.response.operations.query;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.operations.query.QueryDetails;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * Created by vrieraba on 01/06/2015.
 */
public class QueryTest {
    private static final String A_MERCHANT_ORDER_ID = "123";
    private static final String ERROR_CODE = "1";
    private static final String ERROR_DESCRIPTION = "error";
    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Mock
    private QueryDetails queryDetails;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(queryDetails.getOrderId()).thenReturn(A_MERCHANT_ORDER_ID);
    }

    @Test
    public void createQuery() {
        Query query = Query.createQuery(queryDetails.getOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, CollectionActionType.COLLECT, MovementStatus.PAID);

        assertNotNull(query);
        assertEquals(query.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(query.getCode(), ERROR_CODE);
        assertEquals(query.getErrorDescription(), ERROR_DESCRIPTION);
        assertEquals(query.getAction(), MovementAction.QUERY);
        assertEquals(query.getStatus(), MovementStatus.PAID);
        assertEquals(query.getCollectionActionType(), CollectionActionType.COLLECT);
    }

    @Test
    public void createFailedQuery() {
        Query query = Query.createFailedQuery(queryDetails.getOrderId(), A_MONEY, ERROR_CODE, ERROR_DESCRIPTION, CollectionActionType.COLLECT);

        assertNotNull(query);
        assertEquals(query.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(query.getCode(), ERROR_CODE);
        assertEquals(query.getErrorDescription(), ERROR_DESCRIPTION);
        assertEquals(query.getAction(), MovementAction.QUERY);
        assertEquals(query.getStatus(), MovementStatus.QUERYERR);
        assertEquals(query.getCollectionActionType(), CollectionActionType.COLLECT);

    }

    @Test
    public void createFakeQuery() {
        Query query = Query.createFakeQuery(queryDetails.getOrderId(), A_MONEY, CollectionActionType.COLLECT, MovementStatus.QUERYERR);

        assertNotNull(query);
        assertEquals(query.getMerchantOrderId(), A_MERCHANT_ORDER_ID);
        assertEquals(query.getAction(), MovementAction.QUERY);
        assertEquals(query.getStatus(), MovementStatus.QUERYERR);
        assertEquals(query.getCollectionActionType(), CollectionActionType.COLLECT);

    }
}
