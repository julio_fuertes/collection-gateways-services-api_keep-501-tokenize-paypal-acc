package com.odigeo.cgsapi.v12.request;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class GenderTest {

    @Test
    public void testGenderLength() {
        assertEquals(Gender.MALE.name(), "MALE");
        assertEquals(Gender.FEMALE.name(), "FEMALE");
        assertEquals(Gender.values().length, 2);
    }
}
