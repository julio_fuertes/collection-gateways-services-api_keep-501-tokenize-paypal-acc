package com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.alipay;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;

public class AlipayRefundDetailsTest {

    private static final Money A_MONEY = Money.createMoney(new BigDecimal("12.35"), Currency.getInstance("EUR"));

    @Test
    public void alipayConfirmationDetailsTest() {
        AlipayRefundDetails details = new AlipayRefundDetails("123", A_MONEY, "transactionId", "paymentCountry");
        assertEquals(details.getPaymentCountryCode(), "paymentCountry");
        assertEquals(details.getGatewayCollectionMethod().getGatewayCollectionMethodType(), GatewayCollectionMethodType.ALIPAY);
        assertEquals(details.getMoney(), A_MONEY);
        assertEquals(details.getMerchantOrderId(), "123");
        assertEquals(details.getTransactionId(), "transactionId");
    }
}
