package com.odigeo.cgsapi.v12;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.deser.std.DateDeserializer;
import org.codehaus.jackson.map.ser.std.DateSerializer;

import java.io.Serializable;
import java.util.Date;

/**
 * Created on 27/04/2016.
 */

public class LocalizedDate implements Serializable {

    private static final long serialVersionUID = 1L;

    private String timeZoneCode;

    @JsonSerialize(using = DateSerializer.class, include = JsonSerialize.Inclusion.NON_EMPTY)
    @JsonDeserialize(using = DateDeserializer.class)
    private Date dateTime;

    public LocalizedDate() {
    }

    public LocalizedDate(String timeZoneCode, Date dateTime) {
        this.timeZoneCode = timeZoneCode;
        this.dateTime = new Date(dateTime.getTime());
    }

    public String getTimeZoneCode() {
        return timeZoneCode;
    }

    public Date getDateTime() {
        Date rtn = null;
        if (dateTime != null) {
            rtn = new Date(dateTime.getTime());
        }
        return rtn;

    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        LocalizedDate rhs = (LocalizedDate) obj;
        return new org.apache.commons.lang.builder.EqualsBuilder()
                .append(this.timeZoneCode, rhs.timeZoneCode)
                .append(this.dateTime.getTime(), rhs.dateTime.getTime())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new org.apache.commons.lang.builder.HashCodeBuilder()
                .append(timeZoneCode)
                .append(dateTime.getTime())
                .toHashCode();
    }

    @Override
    public String toString() {
        return "LocalizedDate{"
                + "timeZoneCode='" + timeZoneCode + '\''
                + ", dateTime=" + dateTime
                + '}';
    }
}
