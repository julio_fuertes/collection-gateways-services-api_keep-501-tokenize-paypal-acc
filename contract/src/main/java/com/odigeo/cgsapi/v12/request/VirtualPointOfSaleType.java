package com.odigeo.cgsapi.v12.request;

/**
 * Created by xavier.tous on 30/12/2014.
 */
public enum VirtualPointOfSaleType {
    SERMEPA,
    GC,
    OGONE,
    PAYPAL,
    WORLDPAY,
    ADYEN,
    EPG
}
