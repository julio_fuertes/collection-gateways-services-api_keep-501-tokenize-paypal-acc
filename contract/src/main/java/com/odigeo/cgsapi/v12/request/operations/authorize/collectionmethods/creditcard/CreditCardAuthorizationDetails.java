package com.odigeo.cgsapi.v12.request.operations.authorize.collectionmethods.creditcard;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.RecurringDetails;
import com.odigeo.cgsapi.v12.request.RecurringPayment;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardHolder;
import com.odigeo.cgsapi.v12.request.operations.authorize.AuthorizationDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * An object with needed information to perform a authorization with a commerce using a {@link CreditCard}.
 *
 * @see AuthorizationDetails
 */
@JsonTypeName("CreditCardAuthorizationDetails")
public class CreditCardAuthorizationDetails extends AuthorizationDetails implements CreditCardHolder, RecurringPayment {
    private static final long serialVersionUID = 1L;

    private CreditCard creditCard;

    private RecurringDetails recurringDetails;

    /*
    Default no-args constructor for Dozer conversion
     */

    protected CreditCardAuthorizationDetails() {
        super();
    }

    public CreditCardAuthorizationDetails(String orderId, Money money, CreditCard creditCard, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails) {
        super(orderId, money, new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, creditCard.getType()), collectionContext, collectionShoppingDetails);
        this.creditCard = creditCard;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    @Override
    public RecurringDetails getRecurringDetails() {
        return recurringDetails;
    }

    @Override
    public void setRecurringDetails(RecurringDetails recurringDetails) {
        this.recurringDetails = recurringDetails;
    }
}

