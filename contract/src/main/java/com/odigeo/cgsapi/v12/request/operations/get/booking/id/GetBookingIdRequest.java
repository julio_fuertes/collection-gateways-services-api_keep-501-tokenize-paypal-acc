package com.odigeo.cgsapi.v12.request.operations.get.booking.id;

import java.io.Serializable;

public class GetBookingIdRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String merchantOrderId;

    //For Dozer
    private GetBookingIdRequest() {
        merchantOrderId = null;
    }

    GetBookingIdRequest(GetBookingIdRequestBuilder builder) {
        merchantOrderId = builder.getMerchantOrderId();
    }

    public String getMerchantOrderId() {
        return merchantOrderId;
    }

}
