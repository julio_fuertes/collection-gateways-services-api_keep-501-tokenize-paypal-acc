package com.odigeo.cgsapi.v12.response;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Map;

/**
 * Contains information used for redirection when a collection operation needs user interaction (Paypal or 3d Secure).
 */
public class RedirectionParameters implements Serializable {

    private static final long serialVersionUID = 1L;

    private final RedirectionType redirectionType;
    private final Integer redirectionStep;
    private final String redirectionUrl;
    private final RedirectionHttpMethod redirectionMethod;
    private final Map<String, String> parameters;
    private final String html;
    private final String javascriptSnippet;

    //no-args constructor for API REST
    private RedirectionParameters() {
        redirectionType = null;
        redirectionStep = null;
        redirectionUrl = null;
        redirectionMethod = null;
        parameters = null;
        html = null;
        javascriptSnippet = null;
    }

    public RedirectionParameters(RedirectionParametersBuilder builder) {
        redirectionType = builder.getRedirectionType();
        redirectionStep = builder.getRedirectionStep();
        redirectionUrl = builder.getRedirectionUrl();
        redirectionMethod = builder.getRedirectionMethod();
        parameters = builder.getParameters();
        html = builder.getHtml();
        javascriptSnippet = builder.getJavascriptSnippet();
    }

    /**
     * Returns redirection URL.
     *
     * @return redirection URL.
     */
    public String getRedirectionUrl() {
        return redirectionUrl;
    }

    /**
     * Returns {@link RedirectionHttpMethod}.
     *
     * @return {@link RedirectionHttpMethod}.
     */
    public RedirectionHttpMethod getRedirectionMethod() {
        return redirectionMethod;
    }

    /**
     * Returns extra parameters.
     *
     * @return extra parameters.
     */
    public Map<String, String> getParameters() {
        return parameters;
    }

    /**
     * Returns the type of the redirection.
     *
     * @return {@link RedirectionType}.
     */
    public RedirectionType getRedirectionType() {
        return redirectionType;
    }

    /**
     * Returns the number of step of the redirection.
     *
     * @return redirectionStep.
     */
    public Integer getRedirectionStep() {
        return redirectionStep;
    }

    /**
     * Returns html.
     *
     * @return html.
     */
    public String getHtml() {
        return html;
    }

    /**
     * Snippet containing the piece of javascript to be including in front-ends.
     *
     * @return javascript.
     */
    public String getJavascriptSnippet() {
        return javascriptSnippet;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RedirectionParameters)) {
            return false;
        }

        RedirectionParameters that = (RedirectionParameters) o;

        return new EqualsBuilder()
                .append(redirectionType, that.redirectionType)
                .append(redirectionUrl, that.redirectionUrl)
                .append(redirectionMethod, that.redirectionMethod)
                .append(parameters, that.parameters)
                .append(html, that.html)
                .append(redirectionStep, that.redirectionStep)
                .append(javascriptSnippet, that.javascriptSnippet)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .append(redirectionType)
                .append(redirectionUrl)
                .append(redirectionMethod)
                .append(parameters)
                .append(html)
                .append(redirectionStep)
                .append(javascriptSnippet)
                .toHashCode();
    }
}
