package com.odigeo.cgsapi.v12.response.operations.notify;

public class ProviderNotificationResponseBuilder {

    private String notificationId;
    private Result result;
    private String message;

    public String getNotificationId() {
        return notificationId;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }

    public ProviderNotificationResponseBuilder notificationId(String notificationId) {
        this.notificationId = notificationId;
        return this;
    }

    public ProviderNotificationResponseBuilder result(Result result) {
        this.result = result;
        return this;
    }

    public ProviderNotificationResponseBuilder message(String message) {
        this.message = message;
        return this;
    }

    public ProviderNotificationResponse build() {
        return new ProviderNotificationResponse(this);
    }
}
