package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.elv;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

public class BankDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    private String bankAccountNumber;
    private String bankLocationId;

    //no-args constructor for API REST
    public BankDetails() {
    }

    /**
     * Default constructor.
     *
     * @param bankAccountNumber
     * @param bankLocationId
     */
    public BankDetails(String bankAccountNumber, String bankLocationId) {
        this.bankAccountNumber = bankAccountNumber;
        this.bankLocationId = bankLocationId;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public String getBankLocationId() {
        return bankLocationId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        BankDetails rhs = (BankDetails) obj;
        return new EqualsBuilder()
                .append(this.bankAccountNumber, rhs.bankAccountNumber)
                .append(this.bankLocationId, rhs.bankLocationId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(bankAccountNumber)
                .append(bankLocationId)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("bankAccountNumber", bankAccountNumber)
                .append("bankLocationId", bankLocationId)
                .toString();
    }
}
