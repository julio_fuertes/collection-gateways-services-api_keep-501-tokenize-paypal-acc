package com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.sofort;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;

/**
 * An object with needed information to perform a refund operation with a commerce using an Sofort account.
 *
 * @author egonz
 * @see RefundDetails
 */
public class SofortRefundDetails extends RefundDetails {

    private static final long serialVersionUID = 1L;

    //no-args constructor for API REST
    public SofortRefundDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId Identifier of the collection transaction to confirm this transaction.
     * @param money           {@link Money} to be refunded.
     * @param transactionId   Identifier for collection transaction that preceded this refund transaction
     */
    public SofortRefundDetails(String merchantOrderId, Money money, String transactionId) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.SOFORT), transactionId);
    }
}
