package com.odigeo.cgsapi.v12.request.operations.authorize.collectionmethods.paypal;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.RecurringDetails;
import com.odigeo.cgsapi.v12.request.RecurringPayment;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.authorize.AuthorizationDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * An object with needed information to perform am authorization operation with a commerce using a PayPal account.
 *
 * @see AuthorizationDetails
 */
@JsonTypeName("PaypalAuthorizationDetails")
public class PaypalAuthorizationDetails extends AuthorizationDetails implements RecurringPayment {

    private static final long serialVersionUID = 1L;

    private String resumeBaseUrl;

    private RecurringDetails recurringDetails;

    /*default no-args constructor for Dozer*/
    private PaypalAuthorizationDetails() {
    }

    /**
     * DEPRECATED
     * Constructor.
     *
     * @param orderId                   Identifier for collection operation.
     * @param money                     {@link Money} to be collected.
     * @param collectionContext         {@link CollectionContext} used in collection.
     * @param collectionShoppingDetails {@link CollectionShoppingDetails} used in collection.
     * @param resumeBaseUrl             Resume URL.
     */
    @Deprecated
    public PaypalAuthorizationDetails(String orderId, Money money, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails, String resumeBaseUrl) {
        super(orderId, money, new GatewayCollectionMethod(GatewayCollectionMethodType.PAYPAL), collectionContext, collectionShoppingDetails);
        this.resumeBaseUrl = resumeBaseUrl;
    }

    /**
     * Constructor.
     *
     * @param orderId                   Identifier for collection operation.
     * @param money                     {@link Money} to be collected.
     * @param gatewayCollectionMethod   {@link GatewayCollectionMethod} used in this collection
     * @param collectionContext         {@link CollectionContext} used in collection.
     * @param collectionShoppingDetails {@link CollectionShoppingDetails} used in collection.
     * @param resumeBaseUrl             Resume URL.
     */
    public PaypalAuthorizationDetails(String orderId, Money money, GatewayCollectionMethod gatewayCollectionMethod, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails, String resumeBaseUrl) {
        super(orderId, money, gatewayCollectionMethod, collectionContext, collectionShoppingDetails);
        this.resumeBaseUrl = resumeBaseUrl;

        if (gatewayCollectionMethod == null || !GatewayCollectionMethodType.PAYPAL.equals(gatewayCollectionMethod.getGatewayCollectionMethodType())) {
            throw new IllegalArgumentException("Invalid GatewayCollectionMethod");
        }
    }

    /**
     * Returns resume URL.
     *
     * @return Resume URL.
     */
    public String getResumeBaseUrl() {
        return resumeBaseUrl;
    }

    @Override
    public RecurringDetails getRecurringDetails() {
        return recurringDetails;
    }

    @Override
    public void setRecurringDetails(RecurringDetails recurringDetails) {
        this.recurringDetails = recurringDetails;
    }

}
