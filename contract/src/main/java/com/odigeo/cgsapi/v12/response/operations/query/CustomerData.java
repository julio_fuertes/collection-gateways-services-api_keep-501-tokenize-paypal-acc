package com.odigeo.cgsapi.v12.response.operations.query;

import java.io.Serializable;

/**
 * Created by ADEFREIT on 20/07/2015.
 */
public final class CustomerData implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String firstName;
    private final String middleName;
    private final String lastName;
    private final String sufix;
    private final String email;
    private final String countryCode;
    private final String business;

    /**
     * Constructor without parameters for Dozer
     */
    private CustomerData() {
        firstName = null;
        middleName = null;
        lastName = null;
        sufix = null;
        email = null;
        countryCode = null;
        business = null;
    }

    public CustomerData(Builder builder) {
        firstName = builder.firstName;
        middleName = builder.middleName;
        lastName = builder.lastName;
        sufix = builder.sufix;
        email = builder.email;
        countryCode = builder.countryCode;
        business = builder.business;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSufix() {
        return sufix;
    }

    public String getEmail() {
        return email;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getBusiness() {
        return business;
    }

    public static class Builder {

        private String firstName;
        private String middleName;
        private String lastName;
        private String sufix;
        private String email;
        private String countryCode;
        private String business;

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder middleName(String middleName) {
            this.middleName = middleName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder sufix(String sufix) {
            this.sufix = sufix;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder countryCode(String countryCode) {
            this.countryCode = countryCode;
            return this;
        }

        public Builder business(String business) {
            this.business = business;
            return this;
        }

        public CustomerData build() {
            return new CustomerData(this);
        }

    }
}
