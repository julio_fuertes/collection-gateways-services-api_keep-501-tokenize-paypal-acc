package com.odigeo.cgsapi.v12.response.operations.collect;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementContainer;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.RecurringContainer;
import com.odigeo.cgsapi.v12.response.RedirectionParameters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.List;

import static java.lang.String.format;

/**
 * An object with information about collection result with a commerce.
 */
public class CollectionResponse implements MovementContainer, RecurringContainer {

    private static final long serialVersionUID = 1L;

    private Integer numCountryCode;
    private CollectionStatus collectionStatus;
    private List<Movement> movements;
    private BigDecimal amount;
    private Currency currency;
    private RedirectionParameters redirectionParameters;
    private GatewayCollectionMethod gatewayCollectionMethod;
    private String recurringTransactionId;

    //no-args constructor for API REST
    public CollectionResponse() {
    }

    /**
     * Default constructor. This uses Builder pattern.
     *
     * @param builder Builder to create an instance.
     */
    CollectionResponse(Builder builder) {
        numCountryCode = builder.numCountryCode;
        collectionStatus = builder.collectionStatus;
        movements = Collections.unmodifiableList(builder.movements);
        amount = builder.amount;
        currency = builder.currency;
        redirectionParameters = builder.redirectionParameters;
        recurringTransactionId = builder.recurringTransactionId;
    }

    /**
     * Returns {@code true} if user interaction is needed.
     *
     * @return {@code true} if user interaction is needed, {@code false} otherwise.
     */
    public boolean needsRedirect() {
        return redirectionParameters != null;
    }

    /**
     * Returns numeric ISO 3166-1 code for credit card country, if a credit card was used for collection.
     * Full list of numeric codes can be found here:
     * <a href='http://unstats.un.org/unsd/methods/m49/m49alpha.htm'>http://unstats.un.org/unsd/methods/m49/m49alpha.htm</a>
     *
     * @return Numeric ISO 3166-1 code for credit card country, if a credit card was used for collection.
     */
    public Integer getNumCountryCode() {
        return numCountryCode;
    }

    /**
     * Sets {@link GatewayCollectionMethod} used in collection operation.
     *
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} used in collection operation.
     */
    public void setGatewayCollectionMethod(GatewayCollectionMethod gatewayCollectionMethod) {
        this.gatewayCollectionMethod = gatewayCollectionMethod;
    }

    /**
     * Returns {@link GatewayCollectionMethod} used in collection operation.
     *
     * @return {@link GatewayCollectionMethod} used in collection operation.
     */
    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

    /**
     * Returns {@link RedirectionParameters} used in case collection needs user interaction.
     *
     * @return {@link RedirectionParameters} used in case collection needs user interaction.
     */
    public RedirectionParameters getRedirectionParameters() {
        return redirectionParameters;
    }

    /**
     * Returns full list with every movement done in payment gateway.
     *
     * @return Full list with every movement done in payment gateway.
     */
    public Collection<Movement> getMovements() {
        return movements;
    }

    /**
     * Returns current status of collection.
     *
     * @return Current status of collection.
     */
    public CollectionStatus getCollectionStatus() {
        return collectionStatus;
    }

    /**
     * Returns numeric amount that has been collected.
     *
     * @return Numeric amount that has been collected.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Returns currency of ecommerce operation.
     *
     * @return currency of ecommerce operation.
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * Builder class for {@link CollectionResponse}.
     */

    /**
     * Indicates the recurring transaction reference for a recurring payment.
     *
     * @return recurring transaction reference if in a recurring payment.
     */
    public String getRecurringTransactionId() {
        return recurringTransactionId;
    }

    /**
     * Set the original transaction reference for a recurring payment.
     *
     * @param recurringTransactionId recurring transaction reference if in a recurring payment.
     */
    public void setRecurringTransactionId(String recurringTransactionId) {
        this.recurringTransactionId = recurringTransactionId;
    }

    public static class Builder {

        private final List<Movement> movements = new ArrayList<Movement>();
        private CollectionStatus collectionStatus;
        private BigDecimal amount;
        private Currency currency;
        private Integer numCountryCode;
        private RedirectionParameters redirectionParameters;
        private String recurringTransactionId;

        /**
         * Adds a {@link Movement} done with payment gateway to current movements list.
         *
         * @param val {@link Movement} done with payment gateway.
         * @return {@code this}.
         */
        public Builder addMovement(Movement val) {
            movements.add(val);
            collectionStatus = transform(val.getStatus());
            amount = val.getAmount();
            currency = val.getCurrency();
            return this;
        }

        /**
         * Sets numeric ISO 3166-1 code for credit card country, if a credit card was used for collection.
         *
         * @param val Numeric ISO 3166-1 code for credit card country.
         * @return {@code this}.
         */
        public Builder numCountryCode(Integer val) {
            numCountryCode = val;
            return this;
        }

        /**
         * Sets {@link RedirectionParameters} to be used in user interaction.
         *
         * @param val {@link RedirectionParameters} to be used in user interaction.
         * @return {@code this}.
         */
        public Builder redirectionParameters(RedirectionParameters val) {
            redirectionParameters = val;
            return this;
        }


        /**
         * Sets {recurringTransactionId} to be used.
         *
         * @param val to be used.
         * @return {@code this}.
         */
        public Builder recurringTransactionId(String val) {
            recurringTransactionId = val;
            return this;
        }

        /**
         * Transforms {@link MovementStatus} to {@link CollectionStatus}.
         *
         * @param movementStatus to be transformed.
         * @return {@link CollectionStatus} resulted.
         */
        private CollectionStatus transform(MovementStatus movementStatus) {
            CollectionStatus result;
            switch (movementStatus) {
            case NEEDS_INTERACTION:
            case INIT_INTERACTION:
                result = CollectionStatus.ON_HOLD;
                break;
            case DECLINED:
                result = CollectionStatus.COLLECTION_DECLINED;
                break;
            case PAID:
                result = CollectionStatus.COLLECTED;
                break;
            case PAYERROR:
                result = CollectionStatus.COLLECTION_ERROR;
                break;
            case USE_BATCH:
                result = CollectionStatus.USE_BATCH_COLLECTION;
                break;
            case REFUNDED:
                result = CollectionStatus.REFUNDED;
                break;
            case REFUNDERR:
                result = CollectionStatus.REFUND_ERROR;
                break;
            case REQUESTED:
                result = CollectionStatus.REQUESTED;
                break;
            default:
                throw new IllegalArgumentException(format("MovementStatus %s is not a valid status for collect operation", movementStatus));
            }
            return result;
        }

        /**
         * Builds {@link CollectionResponse} instance for this builder.
         *
         * @return {@link CollectionResponse} instance for this builder.
         */
        public CollectionResponse build() {
            return new CollectionResponse(this);
        }
    }
}
