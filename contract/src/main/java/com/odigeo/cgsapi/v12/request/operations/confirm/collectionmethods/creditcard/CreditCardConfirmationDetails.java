package com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.creditcard;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * An object with needed information to perform a confirmation operation with a commerce.
 */
public class CreditCardConfirmationDetails extends ConfirmationDetails {
    private static final long serialVersionUID = 1L;
    private CreditCard creditCard;

    //no-args constructor for API REST
    public CreditCardConfirmationDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId         Identifier of the collection transaction to confirm this transaction.
     * @param money                   {@link Money money} to be confirmed.
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} to be used.
     * @param creditCard              {@link CreditCard Credit card} to be used.
     */
    public CreditCardConfirmationDetails(String merchantOrderId, Money money, GatewayCollectionMethod gatewayCollectionMethod, CreditCard creditCard) {
        super(merchantOrderId, money, gatewayCollectionMethod);
        this.creditCard = creditCard;
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId         Identifier of the collection transaction to confirm this transaction.
     * @param money                   {@link Money money} to be confirmed.
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} to be used.
     * @param dynamicDescriptor       Dynamic descriptor show in customer´s bank statement. (Restrictions: Dynamic descriptor only allow the character ".", whitespaces and alphanumeric characters except "ñ". Upper and lower cases are allowed.)
     * @param creditCard              {@link CreditCard Credit card} to be used.
     */
    public CreditCardConfirmationDetails(String merchantOrderId, Money money, GatewayCollectionMethod gatewayCollectionMethod, String dynamicDescriptor, CreditCard creditCard) {
        super(merchantOrderId, money, gatewayCollectionMethod, dynamicDescriptor);
        this.creditCard = creditCard;
    }

    /**
     * Returns {@link CreditCard} to be used.
     *
     * @return {@link CreditCard} to be used.
     */
    public CreditCard getCreditCard() {
        return creditCard;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("creditCard", creditCard)
                .toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(creditCard)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        CreditCardConfirmationDetails rhs = (CreditCardConfirmationDetails) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.creditCard, rhs.creditCard)
                .isEquals();
    }
}
