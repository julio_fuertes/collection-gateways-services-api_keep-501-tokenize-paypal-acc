package com.odigeo.cgsapi.v12.response.operations.cancel;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Created by vrieraba on 28/05/2015.
 */
@JsonTypeName("Cancellation")
public final class Cancellation extends Movement {

    private static final long serialVersionUID = 1L;

    /*default no-args constructor for Dozer*/
    private Cancellation() {
    }

    /**
     * @param merchantOrderId  Merchant order id of this movement.
     * @param amount           Amount.
     * @param currency         Currency.
     * @param code             Merchant error code.
     * @param errorDescription Error description.
     * @param status           Movement status.
     */
    private Cancellation(String merchantOrderId, BigDecimal amount, Currency currency, String code, String errorDescription, MovementStatus status) {
        super(merchantOrderId, code, MovementAction.CANCEL);
        setAmount(amount);
        setCurrency(currency);
        setErrorDescription(errorDescription);
        setStatus(status);
    }

    /**
     * Factory method to create a Cancel movement
     *
     * @param merchantOrderId  Merchant order id of this movement.
     * @param money            Amount and Currency.
     * @param code             Merchant error code.
     * @param errorDescription Error description.
     * @return An Authorization.
     */
    public static Cancellation createCancellation(String merchantOrderId, Money money, String code, String errorDescription, MovementStatus status) {
        return new Cancellation(merchantOrderId, money != null ? money.getAmount() : null, money != null ? money.getCurrency() : null, code, errorDescription, status);
    }

    /**
     * Factory method that creates a Failed Cancellation
     *
     * @param merchantOrderId  Merchant order id of this movement.
     * @param money            Amount and Currency.
     * @param code             Merchant error code.
     * @param errorDescription Error description.
     * @return A Failed Authorization.
     */
    public static Cancellation createFailedCancellation(String merchantOrderId, Money money, String code, String errorDescription) {
        return new Cancellation(merchantOrderId, money != null ? money.getAmount() : null, money != null ? money.getCurrency() : null, code, errorDescription, MovementStatus.CANCELERR);
    }

    /**
     * Factory method that a Fake Cancellation
     *
     * @param merchantOrderId Merchant order id of this movement.
     * @param money           Amount and Currency.
     * @param status          Desired movement status
     * @return A Fake Authorization.
     */
    public static Cancellation createFakeCancellation(String merchantOrderId, Money money, MovementStatus status) {
        return createCancellation(merchantOrderId, money, FAKE_CODE, FAKE_ERROR_MESSAGE, status);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cancellation)) {
            return false;
        }
        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .appendSuper(super.hashCode())
                .toHashCode();
    }
}

