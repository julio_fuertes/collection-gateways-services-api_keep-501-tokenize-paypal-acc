package com.odigeo.cgsapi.v12.request.operations.common;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.deser.std.DateDeserializer;
import org.codehaus.jackson.map.ser.std.DateSerializer;

import java.io.Serializable;
import java.util.Date;

public class Traveller implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer travellerNumber;
    private String name;
    private String middleName;
    private String firstLastName;
    private String secondLastName;

    @JsonSerialize(using = DateSerializer.class, include = JsonSerialize.Inclusion.NON_EMPTY)
    @JsonDeserialize(using = DateDeserializer.class)
    private Date dateOfBirth;

    public Traveller() {
    }

    public Traveller(Integer travellerNumber, String name, String middleName, String firstLastName, String secondLastName, Date dateOfBirth) {
        this.travellerNumber = travellerNumber;
        this.name = name;
        this.middleName = middleName;
        this.firstLastName = firstLastName;
        this.secondLastName = secondLastName;
        this.dateOfBirth = dateOfBirth != null ? new Date(dateOfBirth.getTime()) : null;
    }

    public Integer getTravellerNumber() {
        return travellerNumber;
    }

    public String getName() {
        return name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getFirstLastName() {
        return firstLastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public Date getDateOfBirth() {
        return (dateOfBirth != null ? new Date(dateOfBirth.getTime()) : null);
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Traveller rhs = (Traveller) obj;
        return new EqualsBuilder()
                .append(this.travellerNumber, rhs.travellerNumber)
                .append(this.name, rhs.name)
                .append(this.middleName, rhs.middleName)
                .append(this.firstLastName, rhs.firstLastName)
                .append(this.secondLastName, rhs.secondLastName)
                .append(this.dateOfBirth, rhs.dateOfBirth)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(travellerNumber)
                .append(name)
                .append(middleName)
                .append(firstLastName)
                .append(secondLastName)
                .append(dateOfBirth)
                .toHashCode();
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("travellerNumber", travellerNumber)
                .append("name", name)
                .append("middleName", middleName)
                .append("firstLastName", firstLastName)
                .append("secondLastName", secondLastName)
                .append("dateOfBirth", dateOfBirth)
                .toString();
    }
}
