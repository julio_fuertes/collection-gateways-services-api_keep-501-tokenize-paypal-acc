package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.secure3d;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.RecurringDetails;
import com.odigeo.cgsapi.v12.request.RecurringPayment;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardHolder;
import com.odigeo.cgsapi.v12.request.operations.collect.CollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.secure3d.Secure3dConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.secure3d.Secure3dRefundDetails;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * An object with needed information to perform a collection operation with a
 * commerce using a 3d Secure.
 *
 * @see CollectionDetails
 */
@JsonTypeName("Secure3dCollectionDetails")
public class Secure3dCollectionDetails extends CollectionDetails implements CreditCardHolder, RecurringPayment {
    private static final long serialVersionUID = 1L;
    private CreditCard creditCard;
    @Deprecated
    private String callbackUrl;
    private String finishUrl;
    private RecurringDetails recurringDetails;

    //no-args constructor for API REST
    public Secure3dCollectionDetails() {
    }

    /**
     * Default constructor.
     *
     * @param orderId                   Identifier for collection operation.
     * @param money                     {@link Money} to be collected.
     * @param creditCard                {@link CreditCard} used in collection.
     * @param collectionContext         {@link CollectionContext} used in collection.
     * @param collectionShoppingDetails {@link CollectionShoppingDetails} used in collection.
     * @param callbackUrl               Callback URL.
     * @param finishUrl                 Finish URL.
     */
    @Deprecated
    public Secure3dCollectionDetails(String orderId, Money money, CreditCard creditCard, GatewayCollectionMethodType gatewayCollectionMethodType, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails, String callbackUrl, String finishUrl) {
        super(orderId, money, new CreditCardGatewayCollectionMethod(gatewayCollectionMethodType, creditCard.getType()), collectionContext, collectionShoppingDetails);
        this.creditCard = creditCard;
        this.callbackUrl = callbackUrl;
        this.finishUrl = finishUrl;
    }

    /**
     * Default constructor.
     *
     * @param orderId                   Identifier for collection operation.
     * @param money                     {@link Money} to be collected.
     * @param creditCard                {@link CreditCard} used in collection.
     * @param collectionContext         {@link CollectionContext} used in collection.
     * @param collectionShoppingDetails {@link CollectionShoppingDetails} used in collection.
     * @param finishUrl                 Finish URL.
     */
    public Secure3dCollectionDetails(String orderId, Money money, CreditCard creditCard, GatewayCollectionMethodType gatewayCollectionMethodType, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails, String finishUrl) {
        super(orderId, money, new CreditCardGatewayCollectionMethod(gatewayCollectionMethodType, creditCard.getType()), collectionContext, collectionShoppingDetails);
        this.creditCard = creditCard;
        this.finishUrl = finishUrl;
    }

    /**
     * Returns credit card.
     *
     * @return Credit card.
     */
    @Override
    public CreditCard getCreditCard() {
        return creditCard;
    }

    /**
     * Returns callback URL.
     *
     * @return Callback URL.
     */
    @Deprecated
    public String getCallbackUrl() {
        return callbackUrl;
    }

    /**
     * Returns finish URL.
     *
     * @return finish URL.
     */
    public String getFinishUrl() {
        return finishUrl;
    }

    /**
     * Transforms current {@code CollectionDetails} instance into a
     * {@link RefundDetails} instance. This method must be overriden in every
     *
     * @param transactionId Identifier for collection transaction that preceded this
     *                      refund transaction
     * @return {@link RefundDetails} instance with same parameters as current
     * {@code CollectionDetails} instance.
     */
    @Override
    public RefundDetails toRefundDetails(String transactionId, String merchantOrderId) {
        return new Secure3dRefundDetails(merchantOrderId, getMoney(), creditCard, transactionId);
    }

    /**
     * Transforms current {@code Secure3dCollectionDetails} instance into a
     * {@link Secure3dConfirmationDetails}
     * instance.
     *
     * @param merchantOrderId Identifier for collection transaction that preceded this
     *                        refund transaction.
     * @return {@link Secure3dConfirmationDetails}
     * instance with same parameters as current
     * {@code Secure3dCollectionDetails} instance.
     */
    @Override
    public ConfirmationDetails toConfirmationDetails(String merchantOrderId) {
        return new Secure3dConfirmationDetails(merchantOrderId, getMoney(), creditCard);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Secure3dCollectionDetails rhs = (Secure3dCollectionDetails) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.creditCard, rhs.creditCard)
                .append(this.callbackUrl, rhs.callbackUrl)
                .append(this.finishUrl, rhs.finishUrl)
                .append(this.recurringDetails, rhs.recurringDetails)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(creditCard)
                .append(callbackUrl)
                .append(finishUrl)
                .append(recurringDetails)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("creditCard", creditCard)
                .append("callbackUrl", callbackUrl)
                .append("finishUrl", finishUrl)
                .append("recurringDetails", recurringDetails)
                .toString();
    }

    @Override
    public RecurringDetails getRecurringDetails() {
        return recurringDetails;
    }

    @Override
    public void setRecurringDetails(RecurringDetails recurringDetails) {
        this.recurringDetails = recurringDetails;
    }
}
