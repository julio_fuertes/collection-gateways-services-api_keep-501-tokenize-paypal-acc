package com.odigeo.cgsapi.v12.request.collectionmethods.creditcard;

/**
 * @author amunoz
 * @since 13-mar-2012
 */
public enum CreditCardType {

    VISA_CREDIT("VI", null),
    VISA_DEBIT("VD", VISA_CREDIT),
    MASTER_CARD("CA", null),
    MASTER_CARD_DEBIT("MD", MASTER_CARD),
    MAESTRO("MA", null),
    DINERS_CLUB("DC", null),
    AMERICAN_EXPRESS("AX", null),
    VISA_DELTA("DL", VISA_DEBIT),
    JCB("JC", null),
    VISA_ELECTRON("VE", VISA_DEBIT),
    CARTE_BANCAIRE("CB", null);

    private String code;
    private CreditCardType superType;

    CreditCardType(String code, CreditCardType superType) {
        this.code = code;
        this.superType = superType;
    }

    public String getCode() {
        return code;
    }

    public CreditCardType getSuperType() {
        return superType;
    }

    /**
     * Returns creditCardType for given code.
     *
     * @param code Code of creditCardType.
     * @return creditCardType for given code.
     * @throws IllegalArgumentException if no creditCardType exists for given code.
     */
    public static CreditCardType findByCode(String code) {
        for (CreditCardType creditCardType : values()) {
            if (creditCardType.code.equals(code)) {
                return creditCardType;
            }
        }
        throw new IllegalArgumentException("No CreditCardType for code: " + code);
    }
}
