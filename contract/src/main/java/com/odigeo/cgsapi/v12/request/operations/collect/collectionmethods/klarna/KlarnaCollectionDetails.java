package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.klarna;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.klarna.KlarnaAccountHolder;
import com.odigeo.cgsapi.v12.request.operations.collect.CollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * Created by ggomezbe on 03/04/2018.
 */
@JsonTypeName("KlarnaCollectionDetails")
public final class KlarnaCollectionDetails extends CollectionDetails {

    private static final long serialVersionUID = 1L;

    private final KlarnaAccountHolder klarnaAccountHolder;

    /*default no-args constructor for Dozer*/
    private KlarnaCollectionDetails() {
        klarnaAccountHolder = null;
    }

    public KlarnaCollectionDetails(String orderId, Money money, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails, KlarnaAccountHolder klarnaAccountHolder) {
        super(orderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.KLARNA), collectionContext, collectionShoppingDetails);
        this.klarnaAccountHolder = klarnaAccountHolder;
    }

    public KlarnaAccountHolder getKlarnaAccountHolder() {
        return klarnaAccountHolder;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("klarnaAccountHolder", klarnaAccountHolder)
                .toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        KlarnaCollectionDetails rhs = (KlarnaCollectionDetails) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.klarnaAccountHolder, rhs.klarnaAccountHolder)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(klarnaAccountHolder)
                .toHashCode();
    }

}
