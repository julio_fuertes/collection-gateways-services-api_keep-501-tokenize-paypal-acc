package com.odigeo.cgsapi.v12.request;

/**
 * Created by ggomezbe on 16/04/2018.
 */
public enum Gender {
    MALE, FEMALE
}
