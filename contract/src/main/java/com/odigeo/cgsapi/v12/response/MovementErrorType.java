package com.odigeo.cgsapi.v12.response;

/**
 * A payment gateway movement error type. A payment gateway movement can have one of following error types:
 * <ul>
 * <li>{@link #WRONG_CVV2} Caused by wrong CVV2.</li>
 * <li>{@link #FRAUD_SUSPECT} Caused by a fraud suspect.</li>
 * <li>{@link #WRONG_CREDITCARD_EXPIRATION_DATE} Caused by wrong credit card expiration date.</li>
 * <li>{@link #INSUFFICIENT_CREDIT} Caused by insufficient credit in given collection method.</li>
 * <li>{@link #WRONG_ELV_ACCOUNT} Caused by wrong ELV account.</li>
 * <li>{@link #UNKNOWN} When no other error type can be mapped.</li>
 * <li>{@link #UNSUPPORTED_GATEWAY_COLLECTION_METHOD} Caused by unsupported collection method.</li>
 * <li>{@link #ORDER_NOT_AUTHORIZE} Caused by unsupported collection method.</li>
 * <li>{@link #UNKNOWN_ORDER} No exist any transaction related with the order id provided.</li>
 * <li>{@link #CARD_NOT_IN_AUTHORIZERS_DB} Card not into database.</li>
 * <li>{@link #ACQUIRER_UNAVAILABLE} Acquirer is unavailable.</li>
 * <li>{@link #INVALID_CARD_NUMBER} Invalid card number.</li>
 * <li>{@link #CARD_EXPIRED} The credit card is expired.</li>
 * <li>{@link #DATA_VALIDATION_ERROR} Data is wrong or invalid.</li>
 * <li>{@link #UNKNOWN_ORIGIN_IP} Unknown origin ip address.</li>
 * <li>{@link #INVALID_AMOUNT} Invalid amount to charge.</li>
 * <li>{@link #INVALID_CURRENCY} Invalid currency.</li>
 * <li>{@link #DECLINED} Declined authorization.</li>
 * <li>{@link #ORDER_ALREADY_USED} Order ID has already been used.</li>
 * <li>{@link #TRANSACTION_NO_EXIST} Transaction id no exist.</li>
 *
 * </ul>
 * A movement can only have one error type.
 */
public enum MovementErrorType {

    /**
     * Caused by wrong CVV2.
     */
    WRONG_CVV2,
    /**
     * Caused by a fraud suspect.
     */
    FRAUD_SUSPECT,
    /**
     * Caused by insufficient credit in given collection method.
     */
    INSUFFICIENT_CREDIT,
    /**
     * Caused by wrong credit card expiration date.
     */
    WRONG_CREDITCARD_EXPIRATION_DATE,
    /**
     * Caused by wrong ELV account.
     */
    WRONG_ELV_ACCOUNT,
    /**
     * When no other error type can be mapped.
     */
    UNKNOWN,
    /**
     * Caused by unsupported gateway collection method.
     */
    UNSUPPORTED_GATEWAY_COLLECTION_METHOD,
    /**
     * Order with no authorisation.
     */
    ORDER_NOT_AUTHORIZE,
    /**
     * No exist any transaction related with the order id provided.
     */
    UNKNOWN_ORDER,
    /**
     * Card not into database.
     */
    CARD_NOT_IN_AUTHORIZERS_DB,
    /**
     * Acquirer is unavailable.
     */
    ACQUIRER_UNAVAILABLE,
    /**
     * Invalid card number.
     */
    INVALID_CARD_NUMBER,
    /**
     * The credit card is expired.
     */
    CARD_EXPIRED,
    /**
     * Data is wrong, invalid or expired.
     */
    DATA_VALIDATION_ERROR,
    /**
     * Unknown origin ip address.
     */
    UNKNOWN_ORIGIN_IP,
    /**
     * Invalid amount to charge.
     */
    INVALID_AMOUNT,
    /**
     * Invalid currency.
     */
    INVALID_CURRENCY,
    /**
     * Declined authorisation.
     */
    DECLINED,
    /**
     * Order ID has already been used.
     */
    ORDER_ALREADY_USED,
    /**
     * Transaction id no exist.
     */
    TRANSACTION_NO_EXIST,

    NOT_PERMITTED_TO_CARDHOLDER,

    ORDER_WITHOUT_REFUNDABLE_PAYMENTS,

    REJECTED_BY_BANK,

    AMOUNT_HIGHER_THAN_ALLOWED,

    LOST_CARD,

    BUYERS_FUNDING_SOURCE_FAILED,

    UNEXPECTED_STATUS
}
