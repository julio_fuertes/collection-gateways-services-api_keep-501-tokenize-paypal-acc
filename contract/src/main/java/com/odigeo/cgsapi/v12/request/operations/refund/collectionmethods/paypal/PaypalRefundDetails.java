package com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.paypal;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;

/**
 * An object with needed information to perform a refund operation with a commerce using a PayPal account.
 *
 * @see RefundDetails
 */
public class PaypalRefundDetails extends RefundDetails {

    private static final long serialVersionUID = 1L;

    //no-args constructor for API REST
    public PaypalRefundDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId Identifier of the collection transaction to confirm this transaction.
     * @param money           {@link Money} to be refunded.
     * @param transactionId   Identifier for collection transaction that preceded this refund transaction.
     */
    public PaypalRefundDetails(String merchantOrderId, Money money, String transactionId) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL), transactionId);
    }
}
