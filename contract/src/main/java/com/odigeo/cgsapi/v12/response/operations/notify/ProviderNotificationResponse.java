package com.odigeo.cgsapi.v12.response.operations.notify;

import java.io.Serializable;

public class ProviderNotificationResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Notification ID for trace purposes must be UNIQUE ex: System.currentTimeMillis()
     */
    private final String notificationId;
    private final Result result;
    private final String message;

    private ProviderNotificationResponse() {
        notificationId = null;
        result = null;
        message = null;
    }

    ProviderNotificationResponse(ProviderNotificationResponseBuilder builder) {
        notificationId = builder.getNotificationId();
        result = builder.getResult();
        message = builder.getMessage();
    }

    public String getNotificationId() {
        return notificationId;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }
}
