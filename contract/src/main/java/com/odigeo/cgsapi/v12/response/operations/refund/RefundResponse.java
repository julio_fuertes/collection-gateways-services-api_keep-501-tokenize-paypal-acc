package com.odigeo.cgsapi.v12.response.operations.refund;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementContainer;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Currency;

import static java.lang.String.format;

/**
 * An object with information about refund result with a commerce.
 */
public class RefundResponse implements MovementContainer {

    private static final long serialVersionUID = 1L;

    private CollectionStatus collectionStatus;
    private Collection<Movement> movements;
    private BigDecimal amount;
    private Currency currency;
    private GatewayCollectionMethod gatewayCollectionMethod;

    //no-args constructor for API REST
    public RefundResponse() {
    }

    /**
     * Default constructor. This uses Builder pattern.
     *
     * @param builder Builder to create an instance.
     */
    RefundResponse(Builder builder) {
        collectionStatus = builder.collectionStatus;
        movements = builder.refunds;
        amount = builder.amount;
        currency = builder.currency;
    }

    /**
     * Returns {@link GatewayCollectionMethod} used in collection operation.
     *
     * @return {@link GatewayCollectionMethod} used in collection operation.
     */
    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

    /**
     * Sets {@link GatewayCollectionMethod} used in collection operation.
     *
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} used in collection operation.
     */
    public void setGatewayCollectionMethod(GatewayCollectionMethod gatewayCollectionMethod) {
        this.gatewayCollectionMethod = gatewayCollectionMethod;
    }

    /**
     * Returns full list with every movement done in payment gateway.
     *
     * @return Full list with every movement done in payment gateway.
     */
    public Collection<Movement> getMovements() {
        return movements;
    }

    /**
     * Returns current status of collection.
     *
     * @return Current status of collection.
     */
    public CollectionStatus getCollectionStatus() {
        return collectionStatus;
    }

    /**
     * Returns numeric amount that has been collected.
     *
     * @return Numeric amount that has been collected.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Returns currency of ecommerce operation.
     *
     * @return currency of ecommerce operation.
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * Builder class for {@link RefundResponse}.
     */
    public static class Builder {

        private final Collection<Movement> refunds = new ArrayList<Movement>();
        private CollectionStatus collectionStatus;
        private BigDecimal amount;
        private Currency currency;

        /**
         * Adds a {@link Movement} done with payment gateway to current movements list.
         *
         * @param val {@link Movement} done with payment gateway.
         * @return {@code this}.
         */
        public Builder addMovement(Movement val) {
            refunds.add(val);
            collectionStatus = tranform(val.getStatus());
            amount = val.getAmount();
            currency = val.getCurrency();
            return this;
        }

        /**
         * Transforms {@link MovementStatus} to {@link CollectionStatus}.
         *
         * @param status to be transformed.
         * @return {@link CollectionStatus} resulted.
         */
        private CollectionStatus tranform(MovementStatus status) {
            CollectionStatus result;
            switch (status) {
            case STATUS:
                result = collectionStatus;
                break;
            case DECLINED:
                result = CollectionStatus.REFUNDED_DECLINED;
                break;
            case REFUNDED:
                result = CollectionStatus.REFUNDED;
                break;
            case REFUNDERR:
                result = CollectionStatus.REFUND_ERROR;
                break;
            case QUERYERR:
                result = CollectionStatus.QUERY_ERROR;
                break;
            case CANCEL:
                result = CollectionStatus.COLLECTION_CANCELLED;
                break;
            default:
                throw new IllegalArgumentException(format("MovementStatus %s is not a valid status for refund operation", status));
            }
            return result;
        }

        /**
         * Builds {@link RefundResponse} instance for this builder.
         *
         * @return {@link RefundResponse} instance for this builder.
         */
        public RefundResponse build() {
            return new RefundResponse(this);
        }
    }
}
