package com.odigeo.cgsapi.v12.request.collectionmethods;

import com.edreams.util.logic.ThreeValuedLogic;

/**
 * A collection method type. A collection method can be one of the following types:
 * A collection method can only have one type.
 * <ul>
 * <li>{@link #CREDITCARD} a collection method type with a credit card.</li>
 * <li>{@link #ELV} a collection method type with an ELV account.</li>
 * <li>{@link #PAYPAL} a collection method type with a PayPal account.</li>
 * <li>{@link #SECURE3D} a collection method type with a 3D Secure credit card.</li>
 * <li>{@link #SOFORT} a collection method type with an Sofort account.</li>
 * <li>{@link #COFINOGA} a collection method type with a Cofinoga account.</li>
 * </ul>
 */
public enum GatewayCollectionMethodType {

    /**
     * a collection method type with a credit card.
     */
    CREDITCARD('C', ThreeValuedLogic.TRUE, ThreeValuedLogic.FALSE),
    /**
     * a collection method type with an ELV account.
     */
    ELV('E', ThreeValuedLogic.TRUE, ThreeValuedLogic.FALSE),
    /**
     * a collection method type with a PayPal account.
     */
    PAYPAL('P', ThreeValuedLogic.TRUE, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with a 3D Secure credit card.
     */
    SECURE3D('S', ThreeValuedLogic.UNKNOWN, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with a 3D Secure 2.0 credit card.
     */
    SECURE3D2('2', ThreeValuedLogic.UNKNOWN, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with an Sofort account.
     */
    SOFORT('K', ThreeValuedLogic.TRUE, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with a Cofinoga account.
     */
    COFINOGA('F', ThreeValuedLogic.TRUE, ThreeValuedLogic.FALSE),
    /**
     * a collection method type with a Giropay account.
     */
    GIROPAY('G', ThreeValuedLogic.FALSE, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with a banktransfer account.
     */
    BANK_TRANSFER('B', ThreeValuedLogic.FALSE, ThreeValuedLogic.FALSE),
    /**
     * a collection method type with an Alipay account.
     */
    ALIPAY('L', ThreeValuedLogic.FALSE, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with an Unionpay account.
     */
    UNIONPAY('U', ThreeValuedLogic.FALSE, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with a Trustly account.
     */
    TRUSTLY('J', ThreeValuedLogic.FALSE, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with an Ideal account.
     */
    IDEAL('I', ThreeValuedLogic.FALSE, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with a Sepa Direct Debit account.
     */
    SEPADD('X', ThreeValuedLogic.TRUE, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with a Dotpay account.
     */
    DOTPAY('Z', ThreeValuedLogic.FALSE, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with a Klarna account.
     */
    KLARNA('R', ThreeValuedLogic.FALSE, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with a Klarna_account account.
     */
    KLARNA_ACCOUNT('V', ThreeValuedLogic.FALSE, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with an Applepay account.
     */
    APPLEPAY('W', ThreeValuedLogic.TRUE, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with a Poli account.
     */
    POLI('O', ThreeValuedLogic.TRUE, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with a Multibanco account.
     */
    MULTIBANCO('M', ThreeValuedLogic.FALSE, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with a Samsungpay account.
     */
    SAMSUNGPAY('N', ThreeValuedLogic.TRUE, ThreeValuedLogic.TRUE),
    /**
     * a collection method type with an Androidpay account.
     */
    GOOGLEPAY('Q', ThreeValuedLogic.TRUE, ThreeValuedLogic.TRUE);

    private final char code;
    private final ThreeValuedLogic chargeBackable;
    private final ThreeValuedLogic needsUserInteraction;

    /**
     * @param code           The associated code for this collection method type.
     * @param chargeBackable indicates if this collection method types allows charge backeable.
     */
    GatewayCollectionMethodType(char code, ThreeValuedLogic chargeBackable, ThreeValuedLogic needsUserInteraction) {
        this.code = code;
        this.chargeBackable = chargeBackable;
        this.needsUserInteraction = needsUserInteraction;
    }

    /**
     * Returns collection method type for given code.
     *
     * @param code Code of collection method type.
     * @return Collection method type for given code.
     * @throws IllegalArgumentException if no collection method type exists for given code.
     */
    public static GatewayCollectionMethodType findByCode(char code) {
        for (GatewayCollectionMethodType gatewayCollectionMethodType : values()) {
            if (code == gatewayCollectionMethodType.code) {
                return gatewayCollectionMethodType;
            }
        }
        throw new IllegalArgumentException("No GatewayCollectionMethodType for code: " + code);
    }

    /**
     * Returns associated code for this collection method type.
     *
     * @return Associated code for this collection method type.
     */
    public char code() {
        return code;
    }

    /**
     * Returns if this gatewayCollectionMethodType allows charge backeable.
     *
     * @return if this gatewayCollectionMethodType allows charge backeable.
     */
    public ThreeValuedLogic isChargeBackable() {
        return chargeBackable;
    }


    public ThreeValuedLogic isNeedsUserInteraction() {
        return needsUserInteraction;
    }

}
