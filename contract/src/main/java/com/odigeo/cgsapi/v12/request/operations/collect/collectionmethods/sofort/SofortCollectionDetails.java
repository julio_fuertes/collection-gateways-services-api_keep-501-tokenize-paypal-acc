package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.sofort;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.collect.CollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.sofort.SofortConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.sofort.SofortRefundDetails;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * An object with needed information to perform a collection operation with a
 * commerce using Sofort payment method.
 *
 * @see CollectionDetails
 */
@JsonTypeName("SofortCollectionDetails")
public class SofortCollectionDetails extends CollectionDetails {

    private static final long serialVersionUID = 1L;

    private String callbackUrl;
    private String brand;

    //no-args constructor for API REST
    public SofortCollectionDetails() {
    }

    /**
     * Default constructor.
     *
     * @param orderId           Identifier for collection operation.
     * @param money             {@link Money} to be
     *                          collected.
     * @param collectionContext {@link CollectionContext} used in collection.
     * @param callbackUrl       Url that the user will be redirected to
     * @param brand             brand of the collection
     */
    public SofortCollectionDetails(String orderId, Money money, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails, String callbackUrl, String brand) {
        super(orderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.SOFORT), collectionContext, collectionShoppingDetails);
        this.callbackUrl = callbackUrl;
        this.brand = brand;
    }

    @Override
    public RefundDetails toRefundDetails(String transactionId, String merchantOrderId) {
        return new SofortRefundDetails(merchantOrderId, getMoney(), transactionId);
    }

    @Override
    public ConfirmationDetails toConfirmationDetails(String merchantOrderId) {
        return new SofortConfirmationDetails(merchantOrderId, getMoney());
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public String getBrand() {
        return brand;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("callbackUrl", callbackUrl)
                .append("brand", brand)
                .toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        SofortCollectionDetails rhs = (SofortCollectionDetails) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.callbackUrl, rhs.callbackUrl)
                .append(this.brand, rhs.brand)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(callbackUrl)
                .append(brand)
                .toHashCode();
    }
}
