package com.odigeo.cgsapi.v12.response.operations.query;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementContainer;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.List;

import static java.lang.String.format;

/**
 * An object with information about a query result with a commerce.
 */
public class QueryResponse implements MovementContainer {
    private static final long serialVersionUID = 1L;
    private CollectionStatus collectionStatus;
    private Collection<Movement> movements;
    private BigDecimal amount;
    private Currency currency;
    private GatewayCollectionMethod gatewayCollectionMethod;

    //no-args constructor for API REST
    public QueryResponse() {
    }

    /**
     * Default constructor. This uses Builder pattern.
     *
     * @param builder Builder to create an instance.
     */
    public QueryResponse(Builder builder) {
        movements = Collections.unmodifiableList(builder.movements);
        collectionStatus = builder.collectionStatus;
        amount = builder.amount;
        currency = builder.currency;
    }

    /**
     * Sets {@link GatewayCollectionMethod} to be used in cancel operation.
     *
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} used in ecommerce operation.
     */
    public void setGatewayCollectionMethod(GatewayCollectionMethod gatewayCollectionMethod) {
        this.gatewayCollectionMethod = gatewayCollectionMethod;
    }

    /**
     * Returns {@link GatewayCollectionMethod} used in collection operation.
     *
     * @return {@link GatewayCollectionMethod} used in collection operation.
     */
    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

    /**
     * Returns a collection of movements used in cancel operation.
     *
     * @return A collection of movements used in cancel operation.
     */
    public Collection<Movement> getMovements() {
        return movements;
    }

    /**
     * Returns amount to be cancelled.
     *
     * @return Amount to be cancelled.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Returns currency of ecommerce operation.
     *
     * @return currency of ecommerce operation.
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * Returns current status of collection.
     *
     * @return Current status of collection.
     */
    public CollectionStatus getCollectionStatus() {
        return collectionStatus;
    }

    /**
     * Builder class for {@link QueryResponse}.
     */
    public static class Builder {

        private final List<Movement> movements = new ArrayList<Movement>();
        private BigDecimal amount;
        private Currency currency;
        private CollectionStatus collectionStatus;

        /**
         * Adds a {@link Movement} done with payment gateway to current movements list.
         *
         * @param val {@link Movement} done with payment gateway.
         * @return {@code this}.
         */
        public Builder addMovement(Movement val) {
            movements.add(val);
            amount = val.getAmount();
            currency = val.getCurrency();
            collectionStatus = transform(val.getStatus());
            return this;
        }

        /**
         * Transforms {@link MovementStatus} to {@link CollectionStatus}.
         *
         * @param status to be transformed.
         * @return {@link CollectionStatus}
         */
        private CollectionStatus transform(MovementStatus status) {
            CollectionStatus result;
            switch (status) {
            case STATUS:
                result = collectionStatus;
                break;
            case PAID:
                result = CollectionStatus.COLLECTED;
                break;
            case AUTHORIZED:
                result = CollectionStatus.AUTHORIZED;
                break;
            case REQUESTED:
                result = CollectionStatus.REQUESTED;
                break;
            case DECLINED:
                result = CollectionStatus.COLLECTION_DECLINED;
                break;
            case CANCEL:
                result = CollectionStatus.COLLECTION_CANCELLED;
                break;
            case REFUNDED:
                result = CollectionStatus.REFUNDED;
                break;
            case QUERYERR:
                result = CollectionStatus.QUERY_ERROR;
                break;
            default:
                throw new IllegalArgumentException(format("MovementStatus %s is not a valid status for query operation", status));
            }
            return result;
        }

        /**
         * Builds {@link QueryResponse} instance for this builder.
         *
         * @return {@link QueryResponse} instance for this builder.
         */
        public QueryResponse build() {
            return new QueryResponse(this);
        }
    }
}
