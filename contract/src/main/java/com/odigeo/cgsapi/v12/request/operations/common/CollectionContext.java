package com.odigeo.cgsapi.v12.request.operations.common;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

/**
 * An object with extra information to perform a collection operation with a commerce.
 * Gives support to professional invoicing data.
 * There are Collection Gateways that permits to separate order purchaser information and invoicing purchaser information.
 */
public class CollectionContext implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Locale userLocale;
    private final String userIpAddress;
    private final Purchaser orderPurchaser;
    private final Purchaser invoicingPurchaser;
    private final String clientReferenceId;
    private final String userCountryCode;
    private final String userLanguageCode;
    private final String paymentCountryCode;
    private final String websiteCode;
    private final String brandName;
    private final String sessionId;
    private final String userAgent;
    private final Map<String, Integer> testDimensions;
    private String dynamicDescriptor;

    //no-args constructor for API REST
    public CollectionContext() {
        userLocale = null;
        userIpAddress = null;
        orderPurchaser = null;
        invoicingPurchaser = null;
        clientReferenceId = null;
        userCountryCode = null;
        userLanguageCode = null;
        paymentCountryCode = null;
        websiteCode = null;
        brandName = null;
        sessionId = null;
        userAgent = null;
        testDimensions = null;
    }

    /**
     * Creates a new CollectionContext.
     *
     * @param userLocale         Users locale.
     * @param userIpAddress      Users ip address.
     * @param orderPurchaser     Purchaser data for ordering.
     * @param clientReferenceId  Purchaser data for ordering.
     * @param paymentCountryCode Purchaser data for ordering.
     * @param websiteCode        Web site code.
     * @param brandName          Brand name.
     * @param testDimensions     Map with test dimensions for test A/B.
     */
    public CollectionContext(Locale userLocale, String userIpAddress, Purchaser orderPurchaser, String clientReferenceId, String paymentCountryCode, String websiteCode, String brandName, String sessionId, String userAgent, Map<String, Integer> testDimensions) {
        this(userLocale, userIpAddress, orderPurchaser, null, clientReferenceId, paymentCountryCode, websiteCode, brandName, sessionId, userAgent, testDimensions);
    }

    /**
     * Creates a new CollectionContext.
     *
     * @param userLocale         Users locale.
     * @param userIpAddress      Users ip address.
     * @param orderPurchaser     Purchaser data for ordering.
     * @param invoicingPurchaser Purchaser data for invoicing.
     * @param clientReferenceId  the client booking reference id
     * @param paymentCountryCode
     * @param websiteCode        Web site code.
     * @param brandName          Brand name.
     */
    public CollectionContext(Locale userLocale, String userIpAddress, Purchaser orderPurchaser, Purchaser invoicingPurchaser, String clientReferenceId, String paymentCountryCode, String websiteCode, String brandName, String sessionId, String userAgent, Map<String, Integer> testDimensions) {
        this.userLocale = userLocale;
        this.userIpAddress = userIpAddress;
        this.orderPurchaser = orderPurchaser;
        this.invoicingPurchaser = invoicingPurchaser;
        this.clientReferenceId = clientReferenceId;
        this.userCountryCode = userLocale != null ? userLocale.getCountry() : null;
        this.userLanguageCode = userLocale != null ? userLocale.getLanguage() : null;
        this.paymentCountryCode = paymentCountryCode;
        this.websiteCode = websiteCode;
        this.brandName = brandName;
        this.sessionId = sessionId;
        this.userAgent = userAgent;
        this.testDimensions = testDimensions;
    }

    /**
     * Returns users locale.
     *
     * @return Users locale.
     */
    public Locale getUserLocale() {
        return userLocale;
    }

    /**
     * Returns users ip address.
     *
     * @return Users ip address.
     */
    public String getUserIpAddress() {
        return StringUtils.defaultString(userIpAddress);
    }

    /**
     * Returns purchaser for ordering.
     *
     * @return Purchaser for ordering.
     */
    public Purchaser getOrderPurchaser() {
        return orderPurchaser;
    }

    /**
     * Returns purchaser for invoicing.
     *
     * @return Purchaser for invoicing.
     */
    public Purchaser getInvoicingPurchaser() {
        return invoicingPurchaser;
    }

    /**
     * The client reference id is the id provided by the client as a reference for the booking apart from the
     * system internal booking id.
     *
     * @return client reference id.
     */

    public String getClientReferenceId() {
        return clientReferenceId;
    }

    /**
     * Returns user country code.
     *
     * @return User country code.
     */
    public String getUserCountryCode() {
        return userCountryCode;
    }

    /**
     * Returns user language code.
     *
     * @return User language code.
     */
    public String getUserLanguageCode() {
        return userLanguageCode;
    }

    /**
     * Returns the payment country code.
     *
     * @return The payment country code.
     */
    public String getPaymentCountryCode() {
        return paymentCountryCode;
    }

    /**
     * Returns the web site code.
     *
     * @return The web site code.
     */
    public String getWebsiteCode() {
        return websiteCode;
    }

    /**
     * Returns the brand name.
     *
     * @return The brand name.
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * Returns the user agent.
     *
     * @return The user agent.
     */
    public String getUserAgent() {
        return userAgent;
    }

    /**
     * Returns the session Id.
     *
     * @return The session Id.
     */
    public String getSessionId() {
        return sessionId;
    }

    public Map<String, Integer> getTestDimensions() {
        return testDimensions;
    }

    /**
     * [Optional] Set the dynamic descriptor.
     * [Restrictions] Dynamic descriptor only allow the character ".", whitespaces and alphanumeric characters except "ñ". Upper and lower cases are allowed.
     */
    public void setDynamicDescriptor(String dynamicDescriptor) {
        this.dynamicDescriptor = dynamicDescriptor;
    }

    /**
     * Returns the dynamic descriptor.
     *
     * @return The dynamic descriptor.
     */
    public String getDynamicDescriptor() {
        return dynamicDescriptor;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        CollectionContext rhs = (CollectionContext) obj;
        return new EqualsBuilder()
                .append(this.userLocale, rhs.userLocale)
                .append(this.userIpAddress, rhs.userIpAddress)
                .append(this.orderPurchaser, rhs.orderPurchaser)
                .append(this.invoicingPurchaser, rhs.invoicingPurchaser)
                .append(this.clientReferenceId, rhs.clientReferenceId)
                .append(this.userCountryCode, rhs.userCountryCode)
                .append(this.userLanguageCode, rhs.userLanguageCode)
                .append(this.paymentCountryCode, rhs.paymentCountryCode)
                .append(this.websiteCode, rhs.websiteCode)
                .append(this.brandName, rhs.brandName)
                .append(this.sessionId, rhs.sessionId)
                .append(this.userAgent, rhs.userAgent)
                .append(this.dynamicDescriptor, rhs.dynamicDescriptor)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(userLocale)
                .append(userIpAddress)
                .append(orderPurchaser)
                .append(invoicingPurchaser)
                .append(clientReferenceId)
                .append(userCountryCode)
                .append(userLanguageCode)
                .append(paymentCountryCode)
                .append(websiteCode)
                .append(brandName)
                .append(sessionId)
                .append(userAgent)
                .append(dynamicDescriptor)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("userLocale", userLocale)
                .append("userIpAddress", userIpAddress)
                .append("orderPurchaser", orderPurchaser)
                .append("invoicingPurchaser", invoicingPurchaser)
                .append("clientReferenceId", clientReferenceId)
                .append("userCountryCode", userCountryCode)
                .append("userLanguageCode", userLanguageCode)
                .append("paymentCountryCode", paymentCountryCode)
                .append("websiteCode", websiteCode)
                .append("brandName", brandName)
                .append("sessionId", sessionId)
                .append("userAgent", userAgent)
                .append("dynamicDescriptor", dynamicDescriptor)
                .toString();
    }
}
