package com.odigeo.cgsapi.v12.request.operations.verify.collectionmethods.secure3d;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.RecurringDetails;
import com.odigeo.cgsapi.v12.request.RecurringPayment;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardHolder;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.verify.VerificationDetails;
import org.codehaus.jackson.annotate.JsonTypeName;

import java.util.Map;

/**
 * Created by vrieraba on 21/06/2016.
 */
@JsonTypeName("Secure3dVerificationDetails")
public class Secure3dVerificationDetails extends VerificationDetails implements CreditCardHolder, RecurringPayment {

    private static final long serialVersionUID = 1L;

    private CreditCard creditCard;
    private RecurringDetails recurringDetails;

    //no-args constructor for API REST
    public Secure3dVerificationDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId         Identifier for confirmation.
     * @param money                   {@link Money} to be confirmed.
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} to be used.
     * @param parameters              Map of parameters to verify this collection
     * @param collectionActionType    {@link CollectionActionType} used for this transaction
     * @param creditCard              {@link CreditCard} to be used.
     */
    public Secure3dVerificationDetails(String merchantOrderId, Money money, GatewayCollectionMethod gatewayCollectionMethod, Map<String, String> parameters, CollectionActionType collectionActionType, CreditCard creditCard, CollectionContext collectionContext, Integer interactionStep, String finishUrl) {
        super(merchantOrderId, money, gatewayCollectionMethod, parameters, collectionActionType, collectionContext, interactionStep, finishUrl);
        this.creditCard = creditCard;
    }

    /**
     * Returns {@link CreditCard credit card} to be used.
     *
     * @return {@link CreditCard Credit card} to be used.
     */
    public CreditCard getCreditCard() {
        return creditCard;
    }

    @Override
    public RecurringDetails getRecurringDetails() {
        return recurringDetails;
    }

    @Override
    public void setRecurringDetails(RecurringDetails recurringDetails) {
        this.recurringDetails = recurringDetails;
    }
}
