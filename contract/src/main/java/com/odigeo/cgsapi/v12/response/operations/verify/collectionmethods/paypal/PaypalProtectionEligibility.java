package com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal;

public enum PaypalProtectionEligibility {

    ELIGIBLE("Eligible"),
    PARTIALLY_ELIGIBLE("PartiallyEligible"),
    INELIGIBLE("Ineligible");

    private String code;

    PaypalProtectionEligibility(String code) {
        this.code = code;
    }

    public static PaypalProtectionEligibility findByCode(String code) {
        for (PaypalProtectionEligibility paypalProtectionEligibility : values()) {
            if (paypalProtectionEligibility.code.equals(code)) {
                return paypalProtectionEligibility;
            }
        }
        throw new IllegalArgumentException("No PaypalProtectionEligibility for code: " + code);
    }

    public String getCode() {
        return code;
    }

}
