package com.odigeo.cgsapi.v12.response.operations.query;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Created by ADEFREIT on 20/07/2015.
 */
@JsonTypeName("PaypalQuery")
public final class PaypalQuery extends AbstractQuery {

    private static final long serialVersionUID = 1L;

    private final PaypalCustomerAccountData paypalCustomerAccountData;

    private PaypalQuery(String merchantOrderId, BigDecimal amount, Currency currency, String code, String errorDescription, CollectionActionType collectionActionType, MovementStatus status, PaypalCustomerAccountData paypalCustomerAccountData) {
        super(merchantOrderId, amount, currency, code, errorDescription, collectionActionType, status);
        this.paypalCustomerAccountData = paypalCustomerAccountData;
    }

    /**
     * Factory method to create a Query movement
     *
     * @param merchantOrderId           Merchant order id of this movement.
     * @param money                     Amount and Currency.
     * @param code                      Merchant error code.
     * @param errorDescription          Error description.
     * @param collectionActionType
     * @param status
     * @param paypalCustomerAccountData
     * @return An Authorization.
     */
    public static PaypalQuery createPaypalQuery(String merchantOrderId, Money money, String code, String errorDescription, CollectionActionType collectionActionType, MovementStatus status, PaypalCustomerAccountData paypalCustomerAccountData) {
        return new PaypalQuery(merchantOrderId, money != null ? money.getAmount() : null, money != null ? money.getCurrency() : null, code, errorDescription, collectionActionType, status, paypalCustomerAccountData);
    }

    /**
     * Factory method that creates a Failed PaypalQuery
     *
     * @param merchantOrderId      Merchant order id of this movement.
     * @param money                Amount and Currency.
     * @param code                 Merchant error code.
     * @param errorDescription     Error description.
     * @param collectionActionType
     * @return A Failed Authorization.
     */
    public static PaypalQuery createFailedPaypalQuery(String merchantOrderId, Money money, String code, String errorDescription, CollectionActionType collectionActionType) {
        return createPaypalQuery(merchantOrderId, money, code, errorDescription, collectionActionType, MovementStatus.QUERYERR, null);
    }

    /**
     * Factory method that a Fake Query
     *
     * @param merchantOrderId           Merchant order id of this movement.
     * @param money                     Amount and Currency.
     * @param collectionActionType
     * @param status                    Desired movement status
     * @param paypalCustomerAccountData
     * @return A Fake Authorization.
     */
    public static PaypalQuery createFakePaypalQuery(String merchantOrderId, Money money, CollectionActionType collectionActionType, MovementStatus status, PaypalCustomerAccountData paypalCustomerAccountData) {
        return createPaypalQuery(merchantOrderId, money, FAKE_CODE, FAKE_ERROR_MESSAGE, collectionActionType, status, paypalCustomerAccountData);
    }

    /**
     * Returns the paypalCustomerAccountData
     *
     * @return {@link PaypalCustomerAccountData}
     */
    public PaypalCustomerAccountData getPaypalCustomerAccountData() {
        return paypalCustomerAccountData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaypalQuery)) {
            return false;
        }

        PaypalQuery that = (PaypalQuery) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(paypalCustomerAccountData, that.getPaypalCustomerAccountData())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .appendSuper(super.hashCode())
                .append(paypalCustomerAccountData)
                .toHashCode();
    }
}
