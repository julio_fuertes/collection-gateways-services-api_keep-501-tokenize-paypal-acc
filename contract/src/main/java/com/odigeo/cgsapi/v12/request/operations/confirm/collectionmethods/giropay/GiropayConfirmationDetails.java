package com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.giropay;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;

/**
 * Date: 16/07/13
 */
public class GiropayConfirmationDetails extends ConfirmationDetails {

    private static final long serialVersionUID = 1L;

    //no-args constructor for API REST
    public GiropayConfirmationDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId Identifier for collection transaction that preceded this confirmation transaction.
     * @param money           {@link Money} to be confirmed.
     */
    public GiropayConfirmationDetails(String merchantOrderId, Money money) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.GIROPAY));
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId   Identifier for collection transaction that preceded this confirmation transaction.
     * @param money             {@link Money} to be confirmed.
     * @param dynamicDescriptor Dynamic descriptor show in customer´s bank statement. (Restrictions: Dynamic descriptor only allow the character ".", whitespaces and alphanumeric characters except "ñ". Upper and lower cases are allowed.)
     */
    public GiropayConfirmationDetails(String merchantOrderId, Money money, String dynamicDescriptor) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.GIROPAY), dynamicDescriptor);
    }
}
