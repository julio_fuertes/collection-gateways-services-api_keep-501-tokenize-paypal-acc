package com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal;

/**
 * A paypal account status. A paypal account can be in one of the following statuses:
 * <ul>
 * <li>{@link #VERIFIED} paypal account has been verified.</li>
 * <li>{@link #UNVERIFIED} paypal account has been unverified.</li>
 * <li>{@link #UNKNOWN} paypal status is unknown.</li>
 * </ul>
 */
public enum PaypalAccountStatus {

    /**
     * paypal account has been verified.
     */
    VERIFIED("verified"),
    /**
     * paypal account has been unverified.
     */
    UNVERIFIED("unverified"),
    /**
     * paypal status is unknown.
     */
    UNKNOWN("unknown");

    private final String id;

    /**
     * Default constructor.
     *
     * @param id String returned by Paypal to describe an account status.
     */
    PaypalAccountStatus(String id) {
        this.id = id;
    }

    /**
     * Returns the String returned by Paypal to describe an account status.
     *
     * @return String returned by Paypal to describe an account status.
     */
    public String getId() {
        return id;
    }

    /**
     * Resolves {@link PaypalAccountStatus} associated with an id.
     *
     * @param id String returned by Paypal to describe an account status.
     * @return {@link PaypalAccountStatus} resolved.
     */
    public static PaypalAccountStatus get(String id) {
        for (PaypalAccountStatus status : values()) {
            if (status.id.equalsIgnoreCase(id)) {
                return status;
            }
        }
        return UNKNOWN;
    }
}
