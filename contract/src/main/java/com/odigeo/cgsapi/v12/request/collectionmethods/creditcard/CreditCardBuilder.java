package com.odigeo.cgsapi.v12.request.collectionmethods.creditcard;

/**
 * @author amunoz
 * @since 29-feb-2012
 */
public class CreditCardBuilder {
    private static final String OWNER = "Pepe López";
    private static final CreditCardType TYPE = CreditCardType.VISA_CREDIT;
    private String number = "4111111111111111";
    private static final String VERIFICATION_VALUE = "123";
    private static final String EXPIRATION_MONTH = "12";
    private static final String EXPIRATION_YEAR = "15";

    public CreditCard build() {
        return new CreditCard(OWNER, TYPE, number, VERIFICATION_VALUE, EXPIRATION_MONTH, EXPIRATION_YEAR);
    }

    public CreditCardBuilder withNumber(String number) {
        this.number = number;
        return this;
    }
}
