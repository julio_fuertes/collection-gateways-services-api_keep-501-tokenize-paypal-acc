package com.odigeo.cgsapi.v12.request.operations.get.booking.id;

public class GetBookingIdRequestBuilder {

    private String merchantOrderId;

    public GetBookingIdRequestBuilder merchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
        return this;
    }

    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    public GetBookingIdRequest build() {
        return new GetBookingIdRequest(this);
    }
}
