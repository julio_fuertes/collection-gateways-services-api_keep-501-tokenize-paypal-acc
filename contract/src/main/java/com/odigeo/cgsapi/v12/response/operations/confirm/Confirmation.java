package com.odigeo.cgsapi.v12.response.operations.confirm;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Created by vrieraba on 29/05/2015.
 */
@JsonTypeName("Confirmation")
public final class Confirmation extends Movement {

    private static final long serialVersionUID = 1L;

    /*default no-args constructor for Dozer*/
    private Confirmation() {
    }

    /**
     * @param merchantOrderId  Merchant order id of this movement.
     * @param amount           Amount.
     * @param currency         Currency.
     * @param code             Merchant error code.
     * @param errorDescription Error description.
     * @param status           Movement status.
     */
    private Confirmation(String merchantOrderId, BigDecimal amount, Currency currency, String code, String errorDescription, MovementStatus status) {
        super(merchantOrderId, code, MovementAction.CONFIRM);
        setAmount(amount);
        setCurrency(currency);
        setErrorDescription(errorDescription);
        setStatus(status);
    }

    /**
     * Factory method to create a Confirmation movement
     *
     * @param merchantOrderId  Merchant order id of this movement.
     * @param money            Amount and Currency.
     * @param code             Merchant error code.
     * @param errorDescription Error description.
     * @return An Authorization.
     */
    public static Confirmation createConfirmation(String merchantOrderId, Money money, String code, String errorDescription, MovementStatus status) {
        return new Confirmation(merchantOrderId, money != null ? money.getAmount() : null, money != null ? money.getCurrency() : null, code, errorDescription, status);
    }

    /**
     * Factory method that creates a Failed Confirmation
     *
     * @param merchantOrderId  Merchant order id of this movement.
     * @param money            Amount and Currency.
     * @param code             Merchant error code.
     * @param errorDescription Error description.
     * @return A Failed Authorization.
     */
    public static Confirmation createFailedConfirmation(String merchantOrderId, Money money, String code, String errorDescription) {
        return new Confirmation(merchantOrderId, money != null ? money.getAmount() : null, money != null ? money.getCurrency() : null, code, errorDescription, MovementStatus.PAYERROR);
    }

    /**
     * Factory method that a Fake Confirmation
     *
     * @param merchantOrderId Merchant order id of this movement.
     * @param money           Amount and Currency.
     * @param status          Desired movement status
     * @return A Fake Authorization.
     */
    public static Confirmation createFakeConfirmation(String merchantOrderId, Money money, MovementStatus status) {
        return createConfirmation(merchantOrderId, money, FAKE_CODE, FAKE_ERROR_MESSAGE, status);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Confirmation)) {
            return false;
        }
        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .appendSuper(super.hashCode())
                .toHashCode();
    }
}
