package com.odigeo.cgsapi.v12.request.operations.refund;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.alipay.AlipayRefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.cofinoga.CofinogaCardRefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.creditcard.CreditCardRefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.elv.ElvRefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.giropay.GiropayRefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.paypal.PaypalRefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.secure3d.Secure3dRefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.sofort.SofortRefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.unionpay.UnionpayRefundDetails;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

import java.io.Serializable;

/**
 * An object with needed information to perform a refund operation with a commerce.
 * Defines common information for refund operations.
 * Implementing classes can provide extra information, such as credit card, user language, etc.
 */

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = CofinogaCardRefundDetails.class, name = "CofinogaCardRefundDetails"),
        @JsonSubTypes.Type(value = CreditCardRefundDetails.class, name = "CreditCardRefundDetails"),
        @JsonSubTypes.Type(value = ElvRefundDetails.class, name = "ElvRefundDetails"),
        @JsonSubTypes.Type(value = GiropayRefundDetails.class, name = "GiropayRefundDetails"),
        @JsonSubTypes.Type(value = PaypalRefundDetails.class, name = "PaypalRefundDetails"),
        @JsonSubTypes.Type(value = Secure3dRefundDetails.class, name = "Secure3dRefundDetails"),
        @JsonSubTypes.Type(value = SofortRefundDetails.class, name = "SofortRefundDetails"),
        @JsonSubTypes.Type(value = AlipayRefundDetails.class, name = "AlipayRefundDetails"),
        @JsonSubTypes.Type(value = UnionpayRefundDetails.class, name = "UnionpayRefundDetails")
    })
public class RefundDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    private String merchantOrderId;
    private Money money;
    private GatewayCollectionMethod gatewayCollectionMethod;
    private String transactionId;

    //no-args constructor for API REST
    protected RefundDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId         id of this operation.
     * @param money                   {@link Money} to be refunded.
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} used for refund.
     * @param transactionId           Identifier for collection transaction that preceded this refund transaction.
     * @param merchantOrderId         id of this operation.
     */
    public RefundDetails(String merchantOrderId, Money money, GatewayCollectionMethod gatewayCollectionMethod, String transactionId) {
        this.merchantOrderId = merchantOrderId;
        this.money = money;
        this.gatewayCollectionMethod = gatewayCollectionMethod;
        this.transactionId = transactionId;
    }

    /**
     * Returns identifier for collection order that preceded this refund transaction.
     *
     * @return Identifier for collection order that preceded this refund transaction.
     */
    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    /**
     * Returns {@link Money} used in collection.
     *
     * @return {@link Money} used in collection.
     */
    public Money getMoney() {
        return money;
    }

    /**
     * Returns {@link GatewayCollectionMethod} to be used.
     *
     * @return {@link GatewayCollectionMethod} to be used.
     */
    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

    /**
     * Returns identifier for collection transaction that preceded this refund transaction.
     *
     * @return Identifier for collection transaction that preceded this refund transaction.
     */
    public String getTransactionId() {
        return transactionId;
    }
}
