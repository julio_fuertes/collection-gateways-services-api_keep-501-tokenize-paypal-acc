package com.odigeo.cgsapi.v12.request.operations.collect;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.alipay.AlipayCollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.banktransfer.BankTransferCollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.cofinoga.CofinogaCardCollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.creditcard.CreditCardCollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.elv.ElvCollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.giropay.GiropayCollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.klarna.KlarnaCollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.paypal.PaypalCollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.secure3d.Secure3dCollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.sofort.SofortCollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.unionpay.UnionpayCollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.verify.VerificationDetails;
import com.odigeo.cgsapi.v12.response.MovementContainer;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

import java.io.Serializable;
import java.util.Map;

/**
 * An object with needed information to perform a collection operation with a commerce.
 * Defines common information for collection operations.
 * Implementing classes can provide extra information, such as credit card, user language, etc.
 */

@org.jboss.resteasy.annotations.providers.jaxb.IgnoreMediaTypes("application/*+json")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BankTransferCollectionDetails.class, name = "BankTransferCollectionDetails"),
        @JsonSubTypes.Type(value = CofinogaCardCollectionDetails.class, name = "CofinogaCardCollectionDetails"),
        @JsonSubTypes.Type(value = CreditCardCollectionDetails.class, name = "CreditCardCollectionDetails"),
        @JsonSubTypes.Type(value = ElvCollectionDetails.class, name = "ElvCollectionDetails"),
        @JsonSubTypes.Type(value = GiropayCollectionDetails.class, name = "GiropayCollectionDetails"),
        @JsonSubTypes.Type(value = PaypalCollectionDetails.class, name = "PaypalCollectionDetails"),
        @JsonSubTypes.Type(value = Secure3dCollectionDetails.class, name = "Secure3dCollectionDetails"),
        @JsonSubTypes.Type(value = SofortCollectionDetails.class, name = "SofortCollectionDetails"),
        @JsonSubTypes.Type(value = AlipayCollectionDetails.class, name = "AlipayCollectionDetails"),
        @JsonSubTypes.Type(value = UnionpayCollectionDetails.class, name = "UnionpayCollectionDetails"),
        @JsonSubTypes.Type(value = GenericInteractionCollectionDetails.class, name = "GenericInteractionCollectionDetails"),
        @JsonSubTypes.Type(value = KlarnaCollectionDetails.class, name = "KlarnaCollectionDetails")
    })
public class CollectionDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    private String orderId;
    private Money money;
    private GatewayCollectionMethod gatewayCollectionMethod;
    private CollectionContext collectionContext;
    private CollectionShoppingDetails collectionShoppingDetails;

    //no-args constructor for API REST
    protected CollectionDetails() {
    }

    /**
     * Default constructor.
     *
     * @param orderId                   Identifier for collection operation.
     * @param money                     {@link Money money} to be collected.
     * @param gatewayCollectionMethod   {@link GatewayCollectionMethod} to be used.
     * @param collectionContext         {@link CollectionContext} to be used.
     * @param collectionShoppingDetails {@link CollectionShoppingDetails} to be used.
     */
    public CollectionDetails(String orderId, Money money, GatewayCollectionMethod gatewayCollectionMethod, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails) {
        this.orderId = orderId;
        this.money = money;
        this.gatewayCollectionMethod = gatewayCollectionMethod;
        this.collectionContext = collectionContext;
        this.collectionShoppingDetails = collectionShoppingDetails;
    }

    /**
     * Returns identifier for collection.
     *
     * @return Identifier for collection.
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Returns {@link Money} used in collection.
     *
     * @return {@link Money} used in collection.
     */
    public Money getMoney() {
        return money;
    }

    /**
     * Returns {@link CollectionContext} used in collection.
     *
     * @return {@link CollectionContext} used in collection.
     */
    public CollectionContext getCollectionContext() {
        return collectionContext;
    }

    /**
     * Returns {@link CollectionShoppingDetails} used in collection.
     *
     * @return {@link CollectionShoppingDetails} used in collection.
     */
    public CollectionShoppingDetails getCollectionShoppingDetails() {
        return collectionShoppingDetails;
    }

    /**
     * Returns {@link GatewayCollectionMethod} used in collection.
     *
     * @return {@link GatewayCollectionMethod} used in collection.
     */
    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

    public void updateWith(MovementContainer movementContainer) {
        if (!movementContainer.getMovements().isEmpty()) {
            String orderId = movementContainer.getMovements().iterator().next().getMerchantOrderId();
            this.orderId = orderId != null ? orderId : this.orderId;
        }
    }

    /**
     * Transforms current {@code CollectionDetails} instance into a {@link VerificationDetails} instance.
     *
     * @param parameters Any received parameters that can be needed to verify a collection after a user interaction.
     *                   Paypal returns us the focus. This return URL contains a parameter called code that we use for verification operation and is called here
     *                   signature code.
     * @return {@link VerificationDetails} instance with same parameters as current {@code CollectionDetails} instance
     */
    public VerificationDetails confirmWith(Map<String, String> parameters, String merchantOrderId, CollectionActionType collectionActionType, CollectionContext collectionContext, Integer interactionStep, String finishUrl) {
        return new VerificationDetails(merchantOrderId, money, gatewayCollectionMethod, parameters, collectionActionType, collectionContext, interactionStep, finishUrl);
    }

    /**
     * Transforms current {@code CollectionDetails} instance into a {@link RefundDetails} instance.
     * This method must be overriden in every child in order to build specific {@link GatewayCollectionMethod} related instances.
     *
     * @param transactionId   Identifier for collection transaction that preceded this refund transaction.
     * @param merchantOrderId for order that preceded this refund transaction.
     * @return {@link RefundDetails} instance with same parameters as current {@code CollectionDetails} instance.
     */
    public RefundDetails toRefundDetails(String transactionId, String merchantOrderId) {
        return new RefundDetails(merchantOrderId, money, gatewayCollectionMethod, transactionId);
    }

    /**
     * Transforms current {@code CollectionDetails} instance into a {@link ConfirmationDetails} instance.
     * This method must be overriden in every child in order to build specific {@link GatewayCollectionMethod} related instances.
     *
     * @param merchantOrderId for order that preceded this refund transaction.
     * @return {@link ConfirmationDetails} instance with same parameters as current {@code CollectionDetails} instance.
     */
    public ConfirmationDetails toConfirmationDetails(String merchantOrderId) {
        return new ConfirmationDetails(merchantOrderId, money, gatewayCollectionMethod);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        CollectionDetails rhs = (CollectionDetails) obj;
        return new EqualsBuilder()
                .append(this.orderId, rhs.orderId)
                .append(this.money, rhs.money)
                .append(this.gatewayCollectionMethod, rhs.gatewayCollectionMethod)
                .append(this.collectionContext, rhs.collectionContext)
                .append(this.collectionShoppingDetails, rhs.collectionShoppingDetails)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(orderId)
                .append(money)
                .append(gatewayCollectionMethod)
                .append(collectionContext)
                .append(collectionShoppingDetails)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("orderId", orderId)
                .append("money", money)
                .append("gatewayCollectionMethod", gatewayCollectionMethod)
                .append("collectionContext", collectionContext)
                .append("collectionShoppingDetails", collectionShoppingDetails)
                .toString();
    }
}
