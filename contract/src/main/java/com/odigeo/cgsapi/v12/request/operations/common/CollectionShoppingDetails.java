package com.odigeo.cgsapi.v12.request.operations.common;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dcarballo
 * Date: 28/08/13
 * Time: 14:42
 *
 * An object with shopping extra information to perform a collection operation with a commerce
 */
public class CollectionShoppingDetails implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long bookingId;
    private List<Traveller> travellers;
    private ItineraryDetails itineraryDetails;
    private AccommodationDetails accommodationDetails;

    //no-args constructor for API REST
    public CollectionShoppingDetails() {
    }

    /**
     * Creates a new CollectionShoppingCartDetails.
     *
     * @param bookingId  The booking ID
     * @param travellers The list of travellers in the booking
     */
    public CollectionShoppingDetails(Long bookingId, List<Traveller> travellers) {
        this.bookingId = bookingId;
        this.travellers = travellers;
    }

    /**
     * Returns bookingId
     *
     * @return Booking ID
     */
    public long getBookingId() {
        return bookingId;
    }

    public List<Traveller> getTravellers() {
        return travellers;
    }

    public ItineraryDetails getItineraryDetails() {
        return itineraryDetails;
    }

    public AccommodationDetails getAccommodationDetails() {
        return accommodationDetails;
    }

    public void setItineraryDetails(ItineraryDetails itineraryDetails) {
        this.itineraryDetails = itineraryDetails;
    }

    public void setAccommodationDetails(AccommodationDetails accommodationDetails) {
        this.accommodationDetails = accommodationDetails;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        CollectionShoppingDetails rhs = (CollectionShoppingDetails) obj;
        return new EqualsBuilder()
                .append(this.bookingId, rhs.bookingId)
                .append(this.travellers, rhs.travellers)
                .append(this.itineraryDetails, rhs.itineraryDetails)
                .append(this.accommodationDetails, rhs.accommodationDetails)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(bookingId)
                .append(travellers)
                .append(itineraryDetails)
                .append(accommodationDetails)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("bookingId", bookingId)
                .append("travellers", travellers)
                .append("itineraryDetails", itineraryDetails)
                .append("accommodationDetails", accommodationDetails)
                .toString();
    }
}
