package com.odigeo.cgsapi.v12.request.operations.status;

import java.io.Serializable;
import java.util.Currency;
import java.util.Set;

public class StatusDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    private Set<Currency> supportedCurrencies;
    private long currencyAttemptsTimeout;

    //no-args constructor for API REST
    private StatusDetails() {
    }

    public StatusDetails(Set<Currency> supportedCurrencies, long currencyAttemptsTimeout) {
        this.supportedCurrencies = supportedCurrencies;
        this.currencyAttemptsTimeout = currencyAttemptsTimeout;
    }

    public Set<Currency> getSupportedCurrencies() {
        return supportedCurrencies;
    }

    public long getCurrencyAttemptsTimeout() {
        return currencyAttemptsTimeout;
    }
}
