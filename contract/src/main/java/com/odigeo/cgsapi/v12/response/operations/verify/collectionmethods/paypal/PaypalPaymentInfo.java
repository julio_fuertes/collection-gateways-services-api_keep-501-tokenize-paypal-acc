package com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal;

import java.io.Serializable;

public final class PaypalPaymentInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String paymentType;
    private final PaypalProtectionEligibility protectionEligibility;

    private PaypalPaymentInfo() {
        paymentType = null;
        protectionEligibility = null;
    }

    public PaypalPaymentInfo(Builder builder) {
        paymentType = builder.paymentType;
        protectionEligibility = builder.protectionEligibility;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public PaypalProtectionEligibility getProtectionEligibility() {
        return protectionEligibility;
    }

    public static class Builder {

        private String paymentType;
        private PaypalProtectionEligibility protectionEligibility;

        public Builder paymentType(String paymentType) {
            this.paymentType = paymentType;
            return this;
        }

        public Builder protectionEligibility(PaypalProtectionEligibility protectionEligibility) {
            this.protectionEligibility = protectionEligibility;
            return this;
        }

        public PaypalPaymentInfo build() {
            return new PaypalPaymentInfo(this);
        }
    }

}
