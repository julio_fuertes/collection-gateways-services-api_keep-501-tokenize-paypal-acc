package com.odigeo.cgsapi.v12.request;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

public class RecurringDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    private boolean firstPayment;
    private String recurringTransactionId;

    //no-args constructor for API REST
    public RecurringDetails() {

    }

    public RecurringDetails(boolean firstPayment, String recurringTransactionId) {
        this.firstPayment = firstPayment;
        this.recurringTransactionId = recurringTransactionId;
    }

    /**
     * Indicates if it's the first or a subsequent recurring transaction. Mandatory.
     *
     * @return Whether this is the first payment in a recurring transaction or a subsequent.
     */
    public boolean isFirstPayment() {
        return firstPayment;
    }

    /**
     * Indicates the reference to the original transaction in subsequent transactions. Optional.
     *
     * @return Transaction id of the first payment of the recurring transaction.
     */
    public String getRecurringTransactionId() {
        return recurringTransactionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RecurringDetails that = (RecurringDetails) o;

        return new EqualsBuilder()
                .append(firstPayment, that.firstPayment)
                .append(recurringTransactionId, that.recurringTransactionId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(firstPayment)
                .append(recurringTransactionId)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("firstPayment", firstPayment)
                .append("recurringTransactionId", recurringTransactionId)
                .toString();
    }
}
