package com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.cofinoga;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.cofinoga.CofinogaCard;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;

/**
 * An object with needed information to perform a refund operation with a commerce using a cofinoga account.
 *
 * @author egonz
 * @see RefundDetails
 */
public class CofinogaCardRefundDetails extends RefundDetails {

    private static final long serialVersionUID = 1L;

    private CofinogaCard cofinogaCard;

    //no-args constructor for API REST
    public CofinogaCardRefundDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId Identifier of the collection transaction to confirm this transaction.
     * @param money           {@link Money} to be refunded.
     * @param transactionId   Identifier for collection transaction that preceded this refund transaction
     */
    public CofinogaCardRefundDetails(String merchantOrderId, Money money, CofinogaCard cofinogaCard, String transactionId) {
        super(merchantOrderId, money, new GatewayCollectionMethod(GatewayCollectionMethodType.COFINOGA), transactionId);
        this.cofinogaCard = cofinogaCard;
    }

    /**
     * Returns {@link CofinogaCard cofinogaCard} to be used.
     *
     * @return {@link CofinogaCard cofinogaCard} to be used.
     */
    public CofinogaCard getCofinogaCard() {
        return cofinogaCard;
    }
}
