package com.odigeo.cgsapi.v12.response.operations.verify;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementContainer;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.RecurringContainer;
import com.odigeo.cgsapi.v12.response.RedirectionParameters;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;
import com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal.PaypalVerificationResponse;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.annotate.JsonTypeName;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.List;

import static java.lang.String.format;

/**
 * An object with information about confirmation result with a commerce.
 */
@JsonTypeName("VerificationResponse")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = PaypalVerificationResponse.class, name = "PaypalVerificationResponse")
    })
public class VerificationResponse implements MovementContainer, RecurringContainer {

    private static final long serialVersionUID = 1L;

    private List<Movement> movements;
    private BigDecimal amount;
    private Currency currency;
    private CollectionStatus collectionStatus;
    private GatewayCollectionMethod gatewayCollectionMethod;
    private RedirectionParameters redirectionParameters;
    private String recurringTransactionId;
    private String paymentInstrumentToken;

    //no-args constructor for API REST
    public VerificationResponse() {
    }

    /**
     * Default constructor. This uses Builder pattern.
     *
     * @param builder Builder to create an instance.
     */
    public VerificationResponse(Builder builder) {
        movements = Collections.unmodifiableList(builder.movements);
        amount = builder.amount;
        currency = builder.currency;
        collectionStatus = builder.collectionStatus;
        redirectionParameters = builder.redirectionParameters;
        recurringTransactionId = builder.recurringTransactionId;
        this.paymentInstrumentToken = builder.paymentInstrumentToken;
    }


    /**
     * Sets {@link GatewayCollectionMethod} used in collection operation.
     *
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} used in collection operation.
     */
    public void setGatewayCollectionMethod(GatewayCollectionMethod gatewayCollectionMethod) {
        this.gatewayCollectionMethod = gatewayCollectionMethod;
    }

    /**
     * Returns {@link GatewayCollectionMethod} used in collection operation.
     *
     * @return {@link GatewayCollectionMethod} used in collection operation.
     */
    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

    /**
     * Returns full list with every movement done in payment gateway.
     *
     * @return Full list with every movement done in payment gateway.
     */
    public List<Movement> getMovements() {
        return movements;
    }

    /**
     * Returns current status of collection.
     *
     * @return Current status of collection.
     */
    public CollectionStatus getCollectionStatus() {
        return collectionStatus;
    }

    /**
     * Returns numeric amount that has been collected.
     *
     * @return Numeric amount that has been collected.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Returns currency of ecommerce operation.
     *
     * @return currency of ecommerce operation.
     */
    public Currency getCurrency() {
        return currency;
    }

    public RedirectionParameters getRedirectionParameters() {
        return redirectionParameters;
    }

    @Override
    public String getRecurringTransactionId() {
        return recurringTransactionId;
    }

    public String getPaymentInstrumentToken() {
        return paymentInstrumentToken;
    }

    /**
     * Builder class for {@link VerificationResponse}.
     */
    public static class Builder {

        private final List<Movement> movements = new ArrayList<Movement>();
        private BigDecimal amount;
        private Currency currency;
        private CollectionStatus collectionStatus = CollectionStatus.COLLECTION_DECLINED;
        private RedirectionParameters redirectionParameters;
        private String recurringTransactionId;
        private String paymentInstrumentToken;

        /**
         * Adds a {@link Movement} collection done with payment gateway to current movements list.
         *
         * @param val {@link Movement} collection done with payment gateway.
         * @return {@code this}.
         */
        public Builder addMovements(Iterable<? extends Movement> val) {
            for (Movement movement : val) {
                addMovement(movement);
            }
            return this;
        }

        /**
         * Adds a {@link Movement} done with payment gateway to current movements list.
         *
         * @param val {@link Movement} done with payment gateway.
         * @return {@code this}.
         */
        public Builder addMovement(Movement val) {
            movements.add(val);
            amount = val.getAmount();
            currency = val.getCurrency();
            collectionStatus = transform(val.getStatus());
            return this;
        }

        /**
         * Transforms {@link MovementStatus} to {@link CollectionStatus}
         *
         * @param status to be transformed.
         * @return {@link CollectionStatus} resulted.
         */
        private static CollectionStatus transform(MovementStatus status) {
            CollectionStatus result;
            switch (status) {
            case NEEDS_INTERACTION:
            case INIT_INTERACTION:
                result = CollectionStatus.ON_HOLD;
                break;
            case STATUS:
            case DECLINED:
                result = CollectionStatus.COLLECTION_DECLINED;
                break;
            case CANCEL:
                result = CollectionStatus.COLLECTION_CANCELLED;
                break;
            case AUTHORIZED:
                result = CollectionStatus.AUTHORIZED;
                break;
            case PAID:
                result = CollectionStatus.COLLECTED;
                break;
            case REQUESTED:
                result = CollectionStatus.REQUESTED;
                break;
            case PAYERROR:
            case CANCELERR:
                result = CollectionStatus.COLLECTION_ERROR;
                break;
            default:
                throw new IllegalArgumentException(format("MovementStatus %s is not a valid status for verification operation", status));
            }
            return result;
        }

        /**
         * Returns {@link CollectionStatus} as this is a verification.
         *
         * @param val {@link CollectionStatus} as this is a verification.
         * @return {@link CollectionStatus} as this is a verification.
         */
        public Builder collectionStatus(CollectionStatus val) {
            collectionStatus = val;
            return this;
        }

        public RedirectionParameters getRedirectionParameters() {
            return redirectionParameters;
        }

        public Builder redirectionParameters(RedirectionParameters val) {
            redirectionParameters = val;
            return this;
        }

        public Builder recurringTransactionId(String recurringTransactionId) {
            this.recurringTransactionId = recurringTransactionId;
            return this;
        }

        public Builder paymentInstrumentToken(String paymentInstrumentToken) {
            this.paymentInstrumentToken = paymentInstrumentToken;
            return this;
        }

        /**
         * Builds {@link VerificationResponse} instance for this builder.
         *
         * @return {@link VerificationResponse} instance for this builder.
         */
        public VerificationResponse build() {
            return new VerificationResponse(this);
        }
    }
}
