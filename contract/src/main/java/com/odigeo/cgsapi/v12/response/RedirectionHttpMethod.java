package com.odigeo.cgsapi.v12.response;

/**
 * Http Method
 */

/**
 * Http Method:
 * <ul>
 * <li>{@link #GET} Http method.</li>
 * <li>{@link #POST} Http method.</li>
 * </ul>
 */
public enum RedirectionHttpMethod {

    /**
     * GET Http method.
     */
    GET,
    /**
     * POST Http method.
     */
    POST
}
