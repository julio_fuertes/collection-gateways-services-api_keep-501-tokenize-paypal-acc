package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.cofinoga;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.cofinoga.CofinogaCard;
import com.odigeo.cgsapi.v12.request.operations.collect.CollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.cofinoga.CofinogaCardConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.cofinoga.CofinogaCardRefundDetails;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * An object with needed information to perform a collection operation with a commerce using a {@link CofinogaCard}.
 *
 * @author egonz
 * @see CollectionDetails
 */
@JsonTypeName("CofinogaCardCollectionDetails")
public class CofinogaCardCollectionDetails extends CollectionDetails {

    private static final long serialVersionUID = 1L;

    private CofinogaCard cofinogaCard;
    private String commercialOperation;

    //no-args constructor for API REST
    public CofinogaCardCollectionDetails() {
    }

    /**
     * Default constructor.
     *
     * @param orderId                   order of the movement.
     * @param money                     money to be charged.
     * @param collectionContext         context of the collection.
     * @param collectionShoppingDetails shoping details of the collection.
     * @param cofinogaCard              card going to be used.
     * @param commercialOperation       for this collection
     */
    public CofinogaCardCollectionDetails(String orderId, Money money, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails, CofinogaCard cofinogaCard, String commercialOperation) {
        super(orderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.COFINOGA), collectionContext, collectionShoppingDetails);
        this.cofinogaCard = cofinogaCard;
        this.commercialOperation = commercialOperation;
    }

    @Override
    public RefundDetails toRefundDetails(String transactionId, String merchantOrderId) {
        return new CofinogaCardRefundDetails(merchantOrderId, getMoney(), cofinogaCard, transactionId);
    }

    @Override
    public ConfirmationDetails toConfirmationDetails(String merchantOrderId) {
        return new CofinogaCardConfirmationDetails(merchantOrderId, getMoney(), cofinogaCard);
    }

    /**
     * Returns {@link CofinogaCard} to be used.
     *
     * @return {@link CofinogaCard} to be used.
     */
    public CofinogaCard getCofinogaCard() {
        return cofinogaCard;
    }

    /**
     * Returns commercial operation as {@link String}.
     *
     * @return commercial operation as {@link String}.
     */
    public String getCommercialOperation() {
        return commercialOperation;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        CofinogaCardCollectionDetails rhs = (CofinogaCardCollectionDetails) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.cofinogaCard, rhs.cofinogaCard)
                .append(this.commercialOperation, rhs.commercialOperation)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(cofinogaCard)
                .append(commercialOperation)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("cofinogaCard", cofinogaCard)
                .append("commercialOperation", commercialOperation)
                .toString();
    }
}
