package com.odigeo.cgsapi.v12.response.operations.get.booking.id;

public class GetBookingIdResponseBuilder {

    private String bookingId;

    public String getBookingId() {
        return bookingId;
    }

    public GetBookingIdResponseBuilder bookingId(String bookingId) {
        this.bookingId = bookingId;
        return this;
    }

    public GetBookingIdResponse build() {
        return new GetBookingIdResponse(this);
    }
}
