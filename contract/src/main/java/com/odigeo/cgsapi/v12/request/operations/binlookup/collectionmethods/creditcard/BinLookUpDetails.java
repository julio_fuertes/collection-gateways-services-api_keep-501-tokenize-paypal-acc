package com.odigeo.cgsapi.v12.request.operations.binlookup.collectionmethods.creditcard;

import java.io.Serializable;

/**
 * @author alain.munoz
 * @since 17/01/14
 */
public class BinLookUpDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    private String bin;

    //no-args constructor for API REST
    public BinLookUpDetails() {
    }

    /**
     * Default constructor.
     *
     * @param bin bin used for BinLookUp.
     */
    public BinLookUpDetails(String bin) {
        this.bin = bin;
    }

    /**
     * Returns bin used for BinLookUp.
     *
     * @return bin used for BinLookUp.
     */
    public String getBin() {
        return bin;
    }
}
