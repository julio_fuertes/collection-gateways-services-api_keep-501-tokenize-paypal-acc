package com.odigeo.cgsapi.v12.request.operations.notify;

import com.odigeo.cgsapi.v12.request.VirtualPointOfSaleType;

import java.io.Serializable;
import java.util.Map;

public class ProviderNotificationRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Notification ID for trace purposes must be UNIQUE ex: System.currentTimeMillis()
     */
    private final String notificationId;
    private final VirtualPointOfSaleType virtualPointOfSale;
    private final Map<String, Object> parameters;

    //For Dozer
    private ProviderNotificationRequest() {
        notificationId = null;
        virtualPointOfSale = null;
        parameters = null;
    }

    ProviderNotificationRequest(ProviderNotificationRequestBuilder builder) {
        notificationId = String.valueOf(System.currentTimeMillis());
        virtualPointOfSale = builder.getVirtualPointOfSale();
        parameters = builder.getParameters();
    }

    public String getNotificationId() {
        return notificationId;
    }

    public VirtualPointOfSaleType getVirtualPointOfSale() {
        return virtualPointOfSale;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

}
