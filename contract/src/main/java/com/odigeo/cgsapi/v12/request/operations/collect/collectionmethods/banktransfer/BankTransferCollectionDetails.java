package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.banktransfer;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.collect.CollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * User: esteve.camps
 * Date: 22/07/13
 */
@JsonTypeName("BankTransferCollectionDetails")
public class BankTransferCollectionDetails extends CollectionDetails {
    private static final long serialVersionUID = 1L;
    private BankTransferUserDetails bankTransferUserDetails;

    //no-args constructor for API REST
    public BankTransferCollectionDetails() {
    }

    public BankTransferCollectionDetails(String orderId, Money money, CollectionContext collectionContext, BankTransferUserDetails bankTransferUserDetails, CollectionShoppingDetails collectionShoppingDetails) {
        super(orderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.BANK_TRANSFER), collectionContext, collectionShoppingDetails);
        this.bankTransferUserDetails = bankTransferUserDetails;
    }

    public BankTransferUserDetails getBankTransferUserDetails() {
        return bankTransferUserDetails;
    }

    @Override
    public RefundDetails toRefundDetails(String transactionId, String merchantOrderId) {
        throw new UnsupportedOperationException("Bank transfer implementation does not support refund");
    }

    @Override
    public ConfirmationDetails toConfirmationDetails(String merchantOrderId) {
        throw new UnsupportedOperationException("Bank transfer implementation does not support confirmation");
    }

    @JsonIgnore
    public String getPurchaserFirstname() {
        return bankTransferUserDetails.getPurchaserFirstname();
    }

    @JsonIgnore
    public String getPurchaserLastname() {
        return bankTransferUserDetails.getPurchaserLastname();
    }

    @JsonIgnore
    public String getPurchaserCity() {
        return bankTransferUserDetails.getPurchaserCity();
    }

    @JsonIgnore
    public String getPurchaserCpf() {
        return bankTransferUserDetails.getPurchaserCpf();
    }

    @JsonIgnore
    public String getWebsiteCountryCode() {
        return bankTransferUserDetails.getWebsiteCountryCode();
    }

    @JsonIgnore
    public String getPurchaserStreet() {
        return bankTransferUserDetails.getPurchaserStreet();
    }

    @JsonIgnore
    public String getPurchaserState() {
        return bankTransferUserDetails.getPurchaserState();
    }

    @JsonIgnore
    public String getPurchaserEmail() {
        return bankTransferUserDetails.getPurchaserEmail();
    }

    @JsonIgnore
    public String getPurchaserCountryCode() {
        return bankTransferUserDetails.getPurchaserCountryCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        BankTransferCollectionDetails rhs = (BankTransferCollectionDetails) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.bankTransferUserDetails, rhs.bankTransferUserDetails)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(bankTransferUserDetails)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("bankTransferUserDetails", bankTransferUserDetails)
                .toString();
    }
}
