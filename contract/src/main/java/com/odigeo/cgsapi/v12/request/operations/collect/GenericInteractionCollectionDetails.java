package com.odigeo.cgsapi.v12.request.operations.collect;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

@JsonTypeName("GenericInteractionCollectionDetails")
public class GenericInteractionCollectionDetails extends CollectionDetails {

    private static final long serialVersionUID = 1L;

    private String resumeBaseUrl;

    //no-args constructor for API REST
    public GenericInteractionCollectionDetails() {
    }

    /**
     * Constructor.
     *
     * @param orderId                   Identifier for collection operation.
     * @param money                     {@link Money} to be collected.
     * @param gatewayCollectionMethod   {@link GatewayCollectionMethod} used in this collection
     * @param collectionContext         {@link CollectionContext} used in collection.
     * @param collectionShoppingDetails {@link CollectionShoppingDetails} used in collection.
     * @param resumeBaseUrl             Resume URL.
     */
    public GenericInteractionCollectionDetails(String orderId, Money money, GatewayCollectionMethod gatewayCollectionMethod, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails,
                                               String resumeBaseUrl) {

        super(orderId, money, gatewayCollectionMethod, collectionContext, collectionShoppingDetails);
        this.resumeBaseUrl = resumeBaseUrl;

        if (gatewayCollectionMethod == null) {
            throw new IllegalArgumentException("Invalid GatewayCollectionMethod");
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        GenericInteractionCollectionDetails rhs = (GenericInteractionCollectionDetails) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.resumeBaseUrl, rhs.resumeBaseUrl)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(resumeBaseUrl)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("resumeBaseUrl", resumeBaseUrl)
                .toString();
    }

    /**
     * Returns resume URL.
     *
     * @return Resume URL.
     */
    public String getResumeBaseUrl() {
        return resumeBaseUrl;
    }
}
