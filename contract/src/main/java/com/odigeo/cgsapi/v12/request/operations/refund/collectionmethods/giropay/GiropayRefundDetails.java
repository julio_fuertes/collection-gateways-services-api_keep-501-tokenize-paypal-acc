package com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.giropay;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;

/**
 * Date: 16/07/13
 */
public class GiropayRefundDetails extends RefundDetails {

    private String ibanAccountNumber;
    private String accountOwnerSurname;

    private static final long serialVersionUID = 1L;

    //no-args constructor for API REST
    public GiropayRefundDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId     Identifier of the collection transaction to confirm this transaction.
     * @param money               {@link Money} to be refunded.
     * @param transactionId       Identifier for collection transaction that preceded this refund transaction
     * @param ibanAccountNumber   bank account number in IBAN format where the transaction was made from
     * @param accountOwnerSurname surname of the owner of the Giropay account
     */
    public GiropayRefundDetails(String merchantOrderId, Money money, String transactionId, String ibanAccountNumber, String accountOwnerSurname) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.GIROPAY), transactionId);
        this.ibanAccountNumber = ibanAccountNumber;
        this.accountOwnerSurname = accountOwnerSurname;
    }

    public String getIbanAccountNumber() {
        return ibanAccountNumber;
    }

    public String getAccountOwnerSurname() {
        return accountOwnerSurname;
    }
}
