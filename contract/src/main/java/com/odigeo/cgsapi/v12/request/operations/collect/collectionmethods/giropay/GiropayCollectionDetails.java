package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.giropay;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.collect.CollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.giropay.GiropayConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.giropay.GiropayRefundDetails;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * Date: 16/07/13
 */
@JsonTypeName("GiropayCollectionDetails")
public class GiropayCollectionDetails extends CollectionDetails {
    private static final long serialVersionUID = 1L;
    private String accountOwnerSurname;
    private String ibanAccountNumber;
    private String bicBankCode;
    private String finishUrl;

    //no-args constructor for API REST
    public GiropayCollectionDetails() {
    }

    public GiropayCollectionDetails(String orderId, Money money, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails,
                                    String accountOwnerSurname, String ibanAccountNumber, String bicBankCode, String finishUrl) {
        super(orderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.GIROPAY), collectionContext, collectionShoppingDetails);
        this.accountOwnerSurname = accountOwnerSurname;
        this.ibanAccountNumber = ibanAccountNumber;
        this.bicBankCode = bicBankCode;
        this.finishUrl = finishUrl;
    }

    public String getAccountOwnerSurname() {
        return accountOwnerSurname;
    }

    public String getIbanAccountNumber() {
        return ibanAccountNumber;
    }

    public String getBicBankCode() {
        return bicBankCode;
    }

    public String getFinishUrl() {
        return finishUrl;
    }

    @Override
    public RefundDetails toRefundDetails(String transactionId, String merchantOrderId) {
        return new GiropayRefundDetails(merchantOrderId, getMoney(), transactionId, ibanAccountNumber, accountOwnerSurname);
    }

    @Override
    public ConfirmationDetails toConfirmationDetails(String merchantOrderId) {
        return new GiropayConfirmationDetails(merchantOrderId, getMoney());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        GiropayCollectionDetails rhs = (GiropayCollectionDetails) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.accountOwnerSurname, rhs.accountOwnerSurname)
                .append(this.ibanAccountNumber, rhs.ibanAccountNumber)
                .append(this.bicBankCode, rhs.bicBankCode)
                .append(this.finishUrl, rhs.finishUrl)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(accountOwnerSurname)
                .append(ibanAccountNumber)
                .append(bicBankCode)
                .append(finishUrl)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("accountOwnerSurname", accountOwnerSurname)
                .append("ibanAccountNumber", ibanAccountNumber)
                .append("bicBankCode", bicBankCode)
                .toString();
    }
}
