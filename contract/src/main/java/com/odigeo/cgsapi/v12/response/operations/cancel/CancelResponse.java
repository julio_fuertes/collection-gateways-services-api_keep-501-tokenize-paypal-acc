package com.odigeo.cgsapi.v12.response.operations.cancel;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementContainer;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Currency;

import static java.lang.String.format;

/**
 * An object with information about cancellation result with a commerce.
 */
public class CancelResponse implements MovementContainer {

    private static final long serialVersionUID = 1L;

    private CollectionStatus collectionStatus;
    private Collection<Movement> movements;
    private BigDecimal amount;
    private Currency currency;
    private GatewayCollectionMethod gatewayCollectionMethod;

    //no-args constructor for API REST
    public CancelResponse() {
    }

    /**
     * Default constructor for CancelResponse.
     */
    public CancelResponse(Builder builder) {
        collectionStatus = builder.collectionStatus;
        amount = builder.amount;
        currency = builder.currency;
        movements = builder.movements;
    }

    /**
     * Sets {@link GatewayCollectionMethod} to be used in cancel operation.
     *
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} used in ecommerce operation.
     */
    @Override
    public void setGatewayCollectionMethod(GatewayCollectionMethod gatewayCollectionMethod) {
        this.gatewayCollectionMethod = gatewayCollectionMethod;
    }

    /**
     * Returns {@link GatewayCollectionMethod} used in collection operation.
     *
     * @return {@link GatewayCollectionMethod} used in collection operation.
     */
    @Override
    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

    /**
     * Returns a collection of movements used in cancel operation.
     *
     * @return A collection of movements used in cancel operation.
     */
    @Override
    public Collection<Movement> getMovements() {
        return movements;
    }

    /**
     * Returns amount to be cancelled.
     *
     * @return Amount to be cancelled.
     */
    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Returns currency of ecommerce operation.
     *
     * @return currency of ecommerce operation.
     */
    @Override
    public Currency getCurrency() {
        return currency;
    }

    /**
     * Returns current status of collection.
     *
     * @return Current status of collection.
     */
    @Override
    public CollectionStatus getCollectionStatus() {
        return collectionStatus;
    }

    /**
     * Builder class for {@link CancelResponse}.
     */
    public static class Builder {

        private final Collection<Movement> movements = new ArrayList<Movement>();
        private CollectionStatus collectionStatus;
        private BigDecimal amount;
        private Currency currency;

        /**
         * Adds a {@link Movement} done with payment gateway to current movements list.
         *
         * @param val {@link Movement} done with payment gateway.
         * @return {@code this}.
         */
        public Builder addMovement(Movement val) {
            movements.add(val);
            collectionStatus = tranform(val.getStatus());
            amount = val.getAmount();
            currency = val.getCurrency();
            return this;
        }

        /**
         * Transforms {@link MovementStatus} to {@link CollectionStatus}.
         *
         * @param status to be transformed.
         * @return {@link CollectionStatus} resulted.
         */
        private CollectionStatus tranform(MovementStatus status) {
            CollectionStatus result;
            switch (status) {
            case CANCEL:
                result = CollectionStatus.CANCELLED;
                break;
            case CANCELERR:
                result = CollectionStatus.CANCEL_ERROR;
                break;
            default:
                throw new IllegalArgumentException(format("MovementStatus %s is not a valid status for cancel operation", status));
            }
            return result;
        }

        /**
         * Builds {@link CancelResponse} instance for this builder.
         *
         * @return {@link CancelResponse} instance for this builder.
         */
        public CancelResponse build() {
            return new CancelResponse(this);
        }
    }
}
