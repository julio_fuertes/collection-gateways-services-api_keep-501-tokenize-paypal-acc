package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.banktransfer;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * @author javier.garcia
 * @since 13/08/13
 */
public class BankTransferUserDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    private String purchaserFirstname;
    private String purchaserLastname;
    private String purchaserCity;
    private String purchaserCpf;
    private String purchaserStreet;
    private String purchaserState;
    private String purchaserCountryCode;
    private String purchaserEmail;
    private String websiteCountryCode;

    //no-args constructor for API REST
    public BankTransferUserDetails() {
    }

    public BankTransferUserDetails(String purchaserFirstName, String purchaserLastName, String purchaserCity, String websiteCountryCode) {
        this.purchaserFirstname = purchaserFirstName;
        this.purchaserLastname = purchaserLastName;
        this.purchaserCity = purchaserCity;
        this.websiteCountryCode = websiteCountryCode;
    }

    public String getWebsiteCountryCode() {
        return websiteCountryCode;
    }

    public String getPurchaserCpf() {
        return purchaserCpf;
    }

    public void setPurchaserCpf(String purchaserCpf) {
        this.purchaserCpf = purchaserCpf;
    }

    public String getPurchaserCity() {
        return purchaserCity;
    }

    public String getPurchaserFirstname() {
        return purchaserFirstname;
    }

    public String getPurchaserLastname() {
        return purchaserLastname;
    }

    public String getPurchaserStreet() {
        return purchaserStreet;
    }

    public void setPurchaserStreet(String purchaserStreet) {
        this.purchaserStreet = purchaserStreet;
    }

    public String getPurchaserState() {
        return purchaserState;
    }

    public void setPurchaserState(String purchaserState) {
        this.purchaserState = purchaserState;
    }

    public String getPurchaserEmail() {
        return purchaserEmail;
    }

    public void setPurchaserEmail(String purchaserEmail) {
        this.purchaserEmail = purchaserEmail;
    }

    public String getPurchaserCountryCode() {
        return purchaserCountryCode;
    }

    public void setPurchaserCountryCode(String purchaserCountryCode) {
        this.purchaserCountryCode = purchaserCountryCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        BankTransferUserDetails rhs = (BankTransferUserDetails) obj;
        return new EqualsBuilder()
                .append(this.purchaserFirstname, rhs.purchaserFirstname)
                .append(this.purchaserLastname, rhs.purchaserLastname)
                .append(this.purchaserCity, rhs.purchaserCity)
                .append(this.purchaserCpf, rhs.purchaserCpf)
                .append(this.purchaserStreet, rhs.purchaserStreet)
                .append(this.purchaserState, rhs.purchaserState)
                .append(this.purchaserCountryCode, rhs.purchaserCountryCode)
                .append(this.purchaserEmail, rhs.purchaserEmail)
                .append(this.websiteCountryCode, rhs.websiteCountryCode)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(purchaserFirstname)
                .append(purchaserLastname)
                .append(purchaserCity)
                .append(purchaserCpf)
                .append(purchaserStreet)
                .append(purchaserState)
                .append(purchaserCountryCode)
                .append(purchaserEmail)
                .append(websiteCountryCode)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("purchaserFirstname", purchaserFirstname)
                .append("purchaserLastname", purchaserLastname)
                .append("purchaserCity", purchaserCity)
                .append("purchaserCpf", purchaserCpf)
                .append("purchaserStreet", purchaserStreet)
                .append("purchaserState", purchaserState)
                .append("purchaserCountryCode", purchaserCountryCode)
                .append("purchaserEmail", purchaserEmail)
                .append("websiteCountryCode", websiteCountryCode)
                .toString();
    }
}
