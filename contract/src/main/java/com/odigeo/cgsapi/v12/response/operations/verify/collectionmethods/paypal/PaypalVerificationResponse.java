package com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal;

import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;
import com.odigeo.cgsapi.v12.response.operations.query.PaypalCustomerAccountData;
import com.odigeo.cgsapi.v12.response.operations.verify.VerificationResponse;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * An object with information about verification result with a commerce.
 *
 * @see VerificationResponse
 */
@JsonTypeName("PaypalVerificationResponse")
public class PaypalVerificationResponse extends VerificationResponse {

    private static final long serialVersionUID = 1L;
    private PaypalCustomerAccountData paypalCustomerAccountData;
    private PaypalPaymentInfo paypalPaymentInfo;

    //no-args constructor for API REST
    private PaypalVerificationResponse() {
    }

    /**
     * Default constructor. This uses Builder pattern.
     *
     * @param builder Builder to create an instance.
     */
    public PaypalVerificationResponse(PaypalBuilder builder) {
        super(builder);
        this.paypalCustomerAccountData = builder.paypalCustomerAccountData;
        this.paypalPaymentInfo = builder.paypalPaymentInfo;
    }

    /**
     * Returns PaypalCustomerAccountData containing buyer information
     *
     * @return PaypalCustomerAccountData
     */
    public PaypalCustomerAccountData getPaypalCustomerAccountData() {
        return paypalCustomerAccountData;
    }

    /**
     * Returns PaypalPaymentInfo containing payment information
     *
     * @return PaypalPaymentInfo
     */
    public PaypalPaymentInfo getPaypalPaymentInfo() {
        return paypalPaymentInfo;
    }

    /**
     * Builder class for {@link PaypalVerificationResponse}.
     *
     * @see Builder
     */
    public static class PaypalBuilder extends Builder {

        private PaypalCustomerAccountData paypalCustomerAccountData;
        private PaypalPaymentInfo paypalPaymentInfo;

        /**
         * Adds a {@link Movement} collection done with payment gateway to current movements list.
         *
         * @param val {@link Movement} collection done with payment gateway.
         * @return {@code this}.
         */
        @Override
        public PaypalBuilder addMovements(Iterable<? extends Movement> val) {
            return (PaypalBuilder) super.addMovements(val);
        }

        /**
         * Adds a {@link Movement} done with payment gateway to current movements list.
         *
         * @param val {@link Movement} done with payment gateway.
         * @return {@code this}.
         */
        @Override
        public PaypalBuilder addMovement(Movement val) {
            return (PaypalBuilder) super.addMovement(val);
        }

        /**
         * Returns {@link CollectionStatus} as this is a verification.
         *
         * @param val {@link CollectionStatus} as this is a verification.
         * @return {@link CollectionStatus} as this is a verification.
         */
        @Override
        public PaypalBuilder collectionStatus(CollectionStatus val) {
            return (PaypalBuilder) super.collectionStatus(val);
        }

        /**
         * Adds a {@link PaypalCustomerAccountData} with the customer information
         *
         * @param customerAccountData {@link PaypalCustomerAccountData} with the customer information
         * @return {@code this}.
         */
        public PaypalBuilder paypalCustomerAccountData(PaypalCustomerAccountData customerAccountData) {
            this.paypalCustomerAccountData = customerAccountData;
            return this;
        }

        /**
         * Adds a {@link PaypalPaymentInfo} with the customer information
         *
         * @param paypalPaymentInfo {@link PaypalPaymentInfo} with the customer information
         * @return {@code this}.
         */
        public PaypalBuilder paypalPaymentInfo(PaypalPaymentInfo paypalPaymentInfo) {
            this.paypalPaymentInfo = paypalPaymentInfo;
            return this;
        }

        /**
         * Builds {@link PaypalVerificationResponse} instance for this builder.
         *
         * @return {@link PaypalVerificationResponse} instance for this builder.
         */
        @Override
        public PaypalVerificationResponse build() {
            return new PaypalVerificationResponse(this);
        }
    }
}
