package com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.alipay;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;

public class AlipayConfirmationDetails extends ConfirmationDetails {
    private static final long serialVersionUID = 1L;

    public AlipayConfirmationDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId Identifier for collection transaction that preceded this confirmation transaction.
     * @param money           {@link Money money} to be confirmed.
     */
    public AlipayConfirmationDetails(String merchantOrderId, Money money) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ALIPAY));
    }

    public AlipayConfirmationDetails(String merchantOrderId, Money money, String dynamicDescriptor) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ALIPAY), dynamicDescriptor);
    }
}
