package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.paypal;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.collect.CollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.paypal.PaypalConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.paypal.PaypalRefundDetails;
import com.odigeo.cgsapi.v12.request.operations.verify.VerificationDetails;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

import java.util.Map;

/**
 * An object with needed information to perform a collection operation with a commerce using a PayPal account.
 *
 * @see CollectionDetails
 */
@JsonTypeName("PaypalCollectionDetails")
public class PaypalCollectionDetails extends CollectionDetails {
    private static final long serialVersionUID = 1L;
    private String resumeBaseUrl;

    //no-args constructor for API REST
    public PaypalCollectionDetails() {
    }

    /**
     * DEPRECATED
     * Constructor.
     *
     * @param orderId                   Identifier for collection operation.
     * @param money                     {@link Money} to be collected.
     * @param collectionContext         {@link CollectionContext} used in collection.
     * @param collectionShoppingDetails {@link CollectionShoppingDetails} used in collection.
     * @param resumeBaseUrl             Resume URL.
     */
    @Deprecated
    public PaypalCollectionDetails(String orderId, Money money, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails,
                                   String resumeBaseUrl) {
        super(orderId, money, new GatewayCollectionMethod(GatewayCollectionMethodType.PAYPAL), collectionContext, collectionShoppingDetails);
        this.resumeBaseUrl = resumeBaseUrl;
    }

    /**
     * Constructor.
     *
     * @param orderId                   Identifier for collection operation.
     * @param money                     {@link Money} to be collected.
     * @param gatewayCollectionMethod   {@link GatewayCollectionMethod} used in this collection
     * @param collectionContext         {@link CollectionContext} used in collection.
     * @param collectionShoppingDetails {@link CollectionShoppingDetails} used in collection.
     * @param resumeBaseUrl             Resume URL.
     */
    public PaypalCollectionDetails(String orderId, Money money, GatewayCollectionMethod gatewayCollectionMethod, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails,
                                   String resumeBaseUrl) {
        super(orderId, money, gatewayCollectionMethod, collectionContext, collectionShoppingDetails);
        this.resumeBaseUrl = resumeBaseUrl;

        if (gatewayCollectionMethod == null || !GatewayCollectionMethodType.PAYPAL.equals(gatewayCollectionMethod.getGatewayCollectionMethodType())) {
            throw new IllegalArgumentException("Invalid GatewayCollectionMethod");
        }
    }

    /**
     * Returns resume URL.
     *
     * @return Resume URL.
     */
    public String getResumeBaseUrl() {
        return resumeBaseUrl;
    }

    /**
     * Transforms current {@code PaypalCollectionDetails} instance into a {@link PaypalRefundDetails} instance.
     *
     * @param transactionId Identifier for collection transaction that preceded this refund transaction.
     * @return {@link PaypalRefundDetails} instance with same parameters as current {@code PaypalCollectionDetails} instance.
     */
    @Override
    public RefundDetails toRefundDetails(String transactionId, String merchantOrderId) {
        return new PaypalRefundDetails(merchantOrderId, getMoney(), transactionId);
    }

    /**
     * Transforms current {@code PaypalCollectionDetails} instance into a {@link PaypalConfirmationDetails} instance.
     *
     * @param merchantOrderId for collection transaction that preceded this refund transaction.
     * @return {@link PaypalConfirmationDetails} instance with same parameters as current {@code PaypalCollectionDetails} instance.
     */
    @Override
    public ConfirmationDetails toConfirmationDetails(String merchantOrderId) {
        return new PaypalConfirmationDetails(merchantOrderId, getMoney());
    }

    /**
     * Transforms current {@code CollectionDetails} instance into a {@link VerificationDetails} instance.
     *
     * @param parameters Map of parameters to be used to verify this collection has been successful
     * @return {@link VerificationDetails} instance with same parameters as current {@code CollectionDetails} instance
     */
    @Override
    public VerificationDetails confirmWith(Map<String, String> parameters, String merchantOrderId, CollectionActionType collectionActionType, CollectionContext collectionContext, Integer interactionStep, String finishUrl) {
        return new VerificationDetails(merchantOrderId, getMoney(), GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL), parameters, collectionActionType, collectionContext, interactionStep, finishUrl);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        PaypalCollectionDetails rhs = (PaypalCollectionDetails) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.resumeBaseUrl, rhs.resumeBaseUrl)
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("resumeBaseUrl", resumeBaseUrl)
                .toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(resumeBaseUrl)
                .toHashCode();
    }
}
