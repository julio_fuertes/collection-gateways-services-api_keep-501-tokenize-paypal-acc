package com.odigeo.cgsapi.v12.exceptions;

import javax.ws.rs.core.Response;

public class InvalidParametersException extends CgsApiException {

    public InvalidParametersException(String message) {
        super(message, null, Response.Status.BAD_REQUEST);
    }

    public InvalidParametersException(String message, Throwable cause) {
        super(message, cause, Response.Status.BAD_REQUEST);
    }

}
