package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.unionpay;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.collect.CollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.unionpay.UnionpayConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.unionpay.UnionpayRefundDetails;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class UnionpayCollectionDetails extends CollectionDetails {
    private static final long serialVersionUID = 1L;

    private String resumeBaseUrl;
    private String purchaseDescription;
    private String paymentCountryCode;

    public UnionpayCollectionDetails() {
    }

    public UnionpayCollectionDetails(String orderId, Money money, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails, String resumeBaseUrl, String purchaseDescription) {
        super(orderId, money, new GatewayCollectionMethod(GatewayCollectionMethodType.UNIONPAY), collectionContext, collectionShoppingDetails);
        this.resumeBaseUrl = resumeBaseUrl;
        this.purchaseDescription = purchaseDescription;
        this.paymentCountryCode = collectionContext.getPaymentCountryCode();
    }

    public String getResumeBaseUrl() {
        return resumeBaseUrl;
    }

    public String getPurchaseDescription() {
        return purchaseDescription;
    }

    public String getPaymentCountryCode() {
        return paymentCountryCode;
    }

    @Override
    public RefundDetails toRefundDetails(String transactionId, String merchantOrderId) {
        return new UnionpayRefundDetails(merchantOrderId, getMoney(), transactionId);
    }

    @Override
    public ConfirmationDetails toConfirmationDetails(String merchantOrderId) {
        return new UnionpayConfirmationDetails(merchantOrderId, getMoney());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        UnionpayCollectionDetails rhs = (UnionpayCollectionDetails) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.resumeBaseUrl, rhs.resumeBaseUrl)
                .append(this.purchaseDescription, rhs.purchaseDescription)
                .append(this.paymentCountryCode, rhs.paymentCountryCode)
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("resumeBaseUrl", resumeBaseUrl)
                .append("purchaseDescription", purchaseDescription)
                .append("paymentCountryCode", paymentCountryCode)
                .toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(resumeBaseUrl)
                .append(purchaseDescription)
                .append(paymentCountryCode)
                .toHashCode();
    }
}
