package com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.secure3d;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * An object with needed information to perform a confirmation operation with a commerce.
 *
 * @see ConfirmationDetails
 */
public class Secure3dConfirmationDetails extends ConfirmationDetails {

    private static final long serialVersionUID = 1L;
    private CreditCard creditCard;

    //no-args constructor for API REST
    protected Secure3dConfirmationDetails() {
    }

    /**
     * @param merchantOrderId of the collection transaction to confirm this transaction.
     * @param money           {@link Money} to be confirmed.
     * @param creditCard      {@link CreditCard Credit card} to be used.
     */
    public Secure3dConfirmationDetails(String merchantOrderId, Money money, CreditCard creditCard) {
        super(merchantOrderId, money, new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D, creditCard.getType()));
        this.creditCard = creditCard;
    }

    /**
     * @param merchantOrderId   of the collection transaction to confirm this transaction.
     * @param money             {@link Money} to be confirmed.
     * @param dynamicDescriptor Dynamic descriptor show in customer´s bank statement. (Restrictions: Dynamic descriptor only allow the character ".", whitespaces and alphanumeric characters except "ñ". Upper and lower cases are allowed.)
     * @param creditCard        {@link CreditCard Credit card} to be used.
     */
    public Secure3dConfirmationDetails(String merchantOrderId, Money money, String dynamicDescriptor, CreditCard creditCard) {
        super(merchantOrderId, money, new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D, creditCard.getType()), dynamicDescriptor);
        this.creditCard = creditCard;
    }

    /**
     * Returns {@link CreditCard} to be used.
     *
     * @return {@link CreditCard} to be used.
     */
    public CreditCard getCreditCard() {
        return creditCard;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Secure3dConfirmationDetails rhs = (Secure3dConfirmationDetails) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.creditCard, rhs.creditCard)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(creditCard)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("creditCard", creditCard)
                .toString();
    }
}
