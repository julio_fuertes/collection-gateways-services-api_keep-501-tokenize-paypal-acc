package com.odigeo.cgsapi.v12.response.operations.query;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.annotate.JsonTypeName;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Created by ADEFREIT on 20/07/2015.
 */
@JsonTypeName("AbstractQuery")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Query.class, name = "Query"),
        @JsonSubTypes.Type(value = PaypalQuery.class, name = "PaypalQuery")
    })
public abstract class AbstractQuery extends Movement {

    private CollectionActionType collectionActionType;

    /*Default no-args constructor for DOZER*/
    protected AbstractQuery() {
    }

    /**
     * Parameters constructor
     *
     * @param merchantOrderId      Merchant order id of this movement.
     * @param amount
     * @param currency
     * @param code
     * @param errorDescription
     * @param collectionActionType
     * @param status
     */
    protected AbstractQuery(String merchantOrderId, BigDecimal amount, Currency currency, String code, String errorDescription, CollectionActionType collectionActionType, MovementStatus status) {
        super(merchantOrderId, code, MovementAction.QUERY);
        setAmount(amount);
        setCurrency(currency);
        setErrorDescription(errorDescription);
        setStatus(status);
        this.collectionActionType = collectionActionType;
    }

    /**
     * Returns the collectionActionType used for the transaction.
     *
     * @return {@link CollectionActionType} used for the transaction.
     */
    public CollectionActionType getCollectionActionType() {
        return this.collectionActionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbstractQuery)) {
            return false;
        }
        AbstractQuery that = (AbstractQuery) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(collectionActionType, that.getCollectionActionType())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .appendSuper(super.hashCode())
                .append(collectionActionType)
                .toHashCode();
    }
}
