package com.odigeo.cgsapi.v12.request.operations.common;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

public class Segment implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer segmentNumber;
    private List<Section> sections;
    private String mainCarrier;

    public Segment() {
    }

    public Segment(String mainCarrier, List<Section> sections, Integer segmentNumber) {
        this.mainCarrier = mainCarrier;
        this.sections = sections;
        this.segmentNumber = segmentNumber;
    }

    public Integer getSegmentNumber() {
        return segmentNumber;
    }

    public List<Section> getSections() {
        return sections;
    }

    public String getMainCarrier() {
        return mainCarrier;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Segment rhs = (Segment) obj;
        return new EqualsBuilder()
                .append(this.segmentNumber, rhs.segmentNumber)
                .append(this.sections, rhs.sections)
                .append(this.mainCarrier, rhs.mainCarrier)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(segmentNumber)
                .append(sections)
                .append(mainCarrier)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("segmentNumber", segmentNumber)
                .append("sections", sections)
                .append("mainCarrier", mainCarrier)
                .toString();
    }
}
