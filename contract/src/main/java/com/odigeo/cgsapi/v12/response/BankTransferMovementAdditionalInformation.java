package com.odigeo.cgsapi.v12.response;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * @author javier.garcia
 * @since 25/07/13
 */
@JsonTypeName("BankTransferMovementAdditionalInformation")
public class BankTransferMovementAdditionalInformation implements MovementAdditionalInformation {

    private static final long serialVersionUID = 1L;

    private String paymentReference;
    private String accountHolder;
    private String bankName;
    private String city;
    private String swiftCode;
    private String bankAccountNumber;
    private String iban;
    private String countryDescription;
    private String type;

    //no-args constructor for API REST
    public BankTransferMovementAdditionalInformation() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * The paymentReference.
     *
     * @return The paymentReference.
     */
    public String getPaymentReference() {
        return paymentReference;
    }


    /**
     * The paymentReference.
     *
     * @param paymentReference paymentReference
     */
    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    /**
     * The accountHolder.
     *
     * @return The accountHolder.
     */
    public String getAccountHolder() {
        return accountHolder;
    }

    /**
     * The accountHolder.
     *
     * @param accountHolder accountHolder
     */
    public void setAccountHolder(String accountHolder) {
        this.accountHolder = accountHolder;
    }

    /**
     * The bankName.
     *
     * @return The bankName.
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * The bankName.
     *
     * @param bankName bankName
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * The city.
     *
     * @return The city.
     */
    public String getCity() {
        return city;
    }

    /**
     * The city.
     *
     * @param city city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * The swiftCode.
     *
     * @return The swiftCode.
     */
    public String getSwiftCode() {
        return swiftCode;
    }

    /**
     * The swiftCode.
     *
     * @param swiftCode swiftCode
     */
    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    /**
     * The bankAccountNumber.
     *
     * @return The bankAccountNumber.
     */
    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    /**
     * The bankAccountNumber.
     *
     * @param bankAccountNumber bankAccountNumber
     */
    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    /**
     * The iban.
     *
     * @return The iban.
     */
    public String getIban() {
        return iban;
    }

    /**
     * The iban.
     *
     * @param iban iban
     */
    public void setIban(String iban) {
        this.iban = iban;
    }

    /**
     * The countryDescription.
     *
     * @return The countryDescription.
     */
    public String getCountryDescription() {
        return countryDescription;
    }

    /**
     * The countryDescription.
     *
     * @param countryDescription countryDescription
     */
    public void setCountryDescription(String countryDescription) {
        this.countryDescription = countryDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BankTransferMovementAdditionalInformation)) {
            return false;
        }

        BankTransferMovementAdditionalInformation that = (BankTransferMovementAdditionalInformation) o;

        return new EqualsBuilder()
                .append(accountHolder, that.accountHolder)
                .append(bankAccountNumber, that.bankAccountNumber)
                .append(bankName, that.bankName)
                .append(city, that.city)
                .append(countryDescription, that.countryDescription)
                .append(iban, that.iban)
                .append(paymentReference, that.paymentReference)
                .append(swiftCode, that.swiftCode)
                .append(type, that.type)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(13, 37)
                .append(accountHolder)
                .append(bankAccountNumber)
                .append(bankName)
                .append(city)
                .append(countryDescription)
                .append(iban)
                .append(paymentReference)
                .append(swiftCode)
                .append(type)
                .toHashCode();
    }
}
