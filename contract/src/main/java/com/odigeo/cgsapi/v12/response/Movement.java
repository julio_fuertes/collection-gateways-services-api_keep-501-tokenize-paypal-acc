package com.odigeo.cgsapi.v12.response;

import com.odigeo.cgsapi.v12.response.operations.authorize.Authorization;
import com.odigeo.cgsapi.v12.response.operations.binlookup.BinLookUp;
import com.odigeo.cgsapi.v12.response.operations.cancel.Cancellation;
import com.odigeo.cgsapi.v12.response.operations.collect.Collection;
import com.odigeo.cgsapi.v12.response.operations.confirm.Confirmation;
import com.odigeo.cgsapi.v12.response.operations.query.AbstractQuery;
import com.odigeo.cgsapi.v12.response.operations.refund.Refund;
import com.odigeo.cgsapi.v12.response.operations.verify.Verification;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.annotate.JsonTypeName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

/**
 * An object with information about a payment gateway movement.
 */
@JsonTypeName("Movement")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Movement.class, name = "Movement"),
        @JsonSubTypes.Type(value = Authorization.class, name = "Authorization"),
        @JsonSubTypes.Type(value = Refund.class, name = "Refund"),
        @JsonSubTypes.Type(value = BinLookUp.class, name = "BinLookUp"),
        @JsonSubTypes.Type(value = Collection.class, name = "Collection"),
        @JsonSubTypes.Type(value = Verification.class, name = "Verification"),
        @JsonSubTypes.Type(value = Cancellation.class, name = "Cancellation"),
        @JsonSubTypes.Type(value = Confirmation.class, name = "Confirmation"),
        @JsonSubTypes.Type(value = AbstractQuery.class, name = "AbstractQuery")
    })
public class Movement implements Serializable {

    protected static final String FAKE_CODE = "0";
    protected static final String FAKE_ERROR_MESSAGE = "OK IN TEST MODE";
    private static final long serialVersionUID = 1L;
    private Date creationDate;
    private String merchantOrderId;
    private String code;
    private MovementErrorType errorType;
    private String errorDescription;
    private MovementAction action;
    private MovementStatus status;
    private BigDecimal amount;
    private Currency currency;
    private String terminalId;
    private String transactionId;
    private String merchantStatus;
    private String merchantStatusSubcode;
    private String secure;
    private String transactionIdSub;
    private MovementAdditionalInformation additionalInformation;
    private String protocolVersion;

    //no-args constructor for API REST
    public Movement() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId Order id of this movement.
     * @param code            Code of this movement.
     * @param action          Action of this movement.
     */
    public Movement(String merchantOrderId, String code, MovementAction action) {
        this(merchantOrderId, code, action, null);
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId Order id of this movement.
     * @param code            Code of this movement.
     * @param action          Action of this movement.
     * @param secure          Tells if movement is secure or not.
     */
    public Movement(String merchantOrderId, String code, MovementAction action, String secure) {
        this.merchantOrderId = merchantOrderId;
        this.code = code;
        this.action = action;
        this.secure = secure;
        creationDate = new Date();
    }

    public MovementAdditionalInformation getAdditionalInformation() {
        return additionalInformation;
    }

    /**
     * Sets additional information of this movement.
     *
     * @param additionalInformation Additional information of this movement.
     */
    public void setAdditionalInformation(MovementAdditionalInformation additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    /**
     * Returns merchantOrderId of this movement.
     *
     * @return MerchantOrderId of this movement.
     */
    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    /**
     * Returns code of this movement.
     *
     * @return Code of this movement.
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code of this movement.
     *
     * @param code Code of this movement.
     */
    void setCode(String code) {
        this.code = code;
    }

    /**
     * Returns {@link MovementAction} action of this movement.
     *
     * @return {@link MovementAction} action of this movement.
     */
    public MovementAction getAction() {
        return action;
    }

    /**
     * Returns error description of this movement.
     *
     * @return Error description of this movement.
     */
    public String getErrorDescription() {
        return errorDescription;
    }

    /**
     * Sets error description for this movement.
     *
     * @param val error description.
     */
    protected void setErrorDescription(String val) {
        errorDescription = val;
    }

    /**
     * Returns current status of this movement.
     *
     * @return Current status of this movement.
     */
    public MovementStatus getStatus() {
        return status;
    }

    /**
     * Sets current status for this movement.
     *
     * @param status Current status.
     */
    public void setStatus(MovementStatus status) {
        this.status = status;
    }

    /**
     * Returns numeric amount of monetary amount of this movement.
     *
     * @return Numeric amount of monetary amount of this movement.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * Returns the currency of this movement.
     *
     * @return the currency of this movement.
     */
    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    /**
     * Returns creation date of this movement.
     *
     * @return Creation date of this movement.
     */
    public Date getCreationDate() {
        return new Date(creationDate.getTime());
    }

    /**
     * Returns secure of this movement.
     *
     * @return Secure of this movement.
     */
    public String getSecure() {
        return secure;
    }

    /**
     * Returns terminal id of this movement, if any.
     *
     * @return Terminal id of this movement, if any.
     */
    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * Returns transaction id of this movement in payment gateway server.
     *
     * @return Transaction id of this movement in payment gateway server.
     */
    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * Returns status code given to this movement by commerce.
     *
     * @return Status code given to this movement by commerce.
     */
    public String getMerchantStatus() {
        return merchantStatus;
    }

    public void setMerchantStatus(String merchantStatus) {
        this.merchantStatus = merchantStatus;
    }

    /**
     * Returns status subcode given to this movement by commerce.
     *
     * @return Status subcode given to this movement by commerce.
     */
    public String getMerchantStatusSubcode() {
        return merchantStatusSubcode;
    }

    public void setMerchantStatusSubcode(String merchantStatusSubcode) {
        this.merchantStatusSubcode = merchantStatusSubcode;
    }

    /**
     * Returns error type of this movement.
     *
     * @return Error type of this movement.
     */
    public MovementErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(MovementErrorType errorType) {
        this.errorType = errorType;
    }

    /**
     * Returns {@code true} if movement was declined due to an invalid CVV2, {@code false} otherwise.
     *
     * @return {@code true} if movement was declined due to an invalid CVV2, {@code false} otherwise.
     */
    @JsonIgnore
    public boolean isInvalidCvv2() {
        return errorType == MovementErrorType.WRONG_CVV2;
    }

    /**
     * Returns {@code true} if movement was declined due to suspicious fraud, {@code false} otherwise.
     *
     * @return {@code true} if movement was declined due to suspicious fraud, {@code false} otherwise.
     */

    @JsonIgnore
    public boolean isFraudSuspicious() {
        return errorType == MovementErrorType.FRAUD_SUSPECT;
    }

    public String getTransactionIdSub() {
        return transactionIdSub;
    }

    public void setTransactionIdSub(String transactionIdSub) {
        this.transactionIdSub = transactionIdSub;
    }

    /**
     * Returns the protocolVersion used in the payment
     * @return the protocolVersion used in the payment
     */
    public String getProtocolVersion() {
        return protocolVersion;
    }

    public void setProtocolVersion(String protocolVersion) {
        this.protocolVersion = protocolVersion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Movement)) {
            return false;
        }

        Movement that = (Movement) o;

        return new EqualsBuilder()
                .append(merchantOrderId, that.merchantOrderId)
                .append(code, that.code)
                .append(action, that.action)
                .append(status, that.status)
                .append(amount, that.amount)
                .append(currency, that.currency)
                .append(transactionIdSub, that.transactionIdSub)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .append(merchantOrderId)
                .append(code)
                .append(action)
                .append(status)
                .append(creationDate)
                .append(amount)
                .append(currency)
                .append(transactionIdSub)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("creationDate", creationDate)
                .append("merchantOrderId", merchantOrderId)
                .append("code", code)
                .append("errorType", errorType)
                .append("errorDescription", errorDescription)
                .append("action", action)
                .append("status", status)
                .append("amount", amount)
                .append("currency", currency)
                .append("terminalId", terminalId)
                .append("transactionId", transactionId)
                .append("merchantStatus", merchantStatus)
                .append("merchantStatusSubcode", merchantStatusSubcode)
                .append("secure", secure)
                .append("transactionIdSub", transactionIdSub)
                .append("additionalInformation", additionalInformation)
                .append("protocolVersion", protocolVersion)
                .toString();
    }

}
