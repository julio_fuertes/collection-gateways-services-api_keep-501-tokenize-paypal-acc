package com.odigeo.cgsapi.v12.response;

import java.util.Map;

public class RedirectionParametersBuilder {

    private RedirectionType redirectionType;
    private Integer redirectionStep;
    private String redirectionUrl;
    private RedirectionHttpMethod redirectionMethod;
    private Map<String, String> parameters;
    private String html;
    private String javascriptSnippet;

    public RedirectionParametersBuilder redirectionType(RedirectionType redirectionType) {
        this.redirectionType = redirectionType;
        return this;
    }

    public RedirectionParametersBuilder redirectionStep(Integer redirectionStep) {
        this.redirectionStep = redirectionStep;
        return this;
    }

    public RedirectionParametersBuilder redirectionUrl(String redirectionUrl) {
        this.redirectionUrl = redirectionUrl;
        return this;
    }

    public RedirectionParametersBuilder redirectionMethod(RedirectionHttpMethod redirectionMethod) {
        this.redirectionMethod = redirectionMethod;
        return this;
    }

    public RedirectionParametersBuilder parameters(Map<String, String> parameters) {
        this.parameters = parameters;
        return this;
    }

    public RedirectionParametersBuilder html(String html) {
        this.html = html;
        return this;
    }

    public RedirectionParametersBuilder javascriptSnippet(String javascriptSnippet) {
        this.javascriptSnippet = javascriptSnippet;
        return this;
    }

    public RedirectionType getRedirectionType() {
        return redirectionType;
    }

    public Integer getRedirectionStep() {
        return redirectionStep;
    }

    public String getRedirectionUrl() {
        return redirectionUrl;
    }

    public RedirectionHttpMethod getRedirectionMethod() {
        return redirectionMethod;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public String getHtml() {
        return html;
    }

    public String getJavascriptSnippet() {
        return javascriptSnippet;
    }

    public RedirectionParameters build() {
        return new RedirectionParameters(this);
    }

}
