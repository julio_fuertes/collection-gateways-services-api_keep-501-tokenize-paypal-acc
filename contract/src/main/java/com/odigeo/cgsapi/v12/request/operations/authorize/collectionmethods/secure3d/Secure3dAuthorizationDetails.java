package com.odigeo.cgsapi.v12.request.operations.authorize.collectionmethods.secure3d;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.RecurringDetails;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardHolder;
import com.odigeo.cgsapi.v12.request.operations.authorize.AuthorizationDetails;
import com.odigeo.cgsapi.v12.request.RecurringPayment;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * Created by vrieraba on 15/06/2016.
 */
@JsonTypeName("Secure3dAuthorizationDetails")
public class Secure3dAuthorizationDetails extends AuthorizationDetails implements CreditCardHolder, RecurringPayment {
    private static final long serialVersionUID = 1L;

    private CreditCard creditCard;
    @Deprecated
    private String callbackUrl;
    private String finishUrl;
    private RecurringDetails recurringDetails;

    //no-args constructor for API REST
    public Secure3dAuthorizationDetails() {
    }

    /**
     * Default constructor.
     *
     * @param orderId                   Identifier for collection operation.
     * @param money                     {@link Money} to be collected.
     * @param creditCard                {@link CreditCard} used in collection.
     * @param collectionContext         {@link CollectionContext} used in collection.
     * @param collectionShoppingDetails {@link CollectionShoppingDetails} used in collection.
     * @param callbackUrl               Callback URL.
     * @param finishUrl                 Finish URL.
     * @deprecated
     */
    @Deprecated
    public Secure3dAuthorizationDetails(String orderId, Money money, CreditCard creditCard, GatewayCollectionMethodType gatewayCollectionMethodType, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails, String callbackUrl, String finishUrl) {
        super(orderId, money, new CreditCardGatewayCollectionMethod(gatewayCollectionMethodType, creditCard.getType()), collectionContext, collectionShoppingDetails);
        this.creditCard = creditCard;
        this.callbackUrl = callbackUrl;
        this.finishUrl = finishUrl;
    }

    /**
     * Default constructor.
     *
     * @param orderId                   Identifier for collection operation.
     * @param money                     {@link Money} to be collected.
     * @param creditCard                {@link CreditCard} used in collection.
     * @param collectionContext         {@link CollectionContext} used in collection.
     * @param collectionShoppingDetails {@link CollectionShoppingDetails} used in collection.
     * @param finishUrl                 Finish URL.
     */
    public Secure3dAuthorizationDetails(String orderId, Money money, CreditCard creditCard, GatewayCollectionMethodType gatewayCollectionMethodType, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails, String finishUrl) {
        super(orderId, money, new CreditCardGatewayCollectionMethod(gatewayCollectionMethodType, creditCard.getType()), collectionContext, collectionShoppingDetails);
        this.creditCard = creditCard;
        this.finishUrl = finishUrl;
    }

    /**
     * Returns credit card.
     *
     * @return Credit card.
     */
    @Override
    public CreditCard getCreditCard() {
        return creditCard;
    }

    @Deprecated
    public String getCallbackUrl() {
        return callbackUrl;
    }

    public String getFinishUrl() {
        return finishUrl;
    }

    @Override
    public RecurringDetails getRecurringDetails() {
        return recurringDetails;
    }

    @Override
    public void setRecurringDetails(RecurringDetails recurringDetails) {
        this.recurringDetails = recurringDetails;
    }
}
