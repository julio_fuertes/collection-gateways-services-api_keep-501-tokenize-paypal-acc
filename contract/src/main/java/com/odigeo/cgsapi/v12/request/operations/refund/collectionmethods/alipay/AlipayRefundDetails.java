package com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.alipay;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;

public class AlipayRefundDetails extends RefundDetails {
    private static final long serialVersionUID = 1L;
    private String paymentCountryCode;

    public AlipayRefundDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId Identifier of the collection transaction to confirm this transaction.
     * @param money           {@link Money} to be refunded.
     * @param transactionId   Identifier for collection transaction that preceded this refund transaction
     */
    public AlipayRefundDetails(String merchantOrderId, Money money, String transactionId, String paymentCountryCode) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ALIPAY), transactionId);
        this.paymentCountryCode = paymentCountryCode;
    }

    public String getPaymentCountryCode() {
        return paymentCountryCode;
    }
}
