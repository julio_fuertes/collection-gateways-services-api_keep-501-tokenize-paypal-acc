package com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.unionpay;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;

public class UnionpayRefundDetails extends RefundDetails {
    private static final long serialVersionUID = 1L;

    public UnionpayRefundDetails() {
    }

    public UnionpayRefundDetails(String merchantOrderId, Money money, String transactionId) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.UNIONPAY), transactionId);
    }
}
