package com.odigeo.cgsapi.v12.request.operations.authorize.collectionmethods.klarna;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.klarna.KlarnaAccountHolder;
import com.odigeo.cgsapi.v12.request.operations.authorize.AuthorizationDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * Created by ggomezbe on 03/04/2018.
 */
@JsonTypeName("KlarnaAuthorizationDetails")
public class KlarnaAuthorizationDetails extends AuthorizationDetails {

    private static final long serialVersionUID = 1L;

    private final KlarnaAccountHolder klarnaAccountHolder;

    /*default no-args constructor for Dozer*/
    private KlarnaAuthorizationDetails() {
        klarnaAccountHolder = null;
    }

    public KlarnaAuthorizationDetails(String orderId, Money money, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails, KlarnaAccountHolder klarnaAccountHolder) {
        super(orderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.KLARNA), collectionContext, collectionShoppingDetails);
        this.klarnaAccountHolder = klarnaAccountHolder;
    }

    public KlarnaAccountHolder getKlarnaAccountHolder() {
        return klarnaAccountHolder;
    }
}
