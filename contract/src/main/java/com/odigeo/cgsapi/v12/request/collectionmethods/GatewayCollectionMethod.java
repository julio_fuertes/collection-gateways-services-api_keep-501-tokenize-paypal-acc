package com.odigeo.cgsapi.v12.request.collectionmethods;

import com.edreams.util.logic.ThreeValuedLogic;
import com.odigeo.cgsapi.v12.request.CaptureMode;
import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * An object with information about collection method.
 */

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = CreditCardGatewayCollectionMethod.class, name = "CreditCardGatewayCollectionMethod")
    })
public class GatewayCollectionMethod implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Map<GatewayCollectionMethodType, GatewayCollectionMethod> COLLECTION_METHOD_MAP;

    static {
        Map<GatewayCollectionMethodType, GatewayCollectionMethod> aMap = new HashMap<GatewayCollectionMethodType, GatewayCollectionMethod>();
        aMap.put(GatewayCollectionMethodType.ELV, new GatewayCollectionMethod(GatewayCollectionMethodType.ELV));
        aMap.put(GatewayCollectionMethodType.PAYPAL, new GatewayCollectionMethod(GatewayCollectionMethodType.PAYPAL));
        aMap.put(GatewayCollectionMethodType.SOFORT, new GatewayCollectionMethod(GatewayCollectionMethodType.SOFORT));
        aMap.put(GatewayCollectionMethodType.COFINOGA, new GatewayCollectionMethod(GatewayCollectionMethodType.COFINOGA));
        aMap.put(GatewayCollectionMethodType.GIROPAY, new GatewayCollectionMethod(GatewayCollectionMethodType.GIROPAY));
        aMap.put(GatewayCollectionMethodType.BANK_TRANSFER, new GatewayCollectionMethod(GatewayCollectionMethodType.BANK_TRANSFER));
        aMap.put(GatewayCollectionMethodType.ALIPAY, new GatewayCollectionMethod(GatewayCollectionMethodType.ALIPAY));
        aMap.put(GatewayCollectionMethodType.UNIONPAY, new GatewayCollectionMethod(GatewayCollectionMethodType.UNIONPAY));
        aMap.put(GatewayCollectionMethodType.KLARNA, new GatewayCollectionMethod(GatewayCollectionMethodType.KLARNA));
        COLLECTION_METHOD_MAP = Collections.unmodifiableMap(aMap);
    }

    private int id;
    private GatewayCollectionMethodType gatewayCollectionMethodType;
    @Deprecated
    private CollectionActionType collectionMode;
    @Deprecated
    private CaptureMode captureMode;

    //no-args constructor for API REST
    public GatewayCollectionMethod() {
    }

    /**
     * Constructor with {@link GatewayCollectionMethodType} parameter.
     *
     * @param gatewayCollectionMethodType {@link GatewayCollectionMethodType} being used in this {@code GatewayCollectionMethod}.
     */
    public GatewayCollectionMethod(GatewayCollectionMethodType gatewayCollectionMethodType) {
        this();
        this.gatewayCollectionMethodType = gatewayCollectionMethodType;
        collectionMode = CollectionActionType.COLLECT;
    }

    /**
     * Builds {@code GatewayCollectionMethod} for given {@link GatewayCollectionMethodType}.
     *
     * @param gatewayCollectionMethodType Type of needed {@code GatewayCollectionMethod}.
     * @return {@code GatewayCollectionMethod} for given {@link GatewayCollectionMethodType}.
     * @throws UnsupportedOperationException if given {@link GatewayCollectionMethodType} has no associated {@code GatewayCollectionMethod}.
     */
    public static GatewayCollectionMethod valueOf(GatewayCollectionMethodType gatewayCollectionMethodType) {
        GatewayCollectionMethod result = COLLECTION_METHOD_MAP.get(gatewayCollectionMethodType);
        if (result == null) {
            throw new UnsupportedOperationException(String.format("Cannot create an instance of GatewayCollectionMethod for type: %s", gatewayCollectionMethodType));
        }
        return result;
    }

    /**
     * Returns collection method id.
     *
     * @return Collection method id.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets collection method id.
     *
     * @param id Collection method id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns if this gatewayCollectionMethodType allows charge backeable.
     *
     * @return if this gatewayCollectionMethodType allows charge backeable.
     */
    public ThreeValuedLogic isChargeBackable() {
        return gatewayCollectionMethodType.isChargeBackable();
    }

    /**
     * Returns the collectionMode for this {@code GatewayCollectionMethod}.
     *
     * @return CollectionActionType for this {@code GatewayCollectionMethod}.
     */
    @Deprecated
    public CollectionActionType getCollectionMode() {
        return collectionMode;
    }

    /**
     * Sets the collectionMode for this {@code GatewayCollectionMethod}.
     *
     * @param val Collection method collectionMode.
     */
    @Deprecated
    public void setCollectionMode(CollectionActionType val) {
        this.collectionMode = val;
    }

    /**
     * Returns the captureMode for this {@code GatewayCollectionMethod}.
     *
     * @return CaptureMode for this {@code GatewayCollectionMethod}.
     */
    @Deprecated
    public CaptureMode getCaptureMode() {
        return captureMode;
    }

    /**
     * DEPRECATED
     * Sets the captureMode for this {@code GatewayCollectionMethod}.
     *
     * @param val Collection method CaptureMode.
     */
    @Deprecated
    public void setCaptureMode(CaptureMode val) {
        this.captureMode = val;
    }

    /**
     * Returns type for this {@code GatewayCollectionMethod}.
     *
     * @return Type for this {@code GatewayCollectionMethod}.
     */
    public GatewayCollectionMethodType getGatewayCollectionMethodType() {
        return gatewayCollectionMethodType;
    }

    /**
     * Returns if this gatewayCollectionMethod has the gatewayCollectionMethod parameter in top of its hierarchy
     *
     * @param gatewayCollectionMethod {@code GatewayCollectionMethod} searched
     * @return if this gatewayCollectionMethod has the gatewayCollectionMethod parameter in top of its hierarchy
     */
    public boolean hasSuperGatewayCollectionMethod(GatewayCollectionMethod gatewayCollectionMethod) {
        return false;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("id", id)
                .append("gatewayCollectionMethodType", gatewayCollectionMethodType)
                .append("collectionMode", collectionMode)
                .toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        GatewayCollectionMethod rhs = (GatewayCollectionMethod) obj;
        return new EqualsBuilder()
                .append(this.gatewayCollectionMethodType, rhs.gatewayCollectionMethodType)
                .append(this.collectionMode, rhs.collectionMode)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(gatewayCollectionMethodType)
                .append(collectionMode)
                .toHashCode();
    }
}
