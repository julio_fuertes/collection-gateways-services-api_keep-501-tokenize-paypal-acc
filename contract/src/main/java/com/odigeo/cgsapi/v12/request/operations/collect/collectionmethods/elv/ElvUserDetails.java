package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.elv;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

public class ElvUserDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    private String name;
    private String lastName;
    private String street;
    private String streetNumber;
    private String city;
    private String zipCode;
    private String countryCode;

    //no-args constructor for API REST
    public ElvUserDetails() {
    }

    /**
     * Default constructor.
     *
     * @param name
     * @param lastName
     * @param street
     * @param streetNumber
     * @param city
     * @param zipCode
     * @param countryCode
     */
    public ElvUserDetails(String name, String lastName, String street, String streetNumber, String city, String zipCode, String countryCode) {
        super();
        this.name = name;
        this.lastName = lastName;
        this.street = street;
        this.streetNumber = streetNumber;
        this.city = city;
        this.zipCode = zipCode;
        this.countryCode = countryCode;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getStreet() {
        return street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getCity() {
        return city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        ElvUserDetails rhs = (ElvUserDetails) obj;
        return new EqualsBuilder()
                .append(this.name, rhs.name)
                .append(this.lastName, rhs.lastName)
                .append(this.street, rhs.street)
                .append(this.streetNumber, rhs.streetNumber)
                .append(this.city, rhs.city)
                .append(this.zipCode, rhs.zipCode)
                .append(this.countryCode, rhs.countryCode)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(name)
                .append(lastName)
                .append(street)
                .append(streetNumber)
                .append(city)
                .append(zipCode)
                .append(countryCode)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("lastName", lastName)
                .append("street", street)
                .append("streetNumber", streetNumber)
                .append("city", city)
                .append("zipCode", zipCode)
                .append("countryCode", countryCode)
                .toString();
    }
}
