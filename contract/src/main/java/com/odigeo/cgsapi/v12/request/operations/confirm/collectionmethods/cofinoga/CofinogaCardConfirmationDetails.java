package com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.cofinoga;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.cofinoga.CofinogaCard;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * An object with needed information to perform a confirmation operation with a commerce using a cofinoga account.
 *
 * @author egonz
 * @see ConfirmationDetails
 */
public class CofinogaCardConfirmationDetails extends ConfirmationDetails {

    private static final long serialVersionUID = 1L;

    private CofinogaCard cofinogaCard;

    //no-args constructor for API REST
    public CofinogaCardConfirmationDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId Identifier of the collection transaction to confirm this transaction.
     * @param money           {@link Money money} to be confirmed.
     */
    public CofinogaCardConfirmationDetails(String merchantOrderId, Money money, CofinogaCard cofinogaCard) {
        super(merchantOrderId, money, new GatewayCollectionMethod(GatewayCollectionMethodType.COFINOGA));
        this.cofinogaCard = cofinogaCard;
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId   Identifier of the collection transaction to confirm this transaction.
     * @param money             {@link Money money} to be confirmed.
     * @param dynamicDescriptor Dynamic descriptor show in customer´s bank statement. (Restrictions: Dynamic descriptor only allow the character ".", whitespaces and alphanumeric characters except "ñ". Upper and lower cases are allowed.)
     */
    public CofinogaCardConfirmationDetails(String merchantOrderId, Money money, CofinogaCard cofinogaCard, String dynamicDescriptor) {
        super(merchantOrderId, money, new GatewayCollectionMethod(GatewayCollectionMethodType.COFINOGA), dynamicDescriptor);
        this.cofinogaCard = cofinogaCard;
    }

    /**
     * Returns {@link CofinogaCard} to be used.
     *
     * @return {@link CofinogaCard} to be used.
     */
    public CofinogaCard getCofinogaCard() {
        return cofinogaCard;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        CofinogaCardConfirmationDetails rhs = (CofinogaCardConfirmationDetails) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.cofinogaCard, rhs.cofinogaCard)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(cofinogaCard)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("cofinogaCard", cofinogaCard)
                .toString();
    }
}
