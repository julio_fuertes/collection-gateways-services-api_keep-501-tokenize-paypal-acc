package com.odigeo.cgsapi.v12.request.operations.confirm;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.alipay.AlipayConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.cofinoga.CofinogaCardConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.creditcard.CreditCardConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.elv.ElvConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.giropay.GiropayConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.paypal.PaypalConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.secure3d.Secure3dConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.sofort.SofortConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.unionpay.UnionpayConfirmationDetails;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

import java.io.Serializable;

/**
 * An object with needed information to perform a confirmation operation with a commerce.
 */

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = CofinogaCardConfirmationDetails.class, name = "CofinogaCardConfirmationDetails"),
        @JsonSubTypes.Type(value = CreditCardConfirmationDetails.class, name = "CreditCardConfirmationDetails"),
        @JsonSubTypes.Type(value = ElvConfirmationDetails.class, name = "ElvConfirmationDetails"),
        @JsonSubTypes.Type(value = GiropayConfirmationDetails.class, name = "GiropayConfirmationDetails"),
        @JsonSubTypes.Type(value = PaypalConfirmationDetails.class, name = "PaypalConfirmationDetails"),
        @JsonSubTypes.Type(value = Secure3dConfirmationDetails.class, name = "Secure3dConfirmationDetails"),
        @JsonSubTypes.Type(value = SofortConfirmationDetails.class, name = "SofortConfirmationDetails"),
        @JsonSubTypes.Type(value = AlipayConfirmationDetails.class, name = "AlipayConfirmationDetails"),
        @JsonSubTypes.Type(value = UnionpayConfirmationDetails.class, name = "UnionpayConfirmationDetails")
    })
public class ConfirmationDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    private String merchantOrderId;
    private Money money;
    private GatewayCollectionMethod gatewayCollectionMethod;
    private String dynamicDescriptor;

    //no-args constructor for API REST
    protected ConfirmationDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId         Identifier for confirmation operation.
     * @param money                   {@link Money} to be confirmed.
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} used for confirm.
     */
    public ConfirmationDetails(String merchantOrderId, Money money, GatewayCollectionMethod gatewayCollectionMethod) {
        this.merchantOrderId = merchantOrderId;
        this.money = money;
        this.gatewayCollectionMethod = gatewayCollectionMethod;
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId         Identifier for confirmation operation.
     * @param money                   {@link Money} to be confirmed.
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} used for confirm.
     * @param dynamicDescriptor       Dynamic descriptor show in customer´s bank statement. (Restrictions: Dynamic descriptor only allow the character ".", whitespaces and alphanumeric characters except "ñ". Upper and lower cases are allowed.)
     */
    public ConfirmationDetails(String merchantOrderId, Money money, GatewayCollectionMethod gatewayCollectionMethod, String dynamicDescriptor) {
        this.merchantOrderId = merchantOrderId;
        this.money = money;
        this.gatewayCollectionMethod = gatewayCollectionMethod;
        this.dynamicDescriptor = dynamicDescriptor;
    }

    /**
     * Returns identifier for collection order that preceded this confirm transaction.
     *
     * @return Identifier for collection order that preceded this confirm transaction.
     */
    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    /**
     * Returns {@link Money} used in collection.
     *
     * @return {@link Money} used in collection.
     */
    public Money getMoney() {
        return money;
    }

    /**
     * Returns {@link GatewayCollectionMethod} to be used.
     *
     * @return {@link GatewayCollectionMethod} to be used.
     */
    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

    public String getDynamicDescriptor() {
        return dynamicDescriptor;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        ConfirmationDetails rhs = (ConfirmationDetails) obj;
        return new EqualsBuilder()
                .append(this.merchantOrderId, rhs.merchantOrderId)
                .append(this.money, rhs.money)
                .append(this.gatewayCollectionMethod, rhs.gatewayCollectionMethod)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(merchantOrderId)
                .append(money)
                .append(gatewayCollectionMethod)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("merchantOrderId", merchantOrderId)
                .append("money", money)
                .append("gatewayCollectionMethod", gatewayCollectionMethod)
                .toString();
    }
}
