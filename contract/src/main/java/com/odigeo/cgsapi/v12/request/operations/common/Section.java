package com.odigeo.cgsapi.v12.request.operations.common;

import com.odigeo.cgsapi.v12.LocalizedDate;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

public class Section implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer sectionNumber;
    private String productType;
    private String carrierCode;
    private String flightNumber;
    private String departureIataCode;
    private String arrivalIataCode;
    private LocalizedDate departureDate;
    private LocalizedDate arrivalDate;
    private String departureCityName;
    private String arrivalCityName;
    private String serviceClass;
    private String fareBasisCode;
    private Integer numStopOver;

    public Section() {
    }

    public Section(Integer sectionNumber, String productType, String carrierCode, String flightNumber, String departureIataCode, String arrivalIataCode, LocalizedDate departureDate, LocalizedDate arrivalDate, String departureCityName, String arrivalCityName, String serviceClass) {
        this.sectionNumber = sectionNumber;
        this.productType = productType;
        this.carrierCode = carrierCode;
        this.flightNumber = flightNumber;
        this.departureIataCode = departureIataCode;
        this.arrivalIataCode = arrivalIataCode;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.departureCityName = departureCityName;
        this.arrivalCityName = arrivalCityName;
        this.serviceClass = serviceClass;
    }

    public Integer getSectionNumber() {
        return sectionNumber;
    }

    public String getProductType() {
        return productType;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public String getDepartureIataCode() {
        return departureIataCode;
    }

    public String getArrivalIataCode() {
        return arrivalIataCode;
    }

    public LocalizedDate getDepartureDate() {
        return departureDate;
    }

    public LocalizedDate getArrivalDate() {
        return arrivalDate;
    }

    public String getDepartureCityName() {
        return departureCityName;
    }

    public String getArrivalCityName() {
        return arrivalCityName;
    }

    public String getServiceClass() {
        return serviceClass;
    }

    public String getFareBasisCode() {
        return fareBasisCode;
    }

    public Integer getNumStopOver() {
        return numStopOver;
    }

    public void setFareBasisCode(String fareBasisCode) {
        this.fareBasisCode = fareBasisCode;
    }

    public void setNumStopOver(Integer numStopOver) {
        this.numStopOver = numStopOver;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Section rhs = (Section) obj;
        return new EqualsBuilder()
                .append(this.sectionNumber, rhs.sectionNumber)
                .append(this.productType, rhs.productType)
                .append(this.carrierCode, rhs.carrierCode)
                .append(this.flightNumber, rhs.flightNumber)
                .append(this.departureIataCode, rhs.departureIataCode)
                .append(this.arrivalIataCode, rhs.arrivalIataCode)
                .append(this.departureDate, rhs.departureDate)
                .append(this.arrivalDate, rhs.arrivalDate)
                .append(this.departureCityName, rhs.departureCityName)
                .append(this.arrivalCityName, rhs.arrivalCityName)
                .append(this.serviceClass, rhs.serviceClass)
                .append(this.fareBasisCode, rhs.fareBasisCode)
                .append(this.numStopOver, rhs.numStopOver)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(sectionNumber)
                .append(productType)
                .append(carrierCode)
                .append(flightNumber)
                .append(departureIataCode)
                .append(arrivalIataCode)
                .append(departureDate)
                .append(arrivalDate)
                .append(departureCityName)
                .append(arrivalCityName)
                .append(serviceClass)
                .append(fareBasisCode)
                .append(numStopOver)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "Section{"
                + "sectionNumber=" + sectionNumber
                + ", productType='" + productType + '\''
                + ", carrierCode='" + carrierCode + '\''
                + ", flightNumber='" + flightNumber + '\''
                + ", departureIataCode='" + departureIataCode + '\''
                + ", arrivalIataCode='" + arrivalIataCode + '\''
                + ", departureDate=" + departureDate + '\''
                + ", arrivalDate=" + arrivalDate + '\''
                + ", departureCityName='" + departureCityName + '\''
                + ", arrivalCityName='" + arrivalCityName + '\''
                + ", serviceClass='" + serviceClass + '\''
                + ", fareBasisCode='" + fareBasisCode + '\''
                + ", numStopOver='" + numStopOver + '\''
                + '}';
    }
}
