package com.odigeo.cgsapi.v12.request;

/**
 * Collection action type. A collection action type can be one of the following actions:
 * <ul>
 * <li>{@link #COLLECT} A direct payment action.</li>
 * <li>{@link #AUTHORIZE} An authorization action.</li>
 * </ul>
 */
public enum CollectionActionType {
    /**
     * A direct payment action.
     */
    @Deprecated
    DIRECTY_PAYMENT,
    /**
     * An authorization action.
     */
    AUTHORIZE,
    /**
     * Replace DRECTY_PAYMENT. we have to keep the two values
     * until we have the syrevenue-518 full project site.
     */
    COLLECT;

    /**
     * @param code
     * @return {@code CollectionActionType} if the argument is not {@code null} and it
     * represents an equivalent {@code CollectionActionType} value ignoring case; {@code
     * null} otherwise
     */
    public static CollectionActionType getActionType(String code) {
        for (CollectionActionType type : CollectionActionType.values()) {
            if (type.name().equalsIgnoreCase(code)) {
                return type;
            }
        }
        throw new IllegalArgumentException("No CollectionActionType for code: " + code);
    }
}
