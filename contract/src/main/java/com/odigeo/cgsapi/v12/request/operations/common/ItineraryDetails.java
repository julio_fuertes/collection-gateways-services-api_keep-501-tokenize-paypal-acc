package com.odigeo.cgsapi.v12.request.operations.common;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

public class ItineraryDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<Segment> segments;
    private List<String> pnrs;

    //no-args constructor for API REST
    public ItineraryDetails() {
    }

    /**
     * Creates a new CollectionShoppingDetails.
     *
     * @param pnrs     The list of PNRs in the itinerary
     * @param segments The list of segments in the booking
     */
    public ItineraryDetails(List<String> pnrs, List<Segment> segments) {
        this.pnrs = pnrs;
        this.segments = segments;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public List<String> getPnrs() {
        return pnrs;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        ItineraryDetails rhs = (ItineraryDetails) obj;
        return new EqualsBuilder()
                .append(this.segments, rhs.segments)
                .append(this.pnrs, rhs.pnrs)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(segments)
                .append(pnrs)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("segments", segments)
                .append("pnrs", pnrs)
                .toString();
    }
}
