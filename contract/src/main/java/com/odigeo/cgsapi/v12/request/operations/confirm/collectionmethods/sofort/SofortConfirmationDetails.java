package com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.sofort;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;

/**
 * An object with needed information to perform a confirmation operation with a commerce using an Sofort account.
 *
 * @author egonz
 * @see ConfirmationDetails
 */
public class SofortConfirmationDetails extends ConfirmationDetails {

    private static final long serialVersionUID = 1L;

    //no-args constructor for API REST
    protected SofortConfirmationDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId Identifier of the collection transaction to confirm this transaction.
     * @param money           {@link Money money} to be refunded.
     */
    public SofortConfirmationDetails(String merchantOrderId, Money money) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.SOFORT));
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId   Identifier of the collection transaction to confirm this transaction.
     * @param money             {@link Money money} to be refunded.
     * @param dynamicDescriptor Dynamic descriptor show in customer´s bank statement. (Restrictions: Dynamic descriptor only allow the character ".", whitespaces and alphanumeric characters except "ñ". Upper and lower cases are allowed.)
     */
    public SofortConfirmationDetails(String merchantOrderId, Money money, String dynamicDescriptor) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.SOFORT), dynamicDescriptor);
    }
}
