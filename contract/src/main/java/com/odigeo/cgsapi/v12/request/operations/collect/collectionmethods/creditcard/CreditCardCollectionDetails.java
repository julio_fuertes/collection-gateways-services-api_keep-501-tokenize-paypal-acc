package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.creditcard;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.RecurringDetails;
import com.odigeo.cgsapi.v12.request.RecurringPayment;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardHolder;
import com.odigeo.cgsapi.v12.request.operations.collect.CollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.creditcard.CreditCardConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.creditcard.CreditCardRefundDetails;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * An object with needed information to perform a collection operation with a commerce using a {@link CreditCard}.
 *
 * @see CollectionDetails
 */
@JsonTypeName("CreditCardCollectionDetails")
public class CreditCardCollectionDetails extends CollectionDetails implements CreditCardHolder, RecurringPayment {
    private static final long serialVersionUID = 1L;
    private CreditCard creditCard;
    private RecurringDetails recurringDetails;


    //no-args constructor for API REST
    public CreditCardCollectionDetails() {
    }

    /**
     * Default constructor.
     *
     * @param orderId                   Identifier for collection operation.
     * @param money                     {@link Money} to be collected.
     * @param creditCard                {@link CreditCard} to be used.
     * @param collectionContext         {@link CollectionContext} used in collection.
     * @param collectionShoppingDetails {@link CollectionShoppingDetails} used in collection.
     */
    public CreditCardCollectionDetails(String orderId, Money money, CreditCard creditCard, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails) {
        super(orderId, money, new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, creditCard.getType()), collectionContext, collectionShoppingDetails);
        this.creditCard = creditCard;
    }

    /**
     * Returns {@link CreditCard} to be used.
     *
     * @return {@link CreditCard} to be used.
     */
    public CreditCard getCreditCard() {
        return creditCard;
    }

    /**
     * Transforms current {@code CreditCardCollectionDetails} instance into a {@link CreditCardRefundDetails} instance.
     *
     * @param transactionId Identifier for collection transaction that preceded this refund transaction.
     * @return {@link CreditCardRefundDetails} instance with same parameters as current {@code CreditCardCollectionDetails} instance.
     * @see CollectionDetails#toRefundDetails(String, String) 
     */
    @Override
    public RefundDetails toRefundDetails(String transactionId, String merchantOrderId) {
        return new CreditCardRefundDetails(merchantOrderId, getMoney(), creditCard, transactionId);
    }

    /**
     * Transforms current {@code CreditCardCollectionDetails} instance into a {@link CreditCardConfirmationDetails} instance.
     *
     * @param merchantOrderId for collection transaction that preceded this refund transaction.
     * @return {@link CreditCardConfirmationDetails} instance with same parameters as current {@code CreditCardCollectionDetails} instance.
     */
    @Override
    public ConfirmationDetails toConfirmationDetails(String merchantOrderId) {
        return new CreditCardConfirmationDetails(merchantOrderId, getMoney(), getGatewayCollectionMethod(), creditCard);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        CreditCardCollectionDetails rhs = (CreditCardCollectionDetails) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.creditCard, rhs.creditCard)
                .append(this.recurringDetails, rhs.recurringDetails)
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("creditCard", creditCard)
                .append("recurringDetails", recurringDetails)
                .toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(creditCard)
                .append(recurringDetails)
                .toHashCode();
    }

    @Override
    public RecurringDetails getRecurringDetails() {
        return recurringDetails;
    }

    @Override
    public void setRecurringDetails(RecurringDetails recurringDetails) {
        this.recurringDetails = recurringDetails;
    }
}
