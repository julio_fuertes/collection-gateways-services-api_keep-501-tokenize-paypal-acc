package com.odigeo.cgsapi.v12.request.collectionmethods.cofinoga;

import com.edreams.util.Checks;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * An object with information about a cofinoga card.
 *
 * @author egonz
 */
public class CofinogaCard implements Serializable {

    private static final long serialVersionUID = 1L;
    private String owner;
    private String number;
    private String dayOfBirth;
    private String monthOfBirth;
    private String yearOfBirth;

    // no-args constructor for API rest
    public CofinogaCard() {
    }

    /**
     * Default constructor.
     *
     * @param owner        Cofinoga card owner.
     * @param number       Cofinoga card number.
     * @param dayOfBirth   Cofinoga card's owner day of birth. Example: 05
     * @param monthOfBirth Cofinoga card's owner month of birth. Example: 07
     * @param yearOfBirth  Cofinoga card's owner year of birth. Example: 1947
     */
    public CofinogaCard(String owner, String number, String dayOfBirth, String monthOfBirth, String yearOfBirth) {
        this.owner = owner;
        this.number = number;
        this.dayOfBirth = dayOfBirth;
        this.monthOfBirth = monthOfBirth;
        this.yearOfBirth = yearOfBirth;
        validate();
    }

    /**
     * Validates mandatory data for cofinoga card.
     */
    private void validate() {
        Checks.checkNotNull(owner, "owner");
        Checks.checkNotNull(number, "number");
        Checks.checkNotNull(dayOfBirth, "dayOfBirth");
        Checks.checkValidLength(dayOfBirth, 2, "dayOfBirth");
        Checks.checkNotNull(monthOfBirth, "monthOfBirth");
        Checks.checkValidLength(monthOfBirth, 2, "monthOfBirth");
        Checks.checkNotNull(yearOfBirth, "yearOfBirth");
        Checks.checkValidLength(yearOfBirth, 4, "yearOfBirth");
    }

    /**
     * Returns cofinoga card owner.
     *
     * @return Cofinoga card owner.
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Returns cofinoga card number.
     *
     * @return Cofinoga card number.
     */
    public String getNumber() {
        return number;
    }

    /**
     * Returns day of birth as {@link String}.
     *
     * @return day of birth as {@link String}.
     */
    public String getDayOfBirth() {
        return dayOfBirth;
    }

    /**
     * Returns month of birth as {@link String}.
     *
     * @return month of birth as {@link String}.
     */
    public String getMonthOfBirth() {
        return monthOfBirth;
    }

    /**
     * Returns year of birth as {@link String}.
     *
     * @return year of birth as {@link String}.
     */
    public String getYearOfBirth() {
        return yearOfBirth;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(number).append(dayOfBirth).append(monthOfBirth).append(yearOfBirth).toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CofinogaCard that = (CofinogaCard) o;

        return new EqualsBuilder().append(number, that.number).append(dayOfBirth, that.dayOfBirth).append(monthOfBirth, that.monthOfBirth).append(yearOfBirth, that.yearOfBirth).isEquals();
    }

    @Override
    public String toString() {
        return "CofinogaCard [owner=" + owner + ", number=" + number + ", dayOfBirth=" + dayOfBirth + ", monthOfBirth=" + monthOfBirth + ", yearOfBirth=" + yearOfBirth + "]";
    }
}
