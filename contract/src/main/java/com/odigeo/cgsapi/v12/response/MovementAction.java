package com.odigeo.cgsapi.v12.response;

/**
 * A payment gateway movement action. A payment gateway movement can be one of the following actions:
 * <ul>
 * <li>{@link #COLLECT} A direct payment action.</li>
 * <li>{@link #AUTHORIZE} An authorization action.</li>
 * <li>{@link #REQUEST} A request action with no communication to payment gateway, such as 3D Secure init action.</li>
 * <li>{@link #CONFIRM} A confirm action.</li>
 * <li>{@link #QUERY} A query action.</li>
 * <li>{@link #REFUND} A refund action.</li>
 * <li>{@link #CANCEL} A cancel action.</li>
 * </ul>
 * A movement can only be an action.
 */
public enum MovementAction {

    /**
     * A direct payment action.
     */
    COLLECT,
    /**
     * An authorization action.
     */
    AUTHORIZE,
    /**
     * A request action with no communication to payment gateway, such as 3D Secure init action,
     * or a request action with communication to payment gateway to indicate a payment will be ordered.
     */
    REQUEST,
    /**
     * A confirm action.
     */
    CONFIRM,
    /**
     * A query action.
     */
    QUERY,
    /**
     * A refund action.
     */
    REFUND,
    /**
     * A manual refund action.
     */
    MANUAL_REFUND,
    /**
     * A cancel action.
     */
    CANCEL,
    /**
     * A verify collection action.
     */
    VERIFY,
    /**
     * A bin look up action.
     */
    BIN_LOOK_UP,
    /**
     * A chargeback action
     */
    CHARGEBACK
}
