package com.odigeo.cgsapi.v12.response.operations.notify;

public enum Result {
    SUCCESS,
    ERROR
}
