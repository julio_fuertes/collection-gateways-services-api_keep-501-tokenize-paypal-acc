package com.odigeo.cgsapi.v12.response.operations.binlookup;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementContainer;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.List;

/**
 * @author alain.munoz
 * @since 17/01/14
 */
public class BinLookUpResponse implements MovementContainer {

    private static final long serialVersionUID = 1L;

    private CreditCardType creditCardType;
    private List<Movement> movements;
    private BigDecimal amount;
    private Currency currency;
    private String countryCode;
    private Integer numCountryCode;
    private GatewayCollectionMethod gatewayCollectionMethod;

    //no-args constructor for API REST
    public BinLookUpResponse() {
    }

    /**
     * Default constructor.
     *
     * @param binLookUp BinLookUp to construct response.
     */
    public BinLookUpResponse(BinLookUp binLookUp) {
        this.movements = Collections.singletonList((Movement) binLookUp);
        this.amount = binLookUp.getAmount();
        this.currency = binLookUp.getCurrency();
        this.countryCode = binLookUp.getCountryCode();
        this.numCountryCode = binLookUp.getNumCountryCode();
        this.creditCardType = binLookUp.getCreditCardType();
    }

    /**
     * Returns numeric ISO 3166-1 code for credit card country, if a credit card was used for BinLookUp.
     * Full list of numeric codes can be found here:
     * <a href='http://unstats.un.org/unsd/methods/m49/m49alpha.htm'>http://unstats.un.org/unsd/methods/m49/m49alpha.htm</a>
     *
     * @return Numeric ISO 3166-1 code for credit card country, if a credit card was used for BinLookUp.
     */
    public Integer getNumCountryCode() {
        return numCountryCode;
    }

    /**
     * Sets numeric ISO 3166-1 code for credit card country.
     *
     * @param numCountryCode Numeric ISO 3166-1 code for credit card country.
     */
    public void setNumCountryCode(Integer numCountryCode) {
        this.numCountryCode = numCountryCode;
    }

    /**
     * Returns 2 letters ISO 3166-1 code for credit card country, if a credit card was used for BinLookUp.
     * Full list of numeric codes can be found here:
     * <a href='http://unstats.un.org/unsd/methods/m49/m49alpha.htm'>http://unstats.un.org/unsd/methods/m49/m49alpha.htm</a>
     *
     * @return 2 letters ISO 3166-1 code for credit card country, if a credit card was used for BinLookUp.
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets 2 letters ISO 3166-1 code for credit card country.
     *
     * @param countryCode 2 letters ISO 3166-1 code for credit card country.
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return a credit card type code given by the commerce in the BinLookUp movement.
     */
    public CreditCardType getCreditCardType() {
        return creditCardType;
    }

    /**
     * Returns {@link GatewayCollectionMethod} used in collection operation.
     *
     * @return {@link GatewayCollectionMethod} used in collection operation.
     */
    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

    /**
     * Sets {@link GatewayCollectionMethod} used in collection operation.
     *
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} used in collection operation.
     */
    public void setGatewayCollectionMethod(GatewayCollectionMethod gatewayCollectionMethod) {
        this.gatewayCollectionMethod = gatewayCollectionMethod;
    }

    /**
     * Returns full list with every movement done in payment gateway.
     *
     * @return Full list with every movement done in payment gateway.
     */
    public Collection<Movement> getMovements() {
        return movements;
    }

    /**
     * Returns {@link CollectionStatus#INIT} as this is a BinLookUp.
     *
     * @return {@link CollectionStatus#INIT} as this is a BinLookUp.
     */
    @JsonIgnore
    public CollectionStatus getCollectionStatus() {
        return CollectionStatus.INIT;
    }

    /**
     * Returns numeric amount that has been collected.
     *
     * @return Numeric amount that has been collected.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Returns currency of ecommerce operation.
     *
     * @return currency of ecommerce operation.
     */
    public Currency getCurrency() {
        return currency;
    }
}
