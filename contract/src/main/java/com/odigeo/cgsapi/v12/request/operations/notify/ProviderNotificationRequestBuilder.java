package com.odigeo.cgsapi.v12.request.operations.notify;

import com.odigeo.cgsapi.v12.request.VirtualPointOfSaleType;

import java.util.Map;

public class ProviderNotificationRequestBuilder {

    private VirtualPointOfSaleType virtualPointOfSale;
    private Map<String, Object> parameters;

    public ProviderNotificationRequestBuilder virtualPointOfSale(VirtualPointOfSaleType virtualPointOfSale) {
        this.virtualPointOfSale = virtualPointOfSale;
        return this;
    }

    public ProviderNotificationRequestBuilder parameters(Map<String, Object> parameters) {
        this.parameters = parameters;
        return this;
    }

    public VirtualPointOfSaleType getVirtualPointOfSale() {
        return virtualPointOfSale;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public ProviderNotificationRequest build() {
        return new ProviderNotificationRequest(this);
    }
}
