package com.odigeo.cgsapi.v12.request.collectionmethods.creditcard;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;

/**
 * An object with information about collection method with a {@link CreditCard} code.
 */
public class CreditCardGatewayCollectionMethod extends GatewayCollectionMethod {
    private static final long serialVersionUID = 1L;
    private CreditCardType type;

    //no-args constructor for API REST
    public CreditCardGatewayCollectionMethod() {
    }

    /**
     * Constructor with {@link CreditCard} code.
     *
     * @param gatewayCollectionMethodType CollectionMethodType.CREDITCARD
     * @param type                        {@link CreditCardType} being used in collection.
     */
    public CreditCardGatewayCollectionMethod(GatewayCollectionMethodType gatewayCollectionMethodType, CreditCardType type) {
        super(gatewayCollectionMethodType);
        this.type = type;
    }

    /**
     * Returns Code for this {@code CreditCardCollectionMethod}.
     *
     * @return Code for this {@code CreditCardCollectionMethod}.
     */
    public CreditCardType getType() {
        return type;
    }

    /**
     * Sets {@link CreditCard} code to be used in cancel operation.
     *
     * @param type {@link CreditCardType} to be used in ecommerce operation.
     */
    public void setType(CreditCardType type) {
        this.type = type;
    }

    /**
     * Returns if this gatewayCollectionMethod has the gatewayCollectionMethod parameter in top of its hierarchy
     *
     * @param gatewayCollectionMethod {@code GatewayCollectionMethod} searched
     * @return if this gatewayCollectionMethod has the gatewayCollectionMethod parameter in top of its hierarchy
     */
    public boolean hasSuperGatewayCollectionMethod(GatewayCollectionMethod gatewayCollectionMethod) {
        if (getType().getSuperType() != null) {
            CreditCardGatewayCollectionMethod creditCardGatewayCollectionMethod = new CreditCardGatewayCollectionMethod(getGatewayCollectionMethodType(), getType().getSuperType());
            return creditCardGatewayCollectionMethod.equals(gatewayCollectionMethod) || creditCardGatewayCollectionMethod.hasSuperGatewayCollectionMethod(gatewayCollectionMethod);
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CreditCardGatewayCollectionMethod)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        CreditCardGatewayCollectionMethod that = (CreditCardGatewayCollectionMethod) o;
        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString())
                .append(",{type=").append(type)
                .append('}');
        return sb.toString();
    }
}
