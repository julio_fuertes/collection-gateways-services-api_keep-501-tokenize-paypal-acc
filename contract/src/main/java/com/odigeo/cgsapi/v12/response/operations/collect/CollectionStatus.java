package com.odigeo.cgsapi.v12.response.operations.collect;

/**
 * A collection status. A collection operation can be in one of the following statuses:
 * <ul>
 * <li>{@link #INIT} collection operation has not yet started.</li>
 * <li>{@link #ON_HOLD} collection operation requires user interaction, so it has been suspended.</li>
 * <li>{@link #COLLECTED} collection amount has been successfully collected.</li>
 * <li>{@link #AUTHORIZED} authorization has been successfully.</li>
 * <li>{@link #COLLECTED_ZERO} zero amount has been collected.</li>
 * <li>{@link #COLLECTION_DECLINED} collection amount has been declined.</li>
 * <li>{@link #COLLECTION_ERROR} collection operation has some error.</li>
 * <li>{@link #REFUNDED} refund amount has been successfully refunded.</li>
 * <li>{@link #REFUNDED_DECLINED} refund amount has been declined to be refunded.</li>
 * <li>{@link #REFUND_ERROR} refund operation has some error.</li>
 * <li>{@link #QUERY_ERROR} query operation had an error.</li>
 * </ul>
 */
public enum CollectionStatus {

    /**
     * collection operation has not yet started.
     */
    INIT,
    /**
     * collection amount has been successfully collected.
     */
    COLLECTED,
    /**
     * authorization has been successfully
     */
    AUTHORIZED,
    /**
     * zero amount has been collected.
     */
    COLLECTED_ZERO,
    /**
     * collection has been cancelled by user.
     */
    COLLECTION_CANCELLED,
    /**
     * collection amount has been declined.
     */
    COLLECTION_DECLINED,
    /**
     * collection operation has some error.
     */
    COLLECTION_ERROR,
    /**
     * refund amount has been successfully refunded.
     */
    REFUNDED,
    /**
     * refund amount has been declined to be refunded.
     */
    REFUNDED_DECLINED,
    /**
     * refund operation has some error.
     */
    REFUND_ERROR,
    /**
     * collection operation requires user interaction, so it has been suspended.
     */
    ON_HOLD,
    /**
     * collection operation has been postponed for batch collection.
     */
    USE_BATCH_COLLECTION,
    /**
     * Query operation had an error.
     */
    QUERY_ERROR,
    /**
     * Operation was cancelled.
     */
    CANCELLED,
    /**
     * Cancel operation had an error
     */
    CANCEL_ERROR,
    /**
     * collection cannot be known
     */
    REQUESTED;
}
