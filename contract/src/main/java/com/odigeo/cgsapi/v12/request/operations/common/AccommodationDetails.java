package com.odigeo.cgsapi.v12.request.operations.common;

import com.odigeo.cgsapi.v12.LocalizedDate;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

public class AccommodationDetails implements Serializable {
    private static final long serialVersionUID = 1L;

    private String hotelName;

    private LocalizedDate checkInDate;

    private LocalizedDate checkOutDate;

    private String address;

    private String city;

    private String country;

    public AccommodationDetails() {
    }

    public AccommodationDetails(String hotelName, LocalizedDate checkInDate, LocalizedDate checkOutDate, String address, String city, String country) {
        this.hotelName = hotelName;
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
        this.address = address;
        this.city = city;
        this.country = country;
    }

    public String getHotelName() {
        return hotelName;
    }

    public LocalizedDate getCheckInDate() {
        return checkInDate;
    }

    public LocalizedDate getCheckOutDate() {
        return checkOutDate;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        AccommodationDetails rhs = (AccommodationDetails) obj;
        return new EqualsBuilder()
                .append(this.hotelName, rhs.hotelName)
                .append(this.checkInDate, rhs.checkInDate)
                .append(this.checkOutDate, rhs.checkOutDate)
                .append(this.address, rhs.address)
                .append(this.city, rhs.city)
                .append(this.country, rhs.country)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(hotelName)
                .append(checkInDate)
                .append(checkOutDate)
                .append(address)
                .append(city)
                .append(country)
                .toHashCode();
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("hotelName", hotelName)
                .append("checkInDate", checkInDate)
                .append("checkOutDate", checkOutDate)
                .append("address", address)
                .append("city", city)
                .append("country", country)
                .toString();
    }
}
