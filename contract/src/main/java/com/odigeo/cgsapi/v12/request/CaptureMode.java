package com.odigeo.cgsapi.v12.request;

/**
 * Created by vrieraba on 12/06/2015.
 */
public enum CaptureMode {
    ALWAYS_ONLINE,
    DEFERRABLE,
    NONE;

    /**
     * @param code
     * @return {@code CaptureMode} if the argument is not {@code null} and it
     * represents an equivalent {@code CaptureMode} value ignoring case; {@code
     * null} otherwise
     */
    public static CaptureMode findByCode(String code) {
        for (CaptureMode type : CaptureMode.values()) {
            if (type.name().equalsIgnoreCase(code)) {
                return type;
            }
        }
        throw new IllegalArgumentException("No CaptureMode for code: " + code);
    }
}
