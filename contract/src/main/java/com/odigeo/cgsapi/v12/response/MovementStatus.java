package com.odigeo.cgsapi.v12.response;

/**
 * A payment gateway movement status. A payment gateway movement can only have one of following status:
 * <ul>
 * <li>{@link #NEEDS_INTERACTION} Movement requires user interaction.</li>
 * <li>{@link #INIT_INTERACTION} Movement inits user interaction process.</li>
 * <li>{@link #CANCEL} An error has occured while cancel current movement.</li>
 * <li>{@link #CANCELERR} Movement has been declined by payment gateway server.</li>
 * <li>{@link #DECLINED} Movement has been declined by payment gateway server.</li>
 * <li>{@link #PAID} Movement has been paid in payment gateway server.</li>
 * <li>{@link #AUTHORIZED} Movement has been authorized in payment gateway server.</li>
 * <li>{@link #PAYERROR} An error has occured while paying current movement.</li>
 * <li>{@link #REFUNDED} Movement has been refunded in payment gateway server.</li>
 * <li>{@link #REFUNDERR} An error has occured while refunding current movement.</li>
 * </ul>
 * A movement can only have one status.
 */
public enum MovementStatus {

    /**
     * Movement requires user interaction.
     *
     * @deprecated Use INIT_INTERACTION instead.
     */
    @Deprecated
    NEEDS_INTERACTION,
    /**
     * Movement inits user interaction process.
     */
    INIT_INTERACTION,
    /**
     * Movement has been cancelled in payment gateway server.
     */
    CANCEL,
    /**
     * An error has occured while cancel current movement.
     */
    CANCELERR,
    /**
     * Movement has been declined by payment gateway server.
     */
    DECLINED,
    /**
     * Movement has been paid in payment gateway server.
     */
    PAID,
    /**
     * Movement has been authorized in payment gateway server.
     */
    AUTHORIZED,
    /**
     * An error has occured while paying current movement.
     */
    PAYERROR,
    /**
     * An error has occured while refunding current movement.
     */
    REFUNDERR,
    /**
     * Movement has been refunded in payment gateway server.
     */
    REFUNDED,
    /**
     * Movement is postponed to use batch collection.
     */
    USE_BATCH,
    /**
     * Movement has consulting state of current payment.
     */
    STATUS,
    /**
     * An error has occured while querying current movement.
     */
    QUERYERR,
    /**
     * Movement cannot be verified
     */
    REQUESTED,
    /**
     * For chargeback movements
     */
    CHARGEBACKED,
    /**
     * For chargeback movements
     */
    CHARGEBACK_CANCELLED,
    /**
     * For chargeback movements
     */
    CHARGEBACK_INFO,
    /**
     * Movement invalidates another one
     */
    INVALIDATED
}
