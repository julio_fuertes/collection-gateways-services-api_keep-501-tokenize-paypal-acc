package com.odigeo.cgsapi.v12.response;

public interface RecurringContainer {

    String getRecurringTransactionId();
}
