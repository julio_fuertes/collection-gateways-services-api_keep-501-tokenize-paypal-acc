package com.odigeo.cgsapi.v12.response.operations.query;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.codehaus.jackson.annotate.JsonTypeName;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Created by vrieraba on 01/06/2015.
 */
@JsonTypeName("Query")
public final class Query extends AbstractQuery {

    private static final long serialVersionUID = 1L;

    /*default no-args constructor for Dozer*/
    private Query() {
    }

    /**
     * @param merchantOrderId  Merchant order id of this movement.
     * @param amount           Amount.
     * @param currency         Currency.
     * @param code             Merchant error code.
     * @param errorDescription Error description.
     * @param status           Movement status.
     */
    private Query(String merchantOrderId, BigDecimal amount, Currency currency, String code, String errorDescription, CollectionActionType collectionActionType, MovementStatus status) {
        super(merchantOrderId, amount, currency, code, errorDescription, collectionActionType, status);
    }

    /**
     * Factory method to create a Query movement
     *
     * @param merchantOrderId      Merchant order id of this movement.
     * @param money                Amount and Currency.
     * @param code                 Merchant error code.
     * @param errorDescription     Error description.
     * @param collectionActionType
     * @param status
     * @return An Authorization.
     */
    public static Query createQuery(String merchantOrderId, Money money, String code, String errorDescription, CollectionActionType collectionActionType, MovementStatus status) {
        return new Query(merchantOrderId, money != null ? money.getAmount() : null, money != null ? money.getCurrency() : null, code, errorDescription, collectionActionType, status);
    }

    /**
     * Factory method that creates a Failed Query
     *
     * @param merchantOrderId      Merchant order id of this movement.
     * @param money                Amount and Currency.
     * @param code                 Merchant error code.
     * @param errorDescription     Error description.
     * @param collectionActionType
     * @return A Failed Authorization.
     */
    public static Query createFailedQuery(String merchantOrderId, Money money, String code, String errorDescription, CollectionActionType collectionActionType) {
        return createQuery(merchantOrderId, money, code, errorDescription, collectionActionType, MovementStatus.QUERYERR);
    }

    /**
     * Factory method that a Fake Query
     *
     * @param merchantOrderId      Merchant order id of this movement.
     * @param money                Amount and Currency.
     * @param collectionActionType
     * @param status               Desired movement status
     * @return A Fake Authorization.
     */
    public static Query createFakeQuery(String merchantOrderId, Money money, CollectionActionType collectionActionType, MovementStatus status) {
        return createQuery(merchantOrderId, money, FAKE_CODE, FAKE_ERROR_MESSAGE, collectionActionType, status);
    }
}

