package com.odigeo.cgsapi.v12;

import com.odigeo.cgsapi.v12.exceptions.CgsApiException;
import com.odigeo.cgsapi.v12.request.operations.authorize.AuthorizationDetails;
import com.odigeo.cgsapi.v12.request.operations.binlookup.collectionmethods.creditcard.BinLookUpDetails;
import com.odigeo.cgsapi.v12.request.operations.cancel.CancelDetails;
import com.odigeo.cgsapi.v12.request.operations.collect.CollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.get.booking.id.GetBookingIdRequest;
import com.odigeo.cgsapi.v12.request.operations.notify.ProviderNotificationRequest;
import com.odigeo.cgsapi.v12.request.operations.query.QueryDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.status.StatusDetails;
import com.odigeo.cgsapi.v12.request.operations.verify.VerificationDetails;
import com.odigeo.cgsapi.v12.response.operations.authorize.AuthorizationResponse;
import com.odigeo.cgsapi.v12.response.operations.binlookup.BinLookUpResponse;
import com.odigeo.cgsapi.v12.response.operations.cancel.CancelResponse;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionResponse;
import com.odigeo.cgsapi.v12.response.operations.confirm.ConfirmationResponse;
import com.odigeo.cgsapi.v12.response.operations.get.booking.id.GetBookingIdResponse;
import com.odigeo.cgsapi.v12.response.operations.notify.ProviderNotificationResponse;
import com.odigeo.cgsapi.v12.response.operations.query.QueryResponse;
import com.odigeo.cgsapi.v12.response.operations.refund.RefundResponse;
import com.odigeo.cgsapi.v12.response.operations.status.MonitorResponse;
import com.odigeo.cgsapi.v12.response.operations.verify.VerificationResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.net.SocketTimeoutException;

/**
 * This is the contract of the API for the Payment Gateway service v1
 */
@Path("/cgsapi/v12")
public interface CgsApiService {

    String JSON_MIME_TYPE = "application/json; charset=UTF-8";

    @POST
    @Path("/authorize/{id}")
    @Produces(JSON_MIME_TYPE)
    @Consumes(JSON_MIME_TYPE)
    AuthorizationResponse authorize(@javax.ws.rs.PathParam("id") String virtualPointOfSaleCommerce, AuthorizationDetails authorizationDetails) throws CgsApiException, SocketTimeoutException;

    @POST
    @Path("/binLookUp/{id}")
    @Produces(JSON_MIME_TYPE)
    @Consumes(JSON_MIME_TYPE)
    BinLookUpResponse binLookUp(@javax.ws.rs.PathParam("id") String virtualPointOfSaleCommerce, BinLookUpDetails binLookUpDetails) throws CgsApiException, SocketTimeoutException;

    @POST
    @Path("/collect/{id}")
    @Produces(JSON_MIME_TYPE)
    @Consumes(JSON_MIME_TYPE)
    CollectionResponse collect(@javax.ws.rs.PathParam("id") String virtualPointOfSaleCommerce, CollectionDetails collectionDetails) throws CgsApiException, SocketTimeoutException;

    @POST
    @Path("/cancel/{id}")
    @Produces(JSON_MIME_TYPE)
    @Consumes(JSON_MIME_TYPE)
    CancelResponse cancel(@javax.ws.rs.PathParam("id") String virtualPointOfSaleCommerce, CancelDetails cancelDetails) throws CgsApiException, SocketTimeoutException;

    @POST
    @Path("/confirm/{id}")
    @Produces(JSON_MIME_TYPE)
    @Consumes(JSON_MIME_TYPE)
    ConfirmationResponse confirm(@javax.ws.rs.PathParam("id") String virtualPointOfSaleCommerce, ConfirmationDetails confirmationDetails) throws CgsApiException, SocketTimeoutException;

    @POST
    @Path("/query/{id}")
    @Produces(JSON_MIME_TYPE)
    @Consumes(JSON_MIME_TYPE)
    QueryResponse query(@javax.ws.rs.PathParam("id") String virtualPointOfSaleCommerce, QueryDetails queryDetails) throws CgsApiException, SocketTimeoutException;

    @POST
    @Path("/refund/{id}")
    @Produces(JSON_MIME_TYPE)
    @Consumes(JSON_MIME_TYPE)
    RefundResponse refund(@javax.ws.rs.PathParam("id") String virtualPointOfSaleCommerce, RefundDetails refundDetails) throws CgsApiException, SocketTimeoutException;

    @POST
    @Path("/status/{id}")
    @Produces(JSON_MIME_TYPE)
    @Consumes(JSON_MIME_TYPE)
    MonitorResponse status(@javax.ws.rs.PathParam("id") String virtualPointOfSaleCommerce, StatusDetails statusDetails) throws CgsApiException, SocketTimeoutException;

    @POST
    @Path("/verify/{id}")
    @Produces(JSON_MIME_TYPE)
    @Consumes(JSON_MIME_TYPE)
    VerificationResponse verify(@javax.ws.rs.PathParam("id") String virtualPointOfSaleCommerce, VerificationDetails verificationDetails) throws CgsApiException, SocketTimeoutException;

    @POST
    @Path("/notify")
    @Produces(JSON_MIME_TYPE)
    @Consumes(JSON_MIME_TYPE)
    ProviderNotificationResponse notify(ProviderNotificationRequest providerNotificationRequest);

    @POST
    @Path("/get/booking/id")
    @Produces(JSON_MIME_TYPE)
    @Consumes(JSON_MIME_TYPE)
    GetBookingIdResponse getBookingId(GetBookingIdRequest getBookingIdRequest);
}

