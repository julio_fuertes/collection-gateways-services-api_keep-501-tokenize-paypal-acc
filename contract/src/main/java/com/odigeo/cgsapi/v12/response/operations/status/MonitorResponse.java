package com.odigeo.cgsapi.v12.response.operations.status;

import java.io.Serializable;

/**
 * Date: 19/05/14
 */
public class MonitorResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private boolean active;
    private String code;
    private String description;

    //no-args constructor for API REST
    public MonitorResponse() {
    }

    public MonitorResponse(boolean active, String code, String description) {
        this.active = active;
        this.code = code;
        this.description = description;
    }

    public boolean isActive() {
        return active;
    }

    public String getDescription() {
        return description;
    }

    public String getCode() {
        return code;
    }
}
