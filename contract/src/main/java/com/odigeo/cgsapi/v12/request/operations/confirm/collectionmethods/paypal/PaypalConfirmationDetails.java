package com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.paypal;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;

/**
 * An object with needed information to perform a confirmation operation with a commerce.
 *
 * @see ConfirmationDetails
 */
public class PaypalConfirmationDetails extends ConfirmationDetails {

    private static final long serialVersionUID = 1L;

    //no-args constructor for API REST
    public PaypalConfirmationDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId of the collection transaction to confirm this transaction.
     * @param money           {@link Money} to be confirmed.
     */
    public PaypalConfirmationDetails(String merchantOrderId, Money money) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL));
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId   of the collection transaction to confirm this transaction.
     * @param money             {@link Money} to be confirmed.
     * @param dynamicDescriptor Dynamic descriptor show in customer´s bank statement. (Restrictions: Dynamic descriptor only allow the character ".", whitespaces and alphanumeric characters except "ñ". Upper and lower cases are allowed.)
     */
    public PaypalConfirmationDetails(String merchantOrderId, Money money, String dynamicDescriptor) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.PAYPAL), dynamicDescriptor);
    }
}
