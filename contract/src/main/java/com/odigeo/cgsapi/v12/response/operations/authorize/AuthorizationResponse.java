package com.odigeo.cgsapi.v12.response.operations.authorize;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementContainer;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.RecurringContainer;
import com.odigeo.cgsapi.v12.response.RedirectionParameters;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.List;

import static java.lang.String.format;

/**
 * An object with information about authorization result with a commerce.
 */
public class AuthorizationResponse implements MovementContainer, RecurringContainer {

    private static final long serialVersionUID = 1L;

    private CollectionStatus collectionStatus;
    private List<Movement> movements;
    private BigDecimal amount;
    private Currency currency;
    private RedirectionParameters redirectionParameters;
    private GatewayCollectionMethod gatewayCollectionMethod;
    private String recurringTransactionId;

    /*default no-args constructor for Dozer*/
    private AuthorizationResponse() {
    }

    /**
     * Default constructor. This uses Builder pattern.
     *
     * @param builder Builder to create an instance.
     */
    AuthorizationResponse(Builder builder) {
        collectionStatus = builder.collectionStatus;
        movements = Collections.unmodifiableList(builder.movements);
        amount = builder.amount;
        currency = builder.currency;
        redirectionParameters = builder.redirectionParameters;
        recurringTransactionId = builder.recurringTransactionId;
    }

    /**
     * Returns {@code true} if user interaction is needed.
     *
     * @return {@code true} if user interaction is needed, {@code false} otherwise.
     */
    public boolean needsRedirect() {
        return redirectionParameters != null;
    }

    /**
     * Returns {@link GatewayCollectionMethod} used in collection operation.
     *
     * @return {@link GatewayCollectionMethod} used in collection operation.
     */
    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

    /**
     * Sets {@link GatewayCollectionMethod} used in collection operation.
     *
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} used in collection operation.
     */
    public void setGatewayCollectionMethod(GatewayCollectionMethod gatewayCollectionMethod) {
        this.gatewayCollectionMethod = gatewayCollectionMethod;
    }


    /**
     * Returns full list with every movement done in payment gateway.
     *
     * @return Full list with every movement done in payment gateway.
     */
    public Collection<Movement> getMovements() {
        return movements;
    }

    /**
     * Returns {@link CollectionStatus#INIT} as this is a authorization.
     *
     * @return {@link CollectionStatus#INIT} as this is a authorization.
     */
    public CollectionStatus getCollectionStatus() {
        return this.collectionStatus;
    }

    /**
     * Returns numeric amount that has been collected.
     *
     * @return Numeric amount that has been collected.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Returns currency of ecommerce operation.
     *
     * @return currency of ecommerce operation.
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * Returns the RedirectionParameters containing the ulr to which the user will be redirected to initiate the interaction with the commerce
     *
     * @return the RedirectionParameters containing the ulr to which the user will be redirected to initiate the interaction with the commerce
     */
    public RedirectionParameters getRedirectionParameters() {
        return redirectionParameters;
    }

    /**
     * Indicates the recurring transaction reference for a recurring payment.
     *
     * @return recurring transaction reference if in a recurring payment.
     */
    public String getRecurringTransactionId() {
        return recurringTransactionId;
    }

    /**
     * Set the original transaction reference for a recurring payment.
     *
     * @param recurringTransactionId recurring transaction reference if in a recurring payment.
     */
    public void setRecurringTransactionId(String recurringTransactionId) {
        this.recurringTransactionId = recurringTransactionId;
    }

    /**
     * Returns additional information of this movement.
     *
     * @return Additional information of this movement.
     */

    /**
     * Builder class for {@link AuthorizationResponse}.
     */
    public static class Builder {

        private final List<Movement> movements = new ArrayList<Movement>();
        private CollectionStatus collectionStatus;
        private BigDecimal amount;
        private Currency currency;
        private RedirectionParameters redirectionParameters;
        private String recurringTransactionId;

        /**
         * Adds a {@link Movement} done with payment gateway to current movements list.
         *
         * @param val {@link Movement} done with payment gateway.
         * @return {@code this}.
         */
        public Builder addMovement(Movement val) {
            movements.add(val);
            collectionStatus = transform(val.getStatus());
            amount = val.getAmount();
            currency = val.getCurrency();
            return this;
        }

        /**
         * Sets {@link RedirectionParameters} to be used in user interaction.
         *
         * @param val {@link RedirectionParameters} to be used in user interaction.
         * @return {@code this}.
         */
        public Builder redirectionParameters(RedirectionParameters val) {
            redirectionParameters = val;
            return this;
        }

        /**
         * Sets {recurringTransactionId} to be used.
         *
         * @param val to be used.
         * @return {@code this}.
         */
        public Builder recurringTransactionId(String val) {
            recurringTransactionId = val;
            return this;
        }

        /**
         * Transforms {@link MovementStatus} to {@link CollectionStatus}.
         *
         * @param movementStatus to be transformed.
         * @return {@link CollectionStatus} resulted.
         */
        private CollectionStatus transform(MovementStatus movementStatus) {
            CollectionStatus result;
            switch (movementStatus) {
            case INIT_INTERACTION:
                result = CollectionStatus.ON_HOLD;
                break;
            case DECLINED:
                result = CollectionStatus.COLLECTION_DECLINED;
                break;
            case AUTHORIZED:
                result = CollectionStatus.AUTHORIZED;
                break;
            case PAYERROR:
                result = CollectionStatus.COLLECTION_ERROR;
                break;
            case REFUNDED:
                result = CollectionStatus.REFUNDED;
                break;
            case REFUNDERR:
                result = CollectionStatus.REFUND_ERROR;
                break;
            case REQUESTED:
                result = CollectionStatus.REQUESTED;
                break;
            default:
                throw new IllegalArgumentException(format("MovementStatus %s is not a valid status for authorize operation", movementStatus));
            }
            return result;
        }

        /**
         * Builds {@link AuthorizationResponse} instance for this builder.
         *
         * @return {@link AuthorizationResponse} instance for this builder.
         */
        public AuthorizationResponse build() {
            return new AuthorizationResponse(this);
        }
    }

}
