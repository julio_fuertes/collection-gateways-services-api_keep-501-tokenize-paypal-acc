package com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.unionpay;


import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;

public class UnionpayConfirmationDetails extends ConfirmationDetails {

    private static final long serialVersionUID = 1L;

    public UnionpayConfirmationDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId Identifier for confirmation operation.
     * @param money           {@link Money} to be confirmed.
     */
    public UnionpayConfirmationDetails(String merchantOrderId, Money money) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.UNIONPAY));
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId   Identifier for confirmation operation.
     * @param money             {@link Money} to be confirmed.
     * @param dynamicDescriptor Dynamic descriptor show in customer´s bank statement. (Restrictions: Dynamic descriptor only allow the character ".", whitespaces and alphanumeric characters except "ñ". Upper and lower cases are allowed.)
     */
    public UnionpayConfirmationDetails(String merchantOrderId, Money money, String dynamicDescriptor) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.UNIONPAY), dynamicDescriptor);
    }
}

