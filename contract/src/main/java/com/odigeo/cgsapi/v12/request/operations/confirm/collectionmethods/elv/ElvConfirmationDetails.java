package com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.elv;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;

/**
 * An object with needed information to perform a confirmation operation with a commerce using an ELV account.
 *
 * @see ConfirmationDetails
 */
public class ElvConfirmationDetails extends ConfirmationDetails {

    private static final long serialVersionUID = 1L;

    //no-args constructor for API REST
    public ElvConfirmationDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId of the collection transaction to confirm this transaction.
     * @param money           {@link Money money} to be refunded.
     */
    public ElvConfirmationDetails(String merchantOrderId, Money money) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ELV));
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId of the collection transaction to confirm this transaction.
     * @param money           {@link Money money} to be refunded.
     */
    public ElvConfirmationDetails(String merchantOrderId, Money money, String dynamicDescriptor) {
        super(merchantOrderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ELV), dynamicDescriptor);
    }
}
