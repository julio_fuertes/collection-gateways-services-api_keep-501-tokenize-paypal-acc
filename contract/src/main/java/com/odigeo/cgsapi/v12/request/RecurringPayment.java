package com.odigeo.cgsapi.v12.request;

/**
 * Recurring payments should implement this interface to handle the payment details.
 *
 * @author asegurab
 */
public interface RecurringPayment {
    RecurringDetails getRecurringDetails();
    void setRecurringDetails(RecurringDetails recurringDetails);
}
