package com.odigeo.cgsapi.v12.request.collectionmethods.creditcard;

import com.odigeo.cgsapi.v12.request.operations.authorize.collectionmethods.creditcard.CreditCardAuthorizationDetails;
import com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.creditcard.CreditCardCollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.secure3d.Secure3dCollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.creditcard.CreditCardRefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.secure3d.Secure3dRefundDetails;
import com.odigeo.cgsapi.v12.request.operations.verify.collectionmethods.secure3d.Secure3dVerificationDetails;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

/**
 * Credit Card container.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Secure3dRefundDetails.class, name = "Secure3dRefundDetails"),
        @JsonSubTypes.Type(value = Secure3dCollectionDetails.class, name = "Secure3dCollectionDetails"),
        @JsonSubTypes.Type(value = CreditCardAuthorizationDetails.class, name = "CreditCardAuthorizationDetails"),
        @JsonSubTypes.Type(value = CreditCardCollectionDetails.class, name = "CreditCardCollectionDetails"),
        @JsonSubTypes.Type(value = CreditCardRefundDetails.class, name = "CreditCardRefundDetails"),
        @JsonSubTypes.Type(value = Secure3dVerificationDetails.class, name = "Secure3dVerificationDetails")
    })
public interface CreditCardHolder {

    /**
     * Returns {@link CreditCard} to be used.
     *
     * @return {@link CreditCard} to be used.
     */
    CreditCard getCreditCard();
}
