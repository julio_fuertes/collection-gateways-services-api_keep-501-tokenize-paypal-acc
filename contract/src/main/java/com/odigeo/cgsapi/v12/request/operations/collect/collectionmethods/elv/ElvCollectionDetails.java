package com.odigeo.cgsapi.v12.request.operations.collect.collectionmethods.elv;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.collect.CollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.collectionmethods.elv.ElvConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.elv.ElvRefundDetails;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * An object with needed information to perform a collection operation with a commerce using an ELV account.
 *
 * @see CollectionDetails
 */
@JsonTypeName("ElvCollectionDetails")
public class ElvCollectionDetails extends CollectionDetails {
    private static final long serialVersionUID = 1L;
    private BankDetails bankDetails;
    private ElvUserDetails userDetails;

    //no-args constructor for API REST
    public ElvCollectionDetails() {
    }

    /**
     * Constructor.
     *
     * @param orderId                   Identifier for collection operation
     * @param money                     {@link Money} to be collected
     * @param collectionContext         {@link CollectionContext} to be used.
     * @param collectionShoppingDetails {@link CollectionShoppingDetails} to be used.
     */
    public ElvCollectionDetails(String orderId, Money money, BankDetails bankDetails, ElvUserDetails userDetails, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails) {
        super(orderId, money, GatewayCollectionMethod.valueOf(GatewayCollectionMethodType.ELV), collectionContext, collectionShoppingDetails);
        this.bankDetails = bankDetails;
        this.userDetails = userDetails;
    }

    /**
     * Returns {@link BankDetails}
     *
     * @return {@link BankDetails}
     */
    public BankDetails getBankDetails() {
        return bankDetails;
    }

    /**
     * Returns {@link ElvUserDetails}
     *
     * @return {@link ElvUserDetails}
     */
    public ElvUserDetails getUserDetails() {
        return userDetails;
    }

    /**
     * Transforms current {@code ElvCollectionDetails} instance into a {@link ElvRefundDetails} instance.
     *
     * @param transactionId Identifier for collection transaction that preceded this refund transaction.
     * @return {@link ElvRefundDetails} instance with same parameters as current
     * {@code ElvCollectionDetails} instance.
     */
    @Override
    public RefundDetails toRefundDetails(String transactionId, String merchantOrderId) {
        return new ElvRefundDetails(merchantOrderId, getMoney(), transactionId);
    }

    /**
     * Transforms current {@code ElvCollectionDetails} instance into a
     * {@link ElvConfirmationDetails} instance.
     *
     * @param merchantOrderId for collection transaction that preceded this refund transaction.
     * @return {@link ElvConfirmationDetails} instance with same parameters as current {@code ElvCollectionDetails} instance.
     */
    @Override
    public ConfirmationDetails toConfirmationDetails(String merchantOrderId) {
        return new ElvConfirmationDetails(merchantOrderId, getMoney());
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("bankDetails", bankDetails)
                .append("userDetails", userDetails)
                .toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        ElvCollectionDetails rhs = (ElvCollectionDetails) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.bankDetails, rhs.bankDetails)
                .append(this.userDetails, rhs.userDetails)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(bankDetails)
                .append(userDetails)
                .toHashCode();
    }
}
