package com.odigeo.cgsapi.v12.response.operations.verify;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Created by vrieraba on 26/05/2015.
 */
@JsonTypeName("Verification")
public final class Verification extends Movement {

    private static final long serialVersionUID = 1L;

    /*default no-args constructor for Dozer*/
    private Verification() {
    }

    /**
     * @param merchantOrderId      Merchant order id of this movement.
     * @param amount               Amount.
     * @param currency             Currency.
     * @param code                 Merchant error code.
     * @param errorDescription     Error description.
     * @param status               Movement status.
     * @param collectionActionType CollectionActionType used for this transaction
     */
    private Verification(String merchantOrderId, BigDecimal amount, Currency currency, String code, String errorDescription, MovementStatus status, CollectionActionType collectionActionType) {
        super(merchantOrderId, code, (collectionActionType == null || CollectionActionType.COLLECT.equals(collectionActionType)) ? MovementAction.COLLECT : MovementAction.AUTHORIZE);
        setAmount(amount);
        setCurrency(currency);
        setErrorDescription(errorDescription);
        setStatus(status);
    }


    /**
     * Factory method to create a Verification movement
     *
     * @param merchantOrderId      Merchant order id of this movement.
     * @param money                Amount and Currency.
     * @param code                 Merchant error code.
     * @param errorDescription     Error description.
     * @param status               Movement status.
     * @param collectionActionType CollectionActionType used for this transaction
     * @return A Verification.
     */
    public static Verification createVerification(String merchantOrderId, Money money, String code, String errorDescription, MovementStatus status, CollectionActionType collectionActionType) {
        return new Verification(merchantOrderId, money != null ? money.getAmount() : null, money != null ? money.getCurrency() : null, code, errorDescription, status, collectionActionType);
    }

    /**
     * Factory method to create a Failed Verification movement
     *
     * @param merchantOrderId      Merchant order id of this movement.
     * @param money                Amount and Currency.
     * @param code                 Merchant error code.
     * @param errorDescription     Error description.
     * @param collectionActionType CollectionActionType used for this transaction
     * @return A Verification.
     */
    public static Verification createFailedVerification(String merchantOrderId, Money money, String code, String errorDescription, CollectionActionType collectionActionType) {
        return new Verification(merchantOrderId, money != null ? money.getAmount() : null, money != null ? money.getCurrency() : null, code, errorDescription, MovementStatus.PAYERROR, collectionActionType);
    }

    /**
     * Factory method that a Fake Verification
     *
     * @param merchantOrderId      Merchant order id of this movement.
     * @param money                Amount and Currency.
     * @param status               Desired movement status
     * @param collectionActionType CollectionActionType used for this transaction
     * @return A Fake Authorization.
     */
    public static Verification createFakeVerification(String merchantOrderId, Money money, MovementStatus status, CollectionActionType collectionActionType) {
        return createVerification(merchantOrderId, money, FAKE_CODE, FAKE_ERROR_MESSAGE, status, collectionActionType);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Verification)) {
            return false;
        }
        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .appendSuper(super.hashCode())
                .toHashCode();
    }
}
