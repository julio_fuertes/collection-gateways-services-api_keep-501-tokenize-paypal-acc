package com.odigeo.cgsapi.v12.request.operations.query;


import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;

import java.io.Serializable;
import java.util.Currency;

/**
 * An object with needed information to perform a query operation with a commerce.
 */
public class QueryDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    private String orderId;
    private GatewayCollectionMethod gatewayCollectionMethod;
    private Currency currency;
    private String merchantOrderId;
    private CollectionActionType collectionActionType;

    //no-args constructor for API REST
    public QueryDetails() {
    }

    /**
     * Default constructor.
     *
     * @param orderId Identifier for collection operation.
     */
    public QueryDetails(String orderId, GatewayCollectionMethod gatewayCollectionMethod, Currency currency, String merchantOrderId, CollectionActionType collectionActionType) {
        this.orderId = orderId;
        this.gatewayCollectionMethod = gatewayCollectionMethod;
        this.currency = currency;
        this.merchantOrderId = merchantOrderId;
        this.collectionActionType = collectionActionType;
    }

    /**
     * Returns identifier for collection.
     *
     * @return Identifier for collection.
     */
    public String getOrderId() {
        return orderId;
    }

    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

    public Currency getCurrency() {
        return currency;
    }

    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    public CollectionActionType getCollectionActionType() {
        return collectionActionType;
    }
}
