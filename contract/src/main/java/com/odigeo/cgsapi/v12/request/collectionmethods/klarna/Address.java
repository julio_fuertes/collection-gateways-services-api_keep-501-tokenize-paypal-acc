package com.odigeo.cgsapi.v12.request.collectionmethods.klarna;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Required
     * The street name.
     */
    private final String street;
    /*
     * Required
     * The house number or name.
     */
    private final String houseNumberOrName;
    /*
     * Optional
     * The postal code with a maximum of 5 characters for USA and maximum of 10 characters for any other countryCode.
     */
    private String postalCode;
    /*
     * Required
     * The city name.
     */
    private final String city;
    /*
     * Required
     * A valid value is an ISO 2-character countryCode code.
     */
    private final String countryCode;

    /*
    Default no-args constructor for dozer conversion
    */
    private Address() {
        street = null;
        houseNumberOrName = null;
        city = null;
        countryCode = null;
    }


    public Address(String street, String houseNumberOrName, String city, String countryCode) {
        this.street = street;
        this.houseNumberOrName = houseNumberOrName;
        this.city = city;
        this.countryCode = countryCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getStreet() {
        return street;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getHouseNumberOrName() {
        return houseNumberOrName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Address rhs = (Address) obj;
        return new EqualsBuilder()
                .append(this.street, rhs.street)
                .append(this.postalCode, rhs.postalCode)
                .append(this.city, rhs.city)
                .append(this.countryCode, rhs.countryCode)
                .append(this.houseNumberOrName, rhs.houseNumberOrName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(street)
                .append(postalCode)
                .append(city)
                .append(countryCode)
                .append(houseNumberOrName)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("street", street)
                .append("postalCode", postalCode)
                .append("city", city)
                .append("countryCode", countryCode)
                .append("houseNumberOrName", houseNumberOrName)
                .toString();
    }
}

