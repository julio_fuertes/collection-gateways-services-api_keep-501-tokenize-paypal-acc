package com.odigeo.cgsapi.v12.request.operations.cancel;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;

import java.io.Serializable;

/**
 * An object with needed information to perform a cancel operation with a commerce.
 */
public class CancelDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    private String merchantOrderId;
    private GatewayCollectionMethod gatewayCollectionMethod;
    private Money money;

    //no-args constructor for API REST
    public CancelDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId         Identifier for collection operation.
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} to be used.
     */
    public CancelDetails(String merchantOrderId, GatewayCollectionMethod gatewayCollectionMethod) {
        this(merchantOrderId, gatewayCollectionMethod, null);
    }

    /**
     * Constructor used into the refund operation. It is needed due to the necessity of transform a refundDetails into a cancelDetails object in case
     * of Ogone would be our PSP.
     *
     * @param merchantOrderId         Identifier for collection operation.
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} to be used.
     * @param gatewayCollectionMethod {@link Money} to be used.
     */
    public CancelDetails(String merchantOrderId, GatewayCollectionMethod gatewayCollectionMethod, Money money) {
        this.merchantOrderId = merchantOrderId;
        this.gatewayCollectionMethod = gatewayCollectionMethod;
        this.money = money;
    }

    /**
     * Returns identifier for collection.
     *
     * @return Identifier for collection.
     */
    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    /**
     * Returns {@link GatewayCollectionMethod} used in collection.
     *
     * @return {@link GatewayCollectionMethod} used in collection.
     */
    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

    public Money getMoney() {
        return money;
    }
}
