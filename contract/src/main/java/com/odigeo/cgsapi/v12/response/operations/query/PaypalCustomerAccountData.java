package com.odigeo.cgsapi.v12.response.operations.query;

import com.odigeo.cgsapi.v12.response.operations.verify.collectionmethods.paypal.PaypalAccountStatus;

import java.io.Serializable;

/**
 * Created by ADEFREIT on 20/07/2015.
 */
public final class PaypalCustomerAccountData implements Serializable {

    private static final long serialVersionUID = 1L;

    private final CustomerData customerData;
    private final String payerId;
    private final PaypalAccountStatus payerStatus;
    private final String addressStatus;

    /**
     * Constructor without entry parameters
     */
    private PaypalCustomerAccountData() {
        customerData = null;
        payerId = null;
        payerStatus = null;
        addressStatus = null;
    }

    public PaypalCustomerAccountData(Builder builder) {
        customerData = builder.customerData;
        payerId = builder.payerId;
        payerStatus = builder.payerStatus;
        addressStatus = builder.addressStatus;
    }

    public CustomerData getCustomerData() {
        return customerData;
    }

    public String getPayerId() {
        return payerId;
    }

    public PaypalAccountStatus getPayerStatus() {
        return payerStatus;
    }

    public String getAddressStatus() {
        return addressStatus;
    }

    public static class Builder {
        private CustomerData customerData;
        private String payerId;
        private PaypalAccountStatus payerStatus;
        private String addressStatus;


        public Builder customerData(CustomerData customerData) {
            this.customerData = customerData;
            return this;
        }

        public Builder payerId(String payerId) {
            this.payerId = payerId;
            return this;
        }

        public Builder payerStatus(PaypalAccountStatus payerStatus) {
            this.payerStatus = payerStatus;
            return this;
        }

        public Builder addressStatus(String addressStatus) {
            this.addressStatus = addressStatus;
            return this;
        }

        public PaypalCustomerAccountData build() {
            return new PaypalCustomerAccountData(this);
        }
    }

}
