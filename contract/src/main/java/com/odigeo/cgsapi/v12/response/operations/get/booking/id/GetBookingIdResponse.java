package com.odigeo.cgsapi.v12.response.operations.get.booking.id;

import java.io.Serializable;

public class GetBookingIdResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String bookingId;

    private GetBookingIdResponse() {
        bookingId = null;
    }

    GetBookingIdResponse(GetBookingIdResponseBuilder builder) {
        bookingId = builder.getBookingId();
    }

    public String getBookingId() {
        return bookingId;
    }
}
