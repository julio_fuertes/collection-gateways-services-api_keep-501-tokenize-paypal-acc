package com.odigeo.cgsapi.v12.response;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionStatus;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Currency;

/**
 * An object that contains a {@link Collection} of {@link Movement} done with a payment gateway in an ecommerce operation.
 * Commerce must be implemented by any class whose instances contain a {@link Collection} of {@link Movement movements}.
 * Furthermore, a {@code MovementContainer} will provide information about {@link GatewayCollectionMethod}, {@link CollectionStatus}
 * or {@link com.odigeo.cgsapi.v12.request.Money} of ecommerce operation.
 */
public interface MovementContainer extends Serializable {

    /**
     * Sets {@link GatewayCollectionMethod} used in ecommerce operation.
     *
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} used in ecommerce operation.
     */
    void setGatewayCollectionMethod(GatewayCollectionMethod gatewayCollectionMethod);

    /**
     * Returns {@link GatewayCollectionMethod} used in ecommerce operation.
     *
     * @return {@link GatewayCollectionMethod} used in ecommerce operation.
     */
    GatewayCollectionMethod getGatewayCollectionMethod();

    /**
     * Returns full list with every movement done in payment gateway.
     *
     * @return Full list with every movement done in payment gateway.
     */
    Collection<Movement> getMovements();

    /**
     * Returns status of ecommerce operation.
     *
     * @return Status of ecommerce operation.
     */
    CollectionStatus getCollectionStatus();

    /**
     * Returns numeric amount of ecommerce operation.
     *
     * @return Numeric amount of ecommerce operation.
     */
    BigDecimal getAmount();

    /**
     * Returns currency of ecommerce operation.
     *
     * @return currency of ecommerce operation.
     */
    Currency getCurrency();
}
