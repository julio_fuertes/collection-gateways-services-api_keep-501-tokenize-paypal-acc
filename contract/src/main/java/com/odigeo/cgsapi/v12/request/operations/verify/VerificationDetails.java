package com.odigeo.cgsapi.v12.request.operations.verify;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.verify.collectionmethods.secure3d.Secure3dVerificationDetails;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

import java.io.Serializable;
import java.util.Map;

import static com.edreams.util.Checks.checkNotNull;

/**
 * An object with needed information to perform a confirm operation with a commerce.
 * Defines common information for confirm operations.
 * Implementing classes can provide extra information, such as credit card, user language, etc.
 */

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Secure3dVerificationDetails.class, name = "Secure3dVerificationDetails")
    })
public class VerificationDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    private String merchantOrderId;
    private Money money;
    private GatewayCollectionMethod gatewayCollectionMethod;
    private Map<String, String> parameters;
    private CollectionActionType collectionActionType;
    private CollectionContext collectionContext;
    private Integer interactionStep;
    private String finishUrl;

    //no-args constructor for API REST
    protected VerificationDetails() {
    }

    /**
     *
     * @param merchantOrderId         Identifier for verify operation.
     * @param money                   {@link Money} to be verified.
     * @param gatewayCollectionMethod {@link GatewayCollectionMethod} used for verify.
     * @param parameters
     * @param collectionActionType    {@link CollectionActionType} used for this transaction
     * @param collectionContext       {@link CollectionContext} of this transaction
     * @param interactionStep
     * @param finishUrl
     */
    public VerificationDetails(String merchantOrderId, Money money, GatewayCollectionMethod gatewayCollectionMethod, Map<String, String> parameters, CollectionActionType collectionActionType, CollectionContext collectionContext, Integer interactionStep, String finishUrl) {
        this.merchantOrderId = merchantOrderId;
        this.money = money;
        this.gatewayCollectionMethod = gatewayCollectionMethod;
        this.parameters = parameters;
        checkNotNull(this.parameters, "parameters");
        this.collectionActionType = collectionActionType;
        this.collectionContext = collectionContext;
        this.interactionStep = interactionStep;
        this.finishUrl = finishUrl;
    }

    /**
     * Returns identifier for this confirmation.
     *
     * @return Identifier for this confirmation.
     */
    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    /**
     * Returns {@link Money} used in collection/authorization.
     *
     * @return {@link Money} used in collection/authorization.
     */
    public Money getMoney() {
        return money;
    }

    /**
     * Returns {@link GatewayCollectionMethod} to be used.
     *
     * @return {@link GatewayCollectionMethod} to be used.
     */
    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }

    /**
     * Returns parameter value for given name.
     *
     * @param name Name of the parameter
     * @return Parameter value for given name
     */
    public String getParameterValue(String name) {
        return parameters.get(name);
    }

    /**
     * Returns {@link Map} with parameters to be used to verify this collection.
     *
     * @return {@link Map} with parameters to be used to verify this collection.
     */
    public Map<String, String> getParameters() {
        return parameters;
    }

    /**
     * Returns {@link CollectionActionType} used for this transaction.
     *
     * @return {@link CollectionActionType} used for this transaction.
     */
    public CollectionActionType getCollectionActionType() {
        return collectionActionType;
    }

    /**
     * Returns {@link CollectionContext} to be used.
     *
     * @return {@link CollectionContext} to be used.
     */
    public CollectionContext getCollectionContext() {
        return collectionContext;
    }

    /**
     * Returns the number/step of this interaction.
     *
     * @return Number/step of this interaction.
     */
    public Integer getInteractionStep() {
        return interactionStep;
    }

    /**
     * Returns finish URL.
     *
     * @return finish URL.
     */
    public String getFinishUrl() {
        return finishUrl;
    }
}

