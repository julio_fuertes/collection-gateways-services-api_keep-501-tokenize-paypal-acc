package com.odigeo.cgsapi.v12.request.operations.refund.collectionmethods.creditcard;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCard;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardGatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardHolder;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * An object with needed information to perform a refund operation with a commerce using a {@link CreditCard}.
 *
 * @see RefundDetails
 */
@JsonTypeName("CreditCardRefundDetails")
public class CreditCardRefundDetails extends RefundDetails implements CreditCardHolder {

    private static final long serialVersionUID = 1L;
    private CreditCard creditCard;

    //no-args constructor for API REST
    public CreditCardRefundDetails() {
    }

    /**
     * Default constructor.
     *
     * @param merchantOrderId Identifier of the collection transaction to refund this transaction.
     * @param money           {@link Money} to be refunded.
     * @param creditCard      {@link CreditCard} to be used.
     * @param transactionId   Identifier for collection transaction that preceded this refund transaction.
     */
    public CreditCardRefundDetails(String merchantOrderId, Money money, CreditCard creditCard, String transactionId) {
        super(merchantOrderId, money, new CreditCardGatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD, creditCard.getType()), transactionId);
        this.creditCard = creditCard;
    }

    /**
     * Returns {@link CreditCard credit card} to be used.
     *
     * @return {@link CreditCard Credit card} to be used.
     */
    public CreditCard getCreditCard() {
        return creditCard;
    }
}
