package com.odigeo.cgsapi.v12.request.collectionmethods.klarna;

import com.odigeo.cgsapi.v12.request.Gender;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

public class KlarnaAccountHolder implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Required
     * A person´s gender.
     */
    private final Gender gender;
    /*
     * Required
     * A person's first name.
     */
    private final String firstName;
    /*
     * Required
     * A person's last name.
     */
    private final String lastName;
    /**
     * Required
     * Date of birth in format YYYY-MM-DD (ISO 8601)
     */
    private final String birthDate;
    /*
     * Required
     *  This object contains address details
     */
    private final Address address;
    /*
     * Required
     * The telephone number of the shopper.
     */
    private final String telephoneNumber;
    /**
     * Required
     * Email of the shopper
     */
    private final String email;
    /**
     * Optional (Required just for Sweden, Finland, Norway, and Denmark)
     * Provide either the last four digits, or the full social security number.
     */
    private String socialSecurityNumber;

    /*
    Default no-args constructor for dozer conversion
     */
    private KlarnaAccountHolder() {
        gender = null;
        firstName = null;
        lastName = null;
        address = null;
        telephoneNumber = null;
        birthDate = null;
        email = null;
    }

    public KlarnaAccountHolder(Gender gender, String firstName, String lastName, String birthDate, Address address, String telephoneNumber, String email) {
        this.gender = gender;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.address = address;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public Address getAddress() {
        return address;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public String getEmail() {
        return email;
    }


    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        KlarnaAccountHolder rhs = (KlarnaAccountHolder) obj;
        return new EqualsBuilder()
                .append(this.gender, rhs.gender)
                .append(this.firstName, rhs.firstName)
                .append(this.lastName, rhs.lastName)
                .append(this.birthDate, rhs.birthDate)
                .append(this.address, rhs.address)
                .append(this.telephoneNumber, rhs.telephoneNumber)
                .append(this.email, rhs.email)
                .append(this.socialSecurityNumber, rhs.socialSecurityNumber)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(gender != null ? gender.name() : null)
                .append(firstName)
                .append(lastName)
                .append(birthDate)
                .append(address)
                .append(telephoneNumber)
                .append(email)
                .append(socialSecurityNumber)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("gender", gender)
                .append("firstName", firstName)
                .append("lastName", lastName)
                .append("birthDate", birthDate)
                .append("address", address)
                .append("telephoneNumber", telephoneNumber)
                .append("email", email)
                .append("socialSecurityNumber", socialSecurityNumber)
                .toString();
    }
}
