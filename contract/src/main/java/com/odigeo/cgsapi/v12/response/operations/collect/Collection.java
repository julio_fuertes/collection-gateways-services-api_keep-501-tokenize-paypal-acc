package com.odigeo.cgsapi.v12.response.operations.collect;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import com.odigeo.cgsapi.v12.response.RedirectionParameters;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Created by vrieraba on 26/05/2015.
 */
@JsonTypeName("Collection")
public final class Collection extends Movement {

    private static final long serialVersionUID = 1L;

    private RedirectionParameters redirectionParameters;

    /*default no-args constructor for Dozer*/
    private Collection() {
    }

    /**
     * @param merchantOrderId  Merchant order id of this movement.
     * @param amount           Amount.
     * @param currency         Currency.
     * @param code             Merchant error code.
     * @param errorDescription Error description.
     * @param status           Movement status.
     */
    private Collection(String merchantOrderId, BigDecimal amount, Currency currency, String code, String errorDescription, MovementStatus status) {
        super(merchantOrderId, code, MovementAction.COLLECT);
        setAmount(amount);
        setCurrency(currency);
        setErrorDescription(errorDescription);
        setStatus(status);
    }

    /**
     * Sets the redirectionParameters
     * Private method that can only be used in the scope of this class
     */
    private void setRedirectionParameters(RedirectionParameters redirectionParameters) {
        this.redirectionParameters = redirectionParameters;
    }

    /**
     * Returns the redirectionParemeters containing the url to which the user has to be redirected to initiate the interaction with the commerce.
     *
     * @return the redirectionParemeters containing the url to which the user has to be redirected to initiate the interaction with the commerce.
     */
    public RedirectionParameters getRedirectionParameters() {
        return redirectionParameters;
    }

    /**
     * Factory method to create a Collection movement
     *
     * @param merchantOrderId  Merchant order id of this movement.
     * @param money            Amount and Currency.
     * @param code             Merchant error code.
     * @param errorDescription Error description.
     * @return An Authorization.
     */
    public static Collection createCollection(String merchantOrderId, Money money, String code, String errorDescription, MovementStatus status) {
        return new Collection(merchantOrderId, money != null ? money.getAmount() : null, money != null ? money.getCurrency() : null, code, errorDescription, status);
    }

    /**
     * Factory method that creates a Collection movement with needed interaction
     *
     * @param merchantOrderId  Merchant order id of this movement.
     * @param money            Amount and Currency.
     * @param code             Merchant error code.
     * @param errorDescription Error description.
     * @return An Authorization with needed interaction.
     */
    public static Collection createInteractionCollection(String merchantOrderId, Money money, String code, String errorDescription, RedirectionParameters redirectionParameters, MovementStatus status) {
        Collection collection = new Collection(merchantOrderId, money != null ? money.getAmount() : null, money != null ? money.getCurrency() : null, code, errorDescription, status);
        collection.setRedirectionParameters(redirectionParameters);
        return collection;
    }

    /**
     * Factory method that creates a Failed Collection
     *
     * @param merchantOrderId  Merchant order id of this movement.
     * @param money            Amount and Currency.
     * @param code             Merchant error code.
     * @param errorDescription Error description.
     * @return A Failed Authorization.
     */
    public static Collection createFailedCollection(String merchantOrderId, Money money, String code, String errorDescription) {
        return new Collection(merchantOrderId, money != null ? money.getAmount() : null, money != null ? money.getCurrency() : null, code, errorDescription, MovementStatus.PAYERROR);
    }

    /**
     * Factory method that a Fake Collection
     *
     * @param merchantOrderId Merchant order id of this movement.
     * @param money           Amount and Currency.
     * @param status          Desired movement status
     * @return A Fake Authorization.
     */
    public static Collection createFakeCollection(String merchantOrderId, Money money, MovementStatus status) {
        return createCollection(merchantOrderId, money, FAKE_CODE, FAKE_ERROR_MESSAGE, status);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Collection)) {
            return false;
        }
        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .appendSuper(super.hashCode())
                .toHashCode();
    }
}
