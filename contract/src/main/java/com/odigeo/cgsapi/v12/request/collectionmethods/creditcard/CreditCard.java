package com.odigeo.cgsapi.v12.request.collectionmethods.creditcard;

import com.edreams.util.Checks;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * An object with information about credit card.
 */
public class CreditCard implements Serializable {
    private static final long serialVersionUID = 1L;
    private String owner;
    private CreditCardType type;
    private String number;
    private String verificationValue;
    private String expirationMonth;
    private String expirationYear;
    private String expirationDate;

    //no-args constructor for API REST
    public CreditCard() {
    }

    /**
     * Default constructor.
     *
     * @param owner             Credit card owner.
     * @param type              Credit card code.
     * @param number            Credit card number.
     * @param verificationValue Credit card verification value.
     * @param expirationMonth   Credit card expiration month.
     * @param expirationYear    Credit card expiration year.
     */
    public CreditCard(String owner, CreditCardType type, String number,
                      String verificationValue, String expirationMonth,
                      String expirationYear) {
        this.owner = owner;
        this.type = type;
        this.number = number;
        this.verificationValue = verificationValue;
        this.expirationMonth = expirationMonth;
        this.expirationYear = expirationYear;
        this.expirationDate = expirationMonth + expirationYear;
        validate();
    }

    /**
     * Validates mandatory data for credit card.
     */
    private void validate() {
        Checks.checkNotNull(owner, "owner");
        Checks.checkNotNull(type, "type");
        Checks.checkNotNull(number, "number");
        Checks.checkNotNull(expirationMonth, "expirationMonth");
        Checks.checkNotNull(expirationYear, "expirationYear");
    }

    /**
     * Returns credit card owner.
     *
     * @return Credit card owner.
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Returns {@link CreditCardType}.
     *
     * @return {@link CreditCardType}.
     */
    public CreditCardType getType() {
        return type;
    }

    /**
     * Returns credit card number.
     *
     * @return Credit card number.
     */
    public String getNumber() {
        return number;
    }

    /**
     * Returns credit card verification value.
     *
     * @return Credit card verification value.
     */
    public String getVerificationValue() {
        return verificationValue;
    }

    /**
     * Returns ordinal of expiration month as a {@link String}. January is considered to be month "01" and December as "12".
     *
     * @return Ordinal of expiration month as a {@link String}.
     */
    public String getExpirationMonth() {
        return expirationMonth;
    }

    /**
     * Returns last two digits of expiration date as a {@link String}.
     *
     * @return Last two digits of expiration date as a {@link String}.
     */
    public String getExpirationYear() {
        return expirationYear;
    }

    /**
     * Returns expiration date as a {@link String}.
     *
     * @return Expiration date as a {@link String}.
     */
    public String getExpirationDate() {
        return expirationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CreditCard that = (CreditCard) o;

        return new EqualsBuilder()
                .append(number, that.number)
                .append(expirationMonth, that.expirationMonth)
                .append(expirationYear, that.expirationYear)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(number)
                .append(expirationMonth)
                .append(expirationYear)
                .toHashCode();
    }
}
