package com.odigeo.cgsapi.v12.response.operations.binlookup;

import com.odigeo.cgsapi.v12.request.collectionmethods.creditcard.CreditCardType;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * @author alain.munoz
 * @since 17/01/14
 */
@JsonTypeName("BinLookUp")
public class BinLookUp extends Movement {

    private static final long serialVersionUID = 1L;

    private String countryCode;
    private Integer numCountryCode;
    private CreditCardType creditCardType;

    //no-args constructor for API REST
    public BinLookUp() {
    }

    /**
     * @param code             Error code.
     * @param errorDescription Error description.
     * @param countryCode      2 letters ISO 3166-1 code.
     * @param numCountryCode   Numeric ISO 3166-1 code.
     * @param creditCardType   Credit card type.
     */
    public BinLookUp(String code, String errorDescription, String countryCode, Integer numCountryCode, CreditCardType creditCardType) {
        super(null, code, MovementAction.BIN_LOOK_UP);
        this.countryCode = countryCode;
        this.numCountryCode = numCountryCode;
        this.creditCardType = creditCardType;
        setErrorDescription(errorDescription);
        setStatus(MovementStatus.DECLINED);
    }

    /**
     * @param code             Error code.
     * @param errorDescription Error description.
     * @param countryCode      2 letters ISO 3166-1 code.
     * @param numCountryCode   Numeric ISO 3166-1 code.
     */
    public BinLookUp(String code, String errorDescription, String countryCode, Integer numCountryCode) {
        this(code, errorDescription, countryCode, numCountryCode, null);
    }

    /**
     * @param code         Error code.
     * @param errorMessage Error description.
     */
    BinLookUp(String code, String errorMessage) {
        this(code, errorMessage, null, null);
    }

    /**
     * Returns 2 letters ISO 3166-1 code for credit card country, if a credit card was used for BinLookUp.
     * Full list of numeric codes can be found here:
     * <a href='http://unstats.un.org/unsd/methods/m49/m49alpha.htm'>http://unstats.un.org/unsd/methods/m49/m49alpha.htm</a>
     *
     * @return 2 letters ISO 3166-1 code for credit card country, if a credit card was used for BinLookUp.
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Returns numeric ISO 3166-1 code for credit card country, if a credit card was used for BinLookUp.
     * Full list of numeric codes can be found here:
     * <a href='http://unstats.un.org/unsd/methods/m49/m49alpha.htm'>http://unstats.un.org/unsd/methods/m49/m49alpha.htm</a>
     *
     * @return Numeric ISO 3166-1 code for credit card country, if a credit card was used for BinLookUp.
     */
    public Integer getNumCountryCode() {
        return numCountryCode;
    }

    /**
     * Returns a credit card type code given by the commerce in the BinLookUp movement.
     *
     * @return a credit card type code given by the commerce in the BinLookUp movement. Codes are specific from its commerce.
     */
    public CreditCardType getCreditCardType() {
        return creditCardType;
    }

    /**
     * Creates a fake BinLookUp movement.
     *
     * @return A fake BinLookUp.
     */
    public static BinLookUp fakeBinLookUp() {
        return new BinLookUp(FAKE_CODE, FAKE_ERROR_MESSAGE);
    }

    /**
     * Creates a failed BinLookUp movement.
     *
     * @param code         Error code.
     * @param errorMessage Error message.
     * @return A failed BinLookUp.
     */
    public static BinLookUp createFailedBinLookUp(String code, String errorMessage) {
        return new BinLookUp(code, errorMessage);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BinLookUp)) {
            return false;
        }

        BinLookUp that = (BinLookUp) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(creditCardType, that.creditCardType)
                .append(countryCode, that.countryCode)
                .append(numCountryCode, that.numCountryCode)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .appendSuper(super.hashCode())
                .append(creditCardType)
                .append(countryCode)
                .append(numCountryCode)
                .toHashCode();
    }
}
