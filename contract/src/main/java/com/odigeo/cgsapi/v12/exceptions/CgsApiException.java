package com.odigeo.cgsapi.v12.exceptions;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class CgsApiException extends WebApplicationException {

    private final String message;
    private final Throwable cause;

    public CgsApiException(String message) {
        this(message, null);
    }

    public CgsApiException(String message, Throwable cause) {
        super(Response.status(Response.Status.INTERNAL_SERVER_ERROR).
                entity(new Bean(message + (cause != null && cause.getStackTrace().length > 0 ? " \n\tTrace: " + cause.getStackTrace()[0] : ""))).type(MediaType.APPLICATION_JSON).build());
        this.message = message;
        this.cause = cause;
    }

    public CgsApiException(String message, Throwable cause, Response.Status status) {
        super(Response.status(status)
                .entity(new Bean(message + (cause != null && cause.getStackTrace().length > 0 ? " \n\tTrace: " + cause.getStackTrace()[0] : ""))).type(MediaType.APPLICATION_JSON).build());
        this.cause = cause;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getCause() {
        return cause;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Bean {
        private String type;
        private String message;

        Bean(String message) {
            this.message = message;
            this.type = CgsApiException.class.getSimpleName();
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
