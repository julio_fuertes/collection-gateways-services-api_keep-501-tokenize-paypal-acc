package com.odigeo.cgsapi.v12.request.operations.authorize;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.operations.authorize.collectionmethods.creditcard.CreditCardAuthorizationDetails;
import com.odigeo.cgsapi.v12.request.operations.authorize.collectionmethods.klarna.KlarnaAuthorizationDetails;
import com.odigeo.cgsapi.v12.request.operations.authorize.collectionmethods.paypal.PaypalAuthorizationDetails;
import com.odigeo.cgsapi.v12.request.operations.authorize.collectionmethods.secure3d.Secure3dAuthorizationDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

import java.io.Serializable;

/**
 * An object with needed information to perform a authorization operation with a commerce. Used to authorize a Credit Card collection.
 * Defines common information for authorization operations.
 * Implementing classes can provide extra information, such as credit card, user language, etc.
 */

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = CreditCardAuthorizationDetails.class, name = "CreditCardAuthorizationDetails"),
        @JsonSubTypes.Type(value = Secure3dAuthorizationDetails.class, name = "Secure3dAuthorizationDetails"),
        @JsonSubTypes.Type(value = PaypalAuthorizationDetails.class, name = "PaypalAuthorizationDetails"),
        @JsonSubTypes.Type(value = GenericInteractionAuthorizationDetails.class, name = "GenericInteractionAuthorizationDetails"),
        @JsonSubTypes.Type(value = KlarnaAuthorizationDetails.class, name = "KlarnaAuthorizationDetails")
    })
public class AuthorizationDetails implements Serializable {
    private static final long serialVersionUID = 1L;

    private String orderId;
    private Money money;
    private GatewayCollectionMethod gatewayCollectionMethod;
    private CollectionContext collectionContext;
    private CollectionShoppingDetails collectionShoppingDetails;

    /*
    Default no-args constructor for dozer conversion
     */
    protected AuthorizationDetails() {
    }

    /**
     * Default constructor. Used for doze. If you want create no-param constructor, please implements
     * setters methods.
     *
     * @param orderId                   Identifier for authorization.
     * @param money                     {@link Money} to be authorized.
     * @param gatewayCollectionMethod   {@link GatewayCollectionMethod} to be used.
     * @param collectionContext         {@link CollectionContext} to be used.
     * @param collectionShoppingDetails {@link CollectionShoppingDetails} to be used.
     */
    public AuthorizationDetails(String orderId, Money money, GatewayCollectionMethod gatewayCollectionMethod, CollectionContext collectionContext, CollectionShoppingDetails collectionShoppingDetails) {
        this.orderId = orderId;
        this.money = money;
        this.gatewayCollectionMethod = gatewayCollectionMethod;
        this.collectionContext = collectionContext;
        this.collectionShoppingDetails = collectionShoppingDetails;
    }

    /**
     * Returns identifier for authorization.
     *
     * @return Identifier for authorization.
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Returns {@link Money} used in collection.
     *
     * @return {@link Money} used in collection.
     */
    public Money getMoney() {
        return money;
    }

    /**
     * Returns {@link GatewayCollectionMethod} to be used.
     *
     * @return {@link GatewayCollectionMethod} to be used.
     */
    public GatewayCollectionMethod getGatewayCollectionMethod() {
        return gatewayCollectionMethod;
    }


    /**
     * Returns {@link CollectionContext} to be used.
     *
     * @return {@link CollectionContext} to be used.
     */
    public CollectionContext getCollectionContext() {
        return collectionContext;
    }


    /**
     * Returns {@link CollectionShoppingDetails} to be used.
     *
     * @return {@link CollectionShoppingDetails} to be used.
     */
    public CollectionShoppingDetails getCollectionShoppingDetails() {
        return collectionShoppingDetails;
    }

}
