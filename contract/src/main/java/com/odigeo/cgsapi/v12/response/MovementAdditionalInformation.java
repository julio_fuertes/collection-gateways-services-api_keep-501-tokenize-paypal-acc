package com.odigeo.cgsapi.v12.response;

import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.annotate.JsonTypeName;

import java.io.Serializable;

/**
 * @author javier.garcia
 * @since 25/07/13
 */

/**
 * An object with information about a payment gateway movement.
 */
@JsonTypeName("MovementAdditionalInformation")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BankTransferMovementAdditionalInformation.class, name = "BankTransferMovementAdditionalInformation")
    })
public interface MovementAdditionalInformation extends Serializable {

}
