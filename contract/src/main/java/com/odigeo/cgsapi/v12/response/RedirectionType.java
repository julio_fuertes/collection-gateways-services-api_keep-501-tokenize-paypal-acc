package com.odigeo.cgsapi.v12.response;

public enum  RedirectionType {
    HTML,
    JAVASCRIPT,
    FORM_REDIRECTION,
    APPLE_PAY,
    GOOGLE_PAY
}
