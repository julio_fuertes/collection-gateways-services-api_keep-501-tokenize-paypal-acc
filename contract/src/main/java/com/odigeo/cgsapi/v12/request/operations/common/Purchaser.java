package com.odigeo.cgsapi.v12.request.operations.common;

import com.edreams.util.Checks;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * Contains information about the purchaser
 */
public final class Purchaser implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private String lastNames;
    private String address;
    private String zipCode;
    private String city;
    private String countryCode;
    private String email;
    private String telephoneNumber;

    //no-args constructor for API REST
    public Purchaser() {
    }

    /**
     * private constructor.
     *
     * @param name        Purchaser name.
     * @param lastNames   Purchaser last names.
     * @param address     Purchaser address
     * @param zipCode     Purchaser zip code.
     * @param city        Purchaser city name.
     * @param countryCode Purchaser country code.
     * @param email       Purchaser email address.
     */
    private Purchaser(String name, String lastNames, String address, String zipCode, String city, String countryCode, String email, String telephoneNumber) {
        this.name = name;
        this.lastNames = lastNames;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.countryCode = countryCode;
        this.email = email;
        this.telephoneNumber = telephoneNumber;
    }

    /**
     * Static factory method. Creates a new purchaser used for order purposes.
     *
     * @param name        Purchaser name.
     * @param lastNames   Purchaser last names.
     * @param address     Purchaser address
     * @param zipCode     Purchaser zip code.
     * @param city        Purchaser city name.
     * @param countryCode Purchaser country code.
     * @param email       Purchaser email address.
     * @return A purchaser used for order.
     */
    public static Purchaser createOrderPurchaser(String name, String lastNames, String address, String zipCode, String city, String countryCode, String email, String telephoneNumber) {
        Purchaser purchaser = new Purchaser(name, lastNames, address, zipCode, city, countryCode, email, telephoneNumber);
        purchaser.validateOrderPurchaser();
        return purchaser;
    }

    /**
     * Static factory method. Creates a new purchaser used for invoicing purposes. Data used for professional invoicing.
     *
     * @param lastNames   Purchaser last names.
     * @param address     Purchaser address
     * @param zipCode     Purchaser zip code.
     * @param city        Purchaser city name.
     * @param countryCode Purchaser country code.
     * @return A purchaser used for invoicing.
     */
    public static Purchaser createInvoicingPurchaser(String lastNames, String address, String zipCode, String city, String countryCode) {
        Purchaser purchaser = new Purchaser(null, lastNames, address, zipCode, city, countryCode, null, null);
        purchaser.validateInvoicingPurchaser();
        return purchaser;
    }

    /**
     * Validates mandatory data for order purchaser.
     */
    private void validateOrderPurchaser() {
        Checks.checkNotNull(name, "name");
        Checks.checkNotNull(lastNames, "lastNames");
        Checks.checkNotNull(email, "email");
        Checks.checkMail(email);
        Checks.checkNotNull(countryCode, "countryCode");
    }

    /**
     * Validates mandatory data for invoicing purchaser.
     */
    private void validateInvoicingPurchaser() {
        Checks.checkNotNull(lastNames, "lastNames");
        Checks.checkNotNull(address, "address");
        Checks.checkNotNull(city, "city");
        Checks.checkNotNull(countryCode, "countryCode");
    }

    /**
     * Returns purchaser name.
     *
     * @return Purchaser name.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns purchaser last names.
     *
     * @return Purchaser last names.
     */
    public String getLastNames() {
        return lastNames;
    }

    /**
     * Returns purchaser address.
     *
     * @return Purchaser address.
     */
    public String getAddress() {
        return address;
    }

    /**
     * Returns purchaser zip code.
     *
     * @return Purchaser zip code.
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * Returns purchaser city.
     *
     * @return Purchaser city.
     */
    public String getCity() {
        return city;
    }

    /**
     * Returns purchaser country code.
     *
     * @return Purchaser country code.
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Returns purchaser email address.
     *
     * @return Purchaser email address.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Returns Purchaser telephoneNumber.
     *
     * @return Purchaser telephoneNumber.
     */
    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Purchaser)) {
            return false;
        }

        Purchaser that = (Purchaser) o;

        return new EqualsBuilder()
                .append(name, that.name)
                .append(lastNames, that.lastNames)
                .append(address, that.address)
                .append(zipCode, that.zipCode)
                .append(city, that.city)
                .append(countryCode, that.countryCode)
                .append(email, that.email)
                .append(telephoneNumber, that.telephoneNumber)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .append(name)
                .append(lastNames)
                .append(address)
                .append(zipCode)
                .append(city)
                .append(countryCode)
                .append(email)
                .append(telephoneNumber)
                .toHashCode();
    }
}
