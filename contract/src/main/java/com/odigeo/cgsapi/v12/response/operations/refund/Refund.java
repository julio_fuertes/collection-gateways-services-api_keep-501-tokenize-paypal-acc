package com.odigeo.cgsapi.v12.response.operations.refund;

import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementAction;
import com.odigeo.cgsapi.v12.response.MovementStatus;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonTypeName;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * An object with information about a payment gateway refund.
 */
@JsonTypeName("Refund")
public final class Refund extends Movement {

    private static final long serialVersionUID = 1L;

    //no-args constructor for API REST
    private Refund() {
    }

    /**
     * @param merchantOrderId  Merchant order id of this movement.
     * @param amount           Amount.
     * @param currency         Currency.
     * @param code             Merchant error code.
     * @param errorDescription Error description.
     * @param status           Movement status.
     */
    private Refund(String merchantOrderId, BigDecimal amount, Currency currency, String code, String errorDescription, MovementStatus status) {
        super(merchantOrderId, code, MovementAction.COLLECT);
        setAmount(amount);
        setCurrency(currency);
        setErrorDescription(errorDescription);
        setStatus(status);
    }

    /**
     * Factory method to create a Refund movement without implicit MovementStatus
     *
     * @param merchantOrderId  Merchant order id of this movement.
     * @param money            Amount and Currency.
     * @param code             Merchant error code.
     * @param errorDescription Error description.
     * @param refunded         Indicates whether the Refund is refunded or not
     * @return A Refund.
     * TODO: Temporally implementation, we should move to createRefund based on MovementStatus
     */
    @Deprecated
    public static Refund createRefund(String merchantOrderId, Money money, String code, String errorDescription, boolean refunded) {
        return new Refund(merchantOrderId, money != null ? money.getAmount() : null, money != null ? money.getCurrency() : null, code, errorDescription, refunded ? MovementStatus.REFUNDED : MovementStatus.DECLINED);
    }

    /**
     * Factory method to create a Refund movement with a specific MovementStatus
     *
     * @param merchantOrderId  Merchant order id of this movement.
     * @param money            Amount and Currency.
     * @param code             Merchant error code.
     * @param errorDescription Error description.
     * @param status           Movement status.
     * @return A Refund.
     */
    public static Refund createRefund(String merchantOrderId, Money money, String code, String errorDescription, MovementStatus status) {
        return new Refund(merchantOrderId, money != null ? money.getAmount() : null, money != null ? money.getCurrency() : null, code, errorDescription, status);
    }

    /**
     * Factory method that creates a Failed Refund
     *
     * @param merchantOrderId  Merchant order id of this movement.
     * @param money            Amount and Currency.
     * @param code             Merchant error code.
     * @param errorDescription Error description.
     * @return A Failed Refund.
     */
    public static Refund createFailedRefund(String merchantOrderId, Money money, String code, String errorDescription) {
        return new Refund(merchantOrderId, money != null ? money.getAmount() : null, money != null ? money.getCurrency() : null, code, errorDescription, MovementStatus.REFUNDERR);
    }

    /**
     * Factory method that a Fake Refund
     *
     * @param merchantOrderId Merchant order id of this movement.
     * @param money           Amount and Currency.
     * @param status          Desired movement status
     * @return A Fake Refund.
     */
    public static Refund createFakeRefund(String merchantOrderId, Money money, MovementStatus status) {
        return createRefund(merchantOrderId, money, FAKE_CODE, FAKE_ERROR_MESSAGE, status);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Refund)) {
            return false;
        }
        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .appendSuper(super.hashCode())
                .toHashCode();
    }
}

