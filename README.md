# Collection-gateways-services client

[![Build Status](http://bcn-jenkins-01.odigeo.org/jenkins/buildStatus/icon?job=collection-gateways-services-api)](http://bcn-jenkins-01.odigeo.org/jenkins/buildStatus/icon?job=collection-gateways-services-api)

## Description
This repository is a Java client to use the [collection-gateways-services](https://bitbucket.org/odigeoteam/collection-gateways-services). It has also the contract with the current operations of the API.

## Usage 
Import in your pom.xml the following dependency 

```xml
<dependency>
  <groupId>com.odigeo.collection-gateways-services-api.vX</groupId>
  <artifactId>cgs-api-client</artifactId>
  <version>LATEST_VERSION</version>
</dependency>
```

## Legacy Endpoints
- Integration [http://testing-qa51.services.odigeo.com:8080/collection-gateways-services/engineering/build/]
- Production [https://cgs.odigeo.com//collection-gateways-services/engineering/build/]

## Endpoints
- Integration-qa [http://integration-qa-collection-gateways-services.app-qa.services.odigeo.com:8080/collection-gateways-services/engineering/build/]
- Integration [http://integration-collection-gateways-services.app-qa.services.odigeo.com:8080/collection-gateways-services/engineering/build/]
- Production [http://collection-gateways-services.app-prod.services.odigeo.com:8080/collection-gateways-services/engineering/build/]

## URLs
- Metrics [https://cgs.odigeo.com/collection-gateways-services/collection-gateways-notifications-consumer/engineering/metrics/]
- Healthcheck [https://cgs.odigeo.com/collection-gateways-services/collection-gateways-notifications-consumer/engineering/healthcheck/]
- Build [https://cgs.odigeo.com/collection-gateways-services/collection-gateways-notifications-consumer/engineering/build/]

## Documentation
More information in [confluence](https://jira.odigeo.com/wiki/display/KB/Collection+Gateways+Services)

## Changelog
###Version 12.3.0
* BBVA: Create new cgs contract for prime subscriptions (COY-1485)
* WP: Create Worldpay recurring contract (COY-1589)

###Version 12.0.0
* WP 3DS 2.0: Add Device Data Collection interaction for WP (COY-1474)
* WP 3DS 2.0: Resume device data collection interaction for WP (COY-1494)
* WP 3DS 2.0: Resume 3ds 2.0 challenge interaction for WP (COY-1519)
* New status code on Bambora - CGS Mapping (COY-1526)

###Version 11.0.0
* Exceptions after deserializing Paypal verify reponses. (COY-1497)