package com.odigeo.cgsapi.v12;

import com.odigeo.cgsapi.v12.request.CollectionActionType;
import com.odigeo.cgsapi.v12.request.Money;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethodType;
import com.odigeo.cgsapi.v12.request.operations.authorize.AuthorizationDetails;
import com.odigeo.cgsapi.v12.request.operations.binlookup.collectionmethods.creditcard.BinLookUpDetails;
import com.odigeo.cgsapi.v12.request.operations.cancel.CancelDetails;
import com.odigeo.cgsapi.v12.request.operations.collect.CollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionContext;
import com.odigeo.cgsapi.v12.request.operations.common.CollectionShoppingDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.get.booking.id.GetBookingIdRequest;
import com.odigeo.cgsapi.v12.request.operations.notify.ProviderNotificationRequest;
import com.odigeo.cgsapi.v12.request.operations.query.QueryDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.status.StatusDetails;
import com.odigeo.cgsapi.v12.request.operations.verify.VerificationDetails;
import com.odigeo.cgsapi.v12.response.operations.get.booking.id.GetBookingIdResponse;
import com.odigeo.cgsapi.v12.response.operations.notify.ProviderNotificationResponse;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;

public class CgsApiServiceManualTestMockTest {

    private CgsApiServiceManualTestMock service;
    private EasyRandom easyRandom;

    @BeforeClass
    public void init() {
        EasyRandomParameters parameters = new EasyRandomParameters()
                .seed(1L)
                .objectPoolSize(100)
                .stringLengthRange(5, 50)
                .collectionSizeRange(1, 10)
                .scanClasspathForConcreteTypes(true)
                .overrideDefaultInitialization(false)
                .ignoreRandomizationErrors(true);
        easyRandom = new EasyRandom(parameters);
    }

    @BeforeMethod
    public void initMethod() {
        service = new CgsApiServiceManualTestMock(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testNotExistingPAthMakeConstructorFail() {
        new CgsApiServiceManualTestMock("a path that do not exists");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testNotExistingPAthMakeConstructorFail2() {
        new CgsApiServiceManualTestMock("/tmp/adfsdfs");
    }

    @Test
    public void testAuthorize() {
        Assert.assertNotNull(service.authorize(easyRandom.nextObject(String.class), easyRandom.nextObject(AuthorizationDetails.class)));
    }

    @Test
    public void testAuthorizeCREDITCARD() {
        AuthorizationDetails authorizationDetails = new AuthorizationDetails(
                easyRandom.nextObject(String.class),
                easyRandom.nextObject(Money.class),
                new GatewayCollectionMethod(GatewayCollectionMethodType.CREDITCARD),
                easyRandom.nextObject(CollectionContext.class),
                easyRandom.nextObject(CollectionShoppingDetails.class));
        Assert.assertNotNull(service.authorize(easyRandom.nextObject(String.class), authorizationDetails));
    }


    @Test
    public void testVerifyStep1SECURE3D1() {
        VerificationDetails verificationDetails = new VerificationDetails(easyRandom.nextObject(String.class),
                easyRandom.nextObject(Money.class),
                new GatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D),
                new HashMap<String, String>(),
                easyRandom.nextObject(CollectionActionType.class),
                easyRandom.nextObject(CollectionContext.class),
                1,
                easyRandom.nextObject(String.class));
        Assert.assertNotNull(service.verify(easyRandom.nextObject(String.class), verificationDetails));
    }

    @Test
    public void testVerifyStep1SECURE3D2() {
        VerificationDetails verificationDetails = new VerificationDetails(easyRandom.nextObject(String.class),
                easyRandom.nextObject(Money.class),
                new GatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D2),
                new HashMap<String, String>(),
                easyRandom.nextObject(CollectionActionType.class),
                easyRandom.nextObject(CollectionContext.class),
                1,
                easyRandom.nextObject(String.class));
        Assert.assertNotNull(service.verify(easyRandom.nextObject(String.class), verificationDetails));
    }

    @Test
    public void testVerifyStep2SECURE3D2() {
        VerificationDetails verificationDetails = new VerificationDetails(easyRandom.nextObject(String.class),
                easyRandom.nextObject(Money.class),
                new GatewayCollectionMethod(GatewayCollectionMethodType.SECURE3D2),
                new HashMap<String, String>(),
                easyRandom.nextObject(CollectionActionType.class),
                easyRandom.nextObject(CollectionContext.class),
                2,
                easyRandom.nextObject(String.class));
        Assert.assertNotNull(service.verify(easyRandom.nextObject(String.class), verificationDetails));
    }

    @Test
    public void testCollect() {
        String folder = this.getClass().getResource(".").getFile();
        service = new CgsApiServiceManualTestMock(folder);

        Assert.assertNotNull(service.collect(easyRandom.nextObject(String.class), easyRandom.nextObject(CollectionDetails.class)));
    }

    @Test
    public void testCollectExternalFolder() {
        Assert.assertNotNull(service.collect(easyRandom.nextObject(String.class), easyRandom.nextObject(CollectionDetails.class)));
    }


    @Test
    public void testBinLookUp() {
        Assert.assertNotNull(service.binLookUp(easyRandom.nextObject(String.class), easyRandom.nextObject(BinLookUpDetails.class)));
    }

    @Test
    public void testCancel() {
        Assert.assertNotNull(service.cancel(easyRandom.nextObject(String.class), easyRandom.nextObject(CancelDetails.class)));
    }

    @Test
    public void testConfirm() {
        Assert.assertNotNull(service.confirm(easyRandom.nextObject(String.class), easyRandom.nextObject(ConfirmationDetails.class)));
    }

    @Test
    public void testQuery() {
        Assert.assertNotNull(service.query(easyRandom.nextObject(String.class), easyRandom.nextObject(QueryDetails.class)));
    }

    @Test
    public void testRefund() {
        Assert.assertNotNull(service.refund(easyRandom.nextObject(String.class), easyRandom.nextObject(RefundDetails.class)));
    }

    @Test
    public void testStatus() {
        Assert.assertNotNull(service.status(easyRandom.nextObject(String.class), easyRandom.nextObject(StatusDetails.class)));
    }

    @Test
    public void testNotify() {
        ProviderNotificationResponse notify = service.notify(easyRandom.nextObject(ProviderNotificationRequest.class));
        Assert.assertNotNull(notify);
    }

    @Test
    public void testGetBookingId() {
        GetBookingIdResponse bookingId = service.getBookingId(easyRandom.nextObject(GetBookingIdRequest.class));
        Assert.assertNotNull(bookingId);
    }
}
