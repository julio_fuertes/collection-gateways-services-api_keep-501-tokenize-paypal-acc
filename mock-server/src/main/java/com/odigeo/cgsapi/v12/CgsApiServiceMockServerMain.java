package com.odigeo.cgsapi.v12;

import com.google.inject.Guice;
import com.odigeo.commons.test.bootstrap.rest.ApplicationContext;
import com.odigeo.commons.test.mocks.MockServerConfiguration;
import com.odigeo.commons.test.mocks.MockServerService;
import org.apache.log4j.Logger;

public class CgsApiServiceMockServerMain {

    private static final Logger LOGGER = Logger.getLogger(CgsApiServiceMockServerMain.class);
    private static final String EXTERNAL_FOLDER_PROPERTY_NAME = "com.odigeo.csg.mock-server.responsesFolder";

    public static void main(String... args) {
        startMock(getPort(args[0]));
    }

    private static void startMock(final int port) {
        Guice.createInjector()
                .getInstance(MockServerService.class)
                .run(buildMockerServerConfiguration(port));
        LOGGER.info("Mock Server running on port " + port);
    }

    private static MockServerConfiguration buildMockerServerConfiguration(int port) {
        LOGGER.info("If you want to override returned JSON files please set the property -D" + EXTERNAL_FOLDER_PROPERTY_NAME + "=/path/to/my/folder-with-json");
        String property = System.getProperty(EXTERNAL_FOLDER_PROPERTY_NAME);
        LOGGER.info("Property: " + EXTERNAL_FOLDER_PROPERTY_NAME + ": " + property);

        final ApplicationContext applicationContext = buildApplicationContextForGreeterService(new CgsApiServiceManualTestMock(property));
        return new MockServerConfiguration(port, applicationContext, "/collection-gateways-services");
    }

    private static int getPort(String portAsText) {
        return Integer.parseInt(portAsText);
    }

    private static ApplicationContext buildApplicationContextForGreeterService(CgsApiServiceManualTestMock controller) {
        final ApplicationContext applicationContext = new ApplicationContext();
        applicationContext.addSingleton(controller);
        return applicationContext;
    }
}
