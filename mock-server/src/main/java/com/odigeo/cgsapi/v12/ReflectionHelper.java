package com.odigeo.cgsapi.v12;

import com.odigeo.cgsapi.v12.exceptions.CgsApiException;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.security.AccessController;
import java.security.PrivilegedAction;

final class ReflectionHelper {

    private static final Logger LOGGER = Logger.getLogger(ReflectionHelper.class);

    private ReflectionHelper() {
    }

    static void setFieldWithReflection(String fieldName, Object targetObject, Object newValue) {
        if (targetObject == null) {
            return;
        }
        try {
            final Field currencyField = targetObject.getClass().getDeclaredField(fieldName);
            AccessController.doPrivileged(new PrivilegedAction() {
                @Override
                public Object run() {
                    currencyField.setAccessible(true);
                    return null;
                }
            });
            currencyField.set(targetObject, newValue);
        } catch (NoSuchFieldException e) {
            String msg = "Unable to set value[" + newValue + "] for field[" + fieldName + "] in object[" + targetObject + "] ";
            LOGGER.error(msg, e);
            throw new CgsApiException(msg, e);
        } catch (IllegalAccessException e) {
            String msg = "Unable to load json mock response file for ";
            LOGGER.error(msg, e);
            throw new CgsApiException(msg, e);
        }
    }

}
