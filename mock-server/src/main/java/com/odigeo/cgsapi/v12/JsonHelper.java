package com.odigeo.cgsapi.v12;

import com.odigeo.cgsapi.v12.exceptions.CgsApiException;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

class JsonHelper {

    private static final Logger LOGGER = Logger.getLogger(JsonHelper.class);

    private static final String JSON_EXTENSION = ".json";
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final String externalJsonFolder;

    JsonHelper(String externalFolderPath) {
        externalJsonFolder = externalFolderPath;
        if (externalJsonFolder != null
                && !new File(externalJsonFolder).isDirectory()) {
            throw new IllegalArgumentException(externalFolderPath + " is not a folder");
        }
    }

    String toJson(Object object) {
        try {
            return OBJECT_MAPPER.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            LOGGER.error("Unable to serialize to json inputParameter: " + object, e);
            return "Unable to serialize: " + object;
        } catch (IOException e) {
            LOGGER.error("Unable to serialize to json inputParameter: " + object, e);
            return "Unable to serialize: " + object;
        }
    }

    <T> T loadJson(Class<T> response) {
        return loadJson(response, "", "");
    }

    <T> T loadJson(Class<T> response, String prefix) {
        return loadJson(response, prefix, "");
    }

    <T> T loadJson(Class<T> response, final String prefix, final String suffix) {
        try {
            return doLoadJson(response, prefix, suffix);
        } catch (IOException e) {
            String msg = "Unable to load json mock response file for ";
            LOGGER.error(msg, e);
            throw new CgsApiException(msg + response, e);
        }
    }

    private <T> T doLoadJson(Class<T> response, String prefix, String suffix) throws IOException {
        String fileName = FilenameUtils.getName(response.getSimpleName() + suffix + JSON_EXTENSION);
        T t = loadFromFile(prefix, fileName, response);
        if (t != null) {
            return t;
        }

        t = loadFromResources(prefix, fileName, response);
        if (t != null) {
            return t;
        }
        throw new CgsApiException("No JSON file found for response:" + response + ", prefix:" + prefix + ", suffix:" + suffix);
    }

    private <T> T loadFromResources(String prefix, String fileName, Class<T> response) throws IOException {
        List<String> projectsFiles = Arrays.asList(
                prefix + "/" + fileName,
                fileName
        );
        for (String resource : projectsFiles) {
            URL url = this.getClass().getResource(resource);
            if (url != null) {
                try {
                    LOGGER.info("Loading INTERNAL response from: " + url);
                    return OBJECT_MAPPER.readValue(url, response);
                } catch (JsonParseException e) {
                    throw new CgsApiException("Error parsing Json for resource: " + resource, e);
                }
            }
        }
        return null;
    }

    private <T> T loadFromFile(String prefix, String fileName, Class<T> response) throws IOException {
        if (externalJsonFolder == null) {
            return null;
        }

        List<String> externalFiles = Arrays.asList(
                externalJsonFolder + "/" + prefix + "/" + fileName,
                externalJsonFolder + "/" + fileName
        );
        for (String filePath : externalFiles) {
            File file = new File(filePath);
            if (file.exists()) {
                return loadFromFile(response, file);
            }
        }
        return null;
    }

    private <T> T loadFromFile(Class<T> response, File file) throws IOException {
        FileInputStream fis = null;
        try {
            LOGGER.info("Loading EXTERNAL response from: " + file);
            fis = new FileInputStream(file);
            return OBJECT_MAPPER.readValue(fis, response);
        } catch (JsonParseException e) {
            throw new CgsApiException("Error parsing Json for filePath: " + file, e);
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
    }
}
