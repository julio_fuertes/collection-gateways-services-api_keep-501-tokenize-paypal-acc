package com.odigeo.cgsapi.v12;

import com.odigeo.cgsapi.v12.request.collectionmethods.GatewayCollectionMethod;
import com.odigeo.cgsapi.v12.request.operations.authorize.AuthorizationDetails;
import com.odigeo.cgsapi.v12.request.operations.binlookup.collectionmethods.creditcard.BinLookUpDetails;
import com.odigeo.cgsapi.v12.request.operations.cancel.CancelDetails;
import com.odigeo.cgsapi.v12.request.operations.collect.CollectionDetails;
import com.odigeo.cgsapi.v12.request.operations.confirm.ConfirmationDetails;
import com.odigeo.cgsapi.v12.request.operations.get.booking.id.GetBookingIdRequest;
import com.odigeo.cgsapi.v12.request.operations.notify.ProviderNotificationRequest;
import com.odigeo.cgsapi.v12.request.operations.query.QueryDetails;
import com.odigeo.cgsapi.v12.request.operations.refund.RefundDetails;
import com.odigeo.cgsapi.v12.request.operations.status.StatusDetails;
import com.odigeo.cgsapi.v12.request.operations.verify.VerificationDetails;
import com.odigeo.cgsapi.v12.response.Movement;
import com.odigeo.cgsapi.v12.response.MovementContainer;
import com.odigeo.cgsapi.v12.response.operations.authorize.AuthorizationResponse;
import com.odigeo.cgsapi.v12.response.operations.binlookup.BinLookUpResponse;
import com.odigeo.cgsapi.v12.response.operations.cancel.CancelResponse;
import com.odigeo.cgsapi.v12.response.operations.collect.CollectionResponse;
import com.odigeo.cgsapi.v12.response.operations.confirm.ConfirmationResponse;
import com.odigeo.cgsapi.v12.response.operations.get.booking.id.GetBookingIdResponse;
import com.odigeo.cgsapi.v12.response.operations.notify.ProviderNotificationResponse;
import com.odigeo.cgsapi.v12.response.operations.query.QueryResponse;
import com.odigeo.cgsapi.v12.response.operations.refund.RefundResponse;
import com.odigeo.cgsapi.v12.response.operations.status.MonitorResponse;
import com.odigeo.cgsapi.v12.response.operations.verify.VerificationResponse;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.Currency;

class CgsApiServiceManualTestMock implements CgsApiService {

    private static final Logger LOGGER = Logger.getLogger(CgsApiServiceManualTestMock.class);

    private final JsonHelper jsonHelper;

    CgsApiServiceManualTestMock(String externalFolderPath) {
        this.jsonHelper = new JsonHelper(externalFolderPath);
    }

    private void logRequest(Object... inputParameters) {
        LOGGER.info(getMethodName(2) + " - REQUEST: " + formatInputParameters(inputParameters));
    }

    private <T> T logResponse(T returnValue) {
        LOGGER.info(getMethodName(2) + " - RESPONSE: " + jsonHelper.toJson(returnValue));
        return returnValue;
    }

    private String getMethodName(int i) {
        return new Throwable().getStackTrace()[i].getMethodName();
    }

    private String formatInputParameters(Object... inputParameters) {
        StringBuilder sb = new StringBuilder();
        for (Object inputParameter : inputParameters) {
            sb.append(jsonHelper.toJson(inputParameter)).append(", ");
        }
        return sb.toString();
    }


    private void adjustToRequestParameters(GatewayCollectionMethod gatewayCollectionMethod, Currency currency, BigDecimal amount, MovementContainer returnValue) {
        returnValue.setGatewayCollectionMethod(gatewayCollectionMethod);
        ReflectionHelper.setFieldWithReflection("currency", returnValue, currency);
        ReflectionHelper.setFieldWithReflection("amount", returnValue, amount);
        for (Movement movement : returnValue.getMovements()) {
            ReflectionHelper.setFieldWithReflection("currency", movement, currency);
            ReflectionHelper.setFieldWithReflection("amount", movement, amount);
        }
    }

    private String asString(GatewayCollectionMethod gatewayCollectionMethod) {
        return gatewayCollectionMethod.getGatewayCollectionMethodType().name();
    }

    @Override
    public AuthorizationResponse authorize(String virtualPointOfSaleCommerce, AuthorizationDetails authorizationDetails) {
        logRequest(virtualPointOfSaleCommerce, authorizationDetails);
        AuthorizationResponse returnValue = jsonHelper.loadJson(AuthorizationResponse.class, asString(authorizationDetails.getGatewayCollectionMethod()));
        adjustToRequestParameters(authorizationDetails.getGatewayCollectionMethod(),
                authorizationDetails.getMoney().getCurrency(),
                authorizationDetails.getMoney().getAmount(),
                returnValue);
        return logResponse(returnValue);
    }

    @Override
    public VerificationResponse verify(String virtualPointOfSaleCommerce, VerificationDetails verificationDetails) {
        logRequest(virtualPointOfSaleCommerce, verificationDetails);

        VerificationResponse returnValue = jsonHelper.loadJson(VerificationResponse.class,
                asString(verificationDetails.getGatewayCollectionMethod()),
                "_interactionStep_" + verificationDetails.getInteractionStep());
        adjustToRequestParameters(verificationDetails.getGatewayCollectionMethod(),
                verificationDetails.getMoney().getCurrency(),
                verificationDetails.getMoney().getAmount(),
                returnValue);
        ReflectionHelper.setFieldWithReflection("redirectionStep", returnValue.getRedirectionParameters(), verificationDetails.getInteractionStep());
        return logResponse(returnValue);
    }

    @Override
    public CollectionResponse collect(String virtualPointOfSaleCommerce, CollectionDetails collectionDetails) {
        logRequest(virtualPointOfSaleCommerce, collectionDetails);

        CollectionResponse returnValue = jsonHelper.loadJson(CollectionResponse.class, asString(collectionDetails.getGatewayCollectionMethod()));
        adjustToRequestParameters(collectionDetails.getGatewayCollectionMethod(),
                collectionDetails.getMoney().getCurrency(),
                collectionDetails.getMoney().getAmount(),
                returnValue);
        return logResponse(returnValue);
    }

    @Override
    public CancelResponse cancel(String virtualPointOfSaleCommerce, CancelDetails cancelDetails) {
        logRequest(virtualPointOfSaleCommerce, cancelDetails);

        return logResponse(jsonHelper.loadJson(CancelResponse.class, asString(cancelDetails.getGatewayCollectionMethod())));
    }

    @Override
    public ConfirmationResponse confirm(String virtualPointOfSaleCommerce, ConfirmationDetails confirmationDetails) {
        logRequest(virtualPointOfSaleCommerce, confirmationDetails);

        return logResponse(jsonHelper.loadJson(ConfirmationResponse.class, asString(confirmationDetails.getGatewayCollectionMethod())));
    }

    @Override
    public QueryResponse query(String virtualPointOfSaleCommerce, QueryDetails queryDetails) {
        logRequest(virtualPointOfSaleCommerce, queryDetails);

        return logResponse(jsonHelper.loadJson(QueryResponse.class, asString(queryDetails.getGatewayCollectionMethod())));
    }

    @Override
    public RefundResponse refund(String virtualPointOfSaleCommerce, RefundDetails refundDetails) {
        logRequest(virtualPointOfSaleCommerce, refundDetails);

        return logResponse(jsonHelper.loadJson(RefundResponse.class, asString(refundDetails.getGatewayCollectionMethod())));
    }

    @Override
    public BinLookUpResponse binLookUp(String virtualPointOfSaleCommerce, BinLookUpDetails binLookUpDetails) {
        logRequest(virtualPointOfSaleCommerce, binLookUpDetails);

        return logResponse(jsonHelper.loadJson(BinLookUpResponse.class));
    }

    @Override
    public MonitorResponse status(String virtualPointOfSaleCommerce, StatusDetails statusDetails) {
        logRequest(virtualPointOfSaleCommerce, statusDetails);

        return logResponse(jsonHelper.loadJson(MonitorResponse.class));
    }

    @Override
    public ProviderNotificationResponse notify(ProviderNotificationRequest providerNotificationRequest) {
        logRequest(providerNotificationRequest);

        return logResponse(jsonHelper.loadJson(ProviderNotificationResponse.class));
    }

    @Override
    public GetBookingIdResponse getBookingId(GetBookingIdRequest getBookingIdRequest) {
        logRequest(getBookingIdRequest);

        return logResponse(jsonHelper.loadJson(GetBookingIdResponse.class));
    }
}
