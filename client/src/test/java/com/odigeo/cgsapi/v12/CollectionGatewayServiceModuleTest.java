package com.odigeo.cgsapi.v12;

import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class CollectionGatewayServiceModuleTest {

    @Test
    public void testGetServiceConfiguration() {
        CollectionGatewayServiceModule collectionGatewayServiceModule = new CollectionGatewayServiceModule();

        ServiceConfiguration<CgsApiService> serviceServiceConfiguration = collectionGatewayServiceModule.getServiceConfiguration(CgsApiService.class);

        assertNotNull(serviceServiceConfiguration);
        assertEquals(serviceServiceConfiguration.getConnectionConfiguration().getSocketTimeoutInMillis(), 30000);
    }
}
