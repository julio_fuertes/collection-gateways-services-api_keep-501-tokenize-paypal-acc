package com.odigeo.cgsapi.v12;

import com.odigeo.cgsapi.v12.configuration.CollectionGatewayServiceConfigurationBuilder;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;

public class CollectionGatewayServiceModule extends AbstractRestUtilsModule<CgsApiService> {

    public CollectionGatewayServiceModule(ServiceNotificator... serviceNotificators) {
        super(CgsApiService.class, serviceNotificators);
    }

    @Override
    protected ServiceConfiguration<CgsApiService> getServiceConfiguration(Class<CgsApiService> serviceContractClass) {
        return new CollectionGatewayServiceConfigurationBuilder().build();
    }

}
