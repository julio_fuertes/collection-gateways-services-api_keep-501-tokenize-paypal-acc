package com.odigeo.cgsapi.v12.configuration;

import com.odigeo.cgsapi.v12.CgsApiService;
import com.odigeo.commons.rest.RestErrorsHandler;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.configuration.InterceptorConfiguration;
import com.odigeo.commons.rest.error.JsonIgnorePropertiesContextResolver;
import com.odigeo.commons.rest.error.SimpleRestErrorsHandler;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.trackingapi.client.v2.context.ExecutionContextSenderResteasyFactory;

public class CollectionGatewayServiceConfigurationBuilder {
    public static final Class<CgsApiService> SERVICE_CLASS = CgsApiService.class;
    public static final int DEFAULT_SOCKET_TIMEOUT_IN_MILLIS = 30 * 1000;

    private RestErrorsHandler errorsHandler = new SimpleRestErrorsHandler(SERVICE_CLASS);
    private ConnectionConfiguration connectionConfiguration;
    private InterceptorConfiguration interceptorConfiguration;

    public CollectionGatewayServiceConfigurationBuilder() {
        connectionConfiguration = new ConnectionConfiguration();
        connectionConfiguration.setSocketTimeoutInMillis(DEFAULT_SOCKET_TIMEOUT_IN_MILLIS);
        interceptorConfiguration = new InterceptorConfiguration();
        interceptorConfiguration.addInterceptor(ExecutionContextSenderResteasyFactory.getInstance());
    }

    public ServiceConfiguration<CgsApiService> build() {
        ServiceConfiguration<CgsApiService> serviceConfiguration = new ServiceConfiguration.Builder(SERVICE_CLASS)
                .withInterceptorConfiguration(interceptorConfiguration)
                .withConnectionConfiguration(connectionConfiguration)
                .withRestErrorsHandler(errorsHandler).build();
        JsonIgnorePropertiesContextResolver.configureIntoFactory(serviceConfiguration.getFactory());

        return serviceConfiguration;
    }

    public void setInterceptorConfiguration(InterceptorConfiguration interceptorConfiguration) {
        this.interceptorConfiguration = interceptorConfiguration;
    }

    public void setConnectionConfiguration(ConnectionConfiguration connectionConfiguration) {
        this.connectionConfiguration = connectionConfiguration;
    }

    public void setErrorsHandler(RestErrorsHandler errorsHandler) {
        this.errorsHandler = errorsHandler;
    }

}
